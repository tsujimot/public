#!/usr/local/anaconda3/bin/python

########################################################
# [Description]
# Print transitions between two configurations.
########################################################

########################################################
# Imports
########################################################
import sys, os, argparse
import numpy as np
import pandas as pd
from fractions import Fraction

########################################################
# User-defined parameters
########################################################
from axs_t3_01 import get_term_level, Ls


########################################################
# Functions
########################################################
#------------------------------------------------------------
def get_trans(configs1, configs2, flags_show):
    
    #dl
    orbital_angular_momentum = {'s': 0, 'p': 1, 'd': 2, 'f': 3, 'g': 4, 'h': 5}
    dl = 0
    for orb1, orb2 in zip(configs1, configs2):
        if orb1 != orb2:
            l1 = orbital_angular_momentum.get(orb1[-1])
            l2 = orbital_angular_momentum.get(orb2[-1])
            dl = l1 - l2
    
    terms1, levels1 = get_term_level(configs1)
    terms2, levels2 = get_term_level(configs2)

    trans = [[level1, level2] for level1 in levels1 for level2 in levels2]
    flags=[[False, False, False, False, False, False] for i in trans]

    for _i, tran in enumerate(trans):
        dL = tran[0][0] - tran[1][0]
        dS = (tran[0][1] - tran[1][1])/2.0
        dJ = tran[0][2] - tran[1][2]

        # E1
        if (dl in {-1,1}):
            if (dS in {0}):
                if (dL in {-1,0,1}):
                    if (dJ in {-1,0,1}):
                        if (tran[0][0]!=0) or (tran[1][0]!=0):
                            if (tran[0][2]!=0) or (tran[1][2]!=0):
                                flags[_i][0] = True
        # E1 (inter-combination)
        if (dl in {-1,1}):
            if (dS in {-1,1}):
                if (dL in {-1,0,1}):
                    if (dJ in {-1,0,1}):
                        if (tran[0][0]!=0) or (tran[1][0]!=0):
                            if (tran[0][2]!=0) or (tran[1][2]!=0):
                                flags[_i][1] = True
        # M1
        if (dl in {0}):
            if (dS in {0}):
                if (dL in {0}):
                    if (dJ in {-1,0,1}):
                        if (tran[0][2]!=0) or (tran[1][2]!=0):
                            flags[_i][2] = True
        # M1 (inter-combination)
        if (dl in {0}):
            if (dS in {-1,1}):
                if (dL in {0}):
                    if (dJ in {-1,0,1}):
                        if (tran[0][2]!=0) or (tran[1][2]!=0):
                            flags[_i][3] = True
        # E2
        if (dl in {-2,0,2}):
            if (dS in {0}):
                if (dL in {-2,-1,0,1,2}):
                    if (dJ in {-2,-1,0,1,2}):
                        if (tran[0][0]!=0) or (tran[1][0]!=0):
                            if (tran[0][0]!=0) or (tran[1][0]!=1):
                                if (tran[0][0]!=1) or (tran[1][0]!=0):
                                    if (tran[0][2]!=0) or (tran[1][2]!=0):
                                        if (tran[0][2]!=0) or (tran[1][2]!=1):
                                            if (tran[0][2]!=1) or (tran[1][2]!=0):
                                                if (tran[0][2]!=1/2) or (tran[1][2]!=1/2):
                                                    flags[_i][4] = True
        # E2 (inter-combination)
        if (dl in {-2,0,2}):
            if (dS in {-1,1}):
                if (dL in {-2,-1,0,1,2}):
                    if (dJ in {-2,-1,0,1,2}):
                        if (tran[0][0]!=0) or (tran[1][0]!=0):
                            if (tran[0][0]!=0) or (tran[1][0]!=1):
                                if (tran[0][0]!=1) or (tran[1][0]!=0):
                                    if (tran[0][2]!=0) or (tran[1][2]!=0):
                                        if (tran[0][2]!=0) or (tran[1][2]!=1):
                                            if (tran[0][2]!=1) or (tran[1][2]!=0):
                                                if (tran[0][2]!=1/2) or (tran[1][2]!=1/2):
                                                    flags[_i][5] = True
    total=0
    for flag, tran in zip(flags,trans):
        if (flag[0] is True) and (flags_show[0]==1):
            print("[E1 ] {}: ^{} {}_{} ({}) --- {}: ^{} {}_{} ({})".format(configs1, tran[0][1], Ls[tran[0][0]], tran[0][2], tran[0][3], configs2, tran[1][1], Ls[tran[1][0]], tran[1][2], tran[1][3]))
            total+=1
        elif (flag[1] is True) and (flags_show[1]==1):
            print("[E1'] {}: ^{} {}_{} ({}) --- {}: ^{} {}_{} ({})".format(configs1, tran[0][1], Ls[tran[0][0]], tran[0][2], tran[0][3], configs2, tran[1][1], Ls[tran[1][0]], tran[1][2], tran[1][3]))
            total+=1
        elif (flag[2] is True) and (flags_show[2]==1):
            print("[M1 ] {}: ^{} {}_{} ({}) --- {}: ^{} {}_{} ({})".format(configs1, tran[0][1], Ls[tran[0][0]], tran[0][2], tran[0][3], configs2, tran[1][1], Ls[tran[1][0]], tran[1][2], tran[1][3]))
            total+=1
        elif (flag[3] is True) and (flags_show[3]==1):
            print("[M1'] {}: ^{} {}_{} ({}) --- {}: ^{} {}_{} ({})".format(configs1, tran[0][1], Ls[tran[0][0]], tran[0][2], tran[0][3], configs2, tran[1][1], Ls[tran[1][0]], tran[1][2], tran[1][3]))
            total+=1
        elif (flag[4] is True) and (flags_show[4]==1):
            print("[E2 ] {}: ^{} {}_{} ({}) --- {}: ^{} {}_{} ({})".format(configs1, tran[0][1], Ls[tran[0][0]], tran[0][2], tran[0][3], configs2, tran[1][1], Ls[tran[1][0]], tran[1][2], tran[1][3]))
            total+=1
        elif (flag[5] is True) and (flags_show[5]==1):
            print("[E2'] {}: ^{} {}_{} ({}) --- {}: ^{} {}_{} ({})".format(configs1, tran[0][1], Ls[tran[0][0]], tran[0][2], tran[0][3], configs2, tran[1][1], Ls[tran[1][0]], tran[1][2], tran[1][3]))
            total+=1
    print("Total number of transitions: {}".format(total))
    
    return 0

########################################################
# Main routine
########################################################
if __name__ == '__main__':

    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-c1', '--config1',
        help='List of configurations #1',
        dest='configs1',
        type=str,
        nargs='+'
        )
    parser.add_argument(
        '-c2', '--config2',
        help='List of configurations #2',
        dest='configs2',
        type=str,
        nargs='+'
        )
    parser.add_argument(
        '-f', '--flags',
        help="1 (show) or 0 (no show) for [E1, E1', M1, M1', E2, E2']",
        dest='flags_show',
        type=int,
        default=[1,1,1,1,1,1],
        nargs=6
        )
    args = parser.parse_args()

    get_trans(args.configs1, args.configs2, args.flags_show)