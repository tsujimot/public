#!/usr/local/anaconda3/bin/python

########################################################
# [Description]
# Print possible terms and levels for given configurations.
########################################################

########################################################
# Imports
########################################################
import sys, os, argparse
import numpy as np
import pandas as pd
from fractions import Fraction

########################################################
# User-defined parameters
########################################################
ls = ['s', 'p', 'd', 'f', 'g', 'h', 'i', 'k', 'l', 'm', 'n']
Ls = [_l.upper() for _l in ls]
code_base = 32

########################################################
# Functions
########################################################
#------------------------------------------------------------
def encode(n, l, ml, ms):

    _ml = code_base + ml if ml < 0 else ml
    _ms = code_base + ms if ms < 0 else ms
    code = int(code_base**3 * n + code_base**2 * l + code_base**1 * _ml + code_base**0 * _ms)
    return code

#------------------------------------------------------------
def decode(code):
    
    n = code // (code_base**3)
    code -= n * code_base**3
    l = code // (code_base**2)
    code -= l * code_base**2
    _ml = code // (code_base**1)
    code -= _ml * code_base**1
    _ms = code // (code_base**0)
    ml = _ml - code_base if _ml > code_base//2 else _ml
    ms = _ms - code_base if _ms > code_base//2 else _ms
    return n, l, ml, ms

#------------------------------------------------------------
def gen_orbit(config):
        
        n=int(config[0])
        assert n > 0
        try:
            l=ls.index(config[1])
        except:
            print("{} is not a proper configuration.".format(config))
            sys.exit(-1)
        assert n > l
        for ml in range(-l,l+1,1):
            for ms in [-1, 1] : # spins are handled as +/-1
                yield encode(n, l, ml, ms)

#------------------------------------------------------------
def get_term_level(configs):

    # Total number of electrons. 
    N = len(configs)
    assert N < 9 # Up to 6 electrons supported.

    # Get all states for all electron.
    codes_all=[]
    for config in configs:
        codes=[]
        for code in gen_orbit(config):
            codes.append(code)
        codes_all.append(codes)
    
    # Get all combinations.
    if (N==1):
        orbits=[[o1] for o1 in codes_all[0]]
    elif (N==2):
        orbits=[[o1, o2] for o1 in codes_all[0] for o2 in codes_all[1]]
    elif (N==3):
        orbits=[[o1, o2, o3] for o1 in codes_all[0] for o2 in codes_all[1] for o3 in codes_all[2]]
    elif (N==4):
        orbits=[[o1, o2, o3, o4] for o1 in codes_all[0] for o2 in codes_all[1] for o3 in codes_all[2] for o4 in codes_all[3]]
    elif (N==5):
        orbits=[[o1, o2, o3, o4, o5] for o1 in codes_all[0] for o2 in codes_all[1] for o3 in codes_all[2] for o4 in codes_all[3] for o5 in codes_all[4]]
    elif (N==6):
        orbits=[[o1, o2, o3, o4, o5, o6] for o1 in codes_all[0] for o2 in codes_all[1] for o3 in codes_all[2] for o4 in codes_all[3] for o5 in codes_all[4] for o6 in codes_all[5]]
    elif (N==7):
        orbits=[[o1, o2, o3, o4, o5, o6, o7] for o1 in codes_all[0] for o2 in codes_all[1] for o3 in codes_all[2] for o4 in codes_all[3] for o5 in codes_all[4] for o6 in codes_all[5] for o7 in codes_all[6]] 
    elif (N==8):
        orbits=[[o1, o2, o3, o4, o5, o6, o7, o8] for o1 in codes_all[0] for o2 in codes_all[1] for o3 in codes_all[2] for o4 in codes_all[3] for o5 in codes_all[4] for o6 in codes_all[5] for o7 in codes_all[6] for o8 in codes_all[7]]

    # Sort combinations.
    if (N>1):
        orbits = [sorted(orbit) for orbit in orbits]
    # Pauli's exclusion principal.
    orbits = [orbit for orbit in orbits if len(set(orbit)) == N]
    # Electrons are indistinguishable.
    orbits_uniq = []
    orbits=[orbit for orbit in orbits if orbit not in orbits_uniq and not orbits_uniq.append(orbit)]
    
    # terms (ungrouped)
    terms=[]
    for orbit in orbits:
        ML, MS, parity = 0, 0, 0
        for _orbit in orbit :
            ML += decode(_orbit)[2]
            MS += decode(_orbit)[3]
            parity += decode(_orbit)[1]
        terms.append([ML, MS])

    parity=(-1)**parity
    # terms and levels (grouped)
    TERMs=[]
    LEVELs=[]
    while len(terms) > 0:
        ML_max = max([term[0] for term in terms])
        MS_max = max([term[1] for term in terms if term[0] == ML_max])
        terms_picked = [[_ml, _ms] for _ml in range(-ML_max,ML_max+1,1) for _ms in range(-MS_max,MS_max+1,2)]
        for term_picked in terms_picked:
            terms.remove(term_picked)
        TERMs.append([ML_max, MS_max+1, len(terms_picked), parity])
        for J in range(np.abs(2*ML_max-MS_max),2*ML_max+MS_max+1,2):
            LEVELs.append([ML_max, MS_max+1, Fraction(J,2), J+1, parity])
    
    # Unique LEVELs
    LEVELs_uniq = []
    LEVELs=[level for level in LEVELs if level not in LEVELs_uniq and not LEVELs_uniq.append(level)]
    
    # Sort by Hund's rule.
    TERMs=pd.DataFrame(TERMs, columns=['L', 'S', 'g', 'parity']).sort_values(['S', 'L'], ascending=[True, True]).values
    LEVELs=pd.DataFrame(LEVELs, columns=['L', 'S', 'J', 'g', 'parity']).sort_values(['S', 'L', 'J'], ascending=[True, True, False]).values

    # Print
    if __name__ == '__main__':
        print("LS coupling terms.")
        for term in TERMs:
            print("^{} {} (g={}, parity={})".format(term[1], Ls[term[0]], term[2], parity))
        print("Number of terms: {} ({})".format(len(TERMs), np.sum(np.array(TERMs)[:,2])))
        print("")
        print("Fine-structure levels.")
        n_levels=[0, 0]
        for level in LEVELs:
            print("^{} {}_{} (g={}, parity={})".format(level[1], Ls[level[0]], level[2], level[3], parity))
            n_levels[0]+=1
            n_levels[1]+=level[3]
        print("Number of levels: {} ({})".format(n_levels[0], n_levels[1]))

    return TERMs, LEVELs

########################################################
# Main routine
########################################################
if __name__ == '__main__':

    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-c', '--config',
        help='List of configurations',
        dest='configs',
        type=str,
        nargs='+'
        )
    args = parser.parse_args()

    get_term_level(args.configs)