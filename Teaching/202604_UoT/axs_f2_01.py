#!/usr/local/anaconda3/bin/python

########################################################
# [Description]
# Plot E and gf of H-like and He-like transitions.
########################################################

########################################################
# Imports
########################################################
import numpy as np
import pandas as pd
import os

#
from PyAstronomy import pyasl
an = pyasl.AtomicNo()

# plot
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns
sns.set_context("talk", 0.8)
font = {"family":"Times New Roman"}
mpl.rc('font', **font)

########################################################
# User-defined parameters
########################################################
df_a = pd.read_csv("t2_01a.csv")
df_b = pd.read_csv("t2_01b.csv")
df_c = pd.read_csv("t2_01c.csv")
outfile_a = "f2_01a.pdf"
outfile_b = "f2_01b.pdf"
outfile_c = "f2_01c.pdf"

columns=["Ly-a1", "Ly-b1", "Ly-aM1", "He-aw", "He-ax", "He-ay", "He-az", "He-bw"]
labels=["Ly$\\alpha_1$", "Ly$\\beta_1$", "Ly$\\alpha$ M1", "He$\\alpha$w", "He$\\alpha$x", "He$\\alpha$y", "He$\\alpha$z", "He$\\beta$w"]
palette = sns.color_palette("hls",n_colors=len(columns))

from axs_t2_01 import elements
Zs=[an.getAtomicNo(x) for x in elements]


########################################################
# Functions
########################################################
#------------------------------------------------------------
def main():

    # Figure (1)
    fig, ax = plt.subplots(1, 1, figsize=(6,6))
    ax.set_xscale('linear')
    ax.set_yscale('linear')
    ax.set_xlabel('Element')
    ax.set_ylabel('Energy (keV)')
    ax.set_xlim(5.5, 30.5)
    #ax.set_ylim(1e0,1e6)
    ax.set_xticks(Zs)
    ax.set_xticklabels(elements)
    #
    df_a["Z"] = df_a["Element"].apply(lambda x: an.getAtomicNo(x))
    for i in range(len(columns)):
        if i in [0,1,3,7]:
            ax.scatter(df_a["Z"], df_a[columns[i]], ls='solid', lw=2, color=palette[i], label=labels[i], s=20, alpha=1.0)
    ax.legend(loc="upper left", ncol=2)
    plt.tight_layout(pad=0.4, w_pad=0.0, h_pad=0.0)
    plt.savefig(outfile_a)

    plt.close('all')

    # Figure (2)
    fig, ax = plt.subplots(1, 1, figsize=(6,6))
    ax.set_xscale('linear')
    ax.set_yscale('log')
    ax.set_xlabel('Element')
    ax.set_ylabel('Einstein A (1/s)')
    ax.set_xlim(5.5, 30.5)
    #ax.set_ylim(1e0,1e6)
    ax.set_xticks(Zs)
    ax.set_xticklabels(elements)
    #
    df_b["Z"] = df_b["Element"].apply(lambda x: an.getAtomicNo(x))
    for i in range(len(columns)):
        if i in [0,1,2,3,4,5,6,7]:
            ax.scatter(df_b["Z"], df_b[columns[i]].apply(lambda x: 10**x), ls='solid', lw=2, color=palette[i], label=labels[i], s=20, alpha=1.0)
    ax.legend(loc="lower right", ncol=2)
    plt.tight_layout(pad=0.4, w_pad=0.0, h_pad=0.0)
    plt.savefig(outfile_b)
    plt.close('all')

    # Figure (3)
    fig, ax = plt.subplots(1, 1, figsize=(6,6))
    ax.set_xscale('linear')
    ax.set_yscale('log')
    ax.set_xlabel('Element')
    ax.set_ylabel('Oscillator strength')
    ax.set_xlim(5.5, 30.5)
    #ax.set_ylim(1e0,1e6)
    ax.set_xticks(Zs)
    ax.set_xticklabels(elements)
    #
    df_c["Z"] = df_b["Element"].apply(lambda x: an.getAtomicNo(x))
    for i in range(len(columns)):
        if i in [0,1,2,3,4,5,6,7]:
            ax.scatter(df_c["Z"], df_c[columns[i]], ls='solid', lw=2, color=palette[i], label=labels[i], s=20, alpha=1.0)
    ax.legend(loc="lower right", ncol=2)
    plt.tight_layout(pad=0.4, w_pad=0.0, h_pad=0.0)
    plt.savefig(outfile_c)
    plt.close('all')

    return 0




########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    main()