#!/usr/local/anaconda3/bin/python

########################################################
# [Description]
# E and gf of H-like and He-like transitions.
########################################################

########################################################
# Imports
########################################################
import numpy as np
import pandas as pd
import scipy.constants as const
import os
from fractions import Fraction

# Atomic database
import pyatomdb
os.environ['ATOMDB'] = '{}/.atomdb'.format(os.environ['HOME'])
from PyAstronomy import pyasl
an = pyasl.AtomicNo()

# physical constants
from scipy import constants
h = constants.h
c = constants.c
e = constants.e
keV_A = h*c/e*1e7

# Plotly
import plotly.express as px

########################################################
# User-defined parameters
########################################################
Ls_lit = ['S', 'P', 'D', 'F']
Lmax = len(Ls_lit)-1
Nmax = 5
elements = ['C', 'N', 'O', 'Ne', 'Mg', 'Si', 'S', 'Ar', 'Ca', 'Cr', 'Mn', 'Fe', 'Ni']
elements = ['C', 'N', 'O', 'F', 'Ne', 'Na', 'Mg', 'Al', 'Si', 'P', 'S', 'Cl', 'Ar', 'K', 'Ca', 'Sc', 'Ti', 'V', 'Cr', 'Mn', 'Fe', 'Ni']

# Label, cofig_lo, level_lo, config_hi, level_hi
lines_H={
    'Ly-a1' : ["1s1", "1 2S1/2", "2p1", "2 2P3/2"],
    'Ly-a2' : ["1s1", "1 2S1/2", "2p1", "2 2P1/2"],
    'Ly-aM1' : ["1s1", "1 2S1/2", "2s1", "2 2S1/2"],
    'Ly-b1' : ["1s1", "1 2S1/2", "3p1", "3 2P3/2"],
    'Ly-b2' : ["1s1", "1 2S1/2", "3p1", "3 2P1/2"],
    'Ly-g1' : ["1s1", "1 2S1/2", "4p1", "4 2P3/2"],
    'Ly-g2' : ["1s1", "1 2S1/2", "4p1", "4 2P1/2"],
    'Ly-d1' : ["1s1", "1 2S1/2", "5p1", "5 2P3/2"],
    'Ly-d2' : ["1s1", "1 2S1/2", "5p1", "5 2P1/2"],
}
lines_He={
    'He-aw' : ["1s2", "1 1S0", "1s1 2p1", "2 1P1"],
    'He-ax' : ["1s2", "1 1S0", "1s1 2p1", "2 3P2"],
    'He-ay' : ["1s2", "1 1S0", "1s1 2p1", "2 3P1"],
    'He-az' : ["1s2", "1 1S0", "1s1 2s1", "2 3S1"],
    'He-bw' : ["1s2", "1 1S0", "1s1 3p1", "3 1P1"],
#    'He-bx' : ["1s2", "1 1S0", "1s1 3p1", "3 3P2"],
#    'He-by' : ["1s2", "1 1S0", "1s1 3p1", "3 3P1"],
    'He-bz' : ["1s2", "1 1S0", "1s1 3s1", "3 3S1"],
    'He-gw' : ["1s2", "1 1S0", "1s1 4p1", "4 1P1"],
#    'He-gx' : ["1s2", "1 1S0", "1s1 4p1", "4 3P2"],
#    'He-gy' : ["1s2", "1 1S0", "1s1 4p1", "4 3P1"],
    'He-gz' : ["1s2", "1 1S0", "1s1 4s1", "4 3S1"],
}

########################################################
# Functions
########################################################
# -------------------------------------------------------
def conv_deg2J(deg):
    
    # ATOMDB seems to support fine structure splittting only partially and J values need to be guessed from degeneracy.
    deg = int(deg)
    if (deg < 15):
        J=[(deg-1)/2]
    #elif (deg == 9):
    #    J=[0,1,2]
    elif (deg == 15):
        J=[1,2,3]
    elif (deg == 18):
        J=[1.5,2.5,3.5]
    elif (deg == 21):
        J=[2,3,4]
    elif (deg == 24):
        J=[2.5,3.5,4.5]
    elif (deg == 27):
        J=[3,4,5]
    elif (deg == 30):
        J=[3.5,4,5,5.5]
    else:
        J=[np.nan]
    return J

# -------------------------------------------------------
def term_literal(N, L, S, J, tex=True):

    _N = int(N)
    _S = int(2*S+1)
    _L = Ls_lit[int(L)]
    _J=""
    
    for _j in J :
        _J = _J + "{},".format(Fraction(_j*2/2))
    _J = _J[:-1] # Remove trailing ,

    if (tex is True):
        term_literal = "${} ^{{{}}}{}_{{{}}}$".format(_N, _S, _L, _J)
    else:
        term_literal = "{} {}{}{}".format(_N, _S, _L, _J)
    
    return term_literal

# -------------------------------------------------------
def get_levels(Z, C):

    # Get LV data.
    levels = pyatomdb.atomdb.get_data(Z, C, 'LV')
    if (levels is False or len(levels) == 0):
        print("Data unavailable for Z={} and C={}.".format(Z, C))
        return None
        #sys.exit(1)

    data = None
    columns = ['ELEC_CONFIG', 'ENERGY', 'N_QUAN', 'L_QUAN', 'S_QUAN', 'LEV_DEG']
    for column in columns:
        datum = pd.DataFrame(levels[1].data[column].byteswap().newbyteorder(), columns=[column])
        if (data is None):
            data = datum
        else:
            data = pd.concat([data, datum], axis=1)

    # Give index
    data['ID_line'] = data.index+1

    # Select those of interest.
    data = data[(data.N_QUAN <= Nmax) & (data.L_QUAN <= Lmax)].sort_values('ENERGY', ascending=True)

    # Somehow, L and S are sometimes negative in ATOMDB
    data['L_QUAN'] = data['L_QUAN'].apply(np.abs)
    data['S_QUAN'] = data['S_QUAN'].apply(np.abs)

    # Give J.
    data['J_QUAN'] = data["LEV_DEG"].apply(conv_deg2J)

    # Give literal name of terms.
    data['LEV_LIT'] = data[['N_QUAN', 'L_QUAN', 'S_QUAN', 'J_QUAN']].apply(
        lambda x: term_literal(x[0], x[1], x[2], x[3], tex=True), axis=1)
    data['LEV_LIT2'] = data[['N_QUAN', 'L_QUAN', 'S_QUAN', 'J_QUAN']].apply(
        lambda x: term_literal(x[0], x[1], x[2], x[3], tex=False), axis=1)
    data["ELEC_CONFIG"] = data["ELEC_CONFIG"].apply(lambda x: x.rstrip())

    return data

# -------------------------------------------------------
def get_f(x):

    g1, g2, A21, E = x
    nu = const.e * E *1e3 / const.h
    gf = (g2 * A21 * const.epsilon_0 * const.m_e * const.c**3.0) / (2.0*np.pi * nu**2.0 * const.e**2.0)
    return gf / g1


# -------------------------------------------------------
def get_trans(Z, C):

    # Get LA data.
    trans = pyatomdb.atomdb.get_data(Z, C, 'LA')
    if (trans is False or len(trans) == 0):
        print("Data unavailable for Z={} and C={}.".format(Z, C))
        return None
        #sys.exit(1)

    data = None
    columns = ['UPPER_LEV', 'LOWER_LEV', 'WAVELEN', 'EINSTEIN_A']
    for column in columns:
        datum = pd.DataFrame(trans[1].data[column].byteswap().newbyteorder(), columns=[column])
        if (data is None):
            data = datum
        else:
            data = pd.concat([data, datum], axis=1)

    data['ENERGY'] = keV_A/data['WAVELEN']
    data = data.sort_values('EINSTEIN_A', ascending=False) 

    return data

#------------------------------------------------------------
def gen_lines(Z, C, element, lines):

    data_LV=get_levels(Z, C)
    data_LA=get_trans(Z, C)
    for key in lines.keys():
        config_lo, level_lo, config_hi, level_hi = lines[key]
        level_lo=data_LV[(data_LV["ELEC_CONFIG"]==config_lo) & (data_LV["LEV_LIT2"]==level_lo)][['ID_line','LEV_DEG']]
        level_hi=data_LV[(data_LV["ELEC_CONFIG"]==config_hi) & (data_LV["LEV_LIT2"]==level_hi)][['ID_line','LEV_DEG']]

        if (len(level_lo)>0) and (len(level_hi)>0):
            ID_line_lo=level_lo.values[0][0]
            ID_line_hi=level_hi.values[0][0]
            trans=data_LA[(data_LA["LOWER_LEV"]==ID_line_lo) & (data_LA["UPPER_LEV"]==ID_line_hi)]
            trans["LOWER_G"] = level_lo.values[0][1]
            trans["UPPER_G"] = level_hi.values[0][1]
            trans["f21"] = trans[["LOWER_G","UPPER_G","EINSTEIN_A","ENERGY"]].apply(lambda x: get_f(x), axis=1)

            if (len(trans)>0):
                energy=trans["ENERGY"].values[0]
                A=np.log10(trans["EINSTEIN_A"].values[0])
                f21=np.log10(trans["f21"].values[0])
                yield [Z, C, element, key, energy, A, f21]

#------------------------------------------------------------
def main():
    
    # Atomic data
    lines_ext=[]
    for element in elements:
        Z=an.getAtomicNo(element)

        # H-like
        lines=lines_H
        C=Z
        for line in gen_lines(Z, C, element, lines):
            lines_ext.append(line)
        df_lines_ext=pd.DataFrame(lines_ext, columns=["Z", "C", "Element", "Label", "ENERGY", "EINSTEIN_A", "f21"])

        # He-like
        if (Z==1):
            continue
        lines=lines_He
        C=Z-1
        for line in gen_lines(Z, C, element, lines):
            lines_ext.append(line)
        df_lines_ext=pd.DataFrame(lines_ext, columns=["Z", "C", "Element", "Label", "ENERGY", "EINSTEIN_A", "f21"])
    print(df_lines_ext)
    df_a=df_lines_ext.pivot_table(index='Element', columns='Label', values='ENERGY', sort=False)
    df_a.to_csv("t2_01a.csv")
    df_b=df_lines_ext.pivot_table(index='Element', columns='Label', values='EINSTEIN_A', sort=False)
    df_b.to_csv("t2_01b.csv")
    df_b=df_lines_ext.pivot_table(index='Element', columns='Label', values='f21', sort=False)
    df_b.to_csv("t2_01c.csv")
    return 0


########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    main()