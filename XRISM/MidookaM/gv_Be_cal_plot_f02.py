#!/usr/local/anaconda3/bin/python3

########################################################
# Imports
########################################################
import argparse
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import seaborn as sns
from anaconda_navigator.widgets.dialogs.tests import data
from astropy.units import decahour
sns.set_style("white", {
    'axes.facecolor': "1.0",
    'xtick.major.size': 10.0,
    'ytick.major.size': 10.0,
    'xtick.minor.size': 6.0,
    'ytick.minor.size': 6.0,
    'xtick.direction': u'in',
    'ytick.direction': u'in',
    })
sns.set_context("talk", 1.0)


########################################################
# User Setting
########################################################
#Datadir
datadir='/home/astro/Work/XARM/SXS2/20190613/dat/XPD/'

# Format, File name (direct), File name (trans)
Es=[4, 5, 6, 7, 8, 9, 10, 11, 12]



########################################################
# Functions
########################################################
#-------------------------------------------------------
def read_XPD(Es, datadir):
    
    
    data_XPD = None
    for E in Es:
        file_XPD = datadir + 'XPD_%dkeV.txt' % E
        num_header = 1
        columns=['h', 'k', 'l', 'd', 'Freal', 'Fimg', 'F', '2theta', 'I', 'M', 'ID', 'Phase']
        datum_XPD = pd.read_csv(file_XPD, header = num_header, names=columns, delim_whitespace = True)
        datum_XPD['E'] = E
        if data is None:
            data_XPD = datum_XPD
        else:
            data_XPD = pd.concat([data_XPD,datum_XPD], axis=0)
    
    # Remove weak features
    F_min=1.0
    return data_XPD[data_XPD['F']>F_min]

#-------------------------------------------------------
def make_plot(outfile):

    # Plot setup.
    plt.figure(figsize=(8.27,11.69))
    plt.clf()
    plt.subplots_adjust(hspace=0.05)

    # I
    ax1 = plt.subplot(3,1,3)
    ax1.set_xlabel('Energy (eV)')
    ax1.set_ylabel('$I$')
    ax1.set_yscale('linear')
    
    # |F|
    ax2 = plt.subplot(3,1,2, sharex=ax1)
    plt.setp(ax2.get_xticklabels(), visible=False)
    ax2.set_xlabel('')
    ax2.set_ylabel('$|F|$')
    ax2.set_yscale('log')
    #ax2.set_ylim(0.6,1)
    
    # 2theta
    ax3 = plt.subplot(3,1,1, sharex=ax1)
    plt.setp(ax3.get_xticklabels(), visible=False)
    ax3.set_xlabel('')
    ax3.set_ylabel('2$\\theta$')
    ax3.set_yscale('linear')
    
    # Retrieve and plot data.
    data_XPD = read_XPD(Es, datadir)
    
    for h in range(data_XPD['h'].min(), data_XPD['h'].max()+1):
        for k in range(data_XPD['k'].min(), data_XPD['k'].max()+1):
            for l in range(data_XPD['l'].min(), data_XPD['l'].max()+1):
                data_XPD_hkl = data_XPD[(data_XPD['h']==h) & (data_XPD['k']==k) & (data_XPD['l']==l)]
                if (len(data_XPD_hkl) > 1):
                    d=data_XPD_hkl['d'].mean()
                    E=12.399786667738/2.0/d
                                        
                    ax1.plot(data_XPD_hkl['E'], data_XPD_hkl['I'], label='(%d,%d,%d) at %.2f' % (h, k, l, E))
                    ax2.plot(data_XPD_hkl['E'], data_XPD_hkl['F'], label='')
                    ax3.plot(data_XPD_hkl['E'], data_XPD_hkl['2theta'], label='')
                    
    # Plot setting
    for ax in [ax1, ax2, ax3]:
        ax.set_xlim(3.0, 12.5)
        ax.grid(which='both')
        ax.set_xscale('linear')
    ax1.legend(fontsize=12, ncol=3)

    plt.tight_layout(pad=0.4, w_pad=0.0, h_pad=0.0)
    outfile="%s.pdf" % (outfile)
    plt.savefig(outfile, papertyle='A4', orientation='portrait',format='pdf')
    print('The plot is generated in %s.' % outfile)
    plt.close('all')
       
    return 0

########################################################
# Main routine
########################################################
if __name__ == '__main__':

    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-o', '--output',
        help='Output file name origin.',
        dest='outfile',
        type=str,
        default='XPD',
        nargs='?'
        )
    args = parser.parse_args()

    # Check arguments.
    outfile = args.outfile
    
    # Plot
    make_plot(outfile)
