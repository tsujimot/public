#!/usr/local/anaconda3/bin/python3

########################################################
# Imports
########################################################
import argparse
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.signal import savgol_filter
#from scipy import interp
from scipy.interpolate import interp1d


import seaborn as sns
from anaconda_navigator.widgets.dialogs.tests import data
sns.set_style("white", {
    'axes.facecolor': "1.0",
    'xtick.major.size': 10.0,
    'ytick.major.size': 10.0,
    'xtick.minor.size': 6.0,
    'ytick.minor.size': 6.0,
    'xtick.direction': u'in',
    'ytick.direction': u'in',
    })
sns.set_context("talk", 1.0)


########################################################
# User Setting
########################################################
#Datadir
datadir1='//home/astro/Work/XARM/SXS2/20190613/dat/'
datadir2='//home/astro/Work/XARM/SXS2/20191126/dat/'

# Format, File name (direct), File name (trans), rel gain(direct/back, trans/back, direct/front, trans/front)
datasets_cen=[
    ["KEK_BL11B", "1800_2100_5eV_10s_InSbC_OFF_athena.txt", "1800_2100_5eV_20s_InSbC_ON_athena.txt", 'magenta', 10.0, 1000.0, 1.0, 1.0],    
    ["KEK_BL11B", "2050_2400_5eV_10s_InSbC_OFF_athena.txt", "2050_2400_5eV_20s_InSbC_ON_athena.txt", 'cyan', 10.0, 1000.0, 1.0, 1.0],
    ["KEK_BL11B", "2350_3050_5eV_10s_InSbC_OFF_athena.txt", "2350_3050_5eV_20s_InSbC_ON_athena.txt", 'magenta', 10.0, 10.0, 1.0, 1.0],    
    ["KEK_BL11B", "3000_3400_5eV_3400_3500_2eV_2s_InSbC_OFF_athena.txt", "3000_3400_5eV_3400_3500_2eV_10s_InSbC_ON_athena.txt", 'cyan', 10.0, 1.0, 1.0, 1.0],    
    ["KEK_BL11B", "2060_2200_2eV_4s_C_OFF_athena.txt", "2060_2200_2eV_14s_C_ON_athena.txt", 'blue', 10.0, 100.0, 1.0, 1.0],
    ["KEK_BL11B", "2150_2400_2eV_2s_C_OFF_athena.txt", "2150_2400_2eV_10s_C_ON_athena.txt", 'green', 10.0, 100.0, 1.0, 1.0],
    ["KEK_BL11B", "2350_2600_2eV_2s_C_OFF_athena.txt", "2350_2600_2eV_10s_C_ON_athena.txt", 'blue', 1.0,1.0, 1.0, 1.0],
    ["KEK_BL11B", "2550_3000_2eV_2s_C_OFF_athena.txt", "2550_3000_2eV_8s_C_ON_athena.txt", 'green', 1.0, 1.0, 1.0, 1.0],
    ["KEK_BL11B", "2950_3500_2eV_2s_C_OFF_athena.txt", "2950_3500_2eV_6s_C_ON_athena.txt", 'blue', 1.0, 1.0, 1.0, 1.0],
    ["KEK_BL11B", "3450_4000_2eV_2s_C_OFF_athena.txt", "3450_4000_2eV_4s_C_ON_athena.txt", 'green', 1.0, 1.0, 1.0, 1.0],
    ["KEK_BL11B", "3950_5000_2eV_2s_C_OFF_athena.txt", "3950_5000_2eV_2s_C_ON_athena.txt", 'blue', 1.0, 1.0, 1.0, 1.0],
    ["KEK_BL7C", "4000_6000_2eV_2s_C_OFF_athena.txt", "4000_6000_2eV_2s_C_ON_athena.txt", 'red', 1.0, 1.0, 1.0, 1.0],
    ["KEK_BL7C", "5900_8000_2eV_2s_C_OFF_athena.txt", "5900_8000_2eV_2s_C_ON_athena.txt", 'red', 1.0, 1.0, 1.0, 1.0],
    ["KEK_BL7C", "7900_10000_2eV_2s_C_OFF_athena.txt", "7900_10000_2eV_2s_C_ON_athena.txt", 'black', 1.0, 1.0, 1.0, 1.0],
    ["KEK_BL7C", "9900_12000_2eV_2s_C_OFF_athena.txt", "9900_12000_2eV_2s_C_ON_athena.txt", 'black', 1.0, 1.0, 1.0, 1.0],
    ["KEK_BL7C", "4000_5000_2eV_1s_C_OFF_athena.txt", "4000_5000_2eV_1s_C_ON_athena.txt", 'black', 10.0, 10.0, 10.0, 10.0],
    ["KEK_BL7C", "4900_8000_2eV_1s_C_OFF_athena.txt", "4900_8000_2eV_1s_C_ON_athena.txt", 'black', 1.0, 1.0, 1.0, 1.0],
]
datasets_top=[
    ["KEK_BL11B", "2060_2400_2eV_2s_T_OFF_athena.txt", "2060_2400_2eV_10s_T_ON_athena.txt", 'black', 100.0, 1000.0, 1.0, 1.0],
    ["KEK_BL11B", "2350_3000_2eV_2s_T_OFF_athena.txt", "2350_3000_2eV_6s_T_ON_athena.txt", 'black', 10.0, 10.0, 1.0, 1.0],
    ["KEK_BL11B", "2950_4500_2eV_2s_T_OFF_athena.txt", "2950_4500_2eV_4s_T_ON_athena.txt", 'black', 10.0, 10.0, 1.0, 1.0],
    ["KEK_BL7C", "4000_5000_2eV_1s_T_OFF_athena.txt", "4000_5000_2eV_1s_T_ON_athena.txt", 'black', 10.0, 10.0, 10.0, 10.0],
    ["KEK_BL7C", "4900_6000_2eV_1s_T_OFF_athena.txt", "4900_6000_2eV_1s_T_ON_athena.txt", 'black', 1.0, 1.0, 1.0, 1.0],
    ["KEK_BL7C", "5900_8000_2eV_1s_T_OFF_athena.txt", "5900_8000_2eV_1s_T_ON_athena.txt", 'black', 1.0, 1.0, 1.0, 1.0],
    ["KEK_BL7C", "7900_12000_2eV_1s_T_OFF_athena.txt", "7900_12000_2eV_1s_T_ON_athena.txt", 'black', 1.0, 1.0, 1.0, 1.0],

]
datasets_bottom=[
    ["KEK_BL11B", "2060_2400_2eV_2s_B_OFF_athena.txt", "2060_2400_2eV_10s_B_ON_athena.txt", 'black', 100.0, 1000.0, 1.0, 1.0],
    ["KEK_BL11B", "2350_3000_2eV_2s_B_OFF_athena.txt", "2350_3000_2eV_6s_B_ON_athena.txt", 'black', 10.0, 10.0, 1.0, 1.0],
    ["KEK_BL11B", "2950_4500_2eV_2s_B_OFF_athena.txt", "2950_4500_2eV_4s_B_ON_athena.txt", 'black', 10.0, 10.0, 1.0, 1.0],
    ["KEK_BL7C", "4000_5000_2eV_1s_B_OFF_athena.txt", "4000_5000_2eV_1s_B_ON_athena.txt", 'black', 10.0, 10.0, 1.0, 1.0],
    ["KEK_BL7C", "4900_6000_2eV_1s_B_OFF_athena.txt", "4900_6000_2eV_1s_B_ON_athena.txt", 'black', 1.0, 1.0, 1.0, 1.0],
    ["KEK_BL7C", "5900_8000_2eV_1s_B_OFF_athena.txt", "5900_8000_2eV_1s_B_ON_athena.txt", 'black', 1.0, 1.0, 1.0, 1.0],
    ["KEK_BL7C", "7900_12000_2eV_1s_B_OFF_athena.txt", "7900_12000_2eV_1s_B_ON_athena.txt", 'black', 1.0, 1.0, 1.0, 1.0],
]
datasets_pvc=[
    ["KEK_BL11B", "PVC_cal_v2_athena.txt", "PVC_cal_off_athena.txt", 'black', 1.0, 1.0, 1.0, 1.0],
]
datasets_cu=[
    ["KEK_BL7C", None, "Cu_cal_athena.txt", 'black', 1.0, 1.0, 1.0, 1.0],
]
datasets_hisor=[
    ["HISOR_BL11", "2100_2300_2eV_10s_OFF.dat", "2100_2300_2eV_10s_ON.dat", 'blue', 10.0, 1.0, 10.0, 1.0],    
    ["HISOR_BL11", "2100_2778_2eV_10s_OFF.dat", "2100_2778_2eV_2s_ON.dat", 'black', 10.0, 2.0, 10.0, 2.0],
    ["HISOR_BL11", "2450_4000_2eV_1s_OFF.dat", "2450_4000_2eV_1s_ON.dat", 'red', 1.0, 1.0, 1.0, 1.0]
    ]


# Elemental data
# Element, density (g/cm3), guess (um), K edge
data_elem={
    'Be': [1.85, 252., 111.5],
    'Cr': [7.19, 0.0, 5989],
    'Mn': [7.21, 0.0, 6539],  
    'Fe': [7.874, 1.6e-2, 7112], 
    'Ni': [8.908, 1.0e-2, 8333],
    'Cu': [8.96, 0.0, 8979], 
    }

# BDF data
# http://rruff.geo.arizona.edu/AMS/minerals/Beryllium
a, b, c=2.2853, 2.2853, 3.5842

# Smootjing parameter for detrending
sg_order = 11
sg_points = 51


########################################################
# Functions
########################################################
#-------------------------------------------------------
def calc_BDF(e_min, e_max):
    
    print("BDFs in %s-%s keV are:" % (e_min, e_max))
    print("h, k, l, E")

    index_max = 6
    H, K, L = index_max, index_max, index_max
    for h in range(H):
        for k in range(K):
            for l in range(L):
                if (((h>0) or (k>0) or (l>0)) and (h>=k)):
                                                                       
                    # Extinction law
                    sum = (2*h - 2*k + 3*l +3)
                    if (sum%6 ==0):
                        print("(h, k, l)=(%d, %d, %d) is extinguished." % (h,k,l))
                        continue
                    
                    d=a/(np.sqrt(4.0/3.0*(h*h+h*k+k*k) + l*l*a*a/c/c))
                    E=12.399786667738/2.0/d
                    if ((E > e_min) and (E < e_max)):
                        print("%s %s %s %s" % (h, k, l, E))
                        yield [h, k, l, E]

#-------------------------------------------------------
def read_data(datasets, datadir):

    for dataset in datasets:
        file_format = dataset[0]
        file_direct = dataset[1]
        file_trans = dataset[2]
        color = dataset[3]
        rel_gains = dataset[4:8]
            
        if (file_format == 'KEK_BL7C') :
            read_file = read_file_KEK_BL7C
        elif (file_format == 'KEK_BL11B') :
            read_file = read_file_KEK_BL11B
        elif (file_format == 'HISOR_BL11') :
            read_file = read_file_HISOR_BL11
        else:
            print("%s is not a supported format", file_format)
            exit(1)
            
        data = read_file(file_direct, file_trans, rel_gains, datadir)
        yield data, color


#-------------------------------------------------------
def read_file_KEK_BL7C(file_direct, file_trans, rel_gains, datadir):
    
    # Trans (DUT on)
    print("Reading %s..." % file_trans)
    columns_trans=['E_trans_c', 'E_trans_o', 'dT_trans', 'I_trans_front', 'I_trans_back']
    data_trans = pd.read_csv(datadir+file_trans, names=columns_trans, delim_whitespace = True, comment='#')
    data_trans['I_trans_front'] = data_trans['I_trans_front'] / data_trans['dT_trans'] / rel_gains[3]
    data_trans['I_trans_back'] = data_trans['I_trans_back'] / data_trans['dT_trans'] / rel_gains[1]

    # Direct (DUT off). 
    columns_direct=['E_direct_c', 'E_direct_o', 'dT_direct', 'I_direct_front', 'I_direct_back']
    if (file_direct is None):
        file_direct = file_trans
        data_direct = pd.read_csv(datadir+file_direct, names=columns_direct, delim_whitespace = True, comment='#')    
        data_direct['I_direct_front']=1.0
        data_direct['I_direct_back']=1.0
    else:
        # Some data may not have OFF data.        
        print("Reading %s..." % file_direct)
        data_direct = pd.read_csv(datadir+file_direct, names=columns_direct, delim_whitespace = True, comment='#')
    data_direct['I_direct_front'] = data_direct['I_direct_front'] / data_direct['dT_direct'] / rel_gains[2]
    data_direct['I_direct_back'] = data_direct['I_direct_back'] / data_direct['dT_direct'] / rel_gains[0]

    # Calc transmission rate
    data = pd.concat( [ data_direct, data_trans ], axis=1)        
    data['fb_direct'] = data['I_direct_back']/data['I_direct_front']
    data['fb_trans'] = data['I_trans_back']/data['I_trans_front']
    data['dt_front'] = data['I_trans_front']/data['I_direct_front']
    data['dt_back'] = data['I_trans_back']/data['I_direct_back']
    data['Trans_rate'] = data['fb_trans'] / data['fb_direct'] 
    
    file_save = file_direct.replace('_OFF_athena.txt', '.pkl')
    data.to_pickle(file_save)
    print("Data saved to %s." % file_save)
    
    return data        

#-------------------------------------------------------
read_file_KEK_BL11B = read_file_KEK_BL7C


#-------------------------------------------------------
def read_file_HISOR_BL11(file_direct, file_trans, rel_gains, datadir):
    
    # Trans (DUT on)
    print("Reading %s..." % file_trans)
    columns_trans=['E_trans_c', 'E_trans_o', 'I_trans_front', 'I_trans_back', 'I_trans_ssd', 'dT_trans']
    data_trans = pd.read_csv(datadir+file_trans, names=columns_trans, delim_whitespace = True, comment='#')
    data_trans['I_trans_front'] = data_trans['I_trans_front'] / data_trans['dT_trans'] / rel_gains[3]
    data_trans['I_trans_back'] = data_trans['I_trans_back'] / data_trans['dT_trans'] / rel_gains[1]

    # Direct (DUT off). 
    columns_direct=['E_direct_c', 'E_direct_o', 'I_direct_front', 'I_direct_back', 'I_direct_ssd', 'dT_direct']
    if (file_direct is None):
        file_direct = file_trans
        data_direct = pd.read_csv(datadir+file_direct, names=columns_direct, delim_whitespace = True, comment='#')    
        data_direct['I_direct_front']=1.0
        data_direct['I_direct_back']=1.0
    else:
        # Some data may not have OFF data.        
        print("Reading %s..." % file_direct)
        data_direct = pd.read_csv(datadir+file_direct, names=columns_direct, delim_whitespace = True, comment='#')
    data_direct['I_direct_front'] = data_direct['I_direct_front'] / data_direct['dT_direct'] / rel_gains[2]
    data_direct['I_direct_back'] = data_direct['I_direct_back'] / data_direct['dT_direct'] / rel_gains[0]

    # Calc transmission rate
    data = pd.concat( [ data_direct, data_trans ], axis=1)        
    data['fb_direct'] = data['I_direct_back']/data['I_direct_front']
    data['fb_trans'] = data['I_trans_back']/data['I_trans_front']
    data['dt_front'] = data['I_trans_front']/data['I_direct_front']
    data['dt_back'] = data['I_trans_back']/data['I_direct_back']
    data['Trans_rate'] = data['fb_trans'] / data['fb_direct'] 
    
    file_save = file_direct.replace('_OFF_athena.txt', '.pkl')
    data.to_pickle(file_save)
    print("Data saved to %s." % file_save)
    
    return data        


#-------------------------------------------------------
def read_file_model_photoabs_xcom(elem, datadir):

    # Get density and guessed thickness.
    density, thickness_guess = data_elem[elem][0:2]
    
    # Retrieve mass absorption coeff.
    file_model = datadir + ('xcom_%s.dat' % elem)
    num_header = 3
    columns_model=['E', 'Scat_coh', 'Scat_inc', 'Photo_abs', 'N_pair', 'E_pair', 'Tot_coh', 'Tot_noCoh']
    model = pd.read_csv(file_model, header = num_header, names=columns_model, delim_whitespace = True)
    model['E'] = model['E']*1e6 # MeV -> eV
    #model['Trans'] = np.exp(-model['Photo_abs'] * density * thickness_guess * 1e-4)
    model['Trans'] = np.exp(-model['Tot_coh'] * density * thickness_guess * 1e-4)

    return model

#-------------------------------------------------------
def read_file_model_photoabs_xcom2(elem, datadir):

    # Get density and guessed thickness.
    density, thickness_guess = data_elem[elem][0:2]
    
    # Retrieve mass absorption coeff.
    file_model = datadir + ('xcom2_%s.dat' % elem)
    num_header = 3
    columns_model=['E', 'Scat_coh', 'Scat_inc', 'Photo_abs', 'N_pair', 'E_pair', 'Tot_coh', 'Tot_noCoh']
    model = pd.read_csv(file_model, header = num_header, names=columns_model, delim_whitespace = True)
    model['E'] = model['E']*1e6 # MeV -> eV
    #model['Trans'] = np.exp(-model['Photo_abs'] * density * thickness_guess * 1e-4)
    model['Trans'] = np.exp(-model['Tot_coh'] * density * thickness_guess * 1e-4)

    return model

#-------------------------------------------------------
def read_file_model_photoabs_cxro(elem, datadir):

    # Retrieve mass absorption coeff.
    file_model = datadir + ('xray4028.dat')
    num_header = 2
    columns_model=['E', 'Trans']
    model = pd.read_csv(file_model, header = num_header, names=columns_model, delim_whitespace = True)
    model['E'] = model['E']
    #model['Trans'] = np.exp(-model['Photo_abs'] * density * thickness_guess * 1e-4)    

    return model

#-------------------------------------------------------
def get_model(Energy_grid, datadir):

    read_file_model_photoabs = read_file_model_photoabs_xcom2
    model_Trans_Rate_interp_all = None
    
    for elem in data_elem:
        model_photoabs = read_file_model_photoabs(elem, datadir)
        model_interp = interp1d(model_photoabs['E'], model_photoabs['Trans'], bounds_error=False)
        model_Trans_Rate_interp = model_interp(Energy_grid)
        
        if (model_Trans_Rate_interp_all is None):
            model_Trans_Rate_interp_all = model_Trans_Rate_interp
        else:
            model_Trans_Rate_interp_all = model_Trans_Rate_interp_all * model_Trans_Rate_interp

    return model_Trans_Rate_interp_all
    
#-------------------------------------------------------
def make_plot(datadir, datasets, outfile, plt_title, e_min, e_max):

    # Plot setup.
    plt.figure(figsize=(8.27,11.69))
    plt.clf()
    plt.subplots_adjust(hspace=0.05)

    # Transmission rate
    ax1 = plt.subplot(6,1,6)
    ax1.set_xlabel('Energy (keV)')
    ax1.set_ylabel('Data/Be(%d$\mu$m)' % data_elem['Be'][1])
    ax1.set_yscale('linear')
    ax1.set_ylim(0.95,1.05)
    
    # Transmission rate
    ax2 = plt.subplot(6,1,5, sharex=ax1)
    plt.setp(ax2.get_xticklabels(), visible=False)
    ax2.set_xlabel('')
    ax2.set_ylabel('Trans rate')
    ax2.set_yscale('linear')
    #ax2.set_ylim(0.05,1)
    
    # f/b ratio
    ax3 = plt.subplot(6,1,4, sharex=ax1)
    plt.setp(ax3.get_xticklabels(), visible=False)
    ax3.set_xlabel('')
    ax3.set_ylabel('Detrended\nBack/Front')
    ax3.set_yscale('linear')
    ax3.set_ylim(0.99,1.01)
    
    # f/b ratio
    ax4 = plt.subplot(6,1,3, sharex=ax1)
    plt.setp(ax4.get_xticklabels(), visible=False)
    ax4.set_xlabel('')
    ax4.set_ylabel('Back/Front')
    ax4.set_yscale('log')
    #ax4.set_yscale('linear')
    
    # t/d ratio
    ax5 = plt.subplot(6,1,2, sharex=ax1)
    plt.setp(ax5.get_xticklabels(), visible=False)  
    ax5.set_xlabel('')
    ax5.set_ylabel('Trans/Direct')
    ax5.set_yscale('linear')
    
    # Raw
    ax6 = plt.subplot(6,1,1, sharex=ax1)
    plt.setp(ax6.get_xticklabels(), visible=False)  
    ax6.set_xlabel('')
    ax6.set_ylabel('kCnt/gain/$\\Delta$T')
    ax6.set_yscale('linear')
    #ax6.set_ylim(0,50)
    
    # Retrieve and plot data.
    num=0
    _s=1
    for dataset in read_data(datasets, datadir):
        _d, _c = dataset[0:2]

        # Restruct the data to the range displayed and skip if none.
        _d = _d[(_d['E_direct_c']>=e_min*1e3) & (_d['E_direct_c']<=e_max*1e3)].copy()
        
        if (len(_d) == 0):
            continue

        # Plot model
        Energy_grid = _d['E_direct_c']
        model_Trans_Rate_interp = get_model(Energy_grid, datadir)                
        ax1.scatter(_d['E_direct_c']/1e3,_d['Trans_rate']/model_Trans_Rate_interp, color=_c, s=_s)        
        
        if (num == 0):
            _l1, _l2=['Direct', 'Trans'] 
        else:
            _l1, _l2=['', '']         
        ax2.scatter(_d['E_direct_c']/1e3,_d['Trans_rate'], label="", color=_c, s=_s)
        ax2.plot(_d['E_direct_c']/1e3,model_Trans_Rate_interp, linestyle=":", color='gray')
        
        # ax3: Plot detrended trans/direct ratio.             
        # savgol is wrong for unevenly spaced data.
        try:
            _d['fb_direct_trend'] = savgol_filter(_d['fb_direct'], sg_points, sg_order)
            _d['fb_trans_trend']  = savgol_filter(_d['fb_trans'], sg_points, sg_order)
        except:
            _d['fb_direct_trend'] = _d['fb_direct']
            _d['fb_trans_trend'] = _d['fb_trans']
#        def exp_fit(x, a, b, c):
#            return c - np.exp( a * x + b )
#        param_direct, cov_direct = curve_fit(exp_fit, _d['E_direct_c'], _d['fb_direct'])
#        param_trans, cov_trans = curve_fit(exp_fit, _d['E_direct_c'], _d['fb_trans'])
#        _d['fb_direct_trend'] = _d['fb_direct'].apply(lambda x: exp_fit(x, param_direct[0], param_direct[1], param_direct[2]) )
#        _d['fb_trans_trend'] = _d['fb_trans'].apply(lambda x: exp_fit(x, param_trans[0], param_trans[1], param_trans[2]) )
        if (num == 0):
            _l1, _l2=['Direct', 'Trans'] 
        else:
            _l1, _l2=['', '']        
        ax3.scatter(_d['E_direct_c']/1e3,_d['fb_direct']/_d['fb_direct_trend'], label=_l1, color="red", s=_s)
        ax3.scatter(_d['E_direct_c']/1e3,_d['fb_trans']/_d['fb_trans_trend'], label=_l2, color="blue", s=_s)
        
        # ax4 : Plot trans/direct ratio.
        if (num == 0):
            _l1, _l2=['Direct', 'Trans'] 
        else:
            _l1, _l2=['', ''] 
        ax4.scatter(_d['E_direct_c']/1e3,_d['fb_direct'], label=_l1, color="red", s=_s)
        ax4.scatter(_d['E_direct_c']/1e3,_d['fb_trans'], label=_l2, color="blue", s=_s)

        # ax5 : Plot 
        if (num == 0):
            _l1, _l2=['Front', 'Back'] 
        else:
            _l1, _l2=['', ''] 
        ax5.scatter(_d['E_direct_c']/1e3,_d['dt_front'], label=_l1,color="red", s=_s)
        ax5.scatter(_d['E_direct_c']/1e3,_d['dt_back'], label=_l2, color="blue", s=_s)    

        if (num == 0):
            _l1, _l2, _l3, _l4 = ['Direct front', 'Direct back', 'Trans front', 'Trans back'] 
        else:
            _l1, _l2, _l3, _l4 = ['', '', '', ''] 
        ax6.scatter(_d['E_direct_c']/1e3,_d['I_direct_front']/1e3, label=_l1, color="magenta", s=_s)
        ax6.scatter(_d['E_direct_c']/1e3,_d['I_direct_back']/1e3, label=_l2, color="red", s=_s)
        ax6.scatter(_d['E_direct_c']/1e3,_d['I_trans_front']/1e3, label=_l3, color="cyan", s=_s)
        ax6.scatter(_d['E_direct_c']/1e3,_d['I_trans_back']/1e3, label=_l4, color="blue", s=_s)
        num += 1
    
    # Plot setting
    for ax in [ax1, ax2, ax3, ax4, ax5, ax6]:
        ax.set_xlim(e_min, e_max)
        ax.grid(which='both')
        if (e_max - e_min >= 20.0):
            ax.set_xscale('log')
        else:
            ax.set_xscale('linear')
        
        # K edges
        for key in data_elem:
            e_edge=data_elem[key][2]
            if (e_edge/1e3 > e_min) and (e_edge/1e3 < e_max):
                ax.axvline(e_edge/1e3, linestyle=':', linewidth=1.5, color='green')
                if (ax == ax4):
                    ax.annotate('%s edge' % key, xy=(e_edge/1e3, 0.7), fontsize=12, color='green', rotation=90)

        # BDF
        for BDF in calc_BDF(e_min, e_max):
            h, k, l, e_bdf=BDF[0:4]
            ax.axvline(e_bdf, linestyle=':', linewidth=1.5, color='purple')
            if (ax == ax2):
                ax.annotate('(%d,%d,%d)' % (h, k, l), xy=(e_bdf, 0.7), fontsize=12, color='purple', rotation=90)
        
#        # Au M
#        for _e in [3425, 3148, 2743, 2291, 2206]:
#            ax.axvline(_e/1e3, linestyle=':', linewidth=1.5, color='gold')
#        # Hg M
#        for _e in [3562, 3279, 2827, 2385, 2295]:
#            ax.axvline(_e/1e3, linestyle=':', linewidth=1.5, color='silver')

    for ax in [ax3, ax4, ax5, ax6]:
        ax.legend(fontsize=10)
        
    plt.title(plt_title)
    plt.tight_layout(pad=0.4, w_pad=0.0, h_pad=0.0)
    outfile="%s_%s_%s-%s.pdf" % (outfile, plt_title, e_min, e_max)
    plt.savefig(outfile, papertyle='A4', orientation='portrait',format='pdf')
    print('The plot is generated in %s.' % outfile)
    plt.close('all')
       
    return 0

########################################################
# Main routine
########################################################
if __name__ == '__main__':

    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-o', '--output',
        help='Output file name origin.',
        dest='outfile',
        type=str,
        default='KEK_QL',
        nargs='?'
        )
    parser.add_argument(
        '-r', '--range',
        help='Min and max energy (keV) for plot.',
        dest='e_range',
        type=float,
        default=[2.0, 12.0],
        nargs=2
        )
    args = parser.parse_args()

    # Check arguments.
    outfile = args.outfile
    e_min = args.e_range[0]
    e_max = args.e_range[1]
    
    # Plot
    make_plot(datadir1, datasets_cen, outfile, "Center", e_min, e_max)
    make_plot(datadir1, datasets_top, outfile, "Top", e_min, e_max)
    make_plot(datadir1, datasets_bottom, outfile, "Bottom", e_min, e_max)
    #make_plot(datadir1, datasets_pvc, outfile, "PVC", 2.815, 2.50)
    #make_plot(datadir1, datasets_cu, outfile, "Cu", 8.95, 9.05)
    #make_plot(datadir2, datasets_hisor, outfile, "HISOR", e_min, e_max)