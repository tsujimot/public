#!/usr/local/anaconda2/bin/python

############################################################################
# [Function]
#
# Calculate Bragg difraction features.
#
############################################################################


########################################################
# Imports
########################################################
import numpy as np
from scipy import constants

########################################################
# User-defined parameters
########################################################

# Hexagonal
unit_h =np.array([
        [1, 0, 0],
        [np.cos(120.0/180.0*np.pi), np.sin(120.0/180.0*np.pi), 0],
        [0, 0, 1]
    ])

# Cubic
unit_c = [
        [1,0,0],
        [0,1,0],
        [0,0,1],
    ]

# Be hexagonal.
scale_Be =np.array([2.2870, 2.2870, 3.5830])*1e-10 # A





########################################################
# Functions
########################################################
#-------------------------------------------------------
def get_unit(scale, unit):

    for i in range(3):
        unit[i] *= scale[i]
    return unit


#-------------------------------------------------------
def calc_bdf(unit, hkl):

    h, k, l = hkl
    a, b, c = scale_Be
    #d=a/(4/3*(h^{2}+hk+k^{2})+a^{2}*l^{2}/c^{2})^{1/2}

    
    np.sqrt()
    
    4.0/3.0 * (h**2.0 + h*k + k**2.0) / a**2.0 

    unit *= hkl
    inv_unit = np.linalg.inv(unit)
    A = inv_unit.dot([1,1,1])
    norm = np.linalg.norm(A)

    E = norm * constants.h * constants.c / constants.e * 1e-3    
    
    print E 
    return E



########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    a, b, c = 2.2870, 2.2870, 3.5830
    
    # Miller index
    i_max = 5
    for h in range(0,i_max):
        for k in range(0,i_max):
            for l in range(0,i_max):
                if (h==0 and k==0 and l==0):
                    continue
                else:
                    d = a/(4.0/3.0*(h**2.0+h*k+k**2.0)+a**2.0*l**2.0/c*2.0)**0.5
                    print h,k,l, 12.4/2.0/d
                



