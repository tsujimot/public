#!/usr/local/anaconda3/bin/python3

########################################################
# Imports
########################################################
import argparse
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import linecache
from scipy.optimize import curve_fit
from scipy.signal import savgol_filter

import seaborn as sns
sns.set_style("white", {
    'axes.facecolor': "1.0",
    'xtick.major.size': 10.0,
    'ytick.major.size': 10.0,
    'xtick.minor.size': 6.0,
    'ytick.minor.size': 6.0,
    'xtick.direction': u'in',
    'ytick.direction': u'in',
    })
sns.set_context("talk", 1.5)


########################################################
# User Setting
########################################################
#peak=8.0478 # keV
xmin = 0.0
xmax = 1024.0
energy_ArKa1 = 2.9577
energy_WKa1 = 8.3976
energy_NiKa1 = 7.4782

########################################################
# Functions
########################################################
def gain_curve(x, gain, offset):

    return gain * x + offset

#-------------------------------------------------------
def get_go(channels, energies):
    
    param, cov = curve_fit(gain_curve, channels, energies)

    print("gain = %.2f [eV/ch], offset = %.2f [eV]" % (param[0]*1e3, param[1]*1e3))
    return param

#-------------------------------------------------------
def assign_energy(data, lines):

    data_smo= savgol_filter(data.Count, 15, 3) # window size 51, polynomial order 3
    data_smo = pd.DataFrame(data_smo, columns=['Count_smo'])
    data=pd.concat([data,data_smo],axis=1)

    energies=[]
    channels=[]
    for key, row in lines.iterrows():
        if (row.UseforFit ==0):
            continue
        ChPeak = data[(data.Channel>row.ChLo) & (data.Channel<row.ChHi)].sort_values(by='Count_smo', ascending=False).head(1)['Channel'].values[0]
        channels.append(ChPeak)
        energies.append(row.Energy)
    gain, offset = get_go(channels, energies)
    del data['Count_smo']
    data['Energy'] = data['Channel'].apply(lambda x: gain_curve(x, gain, offset))#, axis=1)

    
    return data, gain, offset
    
#-------------------------------------------------------
def make_plot(infiles, outfile_base, lines, e_width):

    # Plot each spectrum
    plt.figure(figsize=(11.69,8.27))
    plt.clf()
    plt.subplots_adjust(hspace=0.05)

    ax1 = plt.subplot(1,1,1)
    ax1.grid(which='y')
    ax1.set_xlabel('Channel')
    ax1.set_ylabel('Rate (1/s)')
    ax1.set_xscale('linear')
    ax1.set_yscale('log')    

    num_sum=0
    exp_time_sum=0.0
    data_sum = None

    for infile in infiles:
        print('Reading %s' % infile)

        # Read exposure time.
        header = linecache.getline(infile, 1)
        exp_time = int(header.split(' ')[4].split('/')[1].replace(')\n',''))
        print('Exposure time %d ms' % exp_time)
        linecache.clearcache() 

        # Read data
        data=pd.read_csv(infile,skiprows=1,header=None,delim_whitespace=True,names=['Channel','Count'])

        # Assign data.
        data, gain, offset = assign_energy(data, lines)

        # Derive Rate
        data['Rate'] = data['Count'] / exp_time * 1e3

        # Plot each
        ax1.plot(data.Channel, data.Rate, label=infile)

        # Add to the sum
        num_sum += 1
        exp_time_sum += exp_time
        if (data_sum is None):
            data_sum = data
        else:
            data_sum += data

    ax1.legend()
    plt.tight_layout(pad=0.4, w_pad=0.0, h_pad=0.0)
    outfile = outfile_base + '_each.pdf'
    plt.savefig(outfile, papertyle='A4', orientation='portrait',format='pdf')
    print('The plot is generated in %s.' % outfile)
    plt.close('all')

    # Process summed data
    data_sum['Channel'] = data_sum['Channel'] / num_sum
    data_sum['Count_err'] = data_sum['Count'].apply(np.sqrt)
    data_sum['Rate'] = data_sum['Count'] / exp_time_sum * 1e3
    data_sum['Rate_err'] = data_sum['Count_err'] / exp_time_sum * 1e3

    # Assign data.
    data_sum, gain, offset = assign_energy(data_sum, lines)

    EnergyLo=lines.head(1).Energy.values[0]-e_width
    EnergyHi=lines.head(1).Energy.values[0]+e_width
    counts_RoI = data_sum[(data_sum.Energy>=EnergyLo) & (data_sum.Energy<=EnergyHi)].Count.sum()
    err_RoI = np.sqrt(counts_RoI)
    print('Num_data, Exp time (s), Counts, Err, E_low, E_high of %s' % lines.head(1).Name.values[0])
    print(num_sum, exp_time_sum/1e3, counts_RoI, err_RoI, EnergyLo, EnergyHi)

    # Setup figure
    plt.figure(figsize=(11.69,8.27))
    plt.clf()
    plt.subplots_adjust(hspace=0.05)

    ax1 = plt.subplot(1,1,1)
    ax1.grid(axis='y')
    ax1.set_xlabel('Channel')
    ax1.set_ylabel('Count rate (1/s)')
    ax1.set_xscale('linear')
    ax1.set_yscale('log')    
    ax1.set_xlim(xmin, xmax)

    ax1.plot(data_sum.Channel, data_sum.Rate, label=infile)

    ax2 = ax1.twiny()
    ax2.set_xlabel('Energy (keV)')
    ax2.set_xscale('linear')
    ax2.set_xlim(gain_curve(xmin, gain, offset), gain_curve(xmax, gain, offset))

    for key, row in lines.iterrows():
        if (row.UseforFit == 1):
            _color='black'
        else:
            _color='grey'
        ax2.axvline(row.Energy, ls=':', color=_color)
        ax2.annotate(row.Name, xy=(row.Energy, 0.1), rotation=90, color=_color)

    ax2.axvspan(EnergyLo,EnergyHi, alpha=0.3, color='grey')        

    plt.tight_layout(pad=0.4, w_pad=0.0, h_pad=0.0)
    outfile = outfile_base + '_sum.pdf'
    plt.savefig(outfile, papertyle='A4', orientation='portrait',format='pdf')
    print('The plot is generated in %s.' % outfile)
    plt.close('all')

    return 0

########################################################
# Main routine
########################################################
if __name__ == '__main__':

    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--infiles',
        help='Input files.',
        dest='infiles',
        type=str,
        nargs='+'
        )
    parser.add_argument(
        '-o', '--output',
        help='Output PDF file name.',
        dest='outfile_base',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-d', '--data',
        help='Data file containing the hints for lines',
        dest='datafile',
        type=str,
        nargs='?',
        default=None
        )
    parser.add_argument(
        '-w', '--width',
        help='Width of the region of interest',
        dest='e_width',
        type=float,
        nargs='?',
        default=0
        )
    args = parser.parse_args()

    # Check arguments.
    infiles = args.infiles
    outfile_base = args.outfile_base[0]
    datafile = args.datafile
    e_width = args.e_width

    # Plot
    lines=pd.read_csv(datafile,skiprows=0,header=None,delim_whitespace=False,names=['ChLo','ChHi','Energy','Name','UseforFit'])
    make_plot(infiles, outfile_base, lines, e_width)
