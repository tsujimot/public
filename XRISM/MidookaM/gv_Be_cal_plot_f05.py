#!/usr/local/anaconda3/bin/python3

########################################################
# Imports
########################################################
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import seaborn as sns
sns.set_style("white", {
    'axes.facecolor': "1.0",
    'xtick.major.size': 10.0,
    'ytick.major.size': 10.0,
    'xtick.minor.size': 6.0,
    'ytick.minor.size': 6.0,
    'xtick.direction': u'in',
    'ytick.direction': u'in',
    })
sns.set_context("talk", 1.0)
palette = sns.color_palette(n_colors=24)


########################################################
# User Setting
########################################################
# Cannot import from gv_Be_cal_calc_f05 for library issues.
datadir='/home/astro/Work/XARM/SXS2/20190613/dat/XPD/'
outdir=datadir + 'Mercury/'
XPD_intensity_file = outdir + "XPD_sim.csv"
XPD_line_file=outdir + 'XPD_sim_peaks.csv'

# Energy range.
e_max = 12.0 # keV
e_min = 2.0 # keV
De = 0.1 # keV
energies = np.linspace(e_min, e_max, np.int((e_max-e_min)/De)+1)

dtheta = 1.0

########################################################
# Functions
########################################################
#-------------------------------------------------------
def read_XPD_intensity(XPD_intensity_file):
    
    columns=['E', 'I_total']
    XPD_intensity = pd.read_csv(XPD_intensity_file, delim_whitespace=True, names=columns, skiprows=0)
    return XPD_intensity

#-------------------------------------------------------
def read_XPD_line(XPD_file):

    columns=['E', 'h', 'k', 'l', 'SA', 'peak_two_theta']
    XPD_lines = pd.read_csv(XPD_file, header=None, names=columns, skiprows=0)
    XPD_lines['E_BDF'] = XPD_lines['E'] * np.sin(np.pi*XPD_lines['peak_two_theta']/2.0/180.0)

    return XPD_lines

#-------------------------------------------------------
def read_XPD_pattern(energy, XPD_intensity, outdir):

    columns=['two_theta','I', 'error']
    infile=outdir + "XPD_sim_%.3f.xye" % energy
    
    XPD_pattern = pd.read_csv(infile, header=None, names=columns, skiprows=1, delim_whitespace=True) 
    I_total=XPD_intensity[np.abs(XPD_intensity.E-energy)<1e-4].I_total.values[0]
    
    if (I_total>0):
        XPD_pattern['I_denorm'] = XPD_pattern['I']/I_total
        
    return XPD_pattern

#-------------------------------------------------------
def get_MI(XPD_lines):

    MI=XPD_lines[['h','k','l','SA','E_BDF']].groupby(['h','k','l']).first().reset_index()
    for key, mi in MI.iterrows():
        if (mi.SA==' True'):
            print("(h, k, l) = (%d, %d, %d) at %.3f keV is absent." % (mi.h, mi.k , mi.l, mi.E_BDF))
        elif (mi.SA==' False'):
            print("(h, k, l) = (%d, %d, %d) at %.3f keV is present." % (mi.h, mi.k , mi.l, mi.E_BDF))
    MI=MI[MI.SA!=' True'].copy()

    return MI

#-------------------------------------------------------
def plot_XPD_pattern(energy, XPD_intensity, outdir):

    XPD_pattern = read_XPD_pattern(energy, XPD_intensity, outdir)
    
    # Plot setup.
    plt.figure(figsize=(11.69, 8.27))
    plt.clf()
    plt.subplots_adjust(hspace=0.05)

    # Transmission rate
    ax1 = plt.subplot(1,1,1)
    ax1.set_xlabel('2$\\theta$ (deg)')
    ax1.set_ylabel('Intensity')
    ax1.set_yscale('linear')

    ax1.plot(XPD_pattern['two_theta'], XPD_pattern['I'])

    plt.title("%.3f keV" % energy)
    plt.tight_layout(pad=0.4, w_pad=0.0, h_pad=0.0)
    outfile=outdir+"XPD_sim_%.3f.pdf" % (energy)
    plt.savefig(outfile, papertyle='A4', orientation='portrait',format='pdf')
    print('The plot is generated in %s.' % outfile)
    plt.close('all')
       
    return 0
    
#-------------------------------------------------------    
def make_plot(energies, XPD_intensity, XPD_lines, MI, outfile):

    # Plot setup.
    plt.figure(figsize=(8.27,11.69))
    plt.clf()
    plt.subplots_adjust(hspace=0.05)

    # I
    ax1 = plt.subplot(3,1,3)
    ax1.set_xlabel('Energy (eV)')
    ax1.set_ylabel('$I_{\\mathrm{line}}$')
    ax1.set_yscale('linear')
    
    # 2theta
    ax2 = plt.subplot(3,1,2, sharex=ax1)
    plt.setp(ax2.get_xticklabels(), visible=False)
    ax2.set_xlabel('')
    ax2.set_ylabel('2$\\theta$')
    ax2.set_yscale('linear')

    # I_total
    ax3 = plt.subplot(3,1,1, sharex=ax1)
    plt.setp(ax3.get_xticklabels(), visible=False)
    ax3.set_xlabel('')
    ax3.set_ylabel('$I_{\\mathrm{total}}$')
    ax3.set_yscale('linear')
    ax3.plot(XPD_intensity.E, XPD_intensity.I_total)
    
    for key, mi in MI.iterrows():
        if ( np.abs(mi.h) + np.abs(mi.k) + np.abs(mi.l) > 3):
            continue
        
        print("Retrieving for Miller index (%d, %d, %d)" % (mi.h, mi.k, mi.l))
        XPD_lines_select = XPD_lines[(XPD_lines.h==mi.h)  & (XPD_lines.k==mi.k) & (XPD_lines.l==mi.l)]

        data = None                    
        for energy in energies:        

            XPD_line_select_ROI = XPD_lines_select[XPD_lines_select.E==energy]
            if (len(XPD_line_select_ROI)==0):
                continue
            else:
                peak_two_theta = XPD_line_select_ROI.peak_two_theta.values[0]
                XPD_pattern = read_XPD_pattern(energy, XPD_intensity, outdir)
                XPD_pattern_ROI = XPD_pattern[(XPD_pattern.two_theta > peak_two_theta - dtheta) & (XPD_pattern.two_theta < peak_two_theta + dtheta) & (XPD_pattern.I_denorm>0)]
                I_line=XPD_pattern_ROI[XPD_pattern_ROI.I_denorm>0].I_denorm.max()
                #I_line=XPD_pattern_ROI[XPD_pattern_ROI.I_denorm>0].I.max()
                #print(len(XPD_pattern_ROI), intensity_total)

                datum = pd.DataFrame([[energy, peak_two_theta, I_line]], columns=['energy', 'peak_two_theta', 'I_line'])

                if (data is None):
                    data = datum
                else:
                    data = pd.concat([data, datum], axis=0)
                    
        ax1.plot(data.energy, data.I_line, label="(%d,%d,%d) at %.3f" % (mi.h, mi.k, mi.l, mi.E_BDF))
        ax2.plot(data.energy, data.peak_two_theta, label="(%d,%d,%d) at %.3f" % (mi.h, mi.k, mi.l, mi.E_BDF))

    # Plot setting
    for ax in [ax1, ax2, ax3]:
        ax.set_xlim(e_min, e_max)
        ax.grid(which='both')
        ax.set_xscale('linear')
        ax.legend(fontsize=12, ncol=3)

    plt.tight_layout(pad=0.4, w_pad=0.0, h_pad=0.0)
    outfile="%s.pdf" % (outfile)
    plt.savefig(outfile, papertyle='A4', orientation='portrait',format='pdf')
    print('The plot is generated in %s.' % outfile)
    plt.close('all')
       
    return 0

########################################################
# Main routine
########################################################
if __name__ == '__main__':

    XPD_intensity = read_XPD_intensity(XPD_intensity_file)
    XPD_lines = read_XPD_line(XPD_line_file)
    MI = get_MI(XPD_lines)

    # Plot XPD pattern    
    #for energy in energies:   
    #    plot_XPD_pattern(energy, XPD_intensity, outdir)
        
    make_plot(energies, XPD_intensity, XPD_lines, MI, 'f05')
    