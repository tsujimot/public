#!/opt/CCDC/Python_API_2019/miniconda/bin/python

########################################################
# Imports
########################################################
import pandas as pd
import numpy as np

########################################################
# User Setting
########################################################
infile='/home/astro/Work/XARM/SXS2/20190613/dat/XPD/VESTA/XPD_CuKa.txt'

data = pd.read_csv(infile, delim_whitespace=True)
data.columns=['h', 'k', 'l', 'd', 'ReF', 'ImF', 'F', 'two_theta', 'I', 'M', 'ID', 'Phase', '']
data=data[data.two_theta>0]
data['theta'] = data['two_theta']/2.0/180.0*np.pi
data['LP'] = (1. + np.cos(2.*data['theta'])**2.)/(np.sin(data['theta'])**2.*np.cos(data['theta']))
data['I_calc']=data['LP']*data['F']**2.0*data['M']/693.807092*100.0
data['I_calc2']=data['F']**2.0*data['M']/90.251726*100.0

print(data)
#data.plot(x='I', y='I_calc', kind='scatter')