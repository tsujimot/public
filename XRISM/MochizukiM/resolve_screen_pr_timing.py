#!/usr/bin/env python3

############################################################
# Description
############################################################
# Relative timing between L and H.


########################################################
# Imports
########################################################
import argparse
import pandas as pd
import numpy as np


########################################################
# User-defined parameters
########################################################
pos_pr=13
len_pr=1024+16
evt_thres1 = 25
evt_thres2 = 75
#ts = np.linspace(0,len_pr-1,len_pr)#*80e-6 # s
#b=-1.1e-4
#c=0.0

########################################################
# Functions
########################################################
#-------------------------------------------------------
def calc_deriv(adcSample):
    deriv=[]
    for k in range(1024):
        sum=0
        for i in range(8):
            sum-=adcSample[k+i]
            sum+=adcSample[k+i+8]
        deriv.append(sum//4)
    return np.array(deriv)

#-------------------------------------------------------
def main(infile, outfile, pixel):

    sample_max=50000
    results = None
    data = pd.read_pickle(infile)
    for i, datum in data.head(sample_max).iterrows():

        adcSample=datum.values[pos_pr:pos_pr+len_pr]
        deriv=calc_deriv(adcSample)
        excess1=np.array(np.where(deriv>=evt_thres1))[0]
        _time_H1 = excess1[(excess1>130) & (excess1<150)]
        if (len(_time_H1)):
            time_H1 = _time_H1[0]
        else:
            time_H1 = np.nan
        excess2=np.array(np.where(deriv>=evt_thres2))[0]
        _time_H2 = excess2[(excess2>130) & (excess2<150)]
        if (len(_time_H2)):
            time_H2 = _time_H2[0]
        else:
            time_H2 = np.nan
        time_L = np.argmax(deriv)
        result=pd.DataFrame([pixel, datum.ITYPE, time_H1, time_H2, time_L, datum.dtrigLP, datum.TIME_VERNIER, datum.TICK_SHIFT, datum.DERIV_MAX, datum.RISE_TIME]).T

        if (results is None):
            results = result
        else:
            results = pd.concat([results, result], axis=0)

    if (len(results)>0):
        results.columns=['pixel', 'grade', 'timeH1', 'timeH2', 'timeL', 'dtrigLP', 'timeVernier', 'tickShift', 'derivMax', 'riseTime']
        results.reset_index(inplace=True, drop=True)
        results.to_pickle('{}'.format(outfile))

        print(results)
    return 0


########################################################
# Main routine
########################################################
if __name__ == '__main__':

    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--input',
        help='Input file name.',
        dest='infile',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-p', '--pixel',
        help='pixel.',
        dest='pixel',
        type=int,
        nargs=1
        )
    parser.add_argument(
        '-o', '--output',
        help='Output file name stem.',
        dest='outfile',
        type=str,
        nargs=1
        )
    args = parser.parse_args()

    main(args.infile[0], args.outfile[0], args.pixel[0])

