#!/usr/bin/env python3

############################################################
# Description
############################################################
# Extract GTI with a stable 50 mK control.


########################################################
# Imports
########################################################
import argparse
import pandas as pd
from astropy.io import fits
from resolve_utils import stime_to_datetime

########################################################
# User-defined parameters
########################################################



########################################################
# Functions
########################################################
#-------------------------------------------------------
def main(file, year0=2019):

    thres_fluc = 2.5
    temp_lo = 49
    temp_hi = 51
    thres_stable = 180

    hdu='HK_SXS_TEMP'

    # Open file.
    with fits.open(file) as hdulist:
        data = hdulist[hdu].data
    
    time = data.field('TIME')
    adrc_ct_ctl_fluc = data.field("ADRC_CT_CTL_FLUC")
    adrc_ct_mon_fluc = data.field("ADRC_CT_MON_FLUC")
    st1_ctl = data.field("ST1_CTL")
    
    df = pd.DataFrame([time, adrc_ct_ctl_fluc, adrc_ct_mon_fluc, st1_ctl]).T
    df.columns=['TIME', 'ADRC_CT_CTL_FLUC', 'ADRC_CT_MON_FLUC', 'ST1_CTL']

    df_RoI = df[(df.ADRC_CT_CTL_FLUC<thres_fluc) & (df.ADRC_CT_MON_FLUC<thres_fluc) & (df.ST1_CTL > temp_lo) & (df.ST1_CTL < temp_hi)].copy()

    if (len(df_RoI)>0):

        df_RoI.reset_index(inplace=True)
        time0=[0.0]
        time0.extend(df_RoI['TIME'].values[:-1])
        df_RoI['dTIME'] = df_RoI['TIME'] - time0

        def print_time(time):
            
            dt = stime_to_datetime(time, year0)
            print(time, dt)

        for gap in df_RoI[df_RoI.dTIME>thres_stable].index:

            if (gap==0):
                time=df_RoI.iloc[gap]['TIME']
                print_time(time)
            else:
                time=df_RoI.iloc[gap-1]['TIME']
                print_time(time)
                time=df_RoI.iloc[gap]['TIME']
                print_time(time)
        time=df_RoI.iloc[-1]['TIME']
        print_time(time)

    return 0


########################################################
# Main routine
########################################################
if __name__ == '__main__':

    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--input',
        help='Input file name.',
        dest='infiles',
        type=str,
        nargs='+'
        )
    parser.add_argument(
        '-y', '--year0',
        help='Year 0',
        dest='year0',
        type=int,
        nargs=1,
        default=2019,
        )
    args = parser.parse_args()

    for file in args.infiles:
        main(file, args.year0[0])

