#!/usr/bin/env python3

############################################################
# Description
############################################################
# Extract GTI with a stable 50 mK control.


########################################################
# Imports
########################################################
import argparse
import pandas as pd
import numpy as np


########################################################
# User-defined parameters
########################################################
GTIfile='/data1/tsujimot/Data/Resolve/20230304/fits/GTI_50mK.pkl'
pr_max = 10000

########################################################
# Functions
########################################################
def inGTI(times_UT, GTIfile):

    ans=[]
    for time_UT in times_UT:
        GTIs=pd.read_pickle(GTIfile)
        filter=GTIs[(GTIs.Start<time_UT) & (GTIs.Stop>time_UT)]

        if (len(filter)==0):
            ans.append(0)
        else:
            ans.append(1)
    return ans

#-------------------------------------------------------
def main(infiles, outfile, pixel):

    data_all = None

    for file in infiles:
        data = pd.read_pickle(file)
        
        data_RoI = data[(data['ITYPE'] == 0) & (data['PIXEL'] == pixel)
            & (data['SLOPE_DIFFER000'] == 0) & (data['QUICK_DOUBLE000'] == 0)
            & (data['TICK_SHIFT'] > -8) & (data['TICK_SHIFT'] < 7)].copy()
        
        data_RoI["dtrigLP"] = data_RoI["TRIG_LP"] -  data_RoI["EDB_TRIG_LP"]
        del data_RoI["TRIG_LP"]
        del data_RoI["EDB_TRIG_LP"]
        ans=inGTI(data_RoI.index.values, GTIfile)
        data_RoI['inGTI'] = ans
        data_RoI_inGTI=data_RoI[data_RoI['inGTI']==1]
        print("{}/{} records selected in {}.".format(len(data_RoI_inGTI), len(data), file))
        if (data_all is None):
            data_all = data_RoI_inGTI
        else:
            data_all = pd.concat([data_all, data_RoI_inGTI], axis=0)

            if (len(data_all) > pr_max):
                print("{} pulse records are collected. Ignoring the remaining files.".format(pr_max))
                break
    
    data_all.to_pickle(outfile)
    print("Saved in {}".format(outfile))
    return 0


########################################################
# Main routine
########################################################
if __name__ == '__main__':

    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--input',
        help='Input file name.',
        dest='infiles',
        type=str,
        nargs='+'
        )
    parser.add_argument(
        '-o', '--output',
        help='Output file name.',
        dest='outfile',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-p', '--pixel',
        help='Pixel to choose (0--35)',
        dest='pixel',
        type=int,
        nargs=1,
        )
    args = parser.parse_args()

    main(args.infiles, args.outfile[0], args.pixel[0])

