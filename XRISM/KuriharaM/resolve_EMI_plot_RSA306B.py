#!/usr/bin/env python3

############################################################################
# [Function]
# Decimate and plot the data taken with RSA306.
#############################################################################

########################################################
# Imports
########################################################
import argparse, os
import numpy as np

# Plotly offline
import plotly.offline as offline
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from myutils import read_qdp, get_plotly_fig, add_line, add_range
import plotly.offline as offline
offline.init_notebook_mode(connected=False)

########################################################
# User-defined parameters
########################################################
sampling_RSA306 = 112e6
sammpling_resolve = 12.5e3
record_len = 50*1024/sammpling_resolve
f_filter4='/data00/astro/Data/Resolve/20210808/dat/RSA306_FIR_4.csv'

########################################################
# Functions
########################################################
def main(infile, outfile_stem):

	# Read data
	Xs_raw = np.load(infile)['arr_0']
	Ys_raw = np.load(infile)['arr_1']
	
	# Amplitude = sqrt(I*I + Q*Q)
	Ys_raw = np.abs(Ys_raw)

	# Start processing.
	Xs = Xs_raw
	Ys = Ys_raw

	# Decimation by (1/4)**6
	filter4=np.loadtxt(f_filter4, delimiter=',')
	for i in range(6):
		sampling_start = sampling_RSA306/1e3/2**(i*2)
		sampling_stop = sampling_start/4
		print("Decimating from {:.0f} kHz to {:.0f} kHz.".format(sampling_start,sampling_stop))
		# Convolve
		Ys = np.convolve(Ys, filter4, mode = 'same')
		Xs = Xs[::4]
		Ys = Ys[::4]

	# Final decimation
	print("Decimating from {:.0f} kHz to {:.0f} kHz.".format(sampling_stop,sampling_stop/2))
	Xs = Xs[::2]
	Ys = Ys[::2]
	
	# Plot
	Layout = go.Layout(
		xaxis = dict(
			title="Time (s)",
			range=[0,record_len],
			type='linear',
			),
		yaxis = dict(
			title="Signal",
			#range=[-40,20],
			type='linear',
			),
		legend = dict(
			orientation="v",
			yanchor="bottom",
			y=0.02,
			xanchor="right",
			x=0.98,       
			),
		showlegend=True
    )     
	fig = get_plotly_fig()
	fig.update_layout(Layout)

	trace = go.Scattergl(
    	name = "raw",
        x = Xs_raw,
        y = Ys_raw,
        mode='markers',
        marker=dict(color="gray", size=1),
        )
	fig.add_trace(trace)

	trace = go.Scattergl(
    	name = "decimated w/o FIR",
        x = Xs_raw[::8192],
        y = Ys_raw[::8192],
        mode='markers',
        marker=dict(color="red", size=5),
        )
	fig.add_trace(trace)
	
	trace = go.Scattergl(
    	name = "decimated w. FIR",
        x = Xs,
        y = Ys,
        mode='markers',
        marker=dict(color="blue", size=5),
        )
	fig.add_trace(trace)

	# Output
	offline.iplot(fig)
	offline.plot(fig, filename='%s.html' % (outfile_stem), auto_open=True, include_mathjax='cdn', validate=False)
	fig.write_image("%s.pdf" % outfile_stem)
	return 0

########################################################
# Main
########################################################
if __name__ == '__main__':
	# Command-line parser
	parser = argparse.ArgumentParser()
	parser.add_argument(
		'-i', '--infile',
	 	help='Input file',
	  	dest='infile',
	   	type=str,
		nargs=1,
		)
	args = parser.parse_args()

	infile = args.infile[0]
	if (os.path.isfile(infile)) : 
		outfile_stem = infile.replace('.npz','')
		main(infile, outfile_stem)
	else:
		print('{} does not exist.'.format(infile))
