#!/usr/bin/env python3

############################################################
# Description
############################################################
# Manipluate the time domain data.


########################################################
# Imports
########################################################
import argparse
from resolve_plot_hk import collate_data, n_panels
from resolve_utils import get_datetime_from_args, filter_by_datetime, abs_zero, save_files, get_pHe4
from myutils import yield_color, add_line, add_range


########################################################
# User-defined parameters
########################################################



########################################################
# Functions
########################################################
#-------------------------------------------------------
def main(indir, period, kind, flg_norm):
    
    data = collate_data(indir, dbhks, datetime_start, datetime_end, resample=resample, rates=rates, quads=quads)     

    return 0


########################################################
# Main routine
########################################################
if __name__ == '__main__':

    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--indir',
        help='Input dir name.',
        dest='indir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-o', '--outdir', 
        help='Output dir.',
        dest='outdir',
        default='./',
        type=str,
        nargs='+'
        )
    parser.add_argument(
        '-s', '--start',
        help='Start datetime (YYYY/MM/DD hh:mm:ss).',
        dest='start',
        type=str,
        nargs=2
        )
    parser.add_argument(
        '-e', '--end',
        help='End datetime (YYYY/MM/DD hh:mm:ss).',
        dest='end',
        type=str,
        nargs=2
        )
    parser.add_argument(
        '-p', '--plot',
        help='Plot.',
        dest='plot',
        type=str,
        nargs=1
        )

    args = parser.parse_args()

    # Time
    datetime_start = get_datetime_from_args(args.start)
    datetime_end = get_datetime_from_args(args.end)
    
    # Plot    
    plot = args.plot[0]
    
    # Tweak for rates and lcs
    rates=None
    quads=None
    if ('rates_' in plot):
        rates=[plot.split("_")[1]]
        plot_new='rates'
    elif ('lc_' in plot):
        quads=[plot.split("_")[1]]
        plot_new='lc'
    else:
        plot_new=plot

    if plot_new in n_panels.keys():
        dbhks = eval('dbhks_'+plot_new)
    from resolve_plot_hk import dbhks