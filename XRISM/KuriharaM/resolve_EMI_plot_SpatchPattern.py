#!/usr/bin/env python3

############################################################################
# [Function]
# Plot S-band patch antenna pattern.
#############################################################################

########################################################
# Imports
########################################################
from abc import abstractmethod
import pandas as pd

# Plotly offline
import plotly.offline as offline
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import plotly.offline as offline
offline.init_notebook_mode(connected=False)

########################################################
# User-defined parameters
########################################################
datadir='/home/astro/Work/XARM/SXS2/20211015/Figures/24/'

########################################################
# Functions
########################################################
def make_plot(axis, infile_calc, infile_measure):

	outfile_stem = "{}/f24{}".format(datadir,axis)

	base=30+10
	# Read data
	pattern_calc = pd.read_csv(infile_calc)
	pattern_calc.columns=["Freq","Phi","Theta","GainTotal","GainTheta","GainPhi"]

	pattern_measure = pd.read_csv(infile_measure, delim_whitespace=True, header=None, skiprows=0)
	pattern_measure.columns=["Angle","Pol","Height","Pow","Freq","RF","Freq","RBW","Gain"]
	pattern_measure_v = pattern_measure[pattern_measure.Pol=="v"]
	pattern_measure_h = pattern_measure[pattern_measure.Pol=="h"]

	t1 = go.Scatterpolar(
	r = pattern_calc['GainTotal'], theta = pattern_calc['Theta'], mode = 'lines', name = 'Total (dB)',
	line_color = 'red'
	)
	t2 = go.Scatterpolar(
	r = pattern_calc['GainPhi'], theta = pattern_calc['Theta'], mode = 'lines', name = 'Phi (dB)',
	line_color = 'blue'
	)
	t3 = go.Scatterpolar(
	r = pattern_calc['GainTheta'], theta = pattern_calc['Theta'], mode = 'lines', name = 'Theta (dB)',
	line_color = 'green'
	)
	t4 = go.Scatterpolar(
	r = pattern_measure_v['Gain']+base, theta = pattern_measure_v['Angle'], mode = 'markers', name = 'Vertical (dB)',
	line_color = 'cyan'
	)
	t5 = go.Scatterpolar(
	r = pattern_measure_h['Gain']+base, theta = pattern_measure_h['Angle'], mode = 'markers', name = 'Horizontal (dB)',
	line_color = 'magenta'
	)

	data = [t1,t2,t3,t4,t5]
	fig = go.Figure(data = data)

	fig.update_layout(
		#title = 'Pattern around {} axis'.format(axis),
		showlegend = True,
		polar = dict(

			radialaxis=dict(range=[-50, 20]),
			angularaxis = dict(
				rotation=90, # start position of angular axis
				direction="counterclockwise"
			),

		)
	)

	# Output
	#offline.iplot(fig)
	#offline.plot(fig, filename='%s.html' % (outfile_stem), auto_open=True, include_mathjax='cdn', validate=False)
	fig.write_image("%s.pdf" % outfile_stem)
	return 0

########################################################
# Main
########################################################
if __name__ == '__main__':

	# Around x
	make_plot('x', 'gain_around_x.csv', 'gain_around_x.dat')

	# Around y
	make_plot('y', 'gain_around_y.csv', 'gain_around_y.dat')

	# Around z
	make_plot('z', 'gain_around_z.csv', 'gain_around_z.dat')
