#!/usr/bin/env python3

############################################################
# [Function]
# Plot scatter plots of events.
############################################################

########################################################
# Imports
########################################################
from sys import exit
import argparse, datetime, os, re, glob
from os.path import exists
import pandas as pd
import numpy as np
#
from resolve_plot_hk import str_now
from resolve_utils import get_datetime_from_args, px_grades
px_grades=['HP', 'MP', 'MS', 'LP', 'LS', 'BL', 'EL']
from resolve_plot_spec import collate_events, energy_ac, energy_px, ac_min, ac_max, px_min, ac_bin, px_max, px_bin, cal_min, cal_max, cal_bin
#
# Plotly offline
import plotly.offline as offline
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import plotly.express as px
#
import threading

########################################################
# User-defined parameters
########################################################


########################################################
# Functions
########################################################
#-------------------------------------------------------
def make_plot(events_RoI, x, y, k, title, outstem):

    fig = px.scatter(events_RoI, x=x, y=y, color=k, marginal_x="histogram", marginal_y="histogram",
        category_orders={
            "ITYPE": ["HP", "MP", "MS", "LP", "LS"],
            "PSP_ID": ["A", "B"]
            }
            )

    Layout = go.Layout(
        title=title,
        showlegend=True,
        )
    fig.update_layout(Layout)
    fig.update_layout(legend= {'itemsizing': 'constant'})


    # Output
    def write_static():
        fig.write_image("%s.png" % outstem, validate=False, engine='kaleido')
        print("File saved in %s.png" % (outstem))        
    def write_html():
        offline.plot(fig, filename='%s.html' % outstem, auto_open=False, include_mathjax='cdn', validate=False)
        print("File saved in %s.html" % (outstem))
    
    t1 = threading.Thread(target=write_static)
    t2 = threading.Thread(target=write_html)
    #
    t1.start()
    t2.start()
    #
    t1.join()
    t2.join()
    
    return 0
    
########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--indir',
        help='Input dir.',
        dest='indir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-o', '--outdir',
        help='Output dir name.',
        dest='outdir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-s', '--start',
        help='Start datetime (YYYY/MM/DD hh:mm:ss).',
        dest='start',
        type=str,
        nargs=2
        )
    parser.add_argument(
        '-e', '--end',
        help='End datetime (YYYY/MM/DD hh:mm:ss).',
        dest='end',
        type=str,
        nargs=2
        )
    parser.add_argument(
        '-r', '--resample',
        help='Resample to reduce size',
        dest='resample',
        type=int,
        default=[100],
        nargs=1
        )
    parser.add_argument(
        '-p', '--plot',
        help='Type of plot (ac, px, cal, or itype).',
        dest='plot',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-q', '--quad',
        help='Quadrant (a0, a1, b0, b1).',
        dest='quad',
        type=str,
        default=[None],
        nargs=1
        )
    args = parser.parse_args()
    plot = args.plot[0]
    quad = args.quad[0]
    
    datetime_start = get_datetime_from_args(args.start)
    datetime_end = get_datetime_from_args(args.end)
    # outstem
    str_start1 = pd.to_datetime(datetime_start).strftime('%Y%m%d_%H%M%S')
    str_end1 = pd.to_datetime(datetime_end).strftime('%Y%m%d_%H%M%S')
    # title
    str_start2 = pd.to_datetime(datetime_start).strftime('%Y/%m/%d %H:%M:%S')
    str_end2 = pd.to_datetime(datetime_end).strftime('%Y/%m/%d %H:%M:%S')

    # Collate events
    if (plot == 'ac'):
        dbdir='ac'
        base=0
    elif (plot == 'px'):
        dbdir='px_cl'
        base=1
    elif (plot == 'cal'):
        dbdir='px_cal'
        base=1
    elif (plot == 'itype'):
        dbdir='px_uf'
        base=1
    events = collate_events(args.indir[0], dbdir, datetime_start, datetime_end, base)
    if (events is None) or len(events) == 0:
        print("No events found.")
        exit(1)

    # plot
    outdir = args.outdir[0]
    if (plot=="ac"):
        outdir_new = outdir.replace('evt','evt/{}'.format(plot))
        if not exists(outdir_new):
            os.makedirs(outdir_new)
        outstem=outdir_new + '/evt_ac_%s-%s' % (str_start1, str_end1)
        title='%s (%s-%s) resampled at 1/%d updated at %s' % (args.plot[0], str_start2, str_end2, args.resample[0], str_now)
        x, y, k='PHA', 'DURATION', 'PSP_ID'
        events_RoI = events[(events.AC_ITYPE==0)][[x, y, k]][::args.resample[0]].copy()
        ac_sides=['A','A','B','B']
        events_RoI['PSP_ID'] = events_RoI['PSP_ID'].apply(lambda x: ac_sides[x])
        make_plot(events_RoI, x, y, k, title, outstem)
    elif (plot=="px"):
        for _px in range(0,36):
            if (_px == 12):
                continue
            outdir_new = outdir.replace('evt','evt/p{:02d}'.format(_px))
            if not exists(outdir_new):
                os.makedirs(outdir_new)
            outstem=outdir_new + '/evt_p%02d_%s-%s' % (_px, str_start1, str_end1)
            title='%s p%02d (%s-%s) resampled at 1/%d updated at %s' % (args.plot[0], _px, str_start2, str_end2, args.resample[0], str_now)
            x, y, k='DERIV_MAX', 'RISE_TIME', 'ITYPE'
            events_RoI = events[(events.PIXEL==_px) & (events.ITYPE<5)][[x, y, k]][::args.resample[0]].copy()
            events_RoI['ITYPE'] = events_RoI['ITYPE'].apply(lambda x: px_grades[x])
            make_plot(events_RoI, x, y, k, title, outstem)
    elif (plot=="cal"):
        _px=12
        outdir_new = outdir.replace('evt','evt/p{:02d}'.format(_px))
        if not exists(outdir_new):
            os.makedirs(outdir_new)
        outstem=outdir_new + '/evt_p%02d_%s-%s' % (_px, str_start1, str_end1)
        title='%s p%02d (%s-%s) resampled at 1/%d updated at %s' % (args.plot[0], _px, str_start2, str_end2, args.resample[0], str_now)
        x, y, k='DERIV_MAX', 'RISE_TIME', 'ITYPE'
        events_RoI = events[(events.PIXEL==_px) & (events.ITYPE<5)][[x, y, k]][::args.resample[0]].copy()
        events_RoI['ITYPE'] = events_RoI['ITYPE'].apply(lambda x: px_grades[x])
        make_plot(events_RoI, x, y, k, title, outstem)
    elif (plot=="itype"):
        outdir_new = outdir
        if not exists(outdir_new):
            os.makedirs(outdir_new)
        outstem=outdir_new + '/itype_%s_%s-%s' % (quad, str_start1, str_end1)
        title='%s (%s) (%s-%s) resampled at 1/%d updated at %s' % (plot, quad, str_start2, str_end2, args.resample[0], str_now)
        x, y, k='TIME', 'ITYPE', 'PIXEL'
        events["TIME"] = events.index
        if (quad=="a0"):
            px_min, px_max=0,9
        elif (quad=="a1"):
            px_min, px_max=9,18
        elif (quad=="b0"):
            px_min, px_max=19,27
        elif (quad=="b1"):
            px_min, px_max=28,36
        events_RoI = events[(events.PIXEL>=px_min) & (events.PIXEL<px_max)][[x, y, k]]
        events_RoI['ITYPE_SHIFT'] = events_RoI['ITYPE']+0.1*events_RoI['PIXEL']
        if (args.resample[0] > 0):
            events_RoI = events_RoI[::args.resample[0]].copy()
        
        events_RoI.sort_values("PIXEL", inplace=True)
        make_plot(events_RoI, x, "ITYPE_SHIFT", k, title, outstem)