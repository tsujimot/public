#!/usr/bin/env python3

############################################################################
# [Function]
# Fetch GOES data (X-ray, proton, electron flux, magnetosphere).
#
############################################################################


########################################################
# Imports
########################################################
import requests
import json
import argparse
import numpy as np
import pandas as pd
from resolve_makedb_fff import save_files_fff_range


########################################################
# User-defined parameters
########################################################
outdir="/data6/ORBIT/goes"
server="https://services.swpc.noaa.gov/json/goes"
sources=["primary", "secondary"]
sources_short=["p", "s"]
durations=["7-day"]
datasets=[
    "xrays", "xray-flares", "xray-background", "magnetometers",
    "differential-protons", "integral-protons", "integral-proton-fluence",
    "differential-electrons", "integral-electrons", "integral-electron-fluence",
    ]
datasets_short=[
    "x", "xFlare", "xBkg", "mag", "pDiff", "eDiff"
]

"""
https://services.swpc.noaa.gov/json/goes/primary/xrays-7-day.json
https://services.swpc.noaa.gov/json/goes/primary/xray-flares-7-day.json
https://services.swpc.noaa.gov/json/goes/primary/xray-background-7-day.json
https://services.swpc.noaa.gov/json/goes/primary/differential-protons-7-day.json
https://services.swpc.noaa.gov/json/goes/primary/integral-proton-fluence-7-day.json
https://services.swpc.noaa.gov/json/goes/primary/integral-protons-7-day.json
https://services.swpc.noaa.gov/json/goes/primary/differential-electrons-7-day.json
https://services.swpc.noaa.gov/json/goes/primary/integral-electrons-7-day.json
https://services.swpc.noaa.gov/json/goes/primary/integral-electron-fluence-7-day.json
https://services.swpc.noaa.gov/json/goes/primary/magnetometers-7-day.json
"""

########################################################
# Functions
########################################################
#------------------------------------------------------------
def makedb():

    for source in sources:
        for dataset, dataset_short in zip(datasets, datasets_short):
            for duration in durations:
                url="{}/{}/{}-{}.json".format(server,source,dataset,duration)
                print("Fetching data from {}...".format(url))
                r = requests.get(url, headers={'Accept': 'application/json'})
                df = pd.DataFrame.from_dict(r.json())
                start=df["time_tag"].values[0]
                stop=df["time_tag"].values[-1]
                outfile="{}/{}_{}_{}_{}_{}.csv".format(outdir,source[0],dataset_short,duration,start,stop)
                try:
                    df.to_csv(outfile)
                except:
                    print("Saving data into {} failed.".format(outfile))
    
    return 0

#------------------------------------------------------------
"""
# Only GOES/XRS data are available by using Fido.

from sunpy import timeseries as ts
from sunpy.net import Fido
from sunpy.net import attrs as a

tstart = "{} {}".format(args.start[0].replace("/","-"), args.start[1].replace("/","-"))
tend = "{} {}".format(args.end[0].replace("/","-"), args.end[1].replace("/","-"))

def get_goes_data(i, tstart, tend):

    result_goes = Fido.search(a.Time(tstart, tend), a.Instrument("XRS"), a.goes.SatelliteNumber(i), a.Resolution("flx1s"))
    file_goes = Fido.fetch(result_goes)

    if (len(file_goes)>0):
        ts_goes = ts.TimeSeries(file_goes)
        ts_goes_all = ts_goes[0]
        for j in range(1,len(ts_goes)):
            ts_goes_all = ts_goes_all.concatenate(ts_goes[j])

        df_goes = ts_goes_all.to_dataframe()
        df_goes = df_goes[(df_goes["xrsa_quality"] == 0) & (df_goes["xrsb_quality"] == 0)][["xrsa","xrsb"]]
        df_goes.columns = ["xrsa{}".format(i), "xrsb{}".format(i)]
        return df_goes
    else:
        return None
    
    goes15=get_goes_data(15, tstart, tend)
    goes16=get_goes_data(16, tstart, tend)
    goes17=get_goes_data(17, tstart, tend)
    goes18=get_goes_data(18, tstart, tend)
    
    for i, goes in enumerate([goes15, goes16, goes17, goes18]):
        if (goes is not None):
            # Save files.
            outdir_new = outdir + '/goes/'
            save_files_fff_range(goes, outdir_new, 'goes{}'.format(15+i), pkl=True, csv=False, stat=True)
"""

########################################################
# Main routine
########################################################
if __name__ == '__main__':

    makedb()