#!/usr/bin/env python3

############################################################
# [Function]
# Plot average pulse or templates.
############################################################

########################################################
# Imports
########################################################
import argparse
import os
import pandas as pd
import numpy as np
import scipy.fftpack as sf
#
from resolve_plot_ns import palette, dashes
from resolve_plot_hk import str_now
from resolve_utils import sampling_rate, save_files
from myutils import add_line, add_range
# Plotly offline
import plotly.offline as offline
import plotly.graph_objects as go
from plotly.subplots import make_subplots
#
import threading

########################################################
# User-defined parameters
########################################################

########################################################
# Functions
########################################################
#-------------------------------------------------------
def get_FD(data, len_FD):

    data /= np.max(data)
    data -= np.mean(data)
    fft = sf.fft(data)[1:len_FD+1]
    data_FD = [np.sqrt(c.real ** 2 + c.imag ** 2) for c in fft]
    Xs_FD = sf.fftfreq(len_FD*2, d=1.0/sampling_rate)[1:len_FD+1]
    
    return data_FD, Xs_FD

#-------------------------------------------------------
def make_plot(infile, outstem, type):

    # Read data
    data = pd.read_pickle(infile)
    
    if (type == 'avgp'):
        title ='Average pulse (%s) updated at %s' % (os.path.split(infile)[1], str_now)
        str0='AVGPULSE'
        str1='AVGDERIV'
        len0=1024
        len1=1024
    elif (type == 'tmpl'):
        title ='Templates (%s) updated at %s' % (os.path.split(infile)[1], str_now)
        str0='TEMPLATE_H'
        str1='TEMPLATE_M'
        len0=1024
        len1=256

    data0 = np.empty([36,len0]) # template_H or avgpulse
    data1 = np.empty([36,len1]) # template_M or avgderiv
    data0_FD = np.empty([36,len0//2]) # template_H or avgpulse
    data1_FD = np.empty([36,len1//2]) # template_H or avgpulse
    Xs0 = np.linspace(0,len0,len0)
    Xs1 = np.linspace(0,len1,len1)

    for i in range(36):
        data0[i,:] = data[data.PIXEL==i][["{}{:03d}".format(str0,i) for i in range(len0)]].values[0]
        data1[i,:] = data[data.PIXEL==i][["{}{:03d}".format(str1,i) for i in range(len1)]].values[0]
        data0_FD[i,:], Xs0_FD = get_FD(data0[i,:], len0//2)
        data1_FD[i,:], Xs1_FD = get_FD(data1[i,:], len1//2)

    # Define layout
    fig = make_subplots(
        rows=2, cols=2, shared_xaxes=False, vertical_spacing=0.1
    )    
    Layout = go.Layout(
        title=title,
        width=1280, 
        height=1280,
        xaxis=dict(            
            autorange = True,
            showgrid = True, 
            tickmode = 'auto'
            ),
        yaxis=dict(
            showgrid = True, 
            tickmode = 'auto',
            exponentformat = 'power'
            ),
        showlegend=True,
        legend = dict(
            x = 1.02, xanchor = 'left',
            y = 1.00, yanchor = 'auto',
            bordercolor = '#444', 
            #borderwidth = 0,
            ),
        )
    fig.update_layout(Layout)
    fig.update_layout(legend= {'itemsizing': 'constant'})

    # data0, time-domain
    fig.update_xaxes(title_text='Sample', type='linear', row=1, col=1)
    fig.update_yaxes(title_text=str0, type='linear', autorange = True, row=1, col=1)
    for px in range(36):
        print("Processing ch%02d" % px)
        fig.add_trace(
            go.Scattergl(
                x=Xs0, y=data0[px,:], 
                name='p%02d' % px, 
                legendgroup=px,
                mode='lines+markers',
                marker=dict(color=palette[px%9], opacity=0.8, size=3),
                line=dict(color=palette[px%9], width=2, dash=dashes[int(px/9)])
            ),
            row=1, col=1
        )

    # data1, time-domain
    fig.update_xaxes(title_text='Sample', type='linear', row=1, col=2)
    fig.update_yaxes(title_text=str1, type='linear', autorange = True, row=1, col=2)
    for px in range(36):
        print("Processing ch%02d" % px)
        fig.add_trace(
            go.Scattergl(
                x=Xs1, y=data1[px,:], 
                name='p%02d' % px, 
                legendgroup=px,
                mode='lines+markers',
                marker=dict(color=palette[px%9], opacity=0.8, size=3),
                line=dict(color=palette[px%9], width=2, dash=dashes[int(px/9)])
            ),
            row=1, col=2
        )

    # data0, freq-domain
    fig.update_xaxes(title_text='Freq (Hz)', type='log', row=2, col=1)
    fig.update_yaxes(title_text=str0, type='log', autorange=False, range=[-6,2], row=2, col=1)
    for px in range(36):
        print("Processing ch%02d" % px)
        fig.add_trace(
            go.Scattergl(
                x=Xs0_FD, y=data0_FD[px,:], 
                name='p%02d' % px, 
                legendgroup=px,
                mode='lines+markers',
                marker=dict(color=palette[px%9], opacity=0.8, size=3),
                line=dict(color=palette[px%9], width=2, dash=dashes[int(px/9)])
            ),
            row=2, col=1
        )

    # data1, freq-domain
    fig.update_xaxes(title_text='Freq (Hz)', type='log', row=2, col=2)
    fig.update_yaxes(title_text=str1, type='log', autorange=False, range=[-6,2], row=2, col=2)
    for px in range(36):
        print("Processing ch%02d" % px)
        fig.add_trace(
            go.Scattergl(
                x=Xs1_FD, y=data1_FD[px,:], 
                name='p%02d' % px, 
                legendgroup=px,
                mode='lines+markers',
                marker=dict(color=palette[px%9], opacity=0.8, size=3),
                line=dict(color=palette[px%9], width=2, dash=dashes[int(px/9)])
            ),
            row=2, col=2
        )

    # Add known lines.
    for i in range(2):
        fig = add_range(fig, 13.93, 16.26, "STC", vert=True, log=True, color='red', row=2, col=i+1)
        fig = add_range(fig, 50.43, 53.83, "JTC", vert=True, log=True, color='blue', row=2, col=i+1)
        fig = add_line(fig, 127.0, 'MTQ', vert=True, log=True, color='green', row=2, col=i+1)
        fig = add_range(fig, 1500/60.0, 4500/60.0, 'RW', vert=True, log=True, color='orange', row=2, col=i+1)
        fig = add_range(fig, 4500, 6250, "Bkg", vert=True, log=True, color='grey', row=2, col=i+1)

    # Output
    def write_static():
        fig.write_image("%s.png" % outstem, validate=False, engine='kaleido')
        print("File saved in %s.png" % (outstem))
    def write_html():
        offline.plot(fig, filename='%s.html' % outstem, auto_open=False, include_mathjax='cdn', validate=False)
        print("File saved in %s.html" % (outstem))
    def save_npz():
        save_files(data0, None, "%s.npz" % outstem)

    t1 = threading.Thread(target=write_static)
    t2 = threading.Thread(target=write_html)
    t3 = threading.Thread(target=save_npz)
    #
    t1.start()
    t2.start()
    t3.start()
    #
    t1.join()
    t2.join()
    t3.join()

    return 0  
    


########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--infile',
        help='Input file.',
        dest='infile',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-o', '--outdir',
        help='Output dir name.',
        dest='outdir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-t', '--type',
        help='avgp or tmpl.',
        dest='type',
        type=str,
        nargs=1
        )

    args = parser.parse_args()
    
    # Outstem
    infile = args.infile[0]
    type = args.type[0]
    outstem=args.outdir[0] + os.path.split(infile)[1].replace('.pkl','')
    make_plot(infile, outstem, type)    
