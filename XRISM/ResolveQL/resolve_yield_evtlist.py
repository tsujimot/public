#!/usr/bin/env python3

############################################################################
# [Function]
# Yield evtlist for plots.
############################################################################


########################################################
# Imports
########################################################
import argparse
import datetime
import pandas as pd
import glob, os, re
import numpy as np
from resolve_makedb_fff import collate_data_fff
from resolve_utils import time_to_utc
from astropy.io import fits


########################################################
# User-defined parameters
########################################################


########################################################
# Functions
########################################################
def read_gti(infile, hdu, hks):

    hdulist = fits.open(infile,memmap=True)
    data = None
    for _i in hks:
        column_name=[_i]
        datum = pd.DataFrame(hdulist[hdu].data.field(_i).byteswap().newbyteorder(),columns=column_name)
        if (data is None):
            data = datum
        else:
            data = pd.concat([data,datum],axis=1) 
    
    return data
#------------------------------------------------------------
def yield_mnv(infile):
    
    target=None
    mnv_time=None
    euler=None

    with open(infile, mode="r") as _f:
        for line in _f:
            if re.match('^Target_Name', line):
                if (target is not None):
                    yield mnv_time_dt, target, euler 
                assert len(line.split()) == 2
                target=line.split()[1]
            elif re.match('^Maneuver_start_time', line):
                mnv_time=line.split()[1]
                mnv_time_dt = datetime.datetime.strptime(mnv_time, '%Y-%m-%dT%H:%M:%S')
            elif re.match('^Euler_angle', line):
                euler=line.split()[1:4]
    yield mnv_time_dt, target, euler

#------------------------------------------------------------
def main(dbdir, pldir, passdir, targetdir):

    # MNV
    mnvfile="/data7/ALL/db/obspln/obspln.pkl"
    
    try:
        mnvdata = pd.read_pickle(mnvfile)
    except:
        mnvdata = None

    infiles = glob.glob('{}/obspln_[0-9]*-[0-9]*.dat'.format(targetdir))
    infiles.sort()

    for infile in infiles:
        for mnv_time, target, euler in yield_mnv(infile):
            if (mnvdata is None):
                mnvdata = pd.DataFrame([mnv_time,target, float(euler[0]), float(euler[1]), float(euler[2])]).T
                mnvdata.columns=["MNV_TIME", "TARGET", "EULER1", "EULER2", "EULER3"]
            else:
                mnvdata_add = pd.DataFrame([mnv_time,target, float(euler[0]), float(euler[1]), float(euler[2])]).T
                mnvdata_add.columns=["MNV_TIME", "TARGET", "EULER1", "EULER2", "EULER3"]
                mnv_time = mnvdata_add["MNV_TIME"].values[0]
                mnvdata = mnvdata[mnvdata["MNV_TIME"]<mnv_time] # Remove all newer than mnv_time.
                mnvdata = pd.concat([mnvdata, mnvdata_add], axis=0)
    
    mnvdata.sort_values("MNV_TIME", inplace=True)
    mnvdata.reset_index(drop=True,inplace=True)
    mnvdata.to_pickle(mnvfile)

    for index, row in mnvdata.iterrows():
        print("{},  , 11, {}".format(row["MNV_TIME"].strftime("%Y/%m/%d %H:%M:%S"), row["TARGET"]))

    # Contacts
    infiles = glob.glob('{}/[0-9]*.csv'.format(passdir))
    infiles.sort()
    if len(infiles) > 0:
        for infile in infiles:
            contacts=pd.read_csv(infile)
            for i, row in contacts[contacts.Primary==1].iterrows():
                antenna=row["Antenna"]
                aos=row["AOS_TLM"]
                los=row["LOS_TLM"]
                print("{}, {}, 3, {}".format(aos.replace("-","/"),los.replace("-","/"),antenna))

    # odb
    infiles = glob.glob('{}/odb/odb*.pkl'.format(dbdir))
    infiles.sort()
    for infile in infiles:
        data = pd.read_pickle(infile)
        for item, row in data.iterrows():
            print("{},  , 4, {}".format(item.strftime("%Y/%m/%d %H:%M:%S"), row.SEQNUM))

    # ns1k
    infiles = glob.glob('{}/ns/1k/ns1k*.npz'.format(dbdir))
    infiles.sort()
    if len(infiles) > 0:
        for infile in infiles:
            _f=infile.split('/')[-1]
            year=int(_f[5:9])
            month=int(_f[9:11])
            day=int(_f[11:13])
            hour=int(_f[14:16])
            min=int(_f[16:18])
            sec=int(_f[18:20])
            dt = datetime.datetime(year, month, day, hour, min, sec)
            print("{},  , 5, nspec".format(dt.strftime("%Y/%m/%d %H:%M:%S")))

    # SAA
    infiles = glob.glob('{}/*/*/*/*/*/*saa_sxs.gti'.format(pldir))
    infiles.sort()
    hdu='STDGTI'
    hks=['START','STOP']
    for infile in infiles:
        data = read_gti(infile, hdu, hks)
        for i,row in data.iterrows():
            start = time_to_utc(row['START'])
            stop = time_to_utc(row['STOP'])
            print("{}, {}, 6, SAA".format(start.strftime("%Y/%m/%d %H:%M:%S"), stop.strftime("%Y/%m/%d %H:%M:%S")))

    # Night earth occultation
    infiles = glob.glob('{}/*/*/*/*/*/*nte_elv.gti'.format(pldir))
    infiles.sort()
    hdu='STDGTI'
    hks=['START','STOP']
    for infile in infiles:
        data = read_gti(infile, hdu, hks)
        for i,row in data.iterrows():
            start = time_to_utc(row['START'])
            stop = time_to_utc(row['STOP'])
            print("{}, {}, 7, NTE".format(start.strftime("%Y/%m/%d %H:%M:%S"), stop.strftime("%Y/%m/%d %H:%M:%S")))

    # Day earth occultation
    infiles = glob.glob('{}/*/*/*/*/*/*dye_elv.gti'.format(pldir))
    infiles.sort()
    hdu='STDGTI'
    hks=['START','STOP']
    for infile in infiles:
        data = read_gti(infile, hdu, hks)
        for i,row in data.iterrows():
            start = time_to_utc(row['START'])
            stop = time_to_utc(row['STOP'])
            print("{}, {}, 8, DYE".format(start.strftime("%Y/%m/%d %H:%M:%S"), stop.strftime("%Y/%m/%d %H:%M:%S")))

    # ADR
    infiles = glob.glob('{}/*/*/*/*/*/*rsl_adr.gti'.format(pldir))
    infiles.sort()
    hdu='GTIADRON'
    hks=['START','STOP']
    for infile in infiles:
        data = read_gti(infile, hdu, hks)
        for i,row in data.iterrows():
            start = time_to_utc(row['START'])
            stop = time_to_utc(row['STOP'])
            print("{}, {}, 9, recycle".format(start.strftime("%Y/%m/%d %H:%M:%S"), stop.strftime("%Y/%m/%d %H:%M:%S")))
    
    return 0

#------------------------------------------------------------
def make_pxac_list(dbdir):

    for kind in ['sd', 'wfrb']:

        outfile='{}/{}/00matching.txt'.format(dbdir, kind)
        with open(outfile, 'w') as _f:

            # px
            infiles_px = glob.glob('{}/{}/px/{}px_*.pkl'.format(dbdir,kind,kind))
            times_px = [datetime.datetime.strptime(os.path.splitext(os.path.basename(_f))[-2].split("_")[1], '%Y%m%d-%H%M%S') for _f in infiles_px]        
            # ac
            infiles_ac = glob.glob('{}/{}/ac/{}ac_*.pkl'.format(dbdir,kind,kind))
            times_ac = [datetime.datetime.strptime(os.path.splitext(os.path.basename(_f))[-2].split("_")[1], '%Y%m%d-%H%M%S') for _f in infiles_ac]
        
            for i, infile_px in enumerate(infiles_px):
                time_px = times_px[i]
                for j, infile_ac in enumerate(infiles_ac):
                    time_ac = times_ac[j]
                    if ( np.abs((time_px - time_ac).total_seconds()) < 4.0):
                        print(infile_px, infile_ac, file=_f)
                        break

    return 0

########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-d', '--db',
        help='dbdir',
        dest='dbdir',
        type=str,
        default=None,
        nargs=1
        )
    parser.add_argument(
        '-p', '--pl',
        help='pldir',
        dest='pldir',
        type=str,
        default=None,
        nargs=1
        )
    parser.add_argument(
        '-c', '--contact',
        help='passdir',
        dest='passdir',
        type=str,
        default=None,
        nargs=1
        )
    parser.add_argument(
        '-t', '--target',
        help='targetdir',
        dest='targetdir',
        type=str,
        default=None,
        nargs=1
        )
    args = parser.parse_args()

    main(args.dbdir[0], args.pldir[0], args.passdir[0], args.targetdir[0])
    make_pxac_list(args.dbdir[0])