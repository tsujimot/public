#!/usr/bin/env python3

############################################################################
# [Function]
# Make SXS HK database for anti-co noise spec frrom sample dump or WFRB.
############################################################################


########################################################
# Imports
########################################################
import argparse, glob
import pandas as pd
import numpy as np
import math
import scipy.fftpack as sf
from resolve_utils import ac_pedestal, ac_thres, save_files, get_datetime_from_args, filter_by_datetime
from resolve_plot_hk import collate_data

########################################################
# User-defined parameters
########################################################

########################################################
# Functions
########################################################
#-------------------------------------------------------
# Generator of ac noise records.
def gen_acnr(datum_ac, px, len_ns, ac_pedestal, ac_thres):

    margin=1.5
    i=0
    for j in range(len(datum_ac)):
        if (datum_ac[j] - ac_pedestal[px] < ac_thres[px]):
            i += 1
        else:
            i=0
        if (i == int(len_ns*1024*margin)):
            yield datum_ac[j-len_ns*1024+1:j+1]
            i=0

#-------------------------------------------------------
def makedb(indir, outdir, datetime_start, datetime_end, mode, len_ns):

    # Get and filter infiles by time
    infiles = glob.glob('{}/{}/ac/{}ac*.pkl'.format(indir,mode, mode)) 
    infiles = filter_by_datetime(infiles, 'epoch', datetime_start, datetime_end, 1)
    if ( len(infiles) == 0 ):
        print("No file found.")
        exit(1)
    infiles.sort(reverse=True)
    
    # Calc nspec
    n_samples=[0, 0, 0, 0] # Total number of nspecs.
    psd = np.zeros(int(len_ns*1024//2))
    psds = [psd, psd, psd, psd] # PSDs.
    asds = [psd, psd, psd, psd] # ASDs.
    filter_func=np.ones(len_ns*1024)

    for infile in infiles:
        
        data = pd.read_pickle(infile)
        if (data is None) or (len(data)==0):
            continue

        stime = data.index[-1]
        print("Reading {}...".format(infile))
        
        # Judge if the noise is taken at cold or warm.
        dbhks={
        'temp' : ["CAMC_CT0", "CAMC_CT1"]
        }
        prepost=60.0 # seconds
        temp_th = 60.0
        hk = collate_data(indir, dbhks, stime+pd.Timedelta(seconds=-prepost), stime+pd.Timedelta(seconds=prepost), resample=0, rates=None, quads=None, pixel=None)
        temp0=hk["CAMC_CT0"].values.mean()
        temp1=hk["CAMC_CT1"].values.mean()
        if ((temp0 < temp_th) and (temp1 < temp_th)):
            print("Noise was collected during cold at {}.".format(stime))
        else:
            print("Noise was collected during warm at {}.".format(stime))
            continue

        for px in range(4):
            if (mode == 'sd') :
                datum = data[(data.PSP_ID==px) & (data.NOISEREC_MODE==2)].iloc[:,3:].values.flatten() 
                if (len(datum)>0):
                    len_sample = int(np.round(len(datum)/1024/25,0)) * 25 * 1024 # 25*(int) samples only. 
                    print("Anti-co {} sampled for {:2d} samples.".format(["A", "A", "B", "B"][px], int(len_sample/1024)))
                else:
                    continue
            elif (mode == 'wfrb'):
                datum = data[(data.PSP_ID==px)].iloc[:,1+2048:2+2048*2].values.flatten() 
                if (len(datum)>0):
                    len_sample = 2**math.floor(np.log2(len(datum)/12500)) * 12500 # 2^(int) s only.
                    print("Anti-co {} sampled for a {:.1f}s.".format(["A", "A", "B", "B"][px], len_sample/12500))
                else:
                    continue

            # Calc and add PSDs.
            n_samples0=n_samples[px]
            for acnr in gen_acnr(datum, px, len_ns, ac_pedestal, ac_thres):
                _fft = sf.fft(acnr*filter_func,len_ns*1024)[1:int(len_ns*1024/2)+1]
                psds[px] = psds[px] + np.abs(_fft)**2.0
                n_samples[px] += 1
            print("{} noise records of {}k length accumulated for anti-co {} in {}".format(n_samples[px] -n_samples0, len_ns, ["A", "A", "B", "B"][px], infile))

    for px in range(4):
        if (n_samples[px] > 0):
            print("{} noise records of {}k length accumulated for anti-co {} in total".format(n_samples[px], len_ns, ["A", "A", "B", "B"][px]))
            psds[px] = psds[px] / n_samples[px]
            asds[px] = np.sqrt(psds[px])
            outdir_new = "{}/nr_ns/ac{:1d}/".format(outdir, px)
            outfile = "nr_ns_ac{:1d}_{}.npz".format(px, stime.strftime("%Y%m%d-%H%M%S"))
            save_files(asds[px], outdir_new, outfile)
            
    return 0
    

########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--indir',
        help='Input dir name.',
        dest='indir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-o', '--outdir', 
        help='Output directory.',
        dest='outdir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-s', '--start',
        help='Start datetime (YYYY/MM/DD hh:mm:ss).',
        dest='start',
        type=str,
        nargs=2
        )
    parser.add_argument(
        '-e', '--end',
        help='End datetime (YYYY/MM/DD hh:mm:ss).',
        dest='end',
        type=str,
        nargs=2
        )
    parser.add_argument(
        '-m', '--mode', 
        help='mode (sd or wfrb)',
        dest='mode',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-l', '--length', 
        help='nspec length (in the unit of k=1024).',
        dest='len_ns',
        type=int,
        nargs=1,
        default=[4],
        )
    args = parser.parse_args()

    datetime_start = get_datetime_from_args(args.start)
    datetime_end = get_datetime_from_args(args.end) + np.timedelta64(1, 's')

    makedb(args.indir[0], args.outdir[0], datetime_start, datetime_end, args.mode[0], args.len_ns[0])