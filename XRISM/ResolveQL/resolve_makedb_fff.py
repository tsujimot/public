#!/usr/bin/env python3

############################################################################
# [Function]
# Make SXS HK database from PPL and PL products.
############################################################################

########################################################
# Imports
########################################################
import argparse
import os
import numpy as np
import pandas as pd
from astropy.io import fits
from resolve_utils import index_stime, save_files, get_psp_card, get_psp_card_alt, cards, cards_alt, stime_to_datetime
from resolve_utils import e1,e2,e3,e4,e5,e6,e7,e8,ez,s1,s2,s3, R_inv, get_ra_dec, get_ra_dec_sun, get_phi_sun, get_beta, get_Gamma
import threading

########################################################
# User-defined parameters
########################################################
timeout_ns1k=300 # seconds to separete two different noise spectra by command.
timeout_ns8k=300 # seconds to separete two different noise spectra by command.
timeout_wfrb=300 #  seconds to separete two different WFRB by command.
timeout_others=60 # seconds to separete two different others by command.


## Attitude
hdu_att='ATTITUDE'
## Orbit
hdu_orb='ORBIT'
## EHK
hdu_ehk='EHK'
## DIST, PCU
hdu_dist_pcu='HK_SXS_DIST'
#hdu_dist_pcu='HK_SXS_DIST_PCU'
## FWE
hdu_fwe='HK_SXS_FWE'
## Cooler driver HK.
hdu_coolers='HK_SXS_COOLERS'
## R/T conversion HK
hdu_temp='HK_SXS_TEMP'
## XBOX-A HK
hdu_xboxa='HK_XBOXA'
## XBOX-B HK
hdu_xboxb='HK_XBOXB'
## PSP ESS HK
hdu_psp_ess='HK_PSP_ESS'
## PSP time HK
#hdu_psp_time='HK_PSP_TIME'
## PSP statistics HK
hdu_psp_stat1='HK_PSP_ST1'
hdu_psp_stat2='HK_PSP_ST2'
hdu_psp_stat3='HK_PSP_ST3'
## PSP-A0 SYS HK
hdu_psp_sys_a0='HK_PSPA0_SYS'
## PSP-A1 SYS HK
hdu_psp_sys_a1=hdu_psp_sys_a0.replace('PSPA0','PSPA1')
## PSP-B0 SYS HK
hdu_psp_sys_b0=hdu_psp_sys_a0.replace('PSPA0','PSPB0')
## PSP-B1 SYS HK
hdu_psp_sys_b1=hdu_psp_sys_a1.replace('PSPA1','PSPB1')
## PSP-A0 USR HK
hdu_psp_usr_a0='HK_PSPA0_USR'
## PSP-A1 USR HK
hdu_psp_usr_a1=hdu_psp_usr_a0.replace('PSPA0','PSPA1')
## PSP-B0 USR HK
hdu_psp_usr_b0=hdu_psp_usr_a0.replace('PSPA0','PSPB0')
## PSP-B1 USR HK
hdu_psp_usr_b1=hdu_psp_usr_a1.replace('PSPA1','PSPB1')
## Gain history file
hdu_ghf='Drift_energy'
hdu_dghf=hdu_ghf

# ns1k
hdu_ns1k="HK_SXS_NOISESPC"
hks_ns1k=["S_TIME", "PIXEL", "NOISESPC"]
# ns8k
hdu_ns8k="HK_SXS_NOISESPC8K"
hks_ns8k=["S_TIME", "PIXEL", "NOISESPC"]
# templates
hdu_tmpl="HK_SXS_TEMPLATE"
hks_tmpl=["S_TIME", "PIXEL", "TEMPLATE_H", "TEMPLATE_M"]
# average pulses
hdu_avgp="HK_SXS_AVGPULSE"
hks_avgp=["S_TIME", "PIXEL", "AVGPULSE", "AVGDERIV"]
# psp_pixel
hdu_psp_pixel="HK_PSP_PIXEL"
hks_psp_pixel=None
# noise records
hdu_nr='EVENTS'
hks_nr=['TIME', 'PIXEL', 'NOISEREC_MODE', 'NOISEREC']
# pulse records
hdu_pr='EVENTS'
hks_pr=['TIME', 'TRIGTIME', 'SAMPLECNT', 'SAMPLECNTTRIG', 'ITYPE', 'PIXEL', 'PHA', 'PI', 'EDB_TRIG_LP', 'TRIG_LP', 'TICK_SHIFT', 'TIME_VERNIER', 'LO_RES_PH', 'DERIV_MAX', 'RISE_TIME', 'SLOPE_DIFFER', 'QUICK_DOUBLE', 'FLAGS', 'PULSEREC']
# sample dump(ac)
hdu_sdac="EVENTS"
hks_sdac=['TIME', 'WFRB_SAMPLE_CNT', 'PSP_ID', 'NOISEREC_MODE', 'NOISEREC']
# sample dump(px)
hdu_sdpx="EVENTS"
hks_sdpx=['TIME', 'WFRB_SAMPLE_CNT', 'PIXEL', 'NOISEREC_MODE', 'NOISEREC']
# wfrb(ac) 
hdu_wfrbac="EVENTS"
hks_wfrbac=["TIME", "PSP_ID", "ADC_SAMPLE", "DERIVATIVE"]
# wfrb(px) 
hdu_wfrbpx="EVENTS"
hks_wfrbpx=["TIME", "PIXEL", "ADC_SAMPLE", "DERIVATIVE"]

# anti-co events
hdu_ac='EVENTS'
hks_ac=['TIME','PSP_ID', 'AC_ITYPE', 'PHA', 'PI', 'DURATION']
# pixel events
hdu_px='EVENTS'
hks_px=['TIME','ITYPE', 'PIXEL', 'PHA', 'PI', 'EPI', 'EPI2', 'LO_RES_PH', 'DERIV_MAX', 'RISE_TIME', 'FLAGS']
hdu_px_cl = hdu_px
hdu_px_uf = hdu_px
hdu_px_cal = hdu_px
hks_px_cl = hks_px
hks_px_uf = hks_px
hks_px_cal = hks_px
# event loss
hdu_el='GTILOST'
hks_el=['TIME', 'START', 'STOP', 'PIXEL', 'EL_LOST_CNT','EL_REASON', 'EL_START_LP', 'EL_STOP_LP']

# SXI CDE
hdu_sxi_cd='HK_SXI_CD_ESHK'

# EPS
hdu_pcu='HK_PCU_ESHK'
hdu_bccu1='HK_BCCU1_ESHK'
hdu_bccu2='HK_BCCU2_ESHK'

# RF
hdu_smu_a_tcim_s_a='HK_SMU_A_TCIM_TCIM_S_A_STS'
hdu_smu_a_tcim_s_b='HK_SMU_A_TCIM_TCIM_S_B_STS'
hdu_smu_a_tcim_x='HK_SMU_A_TCIM_TCIM_X_STS'
hdu_smu_b_tcim_s_a='HK_SMU_B_TCIM_TCIM_S_A_STS'
hdu_smu_b_tcim_s_b='HK_SMU_B_TCIM_TCIM_S_B_STS'
hdu_smu_b_tcim_x='HK_SMU_B_TCIM_TCIM_X_STS'

## AOCS NOM
hdu_acpa_nom='HK_ACPA_HK_NOM'
hdu_acpb_nom='HK_ACPB_HK_NOM'
## AOCS NM
hdu_acpa_nm_1='HK_ACPA_AOCS_HK_NM_1HZ_1'
hdu_acpa_nm_2='HK_ACPA_AOCS_HK_NM_1HZ_2'
hdu_acpb_nm_1='HK_ACPB_AOCS_HK_NM_1HZ_1'
hdu_acpb_nm_2='HK_ACPB_AOCS_HK_NM_1HZ_2'
## AOCS STT
hdu_acpa_stt0='HK_ACPA_AOCS_HK_STT_1HZ_1'
hdu_acpa_stt1='HK_ACPA_AOCS_HK_STT1_R_4HZ'
hdu_acpa_stt2='HK_ACPA_AOCS_HK_STT2_R_4HZ'
hdu_acpa_stt3='HK_ACPA_AOCS_HK_STT3_R_4HZ'
## AOCS SG
hdu_acpa_sg='HK_ACPA_AOCS_HK_SG_MTQ_RCS_1HZ_1'
hdu_acpb_sg='HK_ACPB_AOCS_HK_SG_MTQ_RCS_1HZ_1'
## AOCS MTQ
hdu_acpa_mtq_a='HK_ACPA_AOCS_HK_MTQ_A_R2_8HZ'
hdu_acpa_mtq_b='HK_ACPA_AOCS_HK_MTQ_B_R2_8HZ'
hdu_acpb_mtq_a='HK_ACPB_AOCS_HK_MTQ_A_R2_8HZ'
hdu_acpb_mtq_b='HK_ACPB_AOCS_HK_MTQ_B_R2_8HZ'
## AOCS GAS
hdu_acpa_sg_a='HK_ACPA_AOCS_HK_SG_A_R2_8HZ'
hdu_acpa_sg_b='HK_ACPA_AOCS_HK_SG_B_R2_8HZ'
hdu_acpb_sg_a='HK_ACPB_AOCS_HK_SG_A_R2_8HZ'
hdu_acpb_sg_b='HK_ACPB_AOCS_HK_SG_B_R2_8HZ'
## AOCS PD
hdu_acpa_pd_1='HK_ACPA_PD_DATA_4HZ_1'
hdu_acpb_pd_1='HK_ACPB_PD_DATA_4HZ_1'
hdu_acpa_pd_2='HK_ACPA_PD_DATA_4HZ_2'
hdu_acpb_pd_2='HK_ACPB_PD_DATA_4HZ_2'

# MSE
hdu_smu_a_mse='HK_SMU_A_MSE_MSE_STS'
hdu_smu_b_mse='HK_SMU_B_MSE_MSE_STS'

# HCE
hdu_smu_a_hce_a='HK_SMU_A_AUX_HCE_HK1_HCE_A'
hdu_smu_a_hce_b='HK_SMU_A_AUX_HCE_HK1_HCE_B'
hdu_smu_a_mhce='HK_SMU_A_HCE_M_HCE_SH_SUM'
hdu_smu_b_hce_a=hdu_smu_a_hce_a.replace('SMU_A','SMU_B')
hdu_smu_b_hce_b=hdu_smu_a_hce_b.replace('SMU_A','SMU_B')
hdu_smu_b_mhce=hdu_smu_a_mhce.replace('SMU_A','SMU_B')

# DR
hdu_dr_a='HK_DR_A_DR_HK1'
hdu_dr_a_lv1='HK_DR_A_DR_LV_LIST1'
hdu_dr_a_lv2='HK_DR_A_DR_LV_LIST2'
hdu_dr_a_lv3='HK_DR_A_DR_LV_LIST3'
hdu_dr_a_lv4='HK_DR_A_DR_LV_LIST4'
hdu_dr_a_lv5='HK_DR_A_DR_LV_LIST5'
hdu_dr_a_lv6='HK_DR_A_DR_LV_LIST6'
hdu_dr_a_lv7='HK_DR_A_DR_LV_LIST7'
hdu_dr_a_lv8='HK_DR_A_DR_LV_LIST8'

hdu_dr_b='HK_DR_B_DR_HK1'
hdu_dr_b_lv1='HK_DR_B_DR_LV_LIST1'
hdu_dr_b_lv2='HK_DR_B_DR_LV_LIST2'
hdu_dr_b_lv3='HK_DR_B_DR_LV_LIST3'
hdu_dr_b_lv4='HK_DR_B_DR_LV_LIST4'
hdu_dr_b_lv5='HK_DR_B_DR_LV_LIST5'
hdu_dr_b_lv6='HK_DR_B_DR_LV_LIST6'
hdu_dr_b_lv7='HK_DR_B_DR_LV_LIST7'
hdu_dr_b_lv8='HK_DR_B_DR_LV_LIST8'

########################################################
# Functions
########################################################
#------------------------------------------------------------
def collate_data_fff(infile, hdu, hks, resample_sec, year0=2019, flg_TIME=False):
    'Collata and save data from FFF.'

    print("Reading %s in %s" % (hdu, infile))

    # Open infile.
    try:
        hdulist = fits.open(infile,memmap=True)
    except:
        print('%s : File open error.' % infile)
        return None
    
    try:
        hdulist[hdu]
    except:
        print("%s : No HDU with %s found." % (infile, hdu))
        return None

    if (len(hdulist[hdu].data)==0):
        print('%s : No data found.' % infile)
        return None
    
    # List of HKs to collate.
    if (hks is None):
        if (flg_TIME is False):
            hks=['S_TIME']
        else: 
            hks=['TIME']
        if ( hdu in ['ATTITUDE']):
            try:
                hk_start = hdulist[hdu].data.names.index('QPARAM')
                hks.extend(hdulist[hdu].data.columns.names[hk_start:])        
            except:
                hks.extend(hdulist[hdu].data.columns.names[:])
        #elif ( hdu in ['EHK']):
        #    try:
        #        hk_start = hdulist[hdu].data.names.index('EULER1')
        #        hks.extend(hdulist[hdu].data.columns.names[hk_start:])        
        #    except:
        #        hks.extend(hdulist[hdu].data.columns.names[:])
        elif ( hdu in ['HK_PSP_ST1', 'HK_PSP_ST2', 'HK_PSP_ST3', 'HK_PSP_PIXEL']):
            try:
                hk_start = hdulist[hdu].data.names.index('ADU_CNT')+1
                hks.extend(hdulist[hdu].data.columns.names[hk_start:])
            except:
                hks.extend(hdulist[hdu].data.columns.names[:])
        elif ( hdu in ['HK_SXS_TEMP']):
            try:
                hk_start = hdulist[hdu].data.names.index('US')+1
                hks.extend(hdulist[hdu].data.columns.names[hk_start:])
            except:
                hks.extend(hdulist[hdu].data.columns.names[:])
        else:
            try:
                hk_start = hdulist[hdu].data.names.index('PROC_STATUS')+1
                hks.extend(hdulist[hdu].data.columns.names[hk_start:])        
            except:
                hks.extend(hdulist[hdu].data.columns.names[:])

    # Convert FITS.rec_array to DataFrame column by column.
    # Some items are not properly processed for uninvestigated reasons.
    #if (hdu=='HK_SXS_FWE'):
    #    hk.remove('FWE_LAST_CMD_ID')
    data = None
    for _i in hks:
        if (_i=='CDA_CMP_FREQ_SET') or (_i=='CDB_CMP_FREQ_SET') or (_i=='JTD_CMP_FREQ_SET'):
            continue

        #print(hdu,_i)
        if hasattr(hdulist[hdu].data.field(_i)[0], "__len__"): # if the entry is a list, not a value,
            column_name=[]
            for _j in range(len(hdulist[hdu].data.field(_i)[0])):
                column_name.append(_i + '%03d' % _j)
        else:
            column_name=[_i]
        
        if (hdu=='HK_SXI_CD_ESHK'):
            datum = pd.DataFrame(hdulist[hdu].data.field(_i),columns=column_name)
        else:
            datum = pd.DataFrame(hdulist[hdu].data.field(_i).byteswap().newbyteorder(),columns=column_name)

        if (data is None):
            data = datum
        else:
            data = pd.concat([data,datum],axis=1) 
    
    # Clear the memory
    hdulist.close()

    # Return none if no data
    if ((data is None) or (len(data)==0)):
        print('%s : No data found.' % infile)
        return None

    # Index S_TIME
    data = index_stime(data, year0=year0)
    
    # Resample
    if (resample_sec == 0 ):
        return data
    else:  
        data = data[~data.index.duplicated(keep='last')]
        resample_str=str(resample_sec) + 'S'
        #return data.resample(resample_str).first()
        return data

#------------------------------------------------------------
def add_info_att(data):
    'Add info to att'

    return data

#------------------------------------------------------------
def add_info_orb(data):
    'Add info to orb'

    data["TIME"] = data.index
    data["Gamma"] = data[["TIME"]].apply(lambda x: get_Gamma(x[0])[0]/np.pi*180.0, axis=1)
    data["beta"] = data[["Gamma", "AN", "I"]].apply(lambda x: get_beta(x[0]/180.0*np.pi, x[1]/180.0*np.pi, x[2]/180.0*np.pi)/np.pi*180.0, axis=1)
    del data["TIME"]

    return data

#------------------------------------------------------------
def add_info_ehk(data):
    'Add info to EHK'

    data["TIME"] = data.index
    for e, label in zip([e1, e2, e3, e4, e5, e6, e7, e8, ez, s1, s2, s3], ["RADEC_1", "RADEC_2", "RADEC_3", "RADEC_4", "RADEC_5", "RADEC_6", "RADEC_7", "RADEC_8", "RADEC_Z", "RADEC_STT1", "RADEC_STT2", "RADEC_STT3"]):
        data[label]=data[["EULER1", "EULER2", "EULER3"]].apply(lambda x: R_inv(x[0], x[1], x[2], e), axis=1)
    data["RADEC_SAT"] = data[["TIME", "SAT_LON", "SAT_LAT"]].apply(lambda x: get_ra_dec(x[0], x[1], x[2], vec=True), axis=1)
    # Calc inner products and arccos.
    data["Alpha_Z"] = data[["RADEC_Z","RADEC_SAT"]].apply(lambda x: (np.arccos(-np.dot(x[0], x[1]))/np.pi*180.0), axis=1)
    for i in range(1,9):
        data["Alpha_{:d}".format(i)] = data[["RADEC_{:d}".format(i),"RADEC_SAT"]].apply(lambda x: (np.arccos(-np.dot(x[0], x[1]))/np.pi*180.0), axis=1)
    for i in range(1,4):
        data["Alpha_STT{:d}".format(i)] = data[["RADEC_STT{:d}".format(i),"RADEC_SAT"]].apply(lambda x: (np.arccos(-np.dot(x[0], x[1]))/np.pi*180.0), axis=1)

    h=550.0 # km. height of XRISM
    R=6371 # km. Radius of Earth
    theta = np.arcsin(R/(R+h))
    f=theta*theta/2
    for i in range(1,9):
        data["fDS_{:1d}".format(i)] = data["Alpha_{:1d}".format(i)].apply(lambda x: 1.0 - f*np.max([np.cos(x/180.0*np.pi),0]))

    data["RADEC_Sun"] = data[["TIME"]].apply(lambda x: get_ra_dec_sun(x[0], vec=True), axis=1)
    data["Beta_Z"] = data[["RADEC_Z","RADEC_Sun"]].apply(lambda x: (np.arccos(-np.dot(x[0], x[1]))/np.pi*180.0), axis=1)
    for i in range(1,9):
        data["Beta_{:d}".format(i)] = data[["RADEC_{:d}".format(i),"RADEC_Sun"]].apply(lambda x: (np.arccos(np.dot(x[0], x[1]))/np.pi*180.0), axis=1)

    data["theta_Sun"] = data["Beta_3"]
    data["phi_Sun"] = data[["RADEC_3", "RADEC_Z", "RADEC_Sun"]].apply(lambda x: get_phi_sun(x[0],x[1],x[2]), axis=1)

    del data["TIME"]
    
    return data

#------------------------------------------------------------
def add_info_stat(data, table):
    'Add info to stat HK'

    if (table == 'psp_stat1'):
        Get_psp_card = get_psp_card
        Cards = cards
    elif (table == 'psp_stat2'):
        Get_psp_card = get_psp_card_alt
        Cards = cards_alt
    elif (table == 'psp_stat3'):
        return data

    # Add info.
    for _p in range(36):
        card = Get_psp_card(_p)
        col_name='%s_P%02d_EVT_CNT' % (card, _p)
        data[col_name] = data[eval('\'%s_P%02d_HP_CNT\'' % (card, _p))] + data[eval('\'%s_P%02d_MP_CNT\'' % (card, _p))] + data[eval('\'%s_P%02d_MS_CNT\'' % (card, _p))] + data[eval('\'%s_P%02d_LP_CNT\'' % (card, _p))] + data[eval('\'%s_P%02d_LS_CNT\'' % (card, _p))]
        col_name='%s_P%02d_PR_CNT' % (card, _p)
        data[col_name] = data[eval('\'%s_P%02d_PEDB_CNT\'' % (card, _p))] - data[eval('\'%s_P%02d_REJ_CNT\'' % (card, _p))]
    
    for _c in range(4):
        card = Cards[_c]
        col_name = '%s_EVT_CNT' % card
        data[col_name] = data[eval('\'%s_P%02d_EVT_CNT\'' % (card, _c*9))]
        for _p in range(1,9):
            data[col_name] += data[eval('\'%s_P%02d_EVT_CNT\'' % (card, _c*9+_p))]
    
        col_name = '%s_PEDB_CNT' % card
        data[col_name] = data[eval('\'%s_P%02d_PEDB_CNT\'' % (card, _c*9))]
        for _p in range(1,9):
            data[col_name] += data[eval('\'%s_P%02d_PEDB_CNT\'' % (card, _c*9+_p))]
    
        col_name = '%s_LOST_CNT' % card
        data[col_name] = data[eval('\'%s_P%02d_LOST_CNT\'' % (card, _c*9))]
        for _p in range(1,9):
            data[col_name] += data[eval('\'%s_P%02d_LOST_CNT\'' % (card, _c*9+_p))]

    return data

#------------------------------------------------------------
def save_files_fff_range(data, outdir, table, pkl=True, csv=False, stat=False):
    
    stime_start= data.index[0].strftime("%Y%m%d-%H%M%S")    
    stime_end = data.index[-1].strftime("%Y%m%d-%H%M%S")    
    os.makedirs(outdir, exist_ok=True)
    
    def save_pkl():
        outfile = '%s_%s_%s.pkl' % (table, stime_start, stime_end) 
        save_files(data, outdir, outfile, kind='pkl')
    def save_csv():
        outfile = '%s_%s_%s.csv' % (table, stime_start, stime_end) 
        save_files(data, outdir, outfile, kind='csv')
    def save_stat():
        outfile = '%s_%s_%s_stat.txt' % (table, stime_start, stime_end) 
        save_files(data, outdir, outfile, kind='stat')

    t1 = threading.Thread(target=save_pkl)
    t2 = threading.Thread(target=save_csv)    
    t3 = threading.Thread(target=save_stat)
    
    if (pkl is True):
        t1.start()
    if (csv is True):
        t2.start()
    if (stat is True):
        t3.start()
    
    if (pkl is True):
        t1.join()
    if (csv is True):
        t2.join()
    if (stat is True):
        t3.join()

    outfile = '%s/%s_%s_%s.pkl' % (outdir, table, stime_start, stime_end) 
    return outfile

#------------------------------------------------------------
def save_files_fff_epoch(data, outdir, table, pkl=True, csv=False):
    
    stime_start= data.index[0].strftime("%Y%m%d-%H%M%S")    
        
    def save_pkl():
        outfile = '%s_%s.pkl' % (table, stime_start) 
        save_files(data, outdir, outfile, kind='pkl')
    def save_csv():
        outfile = '%s_%s_%s.csv' % (table, stime_start, stime_end) 
        save_files(data, outdir, outfile, kind='csv')

    t1 = threading.Thread(target=save_pkl)
    t2 = threading.Thread(target=save_csv)    
    if (pkl is True):
        t1.start()
    if (csv is True):
        t2.start()
    
    if (pkl is True):
        t1.join()
    if (csv is True):
        t2.join()
    
    outfile = '%s/%s_%s.pkl' % (outdir, table, stime_start) 
    return outfile

#------------------------------------------------------------
def yield_data_ns(data, len_nspec, timeout):
    'Yield PSP nspec data one by one.'

    # Group and yield
    start_time = data.index[0]
    data_ns= np.zeros((len_nspec, 36))
    cnt=-1

    # Plot for each group.
    for _i in range(len(data)):
        px = int(data.iloc[_i,1])
        # Ignore px > 36
        if ( px > 36 ) :
            continue
        # Fill data. 
        #if ( data.index[_i] < start_time + np.timedelta64(int(timeout)) or (cnt < 35) ):
        if (((data.index[_i]-start_time).total_seconds() < timeout) and cnt < 35):
            data_ns[:,px] = data.iloc[_i,2:2+len_nspec]
            cnt+=1
        # Yield and flush data.
        else:
            yield [start_time, data_ns]
            start_time = data.index[_i]
            data_ns = np.zeros((len_nspec, 36))
            cnt=0

    yield [start_time, data_ns]

#------------------------------------------------------------
def yield_data_sd(data, type):
    'Yield PSP sample dump data one by one.'

    chunks = np.unique(data.WFRB_SAMPLE_CNT.values)
    chunk_thres= 64 # Some sd have not exactly the same WFRB_SAMPLE_CNT values.

    def yield_chunk(chunks, chunk_thres):
        yield chunks[0]
        chunk_prev=chunks[0]
        for chunk in chunks[1:]:
            if (chunk - chunk_prev > chunk_thres):
                yield chunk
                chunk_prev = chunk

    for chunk in yield_chunk(chunks, chunk_thres):
        datum = data[data.WFRB_SAMPLE_CNT-chunk <= chunk_thres]
        datum.sort_index(inplace=True)
        start_time = datum.index[0]

        # Check sample length.
        if (type == "px"):
            n_chan=36
            px_hk='PIXEL'
        elif (type == "ac"):
            n_chan=4
            px_hk='PSP_ID'
        else:
            print("type {} is not supported".format(type))    
            
        nsamples_px=np.zeros(n_chan)
        for px in range(n_chan):
            nsamples_px[px] = len(datum[datum[px_hk]==px])   

        nsamples = int(np.max(nsamples_px))
        assert (nsamples == 50) or (nsamples ==25)
        print("sd_{} at {} with {:d} samples.".format(type, start_time, nsamples))

        # Null pixles with incomplete data.
        for px in np.where(nsamples_px<nsamples)[0]:
            datum = datum[datum[px_hk]!=px].copy()
            print("\tbut {}{:02d} has only {:d} samples thus removed.".format(px_hk, px, int(nsamples_px[px])))

        yield datum


#------------------------------------------------------------
def yield_data_others(data, timeout):
    'Yield data one chunk by chunk.'

    start_time = data.index[0]
    for _i in range(len(data)):
        end_time = data.index[_i]
        if ( end_time > start_time + np.timedelta64(int(timeout), 's')):
            data_chunk = data[(data.index >= start_time) & (data.index <= end_time)]
            yield data_chunk
            start_time = end_time
            
    data_chunk = data[(data.index >= start_time) & (data.index <= end_time)]
    #assert len(data_chunk) == 144
    yield data_chunk

#------------------------------------------------------------
def conv_xls(infile):

    outfile=infile.replace('.pkl','.csv')
    data = pd.read_pickle(infile)
    data = data[~data.duplicated()]
    data = data.set_index(["PSP_ID","PIXEL"], drop=True)
    data.sort_values(by=["PSP_ID","PIXEL"], inplace=True)
    for i in range(32):
        del data['PROC_STATUS{:03d}'.format(i)]
    #data.to_excel(outfile, sheet_name="PSP pixel HK", index=True, header=True)
    data.to_csv(outfile)

    print("{} is saved.".format(outfile))

    return 0

#------------------------------------------------------------
def makedb(infile, outdir, tables, year0):

    for table in tables:                
        hdu=eval('hdu_' + table)
        
        # PSP stat HK
        if table in  ['psp_stat1', 'psp_stat2', 'psp_stat3']:   
            hks=None
            resample_sec=16
            data = collate_data_fff(infile, hdu, hks, resample_sec, year0=year0)
            if (data is None) or (len(data)==0):
                continue
            data = add_info_stat(data, table)
            outdir_new = outdir + '/%s/' % table
            save_files_fff_range(data, outdir_new, table, stat=True)                            
        
        # pr, nr
        elif table in ['pr', 'nr']:        
            hks = eval('hks_' + table)
            resample_sec=0

            data = collate_data_fff(infile, hdu, hks, resample_sec, year0=year0, flg_TIME=True)
            if (data is None) or (len(data)==0):
                continue            
            
            # Extract noise records only.
            if (table == 'nr'):
                data = data[data.NOISEREC_MODE==0]
            if (data is None) or (len(data)==0):
                continue
            
            outdir_new = outdir + '/%s/' % table
            save_files_fff_range(data, outdir_new, table)                            
    
        # ns
        elif table in ['ns1k', 'ns8k']:        
            hks = eval('hks_' + table)
            resample_sec=0
            data = collate_data_fff(infile, hdu, hks, resample_sec, year0=year0, flg_TIME=True)
            if (data is None) or (len(data)==0):
                continue

            # Set the outdir.
            if (table == 'ns1k'):
                len_nspec=512
                outdir_new = outdir + '/ns/1k/'
                timeout=timeout_ns1k
            elif (table == 'ns8k'):
                len_nspec=4096
                outdir_new = outdir + '/ns/8k/'
                timeout=timeout_ns8k
            
            for _i in yield_data_ns(data, len_nspec, timeout):
                stime, data_ns =_i[0], _i[1] 
                if (data_ns is None) or (len(data_ns) ==0):
                    continue            
                outfile = '%s_' % table + stime.strftime("%Y%m%d-%H%M%S") + '.npz'
                save_files(data_ns, outdir_new, outfile, kind='npz')
        
        # sd
        elif table in ['sdpx', 'sdac']:
            hks = eval('hks_' + table)
            resample_sec=0
            data = collate_data_fff(infile, hdu, hks, resample_sec, year0=year0, flg_TIME=True)
            if (data is None) or (len(data)==0):
                continue

            # Extract sample dump only.
            data = data[data.NOISEREC_MODE==2]        
            if (data is None) or (len(data)==0):
                continue
            
            # Set the outdir.
            if (table == 'sdpx'):
                outdir_new = outdir + '/sd/px/'
                for data_chunk in yield_data_sd(data, type="px"):
                    save_files_fff_epoch(data_chunk, outdir_new, table)
            elif (table == 'sdac'):
                data = data[(data.PSP_ID<4)] 
                outdir_new = outdir + '/sd/ac/'
                for data_chunk in yield_data_sd(data, type="ac"):
                    save_files_fff_epoch(data_chunk, outdir_new, table)

        # wfrb and others
        elif table in ['wfrbpx', 'wfrbac', 'avgp', 'tmpl', 'psp_pixel']:        
            hks = eval('hks_' + table)
            resample_sec=0
            data = collate_data_fff(infile, hdu, hks, resample_sec, year0=year0, flg_TIME=True)
            if (data is None) or (len(data)==0):
                continue

            # Set the outdir.
            if (table == 'wfrbpx'):
                outdir_new = outdir + '/wfrb/px/'
                timeout = timeout_wfrb
            elif (table == 'wfrbac'):
                outdir_new = outdir + '/wfrb/ac/'
                timeout = timeout_wfrb
            else:
                outdir_new = outdir + '/%s/' % table
                timeout = timeout_others
            # Split by chunk.
            for data_chunk in yield_data_others(data, timeout):
                outfile = save_files_fff_epoch(data_chunk, outdir_new, table)
                if (table == 'psp_pixel'):
                    conv_xls(outfile)
        
        # evt
        elif table in ['ac', 'px_cl', 'px_uf','px_cal', 'el']:
            hks = eval('hks_' + table)
            resample_sec=0
            data = collate_data_fff(infile, hdu, hks, resample_sec, year0=year0, flg_TIME=True)
            if (data is None) or (len(data)==0):
                continue
            
            # screenining
            if (table == 'px_cl') or (table == 'px_uf') or (table == 'px_cal'):
                data = data[data.ITYPE<6]
            elif (table == 'ac'):
                data = data[data.AC_ITYPE<2]
        
            # Convert START and STOP to UTC
            if (table == 'el'):
                data['START'] = data['START'].apply(stime_to_datetime)
                data['STOP'] = data['STOP'].apply(stime_to_datetime)

            outdir_new = outdir + '/%s/' % table
            save_files_fff_range(data, outdir_new, table)                            
        
        # general HK
        else:
            hks=None
            if (table == 'ghf') or (table == 'dghf'):
                resample_sec=0
                data = collate_data_fff(infile, hdu, hks, resample_sec, year0=year0, flg_TIME=True)
            elif (table == 'att'):
                resample_sec=10
                data = collate_data_fff(infile, hdu, hks, resample_sec, year0=year0, flg_TIME=True)
                data = add_info_att(data)
            elif (table == 'orb'):
                resample_sec=10
                data = collate_data_fff(infile, hdu, hks, resample_sec, year0=year0, flg_TIME=True)
                data = add_info_orb(data)
            elif (table == 'ehk'):
                resample_sec=10
                data = collate_data_fff(infile, hdu, hks, resample_sec, year0=year0, flg_TIME=True)
                data = add_info_ehk(data)
            else:
                resample_sec=1
                data = collate_data_fff(infile, hdu, hks, resample_sec, year0=year0)
            if (data is None) or (len(data)==0):
                continue
            outdir_new = outdir + '/%s/' % table
            save_files_fff_range(data, outdir_new, table, stat=True)         

    return 0


########################################################
# Main routine
########################################################
if __name__ == '__main__':

    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--input',
        help='Input file.',
        dest='infile',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-o', '--outdir', 
        help='Output directory.',
        dest='outdir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-t', '--tables',
        help='Tables',
        dest='tables',
        type=str,
        nargs="*"
        )
    parser.add_argument(
        '-y', '--year0',
        help='Beginning year of time 0.',
        dest='year0',
        type=int,
        nargs=1,
        default=2019,
        )
    args = parser.parse_args()

    makedb(args.infile[0], args.outdir[0], args.tables, args.year0[0])