#!/usr/bin/env python3

############################################################################
# [Function]
# Make SXS HK database for TL plan.
############################################################################


########################################################
# Imports
########################################################
import argparse
import re, os
import datetime
import codecs
import pandas as pd
from myutils import beginning_of_world, end_of_world
from resolve_utils import save_files

########################################################
# User-defined parameters
########################################################
hk_cmd=['TYPE','S_TIME','COMP','CMD','PAR']

########################################################
# Functions
########################################################
def read_tl(infile):

    # Open TL file.
    print("Openning {}.".format(infile))        
    with open(infile, encoding='EUC-JP') as f:
        lines = f.readlines()
        data = []
        
        for line in lines:
            
            line = line.rstrip('\n') # Remove return
            line = line.split('#')[0] # Remove all after the first #.
            
            if ( re.match('^[0-9]{4} *TL', line)) :
                
                line_split=line.split()
                
                assert line_split[1] == "TL"
                assert line_split[5] == "ALL"
                
                date=line_split[3]
                time=line_split[4]
                dt = datetime.datetime.strptime(date+' '+time, '%Y-%m-%d %H:%M:%S')

                comp_cmd=line_split[6]
                comp = comp_cmd.split('.')[0]
                cmd = '.'.join(comp_cmd.split('.')[1:])
                if (len(line_split)>6):
                    par = ' '.join(line_split[7:])
                else:
                    par = ""
                
                data.append(["TL", dt, comp, cmd, par])
                #print(dt, comp, cmd, par)
    return data

#--------------------------------------------------------------------------------
def read_mc(infile):

    # Open file.
    print("Openning {}.".format(infile))        
    with open(infile, encoding='EUC-JP') as f:
        lines = f.readlines()
        data = []
        
        for line in lines:
            line = line.rstrip('\n') # Remove return
            line = line.strip() # Remove white space
            line = line.split('#')[0] # Remove all after the first #.

            if ( re.match('^MC 0x[0-9]{3} ', line)) :
                line_split=line.split()
                assert line_split[0] == "MC"
                mc=int(line_split[1],16)
                assert line_split[2] == "//"
                mc_desc=line_split[3]
                time=0
            
            if ( re.match('^[0-9]{4} ', line)) :
                line_split=line.split()
                comp_cmd=line_split[1]
                comp = comp_cmd.split('.')[0]
                cmd = '.'.join(comp_cmd.split('.')[1:])
                time += 1./16

                # Split par
                if (len(line_split)>2):
                    par = ' '.join(line_split[2:])
                else:
                    par = ""

                # Split wait
                if (re.match('.*:WAIT.*', par)):
                    wait = int(par.split(":WAIT")[1])
                    assert wait > 0
                    time += wait
                    par = par.split(":WAIT")[0]
                else:
                    pass
                data.append([mc, mc_desc, time, comp, cmd, par])
    data_df = pd.DataFrame(data)
    data_df.columns=["MC", "Decription", "time", "COMP", "CMD", "PAR"]
    return data_df

#--------------------------------------------------------------------------------
def main(infiles, outdir):

    for infile in infiles:
        mcsfile=infile.replace("tl-","mc-").replace("TL","MCS")
        mclfile=infile.replace("tl-","mc-").replace("TL","MCL")

        # Open TL file.
        print("Openning {}.".format(infile))
        data_tl = read_tl(infile)

        # Open MC file.
        if os.path.isfile(mcsfile):
            print("Openning {}.".format(mcsfile))
            data_mcs = read_mc(mcsfile)
        if os.path.isfile(mcsfile):
            print("Openning {}.".format(mclfile))
            data_mcl = read_mc(mclfile)
            data_mc = pd.concat([data_mcs, data_mcl])

        # Save data.
        if (len(data_tl)>0):
            data_tl=pd.DataFrame(data_tl, columns=hk_cmd)

            # Index S_TIME
            data_tl=data_tl[data_tl['S_TIME']>beginning_of_world].copy()    
            data_tl.set_index('S_TIME',inplace=True)
            data_tl.sort_index(inplace=True)
            data_tl = data_tl[~data_tl.index.duplicated(keep='last')]

            # Save data (TL)
            outfile='tl_{}_{}.pkl'.format(data_tl.index[0].strftime('%Y%m%d-%H%M%S'), data_tl.index[-1].strftime('%Y%m%d-%H%M%S'))
            save_files(data_tl, outdir, outfile, kind='pkl')
            
            # Save data (MC)
            outfile='mc_{}_{}.pkl'.format(data_tl.index[0].strftime('%Y%m%d-%H%M%S'), data_tl.index[-1].strftime('%Y%m%d-%H%M%S'))
            save_files(data_mc, "{}/../mc/".format(outdir), outfile, kind='pkl')

    return 0

########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--infiles',
        help='Input files.',
        dest='infiles',
        type=str,
        nargs='+'
        )
    parser.add_argument(
        '-o', '--outdir',
        help='Output dir.',
        dest='outdir',
        type=str,
        default='./',
        nargs='?'
        )
    args = parser.parse_args()
    
    main(args.infiles, args.outdir)