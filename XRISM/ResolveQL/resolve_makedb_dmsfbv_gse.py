#!/usr/bin/env python3

############################################################################
# [Function]
# Make SXS HK database for DMS filter bypass valve GSE.
############################################################################

########################################################
# Imports
########################################################
import argparse
import datetime
import numpy as np
import pandas as pd
from resolve_makedb_fff import save_files_fff_range

########################################################
# User-defined parameters
########################################################


########################################################
# Functions
########################################################
#------------------------------------------------------------
def index_time(data):
    'Change TIME to datetime64 and use it as DataFrame index.'

    time0 = np.datetime64('1900-01-01T00:00:00')
    
    # Correct for JST to UTC for TC1 data.        
    def TIME2UTC(dt):            
        return time0 + np.timedelta64(int((dt-2)*24*60*60), 's')

    data['S_TIME'] = data['S_TIME'].apply(lambda x:TIME2UTC(x))
        
    # Index S_TIME
    data.set_index('S_TIME',inplace=True)
    data.sort_index(inplace=True)
    data = data[~data.index.duplicated(keep='last')]    
    
    return data

#------------------------------------------------------------
def makedb(infile, outdir):

    print("Reading %s" % infile)
    data_all = pd.read_csv(infile, skiprows=0)
    
    # Index time.
    data_all['S_TIME'] = data_all['TheTime'] 
    data_all = index_time(data_all)
    data_all['dP1'] = data_all['AIN2'] /376e-3
    data_all['dP2'] = data_all['AIN3'] /376e-3

    # Save files.
    outdir_new = outdir + '/dmsfbv_gse/'
    save_files_fff_range(data_all, outdir_new, 'dmsfbv_gse', pkl=True, csv=False, stat=True)
    
    return 0

########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--input',
        help='Input file.',
        dest='infile',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-o', '--outdir', 
        help='Output directory.',
        dest='outdir',
        type=str,
        nargs=1
        )
    args = parser.parse_args()

    makedb(args.infile[0], args.outdir[0])
