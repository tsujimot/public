#!/usr/bin/env python3

############################################################################
# [Function]
# Make SXS HK database for GOES XRS.
#
############################################################################


########################################################
# Imports
########################################################
import argparse
import os
import datetime
import numpy as np
import pandas as pd

from resolve_fetch_goes import sources, sources_short, durations, datasets, datasets_short
from resolve_makedb_fff import save_files_fff_range

########################################################
# User-defined parameters
########################################################
datasets_short=[
    "x", "xFlare", "xBkg", "mag", 
    "pDiff", "pInt", "pIntFlu",
    "eDiff", "eInt", "eIntFlu"
]

########################################################
# Functions
########################################################
#------------------------------------------------------------
def format_data_mag(data, source):

    sat=int(data['satellite'].median())
    
    "Format magnetometer data."
    del data['time_tag']
    del data['Unnamed: 0']
    del data['satellite']
    
    col_new = ['{}_{}'.format(i, source).replace(' ','') for i in data.columns]
    data.columns=col_new

    return data

#------------------------------------------------------------
def format_data_x(data, source, channel='energy', flux='flux'):

    sat=int(data['satellite'].median())

    data_all = None
    new_cols = []
    for chan in data[channel].unique():
        datum = data[data[channel]==chan][flux]

        if (len(datum)>0):
            new_cols.append('{}_{}'.format(chan, source).replace(' ',''))
            if (data_all is None):
                data_all = datum
            else:
                data_all = pd.concat([data_all, datum], axis=1)

    data_all.columns = new_cols

    return data_all

#------------------------------------------------------------
def format_data_xFlare(data, source):

    sat=int(data['satellite'].median())
    
    "Format flare data."
    for time in ['begin_time', 'max_time', 'end_time']:
        data = data.dropna(subset=time)
        data[time] = data[time].apply(lambda x:datetime.datetime.strptime(x, "%Y-%m-%dT%H:%M:%SZ"))

    del data['time_tag']
    del data['Unnamed: 0']
    del data['satellite']
    
    col_new = ['{}_{}'.format(i, source).replace(' ','') for i in data.columns]
    data.columns=col_new

    return data

#------------------------------------------------------------
def makedb(infile, outdir, source, dataset):

    print("Reading %s" % infile)
    data = pd.read_csv(infile, skiprows=0, encoding="ISO-8859-1")

    # Index S_TIME
    data = data.dropna(subset=['time_tag'])
    data['S_TIME'] = data['time_tag'].apply(lambda x:datetime.datetime.strptime(x, "%Y-%m-%dT%H:%M:%SZ"))
    data.set_index('S_TIME',inplace=True, drop=True)
    data.sort_index(inplace=True)

    if ( dataset == 'x') or ( dataset == 'eDiff') or ( dataset == 'pDiff'):
        data = format_data_x(data, source)
    elif ( dataset == 'mag'):
        data = format_data_mag(data, source)
    elif (dataset == 'xFlare'):
        data = format_data_xFlare(data, source)

    # Save files.
    outdir_new = '{}_{}_{}/'.format(outdir, source[0], dataset)
    #outdir_new = '{}'.format(outdir)
    save_files_fff_range(data, outdir_new, 'goes_{}_{}'.format(source[0], dataset), pkl=True, csv=False, stat=True)
    
    return 0
    
########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--input',
        help='Input file.',
        dest='infile',
        type=str,
        nargs=1
        ),
    parser.add_argument(
        '-o', '--outdir',
        help='Output dir name.',
        dest='outdir',
        type=str,
        nargs=1
        ),
    args = parser.parse_args()

    basename=os.path.basename(args.infile[0])
    src = basename.split('_')[0]
    dataset = basename.split('_')[1]
    
    if (src in sources_short) and (dataset in datasets_short):
        makedb(args.infile[0], args.outdir[0], src, dataset)
    else:
        print("{} or {} not supported.".format(src, dataset))