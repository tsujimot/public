#!/usr/bin/env python3

############################################################################
# [Function]
# Utility functions.
#
# [History]
# 2015/10/02    M. Tsujimoto    Branched from make_sxs_hk_plot.
# 2015/12/16    H. Seta         TNSC test added,
# 2019/12/29    M. Tsujimoto    Changed for Resolve
#
############################################################################
import os   
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '/home/tsujimot/bin/'))

from os.path import exists
from sys import exit
import datetime   
import gzip
import itertools   
#
import pandas as pd 
import numpy as np 
import math
import pickle   
from pandas.io.pytables import read_hdf 
from scipy import signal 
from astropy.time import Time
import seaborn as sns
from myutils import palette


########################################################
# User-defined parameters
########################################################
# Constants.
abs_zero=273.15
lambda_point_he4=2.1768

mtq_B2ratio=8.9999996306552305745721
#
scale_x=6250.0
scale_y_base=5.865e-11
scale_y_base_igor=scale_y_base*np.sqrt(8/3) # Correction for Hanning window.
#
A_nslim=23.0
B_nslim=5.0
#
sampling_rate = 12500 # Hz
noise_timeout=660
ac_pedestal=[-6616, -6616, -6615, -6615]
ac_thres=[25, 25, 25, 25]

#
cards=['A0', 'A1', 'B0', 'B1']
cards_alt=['A1', 'A0', 'B1', 'B0']
grades=['HP', 'MP', 'MS', 'LP', 'LS']
px_grades = grades
ac_grades = ['AC', 'BL', '', 'EP']

# List of source files for generating plots.
dbs_sxs_raw=('px', 'ac', 'el')
dbs_sxs_evt=('evt_px', 'evt_ac', 'evt_el')
dbs_sxs_gse=('gse_md34', 'gse_md34a', 'gse_md34b', 'gse_cd', 'gse_ld', 'gse_gp', 'gse_dmsfbv')
dbs_sxs_eshk=('dist_pcu', 'fwe','coolers', 'temp', 'xboxa', 'xboxb', 'psp_ess')
dbs_sxs_psp1=('psp_stat1', 'psp_stat2', 'psp_stat3', 'psp_pixel', 'avgp', 'tmpl')
dbs_sxs_psp2=('psp_sys_a0', 'psp_sys_a1', 'psp_sys_b0', 'psp_sys_b1', 
            'psp_usr_a0', 'psp_usr_a1', 'psp_usr_b0', 'psp_usr_b1'
            )
dbs_sxs_psp=dbs_sxs_psp1+dbs_sxs_psp2
dbs_sxs_nm=('ns_1k', 'ns_8k', 'nr', 'nr_ns', 'nm_ns', 'nm_nr', 'nm_nr_ns')
dbs_sxs_wfrb=('wfrb_px', 'wfrb_ac')
dbs_others=(
    'sxi_cd', 'att',
    'acpa_nom', 'acpb_nom', 'acpa_nm_1', 'acpa_nm_2', 'acpb_nm_1', 'acpb_nm_2',
    'acpa_mtq_a', 'acpa_mtq_b', 'acpb_mtq_a', 'acpb_mtq_b',
    'acpa_sg_a', 'acpa_sg_b', 'acpb_sg_a', 'acpb_sg_b', 
    'acpa_pd_2', 'acpb_pd_2',
    'bccu1', 'bccu2',
    )
dbs_hce=(
        'smu_a_hce_a', 'smu_a_hce_b', 'smu_b_hce_a', 'smu_b_hce_b'
        )
dbs_gps=(
        'smu_a_gps', 'smu_b_gps', 'smu_a_gpsr_usr', 'smu_b_gpsr_usr'
        )
dbs_tle=(
        'tle', ''
        )

dbs_all=dbs_sxs_raw+dbs_sxs_evt+dbs_sxs_eshk+dbs_sxs_psp+dbs_sxs_nm+dbs_others+dbs_hce+dbs_gps+dbs_sxs_gse+dbs_sxs_wfrb+dbs_tle

dbs_pixel=('ns_1k', 'ns_8k', 'nr', 'nr_ns', 'nm_ns', 'nm_nr', 'nm_nr_ns', 'psp_pixel', 'wfrb_px', 'wfrb_ac', 'tle', 'px', 'ac', 'el')



########################################################
# Functions
########################################################
# get freq from sampling rate and record length
def get_freqs(samplingrate, recordlength):

    f_min = 1./recordlength
    f_num = int(samplingrate/2/f_min)
    
    return np.linspace(1, f_num+1, f_num) * f_min            

#------------------------------------------------------------
def stime_to_datetime(s_time):
    'Convert S_TIME to datetime64 array.'
    
    utc0 = np.datetime64('2014-01-01T00:00:00')
    utc1 = utc0 + np.timedelta64(int(s_time), 's') + np.timedelta64(int((s_time-int(s_time))*1e6), 'us')
    
    t0 = Time(utc0, scale='utc')
    t1 = Time(utc1, scale='utc')
    
    leap_sec0=int((t0.tai.value-t0.utc.value)/1e9)
    leap_sec1=int((t1.tai.value-t1.utc.value)/1e9)

    leap_sec=leap_sec1-leap_sec0

    utc1 = utc1 + np.timedelta64(-round(leap_sec),'s')    
    return utc1

#------------------------------------------------------------
def time_to_utc(time):
    'Get UTC from TIME corrected for the leap seconds.'

    utc0 = datetime.datetime(2014,1,1)
    utc1 = utc0 + datetime.timedelta(seconds=time)

    t0 = Time(utc0, scale='utc')
    t1 = Time(utc1, scale='utc')
    
    leap_sec0=(t0.tai.value-t0.utc.value).total_seconds()
    leap_sec1=(t1.tai.value-t1.utc.value).total_seconds()

    leap_sec=leap_sec1-leap_sec0

    utc1 = utc1 + datetime.timedelta(seconds=-round(leap_sec))

    return utc1

#------------------------------------------------------------
def utc_to_time(utc):
    'Get ahtime from TIME corrected for the leap seconds.'

    utc0 = np.datetime64('2014-01-01T00:00:00')
    utc1 = utc

    time = (utc - utc0).total_seconds()

    t0 = Time(utc0, scale='utc')
    t1 = Time(utc1, scale='utc')
    
    leap_sec0=(t0.tai.value-t0.utc.value).total_seconds()
    leap_sec1=(t1.tai.value-t1.utc.value).total_seconds()

    leap_sec=leap_sec1-leap_sec0

    time = time + round(leap_sec)

    return time

#------------------------------------------------------------
def index_stime(data):
    'Change S_TIME or TIME to datetime64 and use it as DataFrame index.'
    
    if (len(data)>0):
        if ('S_TIME' in data.columns):
            time='S_TIME'
            data[time] = data[time].apply(stime_to_datetime)
        elif ('TIME' in data.columns):
            time='TIME'
            # Somehow, TIME appears twice.
            data_tmp = data.loc[:,~data.columns.duplicated()]
            data = data_tmp
            # Remove not a time
            data = data[data.TIME>0]
            # Convert to datetime
            data[time] = data[time].apply(stime_to_datetime)
        else:
            print("Neither S_TIME nor TIME included.")
            exit(1)
        data.set_index(time,inplace=True)
        data.sort_index(inplace=True)    
        return data
    else:
        return None

#------------------------------------------------------------
def get_datetime_from_args(args_datetime):        

    if ('/' in args_datetime[0]):
        ydm=args_datetime[0].split('/')
    elif ('-' in args_datetime[0]):
        ydm=args_datetime[0].split('-')
    hms=args_datetime[1].split(':')
    try:
        dt = np.datetime64('%s-%s-%sT%s:%s:%s' % (ydm[0], ydm[1], ydm[2], hms[0], hms[1], hms[2]))
    except:
        print("%s has a wrong format" % args_datetime)
        exit(1)

    return dt

#-------------------------------------------------------
def filter_by_datetime(infiles, kind, datetime_start, datetime_end, base=0):

    infiles_new=[]    
    
    if kind in ['range']:        
        for infile in infiles:    
            infile_name = os.path.split(infile)[1] 
            file_start = infile_name.replace('.','_').split('_')[base+1]
            file_end =   infile_name.replace('.','_').split('_')[base+2]
            file_start = datetime.datetime.strptime(file_start, '%Y%m%d-%H%M%S')
            file_end = datetime.datetime.strptime(file_end, '%Y%m%d-%H%M%S')            
            if (datetime_end < file_start) or (datetime_start > file_end):
                pass
            else:
                print("%s is in the time range of interest." % infile_name)
                infiles_new.append(infile)
    elif kind in ['epoch']:
        for infile in infiles:    
            infile_name = os.path.split(infile)[1] 
            file_start = infile_name.replace('.','_').split('_')[base]
            file_start = datetime.datetime.strptime(file_start, '%Y%m%d-%H%M%S')
            
            if (datetime_end < file_start) or (datetime_start > file_start):
                pass
            else:
                print("%s is in the time range of interest." % infile_name)
                infiles_new.append(infile)
                    
    infiles_new.sort()
    return infiles_new

#------------------------------------------------------------
def check_out_of_time(datetime_data, datetime_start, datetime_end):
    'Check a given time is before the start (-1), between the start and end (0), and after the end (+1)'

    if ( datetime_data is not None):
        if ( datetime_data < datetime_start ):
            return -1
        elif ( datetime_data > datetime_end ):
            return 1
        else:
            return 0
    else:
        return None

#------------------------------------------------------------
def save_files(data, outdir, outfile, kind='npy'):
    'Save output files.'

    # Return if no data.
    if (data is None) or (len(data)==0):
        "Data has no record. No file written."
        return 1

    # Output dir if given separately from outfile.
    if (outdir is not None):
        if (outdir is not None) and not exists(outdir):
            os.makedirs(outdir)
            print("The output directory is generated at %s." % outdir)
        outfile = outdir + outfile
    
    # Save files.
    if (kind == 'npy') or (kind == 'npz'):
        try:
            np.savez_compressed(outfile, data)
            print('Data saved in %s' % outfile)
        except:
            print('Failed to save %s' % outfile)            
    elif (kind == 'df') or (kind == 'pkl'):
        try:
            data.to_pickle(outfile)
            print('Data saved in %s' % outfile)
        except:
            print('Failed to save %s' % outfile)   
    elif (kind == 'stat'):
        pd.set_option('display.max_rows', None)
        pd.set_option('display.max_columns', None)
        pd.set_option('display.width', None)
        pd.set_option('display.max_colwidth', None)
        f = open(outfile, 'w')
        if (len(data.columns) > 0):
            print(data.describe().T, file=f)
            print('Stat saved in %s' % outfile)
        f.close()
    elif (kind == 'csv'):
        try:
            data.to_csv(outfile)
            print('Data saved in %s' % outfile)
        except:
            print('Failed to save %s' % outfile)   
    
    return 0

#------------------------------------------------------------
def get_pHe4(temp):
    "Get vapour pressure of He4 at a given T."
    #http://nuclear.unh.edu/~ryan/Doc/He4Vapor.py

    I = 4.6202
    A = 6.399
    B = 2.541
    C = 0.00612
    D = 0.5197
    alpha = 7.00
    beta  = 14.14

    log_p = I - (A/temp)+B*math.log(temp)+(C/2.0)*math.pow(temp,2) 
    log_p = log_p - D*((alpha*beta)/(math.pow(beta,2)+1) - math.pow(temp,-1))*np.arctan(alpha*temp-beta) 
    log_p = log_p - (alpha*D)/(2*(math.pow(beta,2)+1))*math.log(math.pow(temp,2)/(1+(alpha*temp-beta)*(alpha*temp-beta)))
    return 10**log_p * 133.322

#------------------------------------------------------------
def filter_50mK_he_mode(data_all):
    "Filter 50 mK in the He mode."

    return data_all[np.abs(data_all.T_CTS_CT_A-0.049997)<0.000193]

#------------------------------------------------------------
def filter_50mK_cf_mode(data_all):
    "Filter 50 mK in the cryogen-free mode."

    return data_all[np.abs(data_all.T_CTS_CT_A-0.050576)<0.000386]    

#------------------------------------------------------------
def filter_50mK(data_all):
    "Filter 50 mK in the He or cryogen-free mode."

    return data_all[(np.abs(data_all.T_CTS_CT_A-0.049997)<0.000193) | (np.abs(data_all.T_CTS_CT_A-0.050576)<0.000386)]

#------------------------------------------------------------
def detrend_data(data_all):
    "Detrend the data."

#    data_all=data_all.dropna(how='any', axis=1)
    for _hk in data_all.columns:
        try:
            data_all[_hk]=signal.detrend(data_all[_hk])
        except:
            data_all[_hk]=data_all[_hk]

    return data_all

#------------------------------------------------------------
def deriv_data(data_all):
    "Calc time-derivative of data"

#    data_all=data_all.dropna(how='any', axis=1)
    for _hk in data_all.columns:
        try:
            data_all[_hk]=np.gradient(data_all[_hk])
        except:
            data_all[_hk]=data_all[_hk]

    return data_all

#------------------------------------------------------------
def get_psp_card(pixel):
    "Get PSP Spacecard name for a given pixel."

    if (pixel >= 0) and (pixel < 9):
        return 'A0'
    elif (pixel >= 9) and (pixel < 18):
        return 'A1'
    elif (pixel >= 18) and (pixel < 27):
        return 'B0'
    elif (pixel >= 27) and (pixel < 36):
        return 'B1'

##------------------------------------------------------------
def get_psp_card_alt(pixel):
    "Get PSP Spacecard name for a given pixel in alternative config."

    if (pixel >= 0) and (pixel < 9):
        return 'A1'
    elif (pixel >= 9) and (pixel < 18):
        return 'A0'
    elif (pixel >= 18) and (pixel < 27):
        return 'B1'
    elif (pixel >= 27) and (pixel < 36):
        return 'B0'

#------------------------------------------------------------
# Change channel number from side to full.
def ch_side2full(psp_id,ipix):

    if ( psp_id == 0):# A0
        if ( ipix < 18 ):
            return ipix
        elif ( ipix == 18): # For anti-co pixel
            return 36
        else:
            return 99
    elif ( psp_id == 1 ):# A1
        if ( ipix < 18 ):
            return ipix
        elif ( ipix == 18): # For anti-co pixel
            return 36
        else:
            return 99
    elif ( psp_id == 2 ):# B0
        if ( ipix < 18 ) :
            return ipix + 18
        elif ( ipix == 18): # For anti-co pixel
            return 37
        else:
            return 99
    elif ( psp_id == 3 ):# B1
        if ( ipix < 18 ):
            return ipix + 18
        elif ( ipix == 18): # For anti-co pixel
            return 37
    else:
        return 99

##------------------------------------------------------------
def get_mtq_metric(mtq_x,mtq_y,mtq_z, mtq_max=900.0):
    "Get MTQ noise metric."

    mtq_min = -1.0 * mtq_max

    # Normalize.
    mtq_x /= mtq_max
    mtq_y /= mtq_max
    mtq_z /= mtq_max

    # Calc metric.
    def abs_and_triangle(mtq):
        if np.abs(mtq) > 0.5:
            return 1.0-np.abs(mtq)
        else:
            return np.abs(mtq)

#    metric = abs_and_triangle(mtq_x)/2 - abs_and_triangle(mtq_y) + abs_and_triangle(mtq_z)
#    metric_min =
#    metric_max =          

    metric = np.abs(mtq_x/2 - mtq_y + mtq_z)
    metric_min = 0
    metric_max = 2.5

    # Rescale to match the max.
    metric_scaled = (mtq_max - mtq_min) / (metric_max - metric_min) * (metric - metric_min) + mtq_min
         
    return metric_scaled

#------------------------------------------------------------
def get_ac_cycle(date):
    "Return commercial AC power cycle."
    
    if date < datetime.datetime(2020,4,30): # Niihama
        return 60.0 
    elif date < datetime.datetime(2022,5,1): # Tsukuba
        return 50.0
    elif date < datetime.datetime(2022,6,1): # Tanegashima
        return 60.0
    else: # In space,
        return 0.0

#------------------------------------------------------------
def get_dbdir(_d,topdir):
    'Get data file directory.'

    if ( _d >= datetime.datetime(2014,1,2)  and _d <= datetime.datetime(2014,1,17) ):
        return topdir + '/1401-SXS_COMPAT/db/'
    elif ( _d >= datetime.datetime(2014,5,19)  and _d <= datetime.datetime(2014,5,23) ):
        return topdir + '/1405-SXS_EIC/db/'
    elif ( _d >= datetime.datetime(2014,7,17)  and _d <= datetime.datetime(2014,8,1) ):
        return topdir + '/1407-PSP_FM_SPARE/db/'
    elif ( _d >= datetime.datetime(2014,9,30)  and _d <= datetime.datetime(2014,11,20) ):
        return topdir + '/1409_11-FM_DWR_TEST/db/'
    elif ( _d >= datetime.datetime(2014,12,4)  and _d <= datetime.datetime(2015,3,21) ):
        return topdir + '/1412_02-FM_DWR_TEST/db/'
    elif ( _d >= datetime.datetime(2015,3,22)  and _d <= datetime.datetime(2015,4,28) ):
        return topdir + '/1501_04-SC_InT/db/'
    elif ( _d >= datetime.datetime(2015,4,29)  and _d <= datetime.datetime(2015,11,30) ):
        return topdir + '/1504_07-FIT_SXS_MD/db/'
    elif ( _d >= datetime.datetime(2015,12,1)  and _d <= datetime.datetime(2016,2,16) ):
        return topdir + '/1512_1602-TNSC/db/'
    elif ( _d >= datetime.datetime(2016,2,17)):
        return topdir + '/ORBIT/db/'
    else:
        return topdir + '/ORBIT/db/'

#------------------------------------------------------------
def read_data(topdir,dbs,datetime_start,datetime_end,resample_sec):
    'Get data from files.'

    date_start = datetime.date(datetime_start.year,datetime_start.month,datetime_start.day)
    date_end = datetime.date(datetime_end.year,datetime_end.month,datetime_end.day)

    ahtime_start=utc_to_ahtime(datetime_start)
    ahtime_end=utc_to_ahtime(datetime_end)
    
    # Stack data for all dbs horizontally.
    data_all = None
    for _db in dbs:        

        # Stack data for all dates vertically.            
        data = None            
        for _d in pd.date_range(date_start,date_end,freq='1D'):

            dbdir = get_dbdir(_d,topdir)

            if (dbdir is None):
                print('%s is not found for %s.' %(_db, _d))
                exit(1)

            if ( _db in dbs_pixel ):
                resample_sec=0

            try:
                file_name = dbdir + '/'+_d.strftime('%Y%m%d')+'_' + _db + '.pkl.gz'
                _f = gzip.open(file_name,'rb')
                datum= pickle.load(_f)
                _f.close()
            except:
                print('%s : file open error.' % file_name)
                try:
                    file_name = dbdir + '/'+_d.strftime('%Y%m%d')+'_' + _db + '.hdf'
                    datum = read_hdf(file_name,'data')
                except:
                    print('%s : file open error.' % file_name)
                    continue
                else:
                    print('%s : file being read.' % file_name)
            else:
                print('%s : file being read.' % file_name)

            if (datum is None):
                print('%s : file has no data.' % file_name)
                continue

            if ( _db not in dbs_pixel):
                datum=datum.sort_index()
                data_add = datum[datetime_start:datetime_end]
            else:
                datum['S_TIME'] = datum.index
                data_add = datum[(datum.S_TIME>=datetime_start) & (datum.S_TIME<=datetime_end)]                
                
            if ( len(data_add) > 0 and len(data_add.columns)>0) :
                if ( resample_sec != 0):                
                    data_add=data_add.resample(str(resample_sec) + 'S', how='mean')
#                    data_add=data_add.resample(str(resample_sec) + 'S', how='mean', fill_method='ffill')
            if (data is None):
                data = data_add
            else:
                #if (len(data_add.columns>0)):
                data = pd.concat([data,data_add],axis=0,join='outer')

        if (data_all is None):
            data_all = data
        else:
            data_all=pd.concat([data_all,data],axis=1,join='inner')

    return data_all


#------------------------------------------------------------
def collate_hk_evt(topdir, durations, hks, events, sample_range):
    "Collate HK and event data."

    # Collate data_hk.
    data_all=None

    # Iterate over the durations.
    for _i in range(len(durations)):
        datetime_start = datetime.datetime.strptime(durations[_i][0], '%Y/%m/%d %H:%M:%S') + datetime.timedelta(seconds=sample_range[0])
        datetime_end = datetime.datetime.strptime(durations[_i][1], '%Y/%m/%d %H:%M:%S') + datetime.timedelta(seconds=sample_range[1])
        print('Collating date between %s and %s.' % (datetime_start, datetime_end))

        # Iterate over the data_hks.
        data_hk = None
        for key, values in hks.iteritems():

            datum_hk=read_data(topdir,[key],datetime_start,datetime_end,1)    
            if ( datum_hk is None or len(datum_hk) == 0 ):
                continue
            
            # Select necessary HK values.
            data_hk_add=datum_hk[values]

            # Add to data_hk horizontally.
            if ( data_hk is None ):
                data_hk = data_hk_add
            else:
                data_hk = pd.concat([data_hk, data_hk_add], axis=1)

        # Get event data.
        datum_evt=read_data(topdir,events.keys(),datetime_start,datetime_end,0)

        # Iterate over all datum_evt.
        hk_avgs_2d=[]
        for index, row in datum_evt.iterrows():
            avg_start = index + datetime.timedelta(seconds=sample_range[0])
            avg_end   = index + datetime.timedelta(seconds=sample_range[1])

            hk_avgs_1d=[index]
            hk_avgs_1d.append(_i)
            for _r in range(len(row)):
                hk_avgs_1d.append(row[_r])

            for hk in data_hk.columns:
                hk_avg = data_hk[avg_start:avg_end][hk].median()
                hk_avgs_1d.append(hk_avg)
            hk_avgs_2d.append(hk_avgs_1d)

        # Make concat data.
        data_columns=['S_TIME', 'GROUP']
        data_columns.extend(datum_evt.columns)
        data_columns.extend(data_hk.columns)
        data_all_add=pd.DataFrame(hk_avgs_2d,columns=data_columns)
        
        if data_all is None:
            data_all = data_all_add
        else:
            data_all = pd.concat([data_all,data_all_add], axis=0)

    data_all.set_index('S_TIME',inplace=True)

    return data_all

#------------------------------------------------------------
def collate_hk(topdir, durations, hks):
    "Collate HK data."

    # Collate data_hk.
    data_all=None

    # Iterate over the durations.
    for _i in range(len(durations)):
        datetime_start = datetime.datetime.strptime(durations[_i][0], '%Y/%m/%d %H:%M:%S')
        datetime_end = datetime.datetime.strptime(durations[_i][1], '%Y/%m/%d %H:%M:%S')
        print('Collating date between %s and %s.' % (datetime_start, datetime_end))

        # Iterate over the data_hks.
        data_hk = None

        for key, values in hks.iteritems():

            datum_hk=read_data(topdir,[key],datetime_start,datetime_end,1)    
            if ( datum_hk is None or len(datum_hk) == 0 ):
                continue
            
            # Select necessary HK values.
            data_hk_add=datum_hk[values]

            # Add to data_hk horizontally.
            if ( data_hk is None ):
                data_hk = data_hk_add
            else:
                data_hk = pd.concat([data_hk, data_hk_add], axis=1)

        if (data_hk is None):
            continue

        data_hk['GROUP']=_i

        # Make concat data.
        if data_all is None:
            data_all = data_hk
        else:
            data_all = pd.concat([data_all,data_hk], axis=0)

    return data_all

#------------------------------------------------------------
def get_color(i):
    "Return color code generated from a given integer."
    
    """
    prim1=17
    prim2=19
    prim3=89
    
    return '#%02X%02X%02X' % (prim1*i%256,prim2*i%256,prim3*i%256)
"""
    return palette[i%9]

#------------------------------------------------------------
def get_linestyle(i):
    "Return line code generated from a given integer."

    if ( i/9%4 == 0):
        return '-'
    elif ( i/9%4 == 1):
        return '--'
    elif ( i/9%4 == 2):
        return '-.'
    elif ( i/9%4 == 3):
        return ':'

#------------------------------------------------------------
def get_marker(i):
    "Return marker code generated from a given integer."

    if ( i/9%4 == 0):
        return 'o'
    elif ( i/9%4 == 1):
        return 'D'
    elif ( i/9%4 == 2):
        return '^'
    elif ( i/9%4 == 3):
        return 'v'

#------------------------------------------------------------
def set_legend(plt,ax,ncol=9,loc=2,size=10):
    "Set legend."

    def flip(items, ncol):
        return itertools.chain(*[items[i::ncol] for i in range(ncol)])

    handles, labels = ax.get_legend_handles_labels()
    legend=ax.legend(flip(handles, ncol), flip(labels, ncol), 
                loc=loc, ncol=ncol, numpoints=1, scatterpoints=1, handlelength=2, handletextpad=1, 
                frameon=1, shadow=0, columnspacing=0, prop={'size' : size})
    legend.get_frame().set_alpha(0.1)
    legend.get_frame().set_facecolor('grey')
    legend.get_frame().set_edgecolor('white')



