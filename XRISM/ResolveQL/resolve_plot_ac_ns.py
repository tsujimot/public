#!/usr/bin/env python3

############################################################
# [Function]
# Plot ac_ns vs freq during a given time.
############################################################

########################################################
# Import
########################################################
import argparse, glob, os
import pandas as pd
import numpy as np

from resolve_plot_hk import str_now
from resolve_utils import get_datetime_from_args, filter_by_datetime, get_freqs, save_files
from resolve_plot_ns import make_plot_ns


########################################################
# Functions
########################################################
#-------------------------------------------------------
def collate_psd(indir, ns_len, datetime_start, datetime_end):
    
    # All infiles.
    infiles = glob.glob('{}/ac*/*.npz'.format(indir))

    # Filter infiles by time        
    infiles = filter_by_datetime(infiles, 'epoch', datetime_start, datetime_end, 3)

    # Fill in pixel data that do not exist.
    psd2d = np.zeros(int(ns_len/2))   
    psd2d_all = psd2d
    for i in range(35):
        psd2d_all = np.vstack([psd2d_all,psd2d])

    # Add PSD     
    for pixel in range(0,4): 
        infiles_pixel = [x for x in infiles if "ac%01d" % pixel in x]
        n_pixel = 0
        psd2d = np.zeros(int(ns_len/2))
        for infile in infiles_pixel:
            psd2d += np.load(infile)['arr_0']
            n_pixel += 1
        if (n_pixel > 0):
            psd2d /= n_pixel
            
        # Stack data
        if psd2d_all is None:
            psd2d_all = psd2d
            aho   
        else:                
            psd2d_all = np.vstack([psd2d_all,psd2d])
            
    return psd2d_all.T

    
########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--indir',
        help='Input dir.',
        dest='indir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-o', '--outdir',
        help='Output dir name.',
        dest='outdir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-s', '--start',
        help='Start datetime (YYYY/MM/DD hh:mm:ss).',
        dest='start',
        type=str,
        nargs=2
        )
    parser.add_argument(
        '-e', '--end',
        help='End datetime (YYYY/MM/DD hh:mm:ss).',
        dest='end',
        type=str,
        nargs=2
        )
    parser.add_argument(
        '-l', '--length', 
        help='nspec length (in the unit of k=1024).',
        dest='len_ns',
        type=int,
        nargs=1,
        default=[4]
        )
    args = parser.parse_args()

    datetime_start = get_datetime_from_args(args.start)
    datetime_end = get_datetime_from_args(args.end)

    ns_len=(args.len_ns[0])*1024
    samplingrate=12500
    Xs=get_freqs(samplingrate, ns_len / samplingrate)
    
    str_start = pd.to_datetime(datetime_start).strftime('%Y%m%d_%H%M%S')
    str_end = pd.to_datetime(datetime_end).strftime('%Y%m%d_%H%M%S')
    outstem=args.outdir[0] + '/ac_ns_%s-%s' % (str_start, str_end)
    
    str_start = pd.to_datetime(datetime_start).strftime('%Y/%m/%d %H:%M:%S')
    str_end = pd.to_datetime(datetime_end).strftime('%Y/%m/%d %H:%M:%S')
    title='%1dk noise spec of AC (%s-%s) updated at %s' % (ns_len/1024, str_start, str_end, str_now)

    psd2d_all = collate_psd(args.indir[0], ns_len, datetime_start, datetime_end)
    if (len(psd2d_all[psd2d_all!=0])>0):
        data_ns = make_plot_ns(Xs, psd2d_all, title, outstem, ns_len)
        save_files(data_ns, args.outdir[0], "%s.npz" % os.path.basename(outstem))
        