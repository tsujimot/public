#!/usr/bin/env python3

############################################################################
# [Function]
# Make SXS HK database for SITE env monitor.
############################################################################

########################################################
# Imports
########################################################
import argparse
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from resolve_makedb_fff import save_files_fff_range


########################################################
# User-defined parameters
########################################################
pc1_columns = ['Date', 'Time', '0.3um', '0.5um', '1.0um', '2.0um', '5.0um', '10.0um']
pc2_columns = pc1_columns
pc3_columns = ['Timestamp', '0.5um', '1.0um', '5.0um', '10.0um', 'Sample Time', 'Sample Volume', 'CALR']

########################################################
# Functions
########################################################
#------------------------------------------------------------
def index_time1(data):
    'Change TIME to datetime64 and use it as DataFrame index.'

    # S_TIME
    def dt2stime(x):
        x=x.replace('/','-')
        dt = datetime.strptime(x, "%Y-%m-%d %H:%M:%S")          
        return dt
    
    #data.dropna(inplace=True)
    data['S_TIME'] = data[['Date','Time']].apply(lambda x: "{} {}".format(x[0],x[1]), axis=1)
    data['S_TIME'] = data['S_TIME'].apply(lambda x:dt2stime(x))
    del data['Date']
    del data['Time']

    # Index S_TIME
    data.set_index('S_TIME',inplace=True)
    data.sort_index(inplace=True)
    data = data[~data.index.duplicated(keep='last')]    
    
    return data
#------------------------------------------------------------
def index_time3(data):
    'Change TIME to datetime64 and use it as DataFrame index.'

    data['S_TIME'] = data['Timestamp'].apply(lambda x: datetime.strptime(x, "%Y/%m/%d %H:%M:%S"))
    del data['Timestamp']

    # Index S_TIME
    data.set_index('S_TIME',inplace=True)
    data.sort_index(inplace=True)
    data = data[~data.index.duplicated(keep='last')]    
    
    return data

#------------------------------------------------------------
def makedb1(infile, outdir, columns):

    print("Reading %s" % infile)
    data_all = pd.read_csv(infile, skiprows=7, sep='\t', usecols=[0,1,7,8,9,10,11,12])
    data_all.columns=columns

    # Convert unit volume.
    ft3_m3 = 0.028316846592 
    for i in columns[2:8]:
        data_all[i] = data_all[i] / ft3_m3

    # Index time.
    data_all = index_time1(data_all)

    # Save files.
    outdir_new = outdir + '/pc1/'
    save_files_fff_range(data_all, outdir_new, 'pc1', pkl=True, csv=False, stat=True)
    
    return 0

#------------------------------------------------------------
def makedb2(infile, outdir, columns):

    print("Reading %s" % infile)
    data_all = pd.read_csv(infile, skiprows=7, sep='\t', usecols=[0,1,7,8,9,10,11,12])
    data_all.columns=columns

    # Convert unit volume.
    ft3_m3 = 0.028316846592 
    for i in columns[2:8]:
        data_all[i] = data_all[i] / ft3_m3

    # Index time.
    data_all = index_time1(data_all)

    # Save files.
    outdir_new = outdir + '/pc2/'
    save_files_fff_range(data_all, outdir_new, 'pc2', pkl=True, csv=False, stat=True)
    
    return 0
#------------------------------------------------------------
def makedb3(infile, outdir, columns):

    print("Reading %s" % infile)
    data_all = pd.read_excel(infile, sheet_name=0, skiprows=6, skipfooter=4)
    data_all.columns=columns

    # Convert unit volume.
    ft3_m3 = 0.028316846592 
    for i in columns[1:5]:
        data_all[i] = data_all[i] / ft3_m3

    # Index time.
    data_all = index_time3(data_all)

    # Save files.
    outdir_new = outdir + '/pc3/'
    save_files_fff_range(data_all, outdir_new, 'pc3', pkl=True, csv=False, stat=True)
    
    return 0

########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--input',
        help='Input file.',
        dest='infile',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-o', '--outdir', 
        help='Output directory.',
        dest='outdir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-f', '--format', 
        help='Format of the data.',
        dest='format',
        type=int,
        nargs=1
        )
    args = parser.parse_args()

    makedb = eval('makedb{:d}'.format(args.format[0]))
    columns = eval('pc{:d}_columns'.format(args.format[0]))
    makedb(args.infile[0], args.outdir[0], columns)
