#!/usr/bin/env python3

############################################################################
# [Function]
# Yield plot start, end, and resolution in a given range of days or the recent days.
############################################################################


########################################################
# Imports
########################################################
import argparse
import datetime
import pandas as pd
from dateutil.relativedelta import relativedelta
from pytz import timezone
import glob

#
from resolve_utils import get_datetime_from_args, get_datetime_from_args, filter_by_datetime
from resolve_plot_hk import collate_data, collate_data_db
from resolve_makedb_odb import hk_odb


########################################################
# User-defined parameters
########################################################
tz_local=timezone('Asia/Tokyo')
tz_utc=timezone('UTC')
now = datetime.datetime.now() #.astimezone(tz_local)
today = now.replace(hour=0, minute=0, second=0, microsecond=0)
resolve_day_one = datetime.datetime(2019, 8, 31, 0, 0, 0) #.astimezone(tz_utc)
#
db='odb'
hks=hk_odb

########################################################
# Functions
########################################################
#------------------------------------------------------------
def main(dt_start, dt_end, ranges, odbdir, flg_include_today=False):

    # Limit the date range.
    if (dt_start < resolve_day_one):
        dt_start = resolve_day_one
    
    # Hourly
    if ('h' in ranges):
        dt = dt_start.replace(hour=0, minute=0, second=0, microsecond=0)
        while (dt < dt_end) and (dt < now):
            plot_start = dt
            plot_end = (dt + relativedelta(hours=1))
            plot_start_str = plot_start.strftime("%Y/%m/%d %H:%M:%S")
            plot_end_str = plot_end.strftime("%Y/%m/%d %H:%M:%S")
            label =  plot_start.strftime("h%Y%m%d%H%M%S")
            plot_res = 0
            plot_dir='/{}/'.format(plot_start.strftime("%Y/%m/%d/"))
            print(label, plot_start_str, plot_end_str, plot_res, plot_dir)
            dt = dt + datetime.timedelta(hours=1)
    
    # Daily
    if ('d' in ranges):
        dt = dt_start.replace(hour=0, minute=0, second=0, microsecond=0)
        while (dt < dt_end) and (dt < now):
            plot_start = dt
            plot_end = (dt + relativedelta(days=1))
            plot_start_str = plot_start.strftime("%Y/%m/%d %H:%M:%S")
            plot_end_str = plot_end.strftime("%Y/%m/%d %H:%M:%S")
            label = plot_start.strftime("d%Y%m%d%H%M%S")
            plot_res = 1
            plot_dir='/{}/'.format(plot_start.strftime("%Y/%m/%d/"))
            print(label, plot_start_str, plot_end_str, plot_res, plot_dir)
            dt = dt + datetime.timedelta(days=1)

    # Weekly
    if ('w' in ranges):
        def print_w(dt):
            plot_start = dt - relativedelta(days=7)
            plot_end = dt
            plot_start_str = plot_start.strftime("%Y/%m/%d %H:%M:%S")
            plot_end_str = plot_end.strftime("%Y/%m/%d %H:%M:%S")
            label = plot_start.strftime("w%Y%m%d%H%M%S") 
            plot_res = 10
            plot_dir='/{}/'.format(plot_start.strftime("%Y/%m/"))
            print(label, plot_start_str, plot_end_str, plot_res, plot_dir)

        dt = dt_start.replace(hour=0, minute=0, second=0, microsecond=0)
        while (dt < dt_end) and (dt < now):
            if (dt.weekday() == 0) : # Monday
                print_w(dt)
            dt = dt + datetime.timedelta(days=1)
        if (flg_include_today is True):
            dt = now.replace(hour=0, minute=0, second=0, microsecond=0) + relativedelta(days=7-now.weekday())
            print_w(dt)

    # Monthly
    if ('m' in ranges):
        def print_m(dt):
            plot_start = (dt - relativedelta(months=1))
            plot_end = dt
            plot_start_str = plot_start.strftime("%Y/%m/%d %H:%M:%S")
            plot_end_str = plot_end.strftime("%Y/%m/%d %H:%M:%S")
            label = plot_start.strftime("m%Y%m%d%H%M%S") 
            plot_res = 30
            plot_dir='/{}/'.format(plot_start.strftime("%Y/%m/"))
            print(label, plot_start_str, plot_end_str, plot_res, plot_dir)

        dt = dt_start.replace(hour=0, minute=0, second=0, microsecond=0)
        while (dt < dt_end) and (dt < now):
            if (dt.day == 1):
                print_m(dt)
            dt = dt + datetime.timedelta(days=1)
        if (flg_include_today is True):
            dt = now.replace(day=1, hour=0, minute=0, second=0, microsecond=0) + relativedelta(months=1)
            print_m(dt)

    # Yearly
    if ('y' in ranges):
        def print_y(dt):
            plot_start = dt - relativedelta(years=1)
            plot_end = dt
            plot_start_str = plot_start.strftime("%Y/%m/%d %H:%M:%S")
            plot_end_str = plot_end.strftime("%Y/%m/%d %H:%M:%S")
            label = plot_start.strftime("y%Y%m%d%H%M%S")
            plot_res = 600
            plot_dir='/{}/'.format(plot_start.strftime("%Y/"))
            print(label, plot_start_str, plot_end_str, plot_res, plot_dir)
            
        dt = dt_start.replace(hour=0, minute=0, second=0, microsecond=0)
        while (dt < dt_end) and (dt < now):      
            if (dt.day == 1) and  (dt.month == 1):
                print_y(dt)
            dt = dt + datetime.timedelta(days=1)
        if (flg_include_today is True):
            dt = now.replace(month=1, day=1, hour=0, minute=0, second=0, microsecond=0) + relativedelta(years=1)
            print_y(dt)

    # Lifetime
    if ('l' in ranges):
        plot_start =resolve_day_one
        plot_end = now
        plot_start_str = plot_start.strftime("%Y/%m/%d %H:%M:%S")
        plot_end_str = plot_end.strftime("%Y/%m/%d %H:%M:%S")
        label = plot_start.strftime("l%Y%m%d%H%M%S")
        plot_res = 3600
        plot_dir = '/'
        print(label, plot_start_str, plot_end_str, plot_res, plot_dir)

    # Seqnum
    if ('s' in ranges):

        # All infiles.
        infiles = glob.glob('{}/*.pkl'.format(odbdir))
        
        # Filter infiles by time        
        infiles = filter_by_datetime(infiles, 'range', datetime_start, datetime_end, base=db.count("_"))
        
        # Continue if no data.        
        if (len(infiles)==0):
            return 0

        # Collate data
        data = collate_data_db(hks, infiles, datetime_start, datetime_end) 
        data_RoI = data[dt_start:dt_end]
        for item, row in data_RoI.iterrows():
            plot_start = row.START
            plot_end = row.STOP
            plot_start_str = plot_start.strftime("%Y/%m/%d %H:%M:%S")
            plot_end_str = plot_end.strftime("%Y/%m/%d %H:%M:%S")
            label = "s{}".format(row.SEQNUM)
            plot_res = 1
            plot_dir='/{}/'.format(plot_start.strftime("%Y/%m/"))
            print(label, plot_start_str, plot_end_str, plot_res, plot_dir)           

    return 0

########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-s', '--start',
        help='Start datetime (YYYY/MM/DD hh:mm:ss).',
        dest='start',
        type=str,
        default=[None,None],
        nargs=2
        )
    parser.add_argument(
        '-e', '--end',
        help='End datetime (YYYY/MM/DD hh:mm:ss).',
        dest='end',
        type=str,
        default=[None,None],
        nargs=2
        )
    parser.add_argument(
        '-r', '--ranges',
        help='h(our), d(ay), w(eek), m(onth), y(ear), l(ifetime), s(eqnum).',
        dest='ranges',
        type=str,
        default=['d'],
        nargs='+'
        )
    parser.add_argument(
        '-l', '--loop',
        help='Number of days to reprocess in loop mode.',
        dest='loop',
        type=int,
        default=[0],
        nargs=1
        )
    parser.add_argument(
        '-o', '--odb',
        help='ODB db dir',
        dest='odbdir',
        type=str,
        default=[None],
        nargs=1
        )
    parser.add_argument(
        '--include-today',
        help='Include this week, month, and year.',
        dest='flg_include_today',
        action='store_true',
        default=False
        )

    args = parser.parse_args()

    loop = args.loop[0]

    # Loop mode
    if ( loop > 0):
        datetime_start = today - relativedelta(days=loop)
        datetime_end = now
    else:
        datetime_start = get_datetime_from_args(args.start)
        datetime_end = get_datetime_from_args(args.end)
        datetime_start = pd.to_datetime(datetime_start) #.tz_localize(tz_utc)
        datetime_end = pd.to_datetime(datetime_end) #.tz_localize(tz_utc)

    main(datetime_start, datetime_end, args.ranges[0], args.odbdir[0], args.flg_include_today)