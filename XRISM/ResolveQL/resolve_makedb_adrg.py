#!/usr/bin/env python3

############################################################################
# [Function]
# Make SXS HK database for ADRG.
############################################################################

########################################################
# Imports
########################################################
import argparse
import numpy as np
import pandas as pd
from resolve_makedb_fff import save_files_fff_range

########################################################
# User-defined parameters
########################################################

########################################################
# Functions
########################################################
#------------------------------------------------------------
def index_time(data, base):
    'Change TIME to datetime64 and use it as DataFrame index.'
    
    # Correct for JST to UTC for TC1 data.
    def TIME2UTC(dt):  
        time0 = np.datetime64(base)
        return time0 + np.timedelta64(int(dt), 's')

    data['S_TIME'] = data['S_TIME'].apply(lambda x:TIME2UTC(x))

    # Index S_TIME
    data.set_index('S_TIME',inplace=True)
    data.sort_index(inplace=True)
    data = data[~data.index.duplicated(keep='last')]    
    
    return data

#------------------------------------------------------------
def makedb(infile, outdir, base):

    print("Reading %s" % infile)
    data_all = pd.read_table(infile, skiprows=0, encoding="ISO-8859-1")
    col_old=data_all.columns.values
    col_new=['S_TIME']
    [col_new.append(col_old[i]) for i in range(1,len(col_old))]
            
    data_all.columns = col_new
    data_all = data_all.dropna(subset=['S_TIME'])
    
    # Index time.
    data_all = index_time(data_all, base)
    print(data_all.index.values[0])
    # Save files.
    outdir_new = outdir + '/adrg/'
    save_files_fff_range(data_all, outdir_new, 'adrg', pkl=True, csv=False, stat=True)
    
    return 0

########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--input',
        help='Input file.',
        dest='infile',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-o', '--outdir', 
        help='Output directory.',
        dest='outdir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-b', '--base', 
        help='Base time (e.g., "2020-03-22T15:00:00")',
        dest='base',
        type=str,
        nargs=1
        )
    args = parser.parse_args()

    makedb(args.infile[0], args.outdir[0], args.base[0])
