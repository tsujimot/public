#!/usr/bin/env python3

############################################################################
# [Function]
# Make SXS HK database for ODB.
############################################################################


########################################################
# Imports
########################################################
import argparse
import re, os
import datetime
import pandas as pd
from resolve_utils import save_files

########################################################
# User-defined parameters
########################################################
re_odb_start='/ObservationInfo/ObservationLog/ManeuverStartTime'
re_odb_stop='/ObservationInfo/ObservationLog/NextManeuverStartTime'
re_odb_smuunit='/ObservationInfo/ObservationLog/SMUUnit'
re_odb_target='/ObservationInfo/ObservationPlan/TargetName'
re_odb_name_first='/ObservationInfo/ObservationPlan/PrincipalInvestigator/Name/First'
re_odb_name_last='/ObservationInfo/ObservationPlan/PrincipalInvestigator/Name/Last'
re_odb_ra='/ObservationInfo/ObservationPlan/Ra'
re_odb_dec='/ObservationInfo/ObservationPlan/Dec'
hk_odb=['SEQNUM', 'START', 'STOP', 'SMU_UNIT', 'TARGET', 'RA', 'DEC', 'PI_First', 'PI_Last']


########################################################
# Functions
########################################################
def main(f_orig, fs_add):

    data = []

    for f_add in fs_add:

        # Open file.
        seqnum = int(f_add.split('/')[-1].replace('.dat',''))
        print("Openning {} for seqnum {:d}.".format(f_add, seqnum))
        
        with open(f_add, encoding='EUC-JP') as f:
            lines = f.readlines()

            for line in lines:
                line = line.rstrip('\n') # Remove return
                line = line.split('#')[0] # Remove all after the first #.

                if ( re.match(re_odb_start, line)) :
                    start = datetime.datetime.strptime(line.split('=')[1], '%Y%m%d%H%M%S')
                elif ( re.match(re_odb_stop, line) ):
                    stop = datetime.datetime.strptime(line.split('=')[1], '%Y%m%d%H%M%S')
                elif ( re.match(re_odb_smuunit, line) ):
                    smuunit = line.split('=')[1]
                elif ( re.match(re_odb_target, line) ):
                    target = line.split('=')[1]
                elif ( re.match(re_odb_name_first, line) ):
                    name_first = line.split('=')[1]
                elif ( re.match(re_odb_name_last, line) ):
                    name_last = line.split('=')[1]
                elif ( re.match(re_odb_ra, line) ):
                    ra = line.split('=')[1]
                elif ( re.match(re_odb_dec, line) ):
                    dec = line.split('=')[1]
            data.append([seqnum, start, stop, smuunit, target, ra, dec, name_first, name_last])

    # Add data.
    if (len(data)>0):
        data=pd.DataFrame(data, columns=hk_odb)
        
        # Index SEQNUM
        data.set_index('START',inplace=True, drop=False)
        
        # Add to original.
        if (os.path.exists(f_orig) == True):
            data_all = pd.read_pickle(f_orig)
            data_all = pd.concat([data_all, data])
        else:
            print("{} does not exist and newly generated.".format(f_orig))
            data_all = data

        data_all.sort_index(inplace=True)
        data_all = data_all[~data_all.index.duplicated(keep='last')]

        # Save data
        save_files(data_all, None, f_orig, kind='pkl')

    return 0

#----------------------------------------------------------------------------------
def main_new(f_orig, fs_add):

    data = []

    for f_add in fs_add:

        # Open file.        
        with open(f_add, encoding='EUC-JP') as f:
            lines = f.readlines()

            for line in lines:
                line = line.rstrip('\n') # Remove return
                line = line.split('#')[0] # Remove all after the first #.
                if ( re.match("^obsid", line)) :
                    seqnum = "{:09d}".format(int(line.split('=')[1]))
                elif ( re.match("^ManeuverStartTime", line)) :
                    start = datetime.datetime.strptime(line.split('=')[1].replace(" ",""), '%Y%m%d%H%M%S')
                elif ( re.match("^NextManeuverStartTime", line) ):
                    stop = datetime.datetime.strptime(line.split('=')[1].replace(" ",""), '%Y%m%d%H%M%S')
                elif ( re.match("^SMUUnit", line) ):
                    smuunit = line.split('=')[1]
                elif ( re.match("^TargetName", line) ):
                    target = line.split('=')[1]
                elif ( re.match("^FirstName", line) ):
                    name_first = line.split('=')[1]
                elif ( re.match("^LastName", line) ):
                    name_last = line.split('=')[1]
                elif ( re.match("^Ra", line) ):
                    ra = line.split('=')[1]
                elif ( re.match("^Dec", line) ):
                    dec = line.split('=')[1]
            data.append([seqnum, start, stop, smuunit, target, ra, dec, name_first, name_last])

    # Add data.
    if (len(data)>0):
        data=pd.DataFrame(data, columns=hk_odb)
        
        # Index SEQNUM
        data.set_index('START',inplace=True, drop=False)
        
        # Add to original.
        if (os.path.exists(f_orig) == True):
            data_all = pd.read_pickle(f_orig)
            data_all = pd.concat([data_all, data])
        else:
            print("{} does not exist and newly generated.".format(f_orig))
            data_all = data

        data_all.sort_index(inplace=True)
        data_all = data_all[~data_all.index.duplicated(keep='last')]

        # Save data
        save_files(data_all, None, f_orig, kind='pkl')

    return 0

########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-o', '--original',
        help='Oiginal file.',
        dest='f_orig',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-a', '--add',
        help='Add files.',
        dest='fs_add',
        type=str,
        nargs='+'
        )
    args = parser.parse_args()
    
    #main(args.f_orig[0], args.fs_add)
    main_new(args.f_orig[0], args.fs_add)
