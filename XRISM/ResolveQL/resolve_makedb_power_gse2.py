#!/usr/bin/env python3

############################################################################
# [Function]
# Make SXS HK database for SXS-FAN and FWE power gse.
############################################################################

########################################################
# Imports
########################################################
import argparse
import datetime
import numpy as np
import pandas as pd
from resolve_makedb_fff import save_files_fff_range

########################################################
# User-defined parameters
########################################################
power_gse_cols=[
    "S_TIME",
    "V_SXS_FAN","I_SXS_FAN","P_SXS_FAN",
    "V_FWE","I_FWE","P_FWE"]

########################################################
# Functions
########################################################
#------------------------------------------------------------
def index_time(data):
    'Change TIME to datetime64 and use it as DataFrame index.'

    # Correct for JST to UTC for TC1 data.        
    def UTC2DT(time):            
        try:
            stime=datetime.datetime.strptime(time, "%Y/%m/%d %H:%M:%S.%f")        
            return stime 
        except:
            stime=datetime.datetime.strptime(time, "%Y/%m/%d %H:%M:%S")        
            return stime 

    data['S_TIME'] = data['S_TIME'].apply(lambda x:UTC2DT(x))

    # Index S_TIME
    data.set_index('S_TIME',inplace=True)
    data.sort_index(inplace=True)
    data = data[~data.index.duplicated(keep='last')]
    data.index = data.index.round('1s')
    
    return data

#------------------------------------------------------------
def makedb(infile, outdir):

    print("Reading %s" % infile)
    data_all = pd.read_csv(infile, skiprows=1, header=0, encoding="shift-jis")
    
    data_all.columns = power_gse_cols
    data_all = data_all.dropna(subset=['S_TIME'])
    
    # Index time.
    data_all = index_time(data_all)
    
    # Save files.
    outdir_new = outdir + '/power_gse2/'
    save_files_fff_range(data_all, outdir_new, 'power_gse2', pkl=True, csv=False, stat=True)
    
    return 0

########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--input',
        help='Input file.',
        dest='infile',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-o', '--outdir', 
        help='Output directory.',
        dest='outdir',
        type=str,
        nargs=1
        )
    args = parser.parse_args()

    makedb(args.infile[0], args.outdir[0])
