#!/usr/bin/env python3

############################################################
# [Function]
# Plot nr_ns vs time
############################################################

########################################################
# Imports
########################################################
import argparse, datetime, os, re, glob
import pandas as pd
import numpy as np
from sys import exit
from os.path import exists
#
from resolve_utils import scale_y_base, get_freqs, get_datetime_from_args, filter_by_datetime
from resolve_plot_hk import get_events, palette, annotate_events, str_now
from myutils import add_line, add_range, beginning_of_world, end_of_world

# Plotly offline
import plotly.offline as offline
import plotly.graph_objects as go
from plotly.subplots import make_subplots
#
import threading

########################################################
# User-defined parameters
########################################################


########################################################
# Functions
########################################################
#-------------------------------------------------------
def collate_psd(psdfiles, ns_len=1):
    
    starttime_all = []
    psd2d_all = None
    
    for psdfile in psdfiles:        
        
        # Get time from file name.
        filename = os.path.split(psdfile)[1].replace('.npz','')
        stime = re.sub('nr_ns_p[0-9][0-9]_', '', filename)
        stime = pd.to_datetime(datetime.datetime.strptime(stime, "%Y%m%d-%H%M%S"))
        starttime_all.append(stime)
        
        # Load file        
        psd2d = np.load(psdfile)['arr_0']
        
        # Stack data
        if psd2d_all is None:
            psd2d_all = psd2d        
        else:
            psd2d_all = np.vstack([psd2d_all,psd2d])
            
    # Data
    freq_all=get_freqs(12500, 1024 * int(ns_len) / 12500)    
    psd2d_all = psd2d_all * scale_y_base / np.sqrt(ns_len) * 1e9    

    return starttime_all, freq_all, psd2d_all.T

#-------------------------------------------------------
def plotly_2D(Xs, Ys, Zs, outstem, title, datetime_start=beginning_of_world, datetime_end=end_of_world, 
                events=None, lines=None, 
                xtitle='Time (UT)', ytitle='Freq (Hz)', 
                xtype='linear', ytype='log', xint=False, yint=False, 
                zrange=[None, None]):
    
    if (xtype == 'log'):
        xmin, xmax = np.log10(Xs[0]), np.log10(Xs[-1])
    else:
        xtype=None
        if (xint is False):
            xmin, xmax = Xs[0], Xs[-1]
        else:
            xmin, xmax = Xs[0]-np.diff(Xs)[0]/2, Xs[-1]+np.diff(Xs)[0]/2
            
    if (ytype == 'log'):
        ymin, ymax = np.log10(Ys[0]), np.log10(Ys[-1])
    else:
        if (yint is False):
            ymin, ymax = Ys[0], Ys[-1]
        else:
            ymin, ymax = Ys[0]-np.diff(Ys)[0]/2, Ys[-1]+np.diff(Ys)[0]/2
    
    # Colorscale
    zmin = zrange[0]
    zmax = zrange[1]
    if ( zmin is not None) and (zmax is not None) and (zmin*zmax<0):
        colorscale='RdBu'
    else:
        colorscale='deep'

    # Make trace
    #Data = go.Heatmapgl(
    Data = go.Heatmap(
        x=Xs, 
        y=Ys, 
        z=Zs,
        zmin=zmin,
        zmax=zmax,
        colorscale=colorscale,
        zsmooth=False,
        )

    # Define layout
    Layout = go.Layout(
        title=title,
        showlegend=False,
        xaxis=dict(
            title = xtitle,
            type = xtype,
            autorange = False,
            range = [xmin, xmax],
            tickmode = 'auto'
            ),
        yaxis=dict(
            title = ytitle,
            type = ytype,
            autorange = False,
            range = [ymin, ymax],
            tickmode = 'auto'
            ),
        )

    fig = make_subplots(
        rows=1, cols=1,
    )
    fig.update_layout(Layout)
    fig.update_layout(legend= {'itemsizing': 'constant'})
    fig.add_trace(Data)
    #fig = go.Figure(data=Data, layout=Layout)

    # Annotation (common)
    #if (events is not None):
    #    fig = annotate_events(events, fig, datetime_start, datetime_end)
    if (lines is not None):
        for k, v in lines.items():
            i_color=0
            fig = add_line(fig, v, k, vert=False, color=palette[i_color], width=0.2, dash='dot')
    
    # Output
    def write_static():
        fig.write_image("%s.png" % outstem, validate=False, engine='kaleido')
        print("File saved in %s.png" % (outstem))        
    def write_html():
        offline.plot(fig, filename='%s.html' % outstem, auto_open=False, include_mathjax='cdn', validate=False)
        print("File saved in %s.html" % (outstem))
    
    t1 = threading.Thread(target=write_static)
    t2 = threading.Thread(target=write_html)
    #
    t1.start()
    t2.start()
    #
    t1.join()
    t2.join()
    
    return 0

    
########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--indir',
        help='Input dir.',
        dest='indir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-o', '--outdir',
        help='Output dir name.',
        dest='outdir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-s', '--start',
        help='Start datetime (YYYY/MM/DD hh:mm:ss).',
        dest='start',
        type=str,
        nargs=2
        )
    parser.add_argument(
        '-e', '--end',
        help='End datetime (YYYY/MM/DD hh:mm:ss).',
        dest='end',
        type=str,
        nargs=2
        )
    parser.add_argument(
        '-R', '--resample',
        help='Resample seconds.',
        dest='resample',
        type=int,
        default=1,
        nargs=1,
        )
    parser.add_argument(
        '-E', '--evtlists',
        help='Event lists.',
        dest='evtlists',
        type=str,
        nargs='*'
        )
    parser.add_argument(
        '-z', '--zrange',
        help='Range for zscale.',
        default=[None, None],
        dest='zrange',
        type=float,
        nargs=2
        )
    args = parser.parse_args()
    
    datetime_start = get_datetime_from_args(args.start)
    datetime_end = get_datetime_from_args(args.end)
    # outstem
    str_start1 = pd.to_datetime(datetime_start).strftime('%Y%m%d_%H%M%S')
    str_end1 = pd.to_datetime(datetime_end).strftime('%Y%m%d_%H%M%S')
    # title
    str_start2 = pd.to_datetime(datetime_start).strftime('%Y/%m/%d %H:%M:%S')
    str_end2 = pd.to_datetime(datetime_end).strftime('%Y/%m/%d %H:%M:%S')

    # events
    events=get_events(args.evtlists, datetime_start, datetime_end)

    # Loop over channels. 
    for px in range(36):        

        # Get and filter infiles by time
        infiles = glob.glob('%s/p%02d/nr_ns*.npz' % (args.indir[0],px)) 
        infiles = filter_by_datetime(infiles, 'epoch', datetime_start, datetime_end, 3)
        infiles = infiles[::args.resample[0]]
        if ( len(infiles) == 0 ):
            print("No file found.")
            exit(1)

        # Title
        title='log10 Power (nV/rtHz) nr_ns p%02d (%s-%s) updated at %s' % (px, str_start2, str_end2, str_now)

        # Output
        monitor='nr_ns'
        outdir_new = args.outdir[0].replace('{}_2D'.format(monitor),'{}_2D/p{:02d}'.format(monitor, px))
        #outdir_new = args.outdir[0] + '/p%02d' % px
        if not exists(outdir_new):
            os.makedirs(outdir_new)
        outstem = outdir_new + '/nr_ns_p%02d_%s-%s' % (px, str_start1, str_end1)

        # Collate, plot, save
        starttime_all, freq_all, psd2d_all = collate_psd(infiles)                
        if (psd2d_all is not None):
            plotly_2D(starttime_all, freq_all, np.log10(psd2d_all).astype(np.float16), outstem, title, datetime_start, datetime_end, events, zrange=args.zrange)    