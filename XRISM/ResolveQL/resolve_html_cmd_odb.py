#!/usr/bin/env python3

############################################################################
# [Function]
# Generate HTML for cmd and odb.
############################################################################


########################################################
# Imports
########################################################
import argparse
import glob
import datetime
import pandas as pd
from resolve_utils import get_datetime_from_args, filter_by_datetime
from resolve_plot_hk import collate_data_db, str_now
from resolve_makedb_cmd import hk_cmd
from resolve_makedb_odb import hk_odb


########################################################
# User-defined parameters
########################################################
db_cmd='cmd'
dbhk_cmd=hk_cmd[1:]
db_odb='odb'
dbhk_odb=hk_odb

#https://www.colordic.org/
bgcolors={
    'CDA'       : "#f0ffff",
    'CDB'       : "#f5fffa",
    'JTD'       : "#f0fff0",
    'PSPA0'     : "#fffff0",
    'PSPA1'     : "#fffff0",
    'PSPB0'     : "#fffff0",
    'PSPB1'     : "#fffff0",
    'XBOX'      : "#fdf5e6",
    'ADRC'      : "#fff5ee",
    'FWE'       : "#fff0f5",
    'SXS_DIST'  : "#ffe4e1",
    'PCU'       : "#ffdab9",
    'SXI'       : "#f0f8ff",
    'SXI_CD'    : "#e6e6fa",
    'SMU_A'     : "#ffdead",
    'SMU_B'     : "#ffdead",
    'TCIM_S_A'  : "#ffe4c4",
    'TCIM_S_B'  : "#ffe4c4",
    'TCIM_X'    : "#ffffe0",
    'HCE_A'     : "#ffefd5",
    'HCE_B'     : "#ffefd5",
    'M_HCE'     : "#faebd7",
    'MSE'       : "#fafad2",
    'BCCU1'     : "#f5deb3",
    'BCCU2'     : "#f5deb3",
    'ACPA'      : "#b0c4de",
    'ACPB'      : "#b0c4de",
    'DR_A'      : "#e0ffff",
    'DR_B'      : "#e0ffff",
}

########################################################
# Functions
########################################################
#-------------------------------------------------------
def print_header_cmd(datetime_start, datetime_end,f):
    print("""
<H1>{:s}-{:s}</H1>
 <TABLE border="2" rules="all">
  <TR>
  <TH>Type</TH>
  <TH>Date (UT)</TH>
  <TH>Time (UT)</TH>
  <TH>Date (JST)</TH>
  <TH>Time (JST)</TH>
  <TH>Comp</TH>
  <TH>CMD</TH>
  <TH>Parameter</TH>
  </TR>
""".format(datetime_start, datetime_end), file=f)
    return 0

#-------------------------------------------------------
def print_header_odb(datetime_start, datetime_end,f):
    print("""
<H1>{:s}-{:s}</H1>
 <TABLE border="2" rules="all">
  <TR>
  <TH>SeqNum</TH>
  <TH>Start</TH>
  <TH>Stop</TH>
  <TH>Target</TH>
  <TH>RA</TH>
  <TH>DEC</TH>
  <TH>PI_Last</TH>
  <TH>PI_First</TH>
  <TH>SMU side</TH>
  </TR>
""".format(datetime_start, datetime_end), file=f)
    return 0

#-------------------------------------------------------
def print_footer_cmd(f):
    print("""
 </TABLE>
""", file=f)
    return 0

print_footer_odb = print_footer_cmd

#-------------------------------------------------------
def print_body_cmd(data, f):
    for stime, item in data.iterrows():
        jst = stime + datetime.timedelta(hours=9)
        comp = item.COMP
        cmd = item.CMD
        par = item.PAR
        type = item.TYPE

        if comp in bgcolors.keys():
            bgcolor = bgcolors[comp]
        else:
            bgcolor = "#ffffff" 
        if ('XBOX' in cmd):
            bgcolor = bgcolors['XBOX']
        
        if (type == 'Real'):
            if (comp == 'FILE'):
                color = "#FFFFFF"
                bgcolor = "#000000"
            else:
                color = "#000000"
        elif (type == 'TL'):
            color = "#000000"
        elif (type == 'MC'):
            color = "#FFFFFF"
            bgcolor = "#888888"
        


        print("""
<TR STYLE="color: {}" BGCOLOR="{}">
 <TD>{}</TD>
 <TD>{}</TD>
 <TD>{}</TD>
 <TD>{}</TD>
 <TD>{}</TD>
 <TD>{}</TD>
 <TD>{}</TD>
 <TD>{}</TD>
</TR>
""".format(color, bgcolor, type, stime.strftime('%Y/%m/%d'), stime.strftime('%H:%M:%S'), jst.strftime('%Y/%m/%d'), jst.strftime('%H:%M:%S'), comp, cmd, par), file=f)
    return 0

#-------------------------------------------------------
def print_body_odb(data, f):
    for stime, item in data.iterrows():
        print("""
<TR>
 <TD>{}</TD>
 <TD>{}</TD>
 <TD>{}</TD>
 <TD>{}</TD>
 <TD>{}</TD>
 <TD>{}</TD>
 <TD>{}</TD>
 <TD>{}</TD>
 <TD>{}</TD>
</TR>
""".format(item.SEQNUM, item.START.strftime('%Y/%m/%d %H:%M:%S'), item.STOP.strftime('%Y/%m/%d %H:%M:%S'), item.TARGET, item.RA, item.DEC, item.PI_Last, item.PI_First, item.SMU_UNIT), file=f)
    return 0

#-------------------------------------------------------
def main(indir, type, outstem, datetime_start, datetime_end):

    db  = eval('db_'+type)
    hks  = eval('dbhk_'+type)
    print_header = eval('print_header_'+type)
    print_body = eval('print_body_'+type)
    print_footer = eval('print_footer_'+type)

    # All infiles.
    infiles = glob.glob('{}/*.pkl'.format(indir))
    
    # Filter infiles by time        
    infiles = filter_by_datetime(infiles, 'range', datetime_start, datetime_end, base=db.count("_"))
    
    # Continue if no data.        
    if (len(infiles)==0):
        return 0

    # Collate data
    data = collate_data_db(hks, infiles, datetime_start, datetime_end) 

    if (type == "cmd"):

        ## Add TL
        # All infiles.
        infiles_tl = glob.glob('{}/../tl/*.pkl'.format(indir))
        infiles_tl.sort()

        # Filter infiles by time        
        infiles_tl = filter_by_datetime(infiles_tl, 'range', datetime_start, datetime_end, base=db.count("_"))

        # Collate data
        if (len(infiles_tl)>0):            
            data_tl = collate_data_db(hks, infiles_tl, datetime_start, datetime_end, overwrite=True) 
            data = pd.concat([data,data_tl])
            data.sort_index(inplace=True)

        ## Expand MC
        # All infiles.
        infiles_mc = glob.glob('{}/../mc/*.pkl'.format(indir))
        infiles_mc.sort()

        # Filter infiles by time        
        infiles_mc = filter_by_datetime(infiles_mc, 'range', datetime_start, datetime_end, base=db.count("_"))

        # Collate data.
        data_mc = None
        if (len(infiles_mc)>0):
            for infile_mc in infiles_mc :
                datum_mc = pd.read_pickle(infile_mc)
                if (data_mc is None):
                    data_mc = datum_mc
                else:
                    # Remove old MCs
                    mcs=set(datum_mc["MC"].values)
                    for mc in mcs:
                        data_mc = data_mc[data_mc.MC!=mc].copy()
                    if (data_mc is None):
                        data_mc = datum_mc
                    else:
                        data_mc = pd.concat([data_mc, datum_mc])

        # Expand MC
        data_mc_mc = data[data.CMD=="DHFS.MC.MC_EACH_EXE"]
        for start, datum_mc_mc in data_mc_mc.iterrows():
            mc_mc=int(datum_mc_mc.PAR,16)
            mc_match=data_mc[data_mc.MC==mc_mc]
            if (len(mc_match) > 0):
                mc_match["S_TIME"] = mc_match["time"].apply(lambda x: start + datetime.timedelta(seconds=x)) 
                mc_match["TYPE"] = "MC"
                mc_match.set_index("S_TIME", inplace=True)
                data = pd.concat([data, mc_match[data.columns]])
        data.sort_index(inplace=True)

    # Output
    outfile = "{}.html".format(outstem)
    with open(outfile, 'w') as f:

        # print header
        print_header(datetime_start, datetime_end, f)
        
        # print main body
        print_body(data, f)
        
        # print footer
        print_footer(f)
        print("Output to {}.".format(outfile))    
    
    return 0

########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--indir',
        help='Input dir name.',
        dest='indir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-o', '--outdir',
        help='Output dir name.',
        dest='outdir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-s', '--start',
        help='Start datetime (YYYY/MM/DD hh:mm:ss).',
        dest='start',
        type=str,
        nargs=2
        )
    parser.add_argument(
        '-e', '--end',
        help='End datetime (YYYY/MM/DD hh:mm:ss).',
        dest='end',
        type=str,
        nargs=2
        )
    parser.add_argument(
        '-t', '--type',
        help='Data type (cmd or odb).',
        dest='type',
        type=str,
        nargs=1,
        choices=['cmd', 'odb']
        )
    args = parser.parse_args()

    plot=args.type[0]

    datetime_start = get_datetime_from_args(args.start)
    datetime_end = get_datetime_from_args(args.end)
    
    str_start = pd.to_datetime(datetime_start).strftime('%Y%m%d_%H%M%S')
    str_end = pd.to_datetime(datetime_end).strftime('%Y%m%d_%H%M%S')
    outstem=args.outdir[0] + '/%s_%s-%s' % (plot, str_start, str_end)    
    main(args.indir[0], args.type[0], outstem, datetime_start, datetime_end)       
