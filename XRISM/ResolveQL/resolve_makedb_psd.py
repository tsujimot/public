#!/usr/bin/env python3

############################################################################
# [Function]
# Make SXS HK database for PSD from raw data in monitor GSEs.
############################################################################

########################################################
# Imports
########################################################
from abc import abstractmethod
import argparse, gzip, os
import numpy as np

from resolve_utils import save_files
from sxs_fft_mod import mlab_psd_average


########################################################
# User-defined parameters
########################################################
chnum_default=8
reduction_factor = 10

########################################################
# Functions
########################################################
#------------------------------------------------------------
def parse_accrawdata(infile, format='npz', chnum=chnum_default):

    if (format == 'npy'):
        data = np.load(infile)
    elif (format == 'npz'):
        try:
            data = np.load(infile)['arr_0']
        except:
            with gzip.GzipFile(infile, "r") as f:
                data=np.load(f, allow_pickle=True)             
    else:
        print("Format %s is not supported." % format)

    # Get time data
    stime = data[0]
    dt = data[1]    
    shotlength = (len(data)-2)/chnum*dt    
    voltages = data[2:]
    voltages = voltages.T.reshape([int(len(voltages)/chnum),chnum]).T
    return voltages, dt, shotlength, stime

#------------------------------------------------------------
def make_psd(acces, dt, trange):

    psd, freq = mlab_psd_average(acces, dt)
    df = freq[1]-freq[0]
    psddata = np.concatenate([[df],trange,psd.flatten()],axis=0)
    psddata = psddata.reshape([len(psddata),1])

    return psddata

#------------------------------------------------------------
def makedb(infile, outdir, format, chnum, gains):

    # Read data.
    voltages, dt, shotlength, stime = parse_accrawdata(infile, format, chnum)
    samplingrate = 1./dt
    
    psd2d_a = [] # Use all data.
    #psd2d_b = [] # Use resampled data by reduction_factor.
    #psd2d_n = [] # Use the first 1/reduction_factor data.     

    # Calc PSD for each channel
    for ch in range(chnum):
        v_ch_a = voltages[ch] * gains[ch]
        #v_ch_b = v_ch_a[0::int(reduction_factor)] 
        #v_ch_n = v_ch_a[0:int(shotlength*samplingrate/reduction_factor)] 

        psddata_a = make_psd(v_ch_a, dt, [stime, stime+shotlength])
        #psddata_b = make_psd(v_ch_b, dt*reduction_factor, [stime, stime+shotlength])
        #psddata_n = make_psd(v_ch_n, dt, [stime, stime+shotlength/reduction_factor])

        psd2d_a.append(psddata_a.flatten()[3:])
        #psd2d_b.append(psddata_b.flatten()[3:])
        #psd2d_n.append(psddata_n.flatten()[3:])
    
    outfile_new= os.path.split(infile)[1].replace('raw','').replace('.%s' % format,'')        
    
    outdir_new = outdir + '/psd_%.1fs_%.1fkHz/' % (shotlength, samplingrate/1e3)
    save_files(psd2d_a, outdir_new, "psda_%s.npz" % outfile_new)
    #save_files(psd2d_b, outdir_new, "psdb_%s.npz" % outfile_new)
    #save_files(psd2d_n, outdir_new, "psdn_%s.npz" % outfile_new)


########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--input',
        help='Input file.',
        dest='infile',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-o', '--outdir', 
        help='Output directory.',
        dest='outdir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-f', '--format',
        help='Format of the input file.',
        dest='format',
        default='npy',
        type=str,
        nargs='?'
        )
    parser.add_argument(
        '-c', '--chnum',
        help='Number of channels.',
        dest='chnum',
        default=chnum_default,
        type=int,
        nargs='?'
        )
    parser.add_argument(
        '-g', '--gain',
        help='Gains with for the number of channels.',
        dest='gains',
        type=float,
        nargs='+'
        )
    args = parser.parse_args()
    makedb(args.infile[0], args.outdir[0], args.format, args.chnum, args.gains)
