#!/usr/bin/env python3

###########################################################
# [Function]
# Generate an Resolve PSP limit plot.
############################################################

########################################################
# Imports
########################################################
import argparse, glob, collections, datetime, time, os
import numpy as np
import pandas as pd
from resolve_utils import get_datetime_from_args, filter_by_datetime, abs_zero, save_files
from myutils import yield_color, add_line, add_range

# Plotly offline
import plotly.offline as offline
import plotly.graph_objects as go
from plotly.subplots import make_subplots

import threading

########################################################
# User-defined parameters
########################################################

########################################################
# Functions
########################################################            


########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--indir',
        help='Input dir name.',
        dest='indir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-o', '--outfile',
        help='Output file name.',
        dest='outfile',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-s', '--start',
        help='Start datetime (YYYY/MM/DD hh:mm:ss).',
        dest='start',
        type=str,
        nargs=2
        )
    parser.add_argument(
        '-e', '--end',
        help='End datetime (YYYY/MM/DD hh:mm:ss).',
        dest='end',
        type=str,
        nargs=2
        )
    parser.add_argument(
        '-r', '--resample',
        help='Resample seconds.',
        dest='resample',
        type=int,
        default=0,
        nargs='?'
        )
    parser.add_argument(
        '-E', '--evtlists',
        help='Event lists.',
        dest='evtlists',
        type=str,
        nargs='*'
        )
    args = parser.parse_args()

    datetime_start = get_datetime_from_args(args.start)
    datetime_end = get_datetime_from_args(args.end)
    
    # Plot    
    plot = args.plot[0]
    
    # Tweak for rates
    if ('rates_' in plot):
        rates=[plot.split("_")[1]]
        plot_new='rates'
    else:
        plot_new=plot
        rates=None

    # events
    events=get_events(args.evtlists, datetime_start, datetime_end)
    
    if plot_new in n_panels.keys():
        dbhks = eval('dbhks_'+plot_new)
        update_fig = eval('update_fig_'+plot_new)
        plot_title = plot.upper()
        n_panel = n_panels[plot_new]
        plotly_hk(args.indir[0], dbhks, n_panel, plot_title, update_fig, args.outfile[0], datetime_start, datetime_end, args.resample, events, rates=rates, flg_adrc=args.flg_adrc, flg_cde=args.flg_cde, flg_dist=args.flg_dist)        
    else:
        print("%s is not supported." % plot)
        exit(1)