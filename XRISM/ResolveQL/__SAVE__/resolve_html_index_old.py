#!/usr/bin/env python3

############################################################################
# [Function]
# Make HTML index page.
############################################################################

########################################################
# Imports
########################################################
import argparse, os, glob, re
from datetime import datetime as dt

########################################################
# User-defined parameters
########################################################
n_col = 7 

########################################################
# Functions
########################################################
#------------------------------------------------------------
def get_time_str_format(file):

    pattern_YMD = '[0-9]{8}'
    pattern_HMS = '[0-9]{6}'
    time_str = re.sub('[a-z\.]', '', file).replace('__','_').replace('__','_').replace('-','_').split('_')
    time_str_YMD=[]
    time_str_HMS=[]

    for str in time_str:
        if (re.match(pattern_YMD, str)):
            time_str_YMD.append(str)
        elif (re.match(pattern_HMS, str)):
            time_str_HMS.append(str)
    
    # Day
    if (len(time_str_YMD) == 1) and (len(time_str_HMS) == 0):
        time_str_format = dt.strptime(time_str_YMD[0], '%Y%m%d').strftime('%Y/%m/%d')
    # Epoch
    elif (len(time_str_YMD) == 1) and (len(time_str_HMS) == 1):
        time_str_format = dt.strptime(time_str_YMD[0]+time_str_HMS[0], '%Y%m%d%H%M%S').strftime('%Y/%m/%d %H:%M:%S')
    # Range
    elif (len(time_str_YMD) == 2) and (len(time_str_HMS) == 2):
        # Day
        if ((time_str_YMD[0] == time_str_YMD[1]) and (time_str_HMS[0]=="000000") and (time_str_HMS[1]=="235959")):
            time_str_format = dt.strptime(time_str_YMD[0], '%Y%m%d').strftime('%Y/%m/%d')
        # Range
        else:
            time_str_format1 = dt.strptime(time_str_YMD[0]+time_str_HMS[0], '%Y%m%d%H%M%S').strftime('%Y/%m/%d %H:%M:%S')
            time_str_format2 = dt.strptime(time_str_YMD[1]+time_str_HMS[1], '%Y%m%d%H%M%S').strftime('%Y/%m/%d %H:%M:%S')
            time_str_format =  time_str_format1 + '-' #+ time_str_format2
    else:
        time_str_format= None
    return time_str_format 

#------------------------------------------------------------
def print_header():
    print("""\
    <table border="1">\
""")
    return 0

#------------------------------------------------------------
def print_footer():
    print("""\
    </table>\
""")
    return 0

#------------------------------------------------------------
def get_batch(files, n_col):
    
    files_remainder = files
    while (len(files_remainder) > n_col):
        yield files_remainder[0:n_col]
        files_remainder = files_remainder[n_col:]
    yield files_remainder

#------------------------------------------------------------
def print_table(indir, n_col, dtype):
    
    files = glob.glob("%s/*.html" % indir[0])
    files = [os.path.split(_f)[1] for _f in files]
    files = [_f for _f in files if 'index' not in _f]
    files.sort(reverse=True)

    for files_batch in get_batch(files, n_col):
        print("""\
        <thead>
            <tr>\
""")
        for i in range(len(files_batch)):
            file = files_batch[len(files_batch)-i-1]

            if (dtype is None):
                print("""\
                <th>%s</th>\
""" % (get_time_str_format(file)))
            else:
                file_base = file.replace('.html','')
                print("""\
                <th><a href=\"%s.%s\">%s</a></th>\
""" % (file_base, dtype, get_time_str_format(file)))

        print("""\
            </tr>
        </thead>\
""")

        print("""\
        <tbody>
            <tr>\
""")

        for i in range(len(files_batch)):
            file = files_batch[len(files_batch)-i-1]
            file_base = file.replace('.html','')

            print("""\
                <td><a href=\"%s.html\" target=\"_blank\"><img src=\"%s.png\" width=\"200\"></a></td>\
""" % (file_base, file_base))

        print("""\
            </tr>
        </tbody>\
""")

    return 0


########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--indir',
        help='Input dir.',
        dest='indir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-n', '--n_col',
        help='Number of columns.',
        dest='n_col',
        type=int,
        default=[n_col],
        nargs=1
        )
    parser.add_argument(
        '-d', '--data',
        help='Data type (csv.gz, npz, None).',
        dest='dtype',
        type=str,
        default=[None],
        nargs=1
        )

    args = parser.parse_args()

    # Print html.
    print_header()
    print_table(args.indir, args.n_col[0], args.dtype[0])
    print_footer()