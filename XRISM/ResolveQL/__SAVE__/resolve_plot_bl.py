#!/usr/bin/env python3

############################################################################
# [Function]
# Make plots for baseline events.
#
############################################################################

########################################################
# Imports
########################################################
import argparse, os
import pandas as pd
import numpy as np
import threading

# Plotly offline
import plotly.offline as offline
import plotly.graph_objects as go
from plotly.subplots import make_subplots

from resolve_utils import get_datetime_from_args #, utc_to_stime
from resolve_plot_hk import annotate_events, get_events
from resolve_plot_ns import palette, dashes
from resolve_plot_spec import collate_events
from myutils import add_line, add_range


########################################################
# User-defined parameters
########################################################
bl_min=-30
bl_max=30
bl_bin=1
bl_bins=(bl_max-bl_max)/bl_bin

hist_center=0
hist_width=30 # PHA
hist_bin_size=1 # PHA

pha='PHA'
time='TIME'

y_max = 25.0
sigma2fwhm=2*np.sqrt(2*np.log(2))


########################################################
# Functions
########################################################
#------------------------------------------------------------
def make_plot_bl(events_bl, title, outstem, resolution, datetime_start, events=None):
    "Plot baseline fitting."

    i_max = int(np.ceil((events_bl.dt[-1])/resolution))
    Xs = [datetime_start +  np.timedelta64(int((i+0.5)*resolution), 's') for i in range(i_max)]

    # Define layout
    fig = make_subplots(rows=4, cols=1, shared_xaxes=True, vertical_spacing=0.02, horizontal_spacing=0.02)    
        
    Layout = go.Layout(
        title=title,
        yaxis_title="Resolution (PHA)",
        showlegend=True,
        width=1200, 
        height=400*4,
        )
    
    fig.update_layout(Layout)
    fig.update_xaxes(range=[datetime_start, datetime_start +  np.timedelta64(int((i_max+1)*resolution), 's')])
    fig.update_yaxes(range=[0, y_max])
    fig.update_layout(legend= {'itemsizing': 'constant'})    
    
    for px in range(36):        
        Ys=[]
        for i in range(i_max):
            Y = np.std(events_bl[(events_bl.dt > i*resolution) & (events_bl.dt <= (i+1)*resolution) & (events_bl.PIXEL==px)][pha].values)
            Ys.append(Y)
        trace = go.Scattergl(
                x=Xs, y=Ys, 
                name='%02d' % px, 
                legendgroup=int(px/9),
                mode='lines+markers',
                marker=dict(color=palette[px%9], opacity=0.8, size=3),
                line=dict(color=palette[px%9], width=2, dash=dashes[0])
            )
        fig.append_trace(trace, int(px/9)+1, 1)

    # Annotation (common)
    if (events is not None):
        fig = annotate_events(events, fig, datetime_start, datetime_end)

    # Output
    def write_static():
        fig.write_image("%s.png" % outstem, validate=False, engine='kaleido')
        print("File saved in %s.png" % (outstem))        
    def write_html():
        offline.plot(fig, filename='%s.html' % outstem, auto_open=False, include_mathjax='cdn', validate=False)
        print("File saved in %s.html" % (outstem))
    
    t1 = threading.Thread(target=write_static)
    t2 = threading.Thread(target=write_html)
    #
    t1.start()
    t2.start()

    return 0


########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--indir',
        help='Input dir.',
        dest='indir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-o', '--outdir',
        help='Output dir name.',
        dest='outdir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-r', '--resolution',
        help='Resolution of binning in secondss.',
        dest='resolution',
        type=float,
        default=[600.0],
        nargs='+'
        )
    parser.add_argument(
        '-s', '--start',
        help='Start datetime (YYYY/MM/DD hh:mm:ss).',
        dest='start',
        type=str,
        nargs=2
        )
    parser.add_argument(
        '-e', '--end',
        help='End datetime (YYYY/MM/DD hh:mm:ss).',
        dest='end',
        type=str,
        nargs=2
        )
    parser.add_argument(
        '-E', '--evtlists',
        help='Event lists.',
        dest='evtlists',
        type=str,
        nargs='*'
        )
    args = parser.parse_args()

    plot='bl'
    
    datetime_start = get_datetime_from_args(args.start)
    datetime_end = get_datetime_from_args(args.end)
    
    str_start = pd.to_datetime(datetime_start).strftime('%Y%m%d_%H%M%S')
    str_end = pd.to_datetime(datetime_end).strftime('%Y%m%d_%H%M%S')
    outstem=args.outdir[0] + '/%s_%s-%s' % (plot, str_start, str_end)
    
    str_start = pd.to_datetime(datetime_start).strftime('%Y/%m/%d %H:%M:%S')
    str_end = pd.to_datetime(datetime_end).strftime('%Y/%m/%d %H:%M:%S')
    title='%s stdev (%s-%s)' % (plot, str_start, str_end)
    
    # events
    events=get_events(args.evtlists, datetime_start, datetime_end)
    events = collate_events(args.indir[0], 'px_uf', datetime_start, datetime_end, 1)
    if (events is not None) and len(events) > 0:        
        events_bl = events[(events.ITYPE==5)][['PIXEL',pha]]
        if (events_bl is not None) and len(events_bl) > 0:
            events_bl['dt'] =(events_bl.index - datetime_start).total_seconds()
            events_bl.sort_values(by=['dt'], inplace=True)
            make_plot_bl(events_bl, title, outstem, args.resolution[0], datetime_start, events=events)        