#!/usr/bin/env python3

############################################################################
# [Function]
# Save the noise spectra, template, and average pulse from FFF files.
#
# [History]
# 2015/01/04    M. Tsujimoto    Written from scratch.
# 2015/06/02    M. Tsujimoto    1k nspec plot.
# 2015/01/04    M. Tsujimoto    8k, 2k nspec plot.
# 2015/07/07    M. Tsujimoto    Outdir option added.
# 2015/07/08    M. Tsujimoto    Bug fix in file flushing.
# 2015/07/09    M. Tsujimoto    Bug fix in file timeout.
# 2015/07/10    M. Tsujimoto    Known sources of noise are shown.
# 2015/07/30    M. Tsujimoto    append_evt only for 1k nspec.
# 2015/08/25    M. Tsujimoto    -d option added.
# 2015/08/25    M. Tsujimoto    S_TIME calculation by np.array, not pd.DataFrame.
# 2015/09/01    M. Tsujimoto    Split into make_sxs_ns_db.py and make_sxs_ns_plot.py.
# 2019/12/29    M. Tsujimoto    Changed for Resolve
#
############################################################################

########################################################
# Imports
########################################################
import argparse
from sys import exit
import numpy as np
import pandas as pd
from astropy.io import fits
from resolve_utils import save_files, index_stime

ns_timeout=900 # in integer seconds.
wfrb_timeout=900

########################################################
# User-defined parameters
########################################################
# ns1k
hdu_ns1k="HK_SXS_NOISESPC"
hks_ns1k=["S_TIME", "PIXEL", "NOISESPC"]
# ns8k
hdu_ns8k="HK_SXS_NOISESPC8K"
hks_ns8k=["S_TIME", "PIXEL", "NOISESPC"]
# templates
hdu_tmpl="HK_SXS_TEMPLATE"
hks_tmpl=[]
# average pulses
hdu_avgp="HK_SXS_AVGPULSE"
hks_avgp=[]
# noise records
hdu_nr='EVENTS'
hks_nr=['S_TIME', 'PIXEL', 'NOISEREC_MODE', 'NOISEREC']
# pulse records
hdu_pr='EVENTS'
hks_pr=['S_TIME', 'PIXEL', 'LO_RES_PH', 'PULSEREC']
# wfrb(ac) 
hdu_wfrbac="EVENTS"
hks_wfrbac=["S_TIME", "ADC_SAMPLE", "DERIVATIVE"]
# wfrb(px) 
hdu_wfrbpx="EVENTS"
hks_wfrbpx=["S_TIME", "PIXEL", "ADC_SAMPLE", "DERIVATIVE"]



########################################################
# Functions
########################################################
#------------------------------------------------------------
def yield_nspec(infile, hdu, hks, len_nspec):
    'Yield PSP noise spec data one by one.'
    
    data = collate_data_ns(infile, hdu, hks)
    if (data is None) or (len(data)==0):
        return [None, None]
        
    # Group and yield    
    start_time = data.index[0]
    data_ns= np.zeros((len_nspec, 36))
    cnt=-1

    # Plot for each group.
    for _i in range(len(data)):
        px = int(data.iloc[_i,0])
        
        # Ignore px > 36
        if ( px > 36 ) :
            continue
        
        # Fill data. 
        if ( data.index[_i] < start_time + np.timedelta64(int(ns_timeout), 's') and ( data_ns[:,px].max() == 0 ) and cnt < 35):
            data_ns[:,px] = data.iloc[_i,1:]
            cnt+=1
            #print("Pixel %02d is the %02d'th data (%d s from the first)." % (data.iloc[_i,0], cnt, (data.index[_i]-start_time).seconds))
        # Yield and flush data.
        else:
            yield [start_time, data_ns]
            start_time = data.index[_i]
            data_ns = np.zeros((len_nspec, 36))
            cnt=0

    yield [start_time, data_ns]

#------------------------------------------------------------
def yield_wfrb(infile, hdu, hks):
    'Yield WFRB data one chunk by chunk.'

    data = collate_data_ns(infile, hdu, hks)
    if (data is None) or (len(data)==0):
        return [None, None]
       
    # Group and yield    
    start_time = data.index[0]    
    for _i in range(len(data)):
        end_time = data.index[_i]
        if ( end_time > start_time + np.timedelta64(int(wfrb_timeout), 's')):
            dataRoI = data[(data.index >= start_time) & (data.index <= end_time)]
            yield [start_time, dataRoI]
            start_time = end_time
            
    dataRoI = data[(data.index >= start_time) & (data.index <= end_time)]
    yield [start_time, dataRoI]

#------------------------------------------------------------
def makedb(infile, outdir, type):

    hdu = eval('hdu_' + type)
    hks = eval('hks_' + type)
    
    if (type == 'ns1k'):
        # ns1k
        for _i in yield_nspec(infile, hdu, hks, 512):
            stime, data =_i[0], _i[1] 
            if (data is None) or (len(data) ==0):
                continue
            outdir_new = outdir + '/ns/1k/'
            outfile = '%s_' % type + stime.strftime("%Y%m%d-%H%M%S") + '.npz'
            save_files(data, outdir_new, outfile)

    elif (type == 'ns8k'):
        # ns8sk
        for _i in yield_nspec(infile, hdu, hks, 4096):
            stime, data =_i[0], _i[1] 
            if (data is None) or (len(data) ==0):
                continue
            outdir_new = outdir + '/ns/8k/'
            outfile = '%s_' % type + stime.strftime("%Y%m%d-%H%M%S") + '.npz'
            save_files(data, outdir_new, outfile)

    elif (type == 'tmpl'):
        pass
    
    elif (type == 'avgp'):
        pass
    
    elif (type == 'nr'):
        pass
    
    elif (type == 'pr'):
        pass
    
    elif (type == 'sd'):
        pass

    elif (type == 'wfrbpx'):
        for _i in yield_wfrb(infile, hdu, hks):
            stime, data =_i[0], _i[1] 
            if (data is None) or (len(data) ==0):
                continue
            outdir_new = outdir + '/wfrb/px/'
            outfile = '%s_' % type + stime.strftime("%Y%m%d-%H%M%S") + '.pkl'            
            save_files(data, outdir_new, outfile, kind='pkl')

    elif (type == 'wfrbac'):
        for _i in yield_wfrb(infile, hdu, hks):
            stime, data =_i[0], _i[1] 
            if (data is None) or (len(data) ==0):
                continue
            outdir_new = outdir + '/wfrb/ac/'
            outfile = '%s_' % type + stime.strftime("%Y%m%d-%H%M%S") + '.pkl'            
            save_files(data, outdir_new, outfile, kind='pkl')

    return 0

########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--input',
        help='Input file.',
        dest='infile',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-o', '--outdir', 
        help='Output directory.',
        dest='outdir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-t', '--type', 
        help='Type (ns1k, ns8k, tmpl, avgp, nr, pr, sd, wfrb_px, wfrb_ac).',
        dest='type',
        type=str,
        nargs=1
        )
    args = parser.parse_args()

    makedb(args.infile[0], args.outdir[0], args.type[0])
