#!/usr/bin/env python3

############################################################
# Description
############################################################
# Converts npz files to csv.


############################################################
# Revisions
############################################################
# 2019/12/28 M. Tsujimoto gzip files as default.
# 2019/12/27 M. Tsujimoto Import from accSetting.
# 2019/12/17 M. Tsujimoto Initial release.


########################################################
# Imports
########################################################
import argparse, gzip
import numpy as np
from accSetting import chnum

########################################################
# Functions
########################################################
def conv_file_TD(infile, outfile, chnum):

    with gzip.GzipFile(infile, "r") as f:
        indata=np.load(f, allow_pickle=True)
    print("%s contains data for %d channels, %.1f kHz sampling, %.1f s length." % (infile, chnum, 1.0/indata[1]/1e3, (len(indata)-2)/chnum*indata[1]))
    num_sample = int((len(indata)-2) / chnum)    
    outdata = indata[2:].reshape(num_sample, chnum)
    np.savetxt(outfile, outdata, delimiter=',')
    
    return 0

def conv_file_FD(infile, outfile, chnum):
    
    with gzip.GzipFile(infile, "r") as f:
        indata=np.load(f, allow_pickle=True)
    print("%s contains %d spectra of %d bins." % (infile, len(indata[0]), len(indata)-3))
    np.savetxt(outfile, indata, delimiter=',')
    
    return 0

########################################################
# Main routine
########################################################
if __name__ == '__main__':

    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--input',
        help='Input file name',
        dest='infile',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-o', '--output',
        help='Output file name',
        dest='outfile',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-n', '--chnum',
        help='Channel number (default %d)' % chnum,
        dest='chnum',
        default=chnum,
        type=float,
        nargs='?'
        )
    args = parser.parse_args()

    infile = args.infile[0]
    outfile = args.outfile[0]
    
    if ('raw' in infile):
        conv_file = conv_file_TD
    elif ('broad' in infile):
        conv_file = conv_file_FD
    elif ('narrow' in infile):
        conv_file = conv_file_FD
    else:
        print("%s is neither time-domain nor freq-domain file.")
        exit(1)
    
    conv_file(infile, outfile, chnum)
    
    