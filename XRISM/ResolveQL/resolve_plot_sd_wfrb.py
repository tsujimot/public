#!/usr/bin/env python3

############################################################
# [Function]
# Plot time and freq domain data of sd and wfrb.
############################################################

########################################################
# Imports
########################################################
import argparse, datetime, os, re
from os.path import exists
import math
import pandas as pd
import numpy as np
import scipy.fftpack as sf
#
from resolve_plot_nr_ns_2D import plotly_2D
from resolve_plot_ns import make_plot_ns, palette, dashes
from resolve_plot_hk import str_now
from resolve_utils import get_freqs, sampling_rate, ac_pedestal, ac_thres, save_files
from myutils import add_line
# Plotly offline
import plotly.offline as offline
import plotly.graph_objects as go
from plotly.subplots import make_subplots
#
import threading

########################################################
# User-defined parameters
########################################################
zmin=-50
zmax=50


########################################################
# Functions
########################################################
#-------------------------------------------------------
def collate_time(data_px, data_ac, mode, len_ns):
    
    Xs=np.linspace(0,len_ns-1,len_ns)/12500
    Ys=np.linspace(0,37,38)
    Zs = None

    # Get values
    for px in range(36):
        if (mode == 'sd') :
            datum_px = data_px[(data_px.PIXEL==px) & (data_px.NOISEREC_MODE==2)].iloc[:,3:3+1024].values.flatten()
        elif (mode == 'wfrb'):
            datum_px = data_px[(data_px.PIXEL==px)].iloc[:,1+2048:1+2048*2].values.flatten() # deriv
            #datum_px = data_px[(data_px.PIXEL==px)].iloc[:,1:1+2048*1].values.flatten() # ADCsample
        # Skip if zero, fil null if short, or truncate and offset if long.
        if (len(datum_px) == 0):
            datum_px = np.zeros(len_ns)
        if (len(datum_px) < len_ns):
            datum_px = np.zeros(len_ns)
        else:
            datum_px = datum_px[:len_ns]
        # Detrend
        datum_px = datum_px - datum_px.mean()

        # Stack
        if (Zs is None):
            Zs = datum_px
        else:
            Zs = np.vstack([Zs,datum_px])
    
    # ac
    if (data_ac is None):
        datum_ac = [np.nan for i in range(len_ns)]
    else:
        for px in range(4):
            if (mode == 'sd') :
                datum_ac = data_ac[(data_ac.PSP_ID==px) & (data_ac.NOISEREC_MODE==2)].iloc[:,3::3+1024].values.flatten() 
            elif (mode == 'wfrb'):
                datum_ac = data_ac[(data_ac.PSP_ID==px)].iloc[:,1+2048:1+2048*2].values.flatten() # deriv
            # Skip if zero, null if short, truncate and offset if long.
            if (len(datum_ac) == 0):
                datum_ac = np.zeros(len_ns)
            elif (len(datum_ac) < len_ns):
                datum_ac = np.zeros(len_ns)
            else:
                datum_ac = datum_ac[:len_ns]
                # Detrend & scale
                datum_ac = (datum_ac - ac_pedestal[px])/ac_thres[px]

            # Stack
            if (Zs is None):
                Zs = datum_ac
            else:
                Zs = np.vstack([Zs,datum_ac])

    return Xs, Ys, Zs

#-------------------------------------------------------
def collate_freq(data_px, data_ac, mode, len_ns, len_chunk):
    
    n_chunk = int(len_ns/len_chunk)
    Xs=get_freqs(sampling_rate, len_chunk/sampling_rate)
    Ys=np.linspace(0,37,38)
    Zs = None

    # Get values
    for px in range(36):
        if (mode == 'sd') :
            datum_px = data_px[(data_px.PIXEL==px) & (data_px.NOISEREC_MODE==2)].iloc[:,3:3+1024].values.flatten() 
        elif (mode == 'wfrb') :
            datum_px = data_px[(data_px.PIXEL==px)].iloc[:,1:1+2048].values.flatten()
        # Skip if zero, fil null if short, or truncate and offset if long.
        if (len(datum_px) == 0):
            datum_px = np.zeros(len_ns)
        elif (len(datum_px) < len_ns):
            datum_px = np.zeros(len_ns)
        else:
            datum_px = datum_px[:len_ns]
            # Detrend
            datum_px = datum_px - datum_px.mean()
            
        # Calc PSD over chunks and add them.
        fft = np.zeros(len_chunk//2)
        if (len(datum_px) > 0):
            for i in range(n_chunk):
                #filter_func=np.ones(len_chunk)
                filter_func=np.hanning(len_chunk)
                _fft = sf.fft(datum_px[i*len_chunk:(i+1)*len_chunk]*filter_func,len_chunk)[1:int(len_chunk/2)+1]
                fft += np.abs(_fft)**2.0
            fft /= n_chunk

        # PSD.
        Z = np.sqrt(fft)
        if (Zs is None):
            Zs = Z
        else:
            Zs = np.vstack([Zs,Z])
    
    #ac
    if (data_ac is None):
        Z = [np.nan for i in range(int(len_chunk/2))]
    else:
        for px in range(4):
            if (mode == 'sd') :
                datum_ac = data_ac[(data_ac.PSP_ID==px) & (data_ac.NOISEREC_MODE==2)].iloc[:,3:3+1024].values.flatten() 
            elif (mode == 'wfrb') :
                datum_ac = data_ac[(data_ac.PSP_ID==px)].iloc[:,1:1+2048].values.flatten()
            # Skip if zero, fil null if short, or truncate and offset if long.
            if (len(datum_ac) == 0):
                datum_ac = np.zeros(len_chunk*n_chunk)
            elif (len(datum_ac) < len_chunk*n_chunk):
                datum_ac = np.zeros(len_chunk*n_chunk)
            else:
                datum_ac = datum_ac[:len_chunk*n_chunk]
                # Remove offset
                datum_ac = datum_ac - ac_pedestal[px]

            # Generator of ac noise records.
            def gen_acnr(datum_ac, px):
                i=0
                for j in range(len(datum_ac)):
                    if (datum_ac[j] < ac_thres[px]):
                        i += 1
                    else:
                        i=0
                    if (i == len_chunk):
                        yield datum_ac[j-len_chunk+1:j+1]
                        i=0

            # Calc PSD
            fft = np.zeros(int(len_chunk//2))
            filter_func=np.ones(len_chunk)

            n_chunk=0
            for acnr in gen_acnr(datum_ac, px):
                _fft = sf.fft(acnr*filter_func,len_chunk)[1:int(len_chunk/2)+1]
                fft += np.abs(_fft)**2.0
                n_chunk += 1
            print("{} noise records of {}k length accumulated for anti-co {}".format(n_chunk, len_chunk//1024, ["A", "A", "B", "B"][px]))

            if (n_chunk > 0):
                fft /= n_chunk
            else:
                fft = np.zeros(int(len_chunk//2))
            
            # PSD.
            Z = np.sqrt(fft)
            if (Zs is None):
                Zs = Z
            else:
                Zs = np.vstack([Zs,Z])
    return Xs, Ys, Zs

#------------------------------------------------------------
def make_plot_td(Xs, data, title, outstem, ytitle, range=[zmin,zmax]):
    "Plot time."

    # Define layout
    Layout = go.Layout(
        title=title,
        xaxis=dict(
            title ='Time (s)',
            type = 'linear',
            autorange = False,
            range = [Xs[0], Xs[-1]],
            #tickmode = 'auto'
            ),
        yaxis=dict(
            title = ytitle,
            type = 'linear',
            autorange = False,
            range = range,
            #tickmode = 'auto'
            ),
        showlegend=True,
        legend = dict(
            x = 1.02, xanchor = 'left',
            y = 1.00, yanchor = 'auto',
            bordercolor = '#444', 
            borderwidth = 0,
            ),
        )
    fig = make_subplots(
        rows=1, cols=1,
    )
    fig.update_layout(Layout)
    fig.update_layout(legend= {'itemsizing': 'constant'})
    fig.update_layout(legend_orientation="h")

    # Plot data
    for px in np.linspace(0,(len(data[0]))-1,(len(data[0]))):
        px = int(px)
        Ys=data[:,px]
        if (np.all(Ys==0.0)): #or (np.all(Ys==None)):
            _color="lightgrey"
        else:
            _color=palette[px%9]
        _dash = dashes[int(px/9)]
        if (px<36):
            _name = '%02d' % px
        else:
            _name = ["A", "B"][(px-36)//2]
        if (np.all(data[:,px]==0.0) == False): # Select channels with data.
            print("Processing ch%02d" % px)
            fig.add_trace(
                go.Scattergl(
                    x=Xs, y=Ys, 
                    name=_name,
                    #legendgroup=int(px/9),
                    mode='lines+markers',
                    marker=dict(color=_color, opacity=0.8, size=3),
                    line=dict(color=_color, width=2, dash=_dash),
                    legendgroup=int(px/9),
                    legendgrouptitle_text="{}".format(["A0", "A1", "B0", "B1", "AC"][int(px/9)]),
                )
        )

    # Output
    def write_static():
        fig.write_image("%s.png" % outstem, validate=False, engine='kaleido', width=1024, height=512, scale=1)
        print("File saved in %s.png" % (outstem))
    def write_html():
        offline.plot(fig, filename='%s.html' % outstem, auto_open=False, include_mathjax='cdn', validate=False)
        print("File saved in %s.html" % (outstem))

    t1 = threading.Thread(target=write_static)
    t2 = threading.Thread(target=write_html)
    #
    t1.start()
    t2.start()
    #
    t1.join()
    t2.join()
    
    return data    
    

########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--input',
        help='Input file.',
        dest='infile',
        type=str,
        nargs=2
        )
    parser.add_argument(
        '-o', '--outdir', 
        help='Output dir.',
        dest='outdir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-m', '--mode', 
        help='mode (sd or wfrb)',
        dest='mode',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-l', '--length', 
        help='nspec length',
        dest='len_ns',
        type=int,
        nargs=1,
        default=[None],
        )
    parser.add_argument(
        '-c', '--chunk', 
        help='number of chunks in DFT for averaging.',
        dest='n_chunk',
        type=int,
        nargs=1,
        default=[None],
        )

    args = parser.parse_args()        
    infile_name = os.path.split(args.infile[0])[1] 
    datetime_start = infile_name.replace('.','_').split('_')[1]
    datetime_start = datetime.datetime.strptime(datetime_start, '%Y%m%d-%H%M%S')

    # Read files.
    data_px = pd.read_pickle(args.infile[0])
    if os.path.exists(args.infile[1]):
        data_ac = pd.read_pickle(args.infile[1])
    else:
        data_ac = None
    mode=args.mode[0]    

    # time
    str_start1 = pd.to_datetime(datetime_start).strftime('%Y%m%d-%H%M%S')
    str_start2 = pd.to_datetime(datetime_start).strftime('%Y/%m/%d %H:%M:%S')
    year = pd.to_datetime(datetime_start).strftime('%Y')
    month = pd.to_datetime(datetime_start).strftime('%m')
    
    # len_ns
    len_ns = args.len_ns[0]
    if (len_ns is None):
        if (mode=="sd"):
            len_ns = 4 * 12500
        elif (mode=="wfrb"):
            len_ns = 32*12500 # 1024*256 #32*125000 #1024 * 16 # * 12500

    # len_chunks    
    n_chunk = args.n_chunk[0]
    if (n_chunk is None):
        n_chunk = int(len_ns/4096)
    len_chunk = int(len_ns/n_chunk)

    # Plot time
    Xs, Ys, Zs = collate_time(data_px, data_ac, mode, len_ns)

    # 1D
    outdir = re.sub('/$','',args.outdir[0]) + '/time/%s/%s/' % (year, month)
    outstem = outdir + '/%s_time_%s' % (mode, str_start1)
    if not exists(outdir):
        os.makedirs(outdir)
    title='Deriv [px] or (raw-ped)/thres [ac] in %s (%s) updatad at %s' % (mode, str_start2, str_now)
    data_td = make_plot_td(Xs, Zs.T, title, outstem, 'Signal', range=[zmin, zmax])
    save_files(data_td, None, "%s.npz" % outstem)

    # 2D
    outdir = re.sub('/$','',args.outdir[0]) + '_2D/time/%s/%s/' % (year, month)
    outstem = outdir + '/%s_time_%s' % (mode, str_start1)
    if not exists(outdir):
        os.makedirs(outdir)
    title='Deriv [px] or (raw-ped)/thres [ac] in %s (%s) updatad at %s' % (mode, str_start2, str_now)
    plotly_2D(Xs, Ys, Zs, outstem, title, xtitle='Time (s)', ytitle='Pixel', ytype='linear', yint=True, events=None, zrange=[zmin, zmax])

    # Plot freq
    Xs, Ys, Zs = collate_freq(data_px, data_ac, mode, len_ns, len_chunk)
    # 1D
    outdir = re.sub('/$','',args.outdir[0]) + '/freq/%s/%s/' % (year, month)
    outstem = outdir + '/%s_freq_%s' % (mode, str_start1)
    if not exists(outdir):
        os.makedirs(outdir)
    title='log Power (ADU/rtHz) in %s (%s) updatad at %s' % (mode, str_start2, str_now)
    data_fd = make_plot_ns(Xs, Zs.T, title, outstem, len(Xs)*2, monitor=mode)
    save_files(data_fd, None, "%s.npz" % outstem)

    # 2D
    outdir = re.sub('/$','',args.outdir[0]) + '_2D/freq/%s/%s/' % (year, month)
    outstem = outdir + '/%s_freq_%s' % (mode, str_start1)
    if not exists(outdir):
        os.makedirs(outdir)
    title='log Power (ADU/rtHz) in %s (%s) updatad at %s' % (mode, str_start2, str_now)
    plotly_2D(Xs, Ys, np.log10(Zs), outstem, title, xtitle='Freq (Hz)', ytitle='Pixel', xtype='log', ytype='linear', yint=True, events=None)