#!/usr/bin/env python3

############################################################################
# [Function]
# Make pixel count rate map from DB.
############################################################################

########################################################
# Imports
########################################################
from sys import exit
import argparse
import pandas as pd
import numpy as np
#
from resolve_utils import get_datetime_from_args
from resolve_plot_hk import collate_data, str_now
from resolve_plot_hk import dbhks_rates as dbhks
# Plotly offline
import plotly.offline as offline
import plotly.graph_objects as go
from plotly.subplots import make_subplots
#
import threading

########################################################
# User-defined parameters
########################################################
boards=['A0', 'A1', 'B0', 'B1']
rates_all=['HP', 'MP', 'MS', 'LP', 'LS', 'PEDB', 'NEDB', 'PREC', 'NREC', 'TLM', 'LOST', 'REJ', 'QD', 'SD']

hovertexts_template=[
    ['Pixel: 12<br />PSP_A1-5<br />Count rate (/s): ', '', '', '', '', '', ''],
    ['', '', 'Pixel: 14<br />PSP_A1-3<br />Count rate (/s): ', 'Pixel: 16<br />PSP_A1-1<br />Count rate (/s): ', 'Pixel: 08<br />PSP_A0-8<br />Count rate (/s): ', 'Pixel: 06<br />PSP_A0-6<br />Count rate (/s): ', 'Pixel: 05<br />PSP_A0-5<br />Count rate (/s): '],
    ['', 'Pixel: 11<br />PSP_A1-6<br />Count rate (/s): ', 'Pixel: 13<br />PSP_A1-4<br />Count rate (/s): ', 'Pixel: 15<br />PSP_A1-3<br />Count rate (/s): ', 'Pixel: 07<br />PSP_A0-7<br />Count rate (/s): ', 'Pixel: 04<br />PSP_A0-4<br />Count rate (/s): ', 'Pixel: 03<br />PSP_A0-3<br />Count rate (/s): '],
    ['Anti-co A<br />PSP_A0-9<br />Count rate (/s): ', 'Pixel: 09<br />PSP_A1-8<br />Count rate (/s): ', 'Pixel: 10<br />PSP_A1-7<br />Count rate (/s): ', 'Pixel: 17<br />PSP_A1-0<br />Count rate (/s): ', 'Pixel: 00<br />PSP_A0-0<br />Count rate (/s): ', 'Pixel: 02<br />PSP_A0-2<br />Count rate (/s): ', 'Pixel: 01<br />PSP_A0-1<br />Count rate (/s): '],
    ['Anti-co B<br />PSP_B0-9<br />Count rate (/s): ', 'Pixel: 19<br />PSP_B0-1<br />Count rate (/s): ', 'Pixel: 20<br />PSP_B0-2<br />Count rate (/s): ', 'Pixel: 18<br />PSP_B0-0<br />Count rate (/s): ', 'Pixel: 35<br />PSP_B1-0<br />Count rate (/s): ', 'Pixel: 28<br />PSP_B1-7<br />Count rate (/s): ', 'Pixel: 27<br />PSP_B1-8<br />Count rate (/s): '],
    ['', 'Pixel: 21<br />PSP_B0-3<br />Count rate (/s): ', 'Pixel: 22<br />PSP_B0-4<br />Count rate (/s): ', 'Pixel: 25<br />PSP_B0-7<br />Count rate (/s): ', 'Pixel: 33<br />PSP_B1-2<br />Count rate (/s): ', 'Pixel: 31<br />PSP_B1-4<br />Count rate (/s): ', 'Pixel: 29<br />PSP_B1-6<br />Count rate (/s): '],
    ['', 'Pixel: 23<br />PSP_B0-5<br />Count rate (/s): ', 'Pixel: 24<br />PSP_B0-6<br />Count rate (/s): ', 'Pixel: 26<br />PSP_B0-8<br />Count rate (/s): ', 'Pixel: 34<br />PSP_B1-1<br />Count rate (/s): ', 'Pixel: 32<br />PSP_B1-3<br />Count rate (/s): ', 'Pixel: 30<br />PSP_B1-5<br />Count rate (/s): '],
    ]

########################################################
# Class
########################################################
#------------------------------------------------------------
def get_map(data):

    # Num 0-35 for calorimeter pixels, 36 for AC-A, 37 for AC-B.
    counts=np.zeros(38)    
    counts[:] = 0.0
    
    # Calorimeter pixels.
    for _p in range(0,36):
        board = boards[_p//9]
        _pp = "%02d" % _p
        for _r in rates:
            counts[_p] += data[eval('\''+board+'_P'+_pp+'_'+_r+'_CNT\'')].mean()
        
    # Anti-co. AC has statistics. only for LS and PEDB, 
    for _r in rates:
        if (_r == 'LOST') :
            counts[36] += (data['A0_ACP_LOST_CNT'].mean(axis=1).mean()+data['A1_ACP_LOST_CNT'].mean(axis=1).mean())
            counts[37] += (data['B0_ACP_LOST_CNT'].mean(axis=1).mean()+data['B1_ACP_LOST_CNT'].mean(axis=1).mean())
        elif (_r == 'PEDB') :
            counts[36] += (data['A0_ACP_EDB_CNT'].mean(axis=1).mean()+data['A1_ACP_EDB_CNT'].mean(axis=1).mean())
            counts[37] += (data['B0_ACP_EDB_CNT'].mean(axis=1).mean()+data['B1_ACP_EDB_CNT'].mean(axis=1).mean())
            
    # count per second.
    counts /= 16
    counts[counts==0.0]=np.nan
    
    # Allocate map.
    dx, dy = 1, 1
    y, x = np.mgrid[slice(-4, 3 + dy, dy), slice(-4, 3 + dx, dx)]
    z=np.zeros((7,7))
    z[:,:] = np.nan
    
    z[0,0]=counts[12]
    z[1,2]=counts[14]
    z[1,3]=counts[16]    
    z[1,4]=counts[8]
    z[1,5]=counts[6]
    z[1,6]=counts[5]

    z[2,1]=counts[11]
    z[2,2]=counts[13]
    z[2,3]=counts[15]    
    z[2,4]=counts[7]
    z[2,5]=counts[4]
    z[2,6]=counts[3]

    z[3,1]=counts[9]
    z[3,2]=counts[10]
    z[3,3]=counts[17]    
    z[3,4]=counts[0]
    z[3,5]=counts[2]
    z[3,6]=counts[1]

    z[4,1]=counts[19]
    z[4,2]=counts[20]
    z[4,3]=counts[18]    
    z[4,4]=counts[35]
    z[4,5]=counts[28]
    z[4,6]=counts[27]

    z[5,1]=counts[21]
    z[5,2]=counts[22]
    z[5,3]=counts[25]    
    z[5,4]=counts[33]
    z[5,5]=counts[31]
    z[5,6]=counts[29]

    z[6,1]=counts[23]
    z[6,2]=counts[24]
    z[6,3]=counts[26]    
    z[6,4]=counts[34]
    z[6,5]=counts[32]
    z[6,6]=counts[30]

    z[3,0]=counts[36]
    z[4,0]=counts[37]

    counts_select=[True for i in range(len(counts))]
    #counts_select[12]=False 
    counts_select[36]=False 
    counts_select[37]=False 
    
    z_min = counts[counts_select].min()
    z_max = counts[counts_select].max()

    return [z, z_min, z_max]

#------------------------------------------------------------
def plot_map_slide(indir, dbhks, title, outstem, datetime_start, datetime_end, resample, rates):
    'Plot map'

    
    # Collate data.
    data = collate_data(indir, dbhks, datetime_start, datetime_end, rates=rates)
    if (data is None) or (len(data)==0):
        return -1

    z_all, z_all_min, z_all_max = get_map(data[datetime_start:datetime_end])

    # Define layout
    Layout = go.Layout(
        title=title,
        xaxis=dict(
            title = 'X',
            type = 'linear',
            autorange = False,
            range = [-0.5, 6.5],            
            showgrid=False,
            ),
        yaxis=dict(
            title = 'Y',
            type = 'linear',
            autorange = False,
            range = [-0.5, 6.5],        
            showgrid=False,
            ),        
        width=800,
        height=800,
        showlegend=False,
        )                    
    fig = make_subplots(
        rows=1, cols=1,
    )
    fig.update_layout(Layout)
    fig.update_layout(legend= {'itemsizing': 'constant'})

    def add_line(x0,y0,x1,y1):
        fig.add_shape(go.layout.Shape(
                type="line",
                xref="x", yref="y",
                x0=x0, x1=x1, y0=y0, y1=y1,
                line=dict(color='white', width=3)
                )
            )
    add_line(0.5,0.5,6.5,0.5)
    add_line(0.5,0.5,0.5,6.5)
    add_line(3.5,0.5,3.5,6.5)
    add_line(0.5,3.5,6.5,3.5)
    add_line(-0.5,0.5,0.5,0.5)
    add_line(0.5,-0.5,0.5,0.5)
    add_line(-0.5,2.5,0.5,2.5)
    add_line(-0.5,3.5,0.5,3.5)
    add_line(-0.5,4.5,0.5,4.5)
    add_line(0.5,2.5,0.5,4.5)
    add_line(0.5,1.5,1.5,1.5)
    add_line(1.5,0.5,1.5,1.5)    
    add_line(-0.5,3.5,0.5,3.5)
    
    # Initial values
    slide_start = datetime_start    
    labels=[]
    
    # Loop over the time.
    while ( slide_start < datetime_end):        
        slide_end = slide_start + np.timedelta64(resample, 's')        
        z, z_min, z_max = get_map(data[slide_start:slide_end])

        hovertexts=[[[] for i in range(7)] for j in range(7)]        
        for i in range(7):
            for j in range(7):
                if (hovertexts_template[i][j] !=""):
                    hovertexts[i][j] = hovertexts_template[i][j] + "%.4f" % z[i][j]
            
        fig.add_trace(
            go.Heatmap(
                z = z,
                zauto=False,
                zmin=z_all_min, 
                zmax=z_all_max,                
                name = "%s" % slide_start,
                type = 'heatmap',
                colorscale = 'Spectral',
                hoverinfo='text',
                text=hovertexts,
                zsmooth=False,
                )
            )
        ts = pd.to_datetime(str(slide_start)) 
        label=ts.strftime('%H:%M')                
        labels.append(label)
        # Next time slot.
        slide_start = slide_end

    # Add step.
    steps = []
    for i in range(len(fig.data)):
        step = dict(
            method="restyle",
            args=["visible", [False] * len(fig.data)],
            label=labels[i],
        )
        step["args"][1][len(steps)] = True
        steps.append(step)
    
    # Make the first visible.
    fig.data[0].visible = True
        
    # Slider.
    ts = pd.to_datetime(str(datetime_start)) 
    sliders = [dict(
        active=len(fig.data)//2,
        currentvalue={"prefix": ts.strftime('%y/%m/%d ')},
        pad={"t": 50},
        steps=steps
    )]
    fig.update_layout(
        sliders=sliders
    )

    # Output
    def write_static():
        fig.write_image("%s.png" % outstem, validate=False, engine='kaleido')
        print("File saved in %s.png" % (outstem))
    def write_html():
        offline.plot(fig, filename='%s.html' % outstem, auto_open=False, include_mathjax='cdn', validate=False)    
        print("File saved in %s.html" % (outstem))

    t1 = threading.Thread(target=write_static)
    t2 = threading.Thread(target=write_html)
    #
    t1.start()
    t2.start()
    #
    t1.join()
    t2.join()
    
    return 0

#------------------------------------------------------------
def plot_map(indir, dbhks, title, outstem, datetime_start, datetime_end, resample, rates):
    'Plot map'

    # Collate data.
    data = collate_data(indir, dbhks, datetime_start, datetime_end, rates=rates)
    if (data is None) or (len(data)==0):
        return -1

    z_all, z_all_min, z_all_max = get_map(data[datetime_start:datetime_end])

    # Define layout
    Layout = go.Layout(
        title=title,
        xaxis=dict(
            title = 'X',
            type = 'linear',
            autorange = False,
            range = [-0.5, 6.5],            
            showgrid=False,
            ),
        yaxis=dict(
            title = 'Y',
            type = 'linear',
            autorange = False,
            range = [-0.5, 6.5],        
            showgrid=False,
            ),        
        width=800,
        height=800,
        showlegend=False,
        )                    
    fig = make_subplots(
        rows=1, cols=1,
    )
    fig.update_layout(Layout)
    fig.update_layout(legend= {'itemsizing': 'constant'})

    def add_line(x0,y0,x1,y1):
        fig.add_shape(go.layout.Shape(
                type="line",
                xref="x", yref="y",
                x0=x0, x1=x1, y0=y0, y1=y1,
                line=dict(color='white', width=3)
                )
            )
    add_line(0.5,0.5,6.5,0.5)
    add_line(0.5,0.5,0.5,6.5)
    add_line(3.5,0.5,3.5,6.5)
    add_line(0.5,3.5,6.5,3.5)
    add_line(-0.5,0.5,0.5,0.5)
    add_line(0.5,-0.5,0.5,0.5)
    add_line(-0.5,2.5,0.5,2.5)
    add_line(-0.5,3.5,0.5,3.5)
    add_line(-0.5,4.5,0.5,4.5)
    add_line(0.5,2.5,0.5,4.5)
    add_line(0.5,1.5,1.5,1.5)
    add_line(1.5,0.5,1.5,1.5)    
    add_line(-0.5,3.5,0.5,3.5)

    hovertexts=[[[] for i in range(7)] for j in range(7)]        
    for i in range(7):
        for j in range(7):
            if (hovertexts_template[i][j] !=""):
                hovertexts[i][j] = hovertexts_template[i][j] + "%.4f" % z_all[i][j]

    fig.add_trace(
        go.Heatmap(
            z = z_all,
            zauto=False,
            zmin=0, #z_all_min, 
            zmax=20, #z_all_max,                
            type = 'heatmap',
            colorscale = 'deep',
            hoverinfo='text',
            text=hovertexts,
            zsmooth=False,
            )
        )

    # Output
    def write_static():
        fig.write_image("%s.png" % outstem, validate=False, engine='kaleido')
        print("File saved in %s.png" % (outstem))
    def write_html():
        offline.plot(fig, filename='%s.html' % outstem, auto_open=False, include_mathjax='cdn', validate=False)    
        print("File saved in %s.html" % (outstem))

    t1 = threading.Thread(target=write_static)
    t2 = threading.Thread(target=write_html)
    #
    t1.start()
    t2.start()
    
    return 0


########################################################
# Main routine
########################################################
if __name__ == '__main__':
            
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--indir',
        help='Input dir.',
        dest='indir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-o', '--outdir',
        help='Output dir name.',
        dest='outdir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-s', '--start',
        help='Start datetime (YYYY/MM/DD hh:mm:ss).',
        dest='start',
        type=str,
        nargs=2
        )
    parser.add_argument(
        '-e', '--end',
        help='End datetime (YYYY/MM/DD hh:mm:ss).',
        dest='end',
        type=str,
        nargs=2
        )
    parser.add_argument(
        '-r', '--resample',
        help='Resolution to plot in seconds.',
        dest='resample',
        type=int,
        default=[0],
        nargs=1
        )
    parser.add_argument(
        '-g', '--grade',
        help='List of rate grade, or event types.',
        dest='rates',
        type=str,
        default='',
        nargs='*'
        )
    args = parser.parse_args()

    datetime_start = get_datetime_from_args(args.start)
    datetime_end = get_datetime_from_args(args.end)
    
    rates = [i.upper() for i in args.rates]    
    for _i in rates:
        if (_i not in rates_all):
            print("%s is not supported. Choose from the following:\n%s" % (_i, rates_all))
            exit(1)

    # Outstem
    str_start = pd.to_datetime(datetime_start).strftime('%Y%m%d_%H%M%S')
    str_end = pd.to_datetime(datetime_end).strftime('%Y%m%d_%H%M%S')
    outstem=args.outdir[0] + '/map_%s_%s-%s' % ('+'.join(rates), str_start, str_end)
    
    # Title
    str_start = pd.to_datetime(datetime_start).strftime('%Y/%m/%d %H:%M:%S')
    str_end = pd.to_datetime(datetime_end).strftime('%Y/%m/%d %H:%M:%S')
    title = 'Rate (1/s) of %s (%s-%s) <br> updated at %s' %  ('+'.join(rates), str_start, str_end, str_now)
    
    plot_map(args.indir[0], dbhks, title, outstem, datetime_start, datetime_end, args.resample[0], rates)           
