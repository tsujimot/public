#!/usr/bin/env python3

########################################################
# Imports
########################################################
import numpy as np
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from plotly.colors import DEFAULT_PLOTLY_COLORS

########################################################
# Time at infinity
########################################################
import datetime
beginning_of_world=datetime.datetime(1970,1,1,0,0,0) # The world started with UNIX, of course!
end_of_world=datetime.datetime(2038,1,19,3,14,7)     # , and ends with UNIX.


########################################################
# Debug
########################################################
def set_trace():
    import sys
    from IPython.core.debugger import Pdb
    Pdb(color_scheme='Linux').set_trace(sys._getframe().f_back)

def debug(f, *args, **kwargs):
    from IPython.core.debugger import Pdb
    pdb = Pdb(color_scheme='Linux')
    return pdb.runcall(f, *args, **kwargs)


########################################################
# QDP
########################################################
def read_qdp(_f, cols, skip=0):
    'Read QDP data file.'

    import pandas as pd
    import sys

    # Open file.
    try:
        data = pd.read_csv(_f, names=cols, header=skip, delim_whitespace=True)
    except:
        print('%s : File open error.' % _f)
        sys.exit(1)

    return data

########################################################
# List
########################################################
def flatten(x):

    import collections
    result = []
    for el in x:
        if isinstance(x, collections.Iterable) and not isinstance(el, str):
            result.extend(flatten(el))
        else:
            result.append(el)
    return result

########################################################
# Color
########################################################
import seaborn as sns    
n_color=12
color_palette = sns.color_palette('hls', n_color)
palette = ['rgb({},{},{})'.format(*[x*256 for x in rgb]) for rgb in color_palette]

def yield_colors(n_color=12):
    color_palette = sns.color_palette('hls', n_color)
    palette = ['rgb({},{},{})'.format(*[x*256 for x in rgb]) for rgb in color_palette]
    for i_color in range(n_color):
        yield palette[i_color%n_color]
_yield_color=yield_colors(n_color=12)

def yield_color(i_color, n_color=12):
    color_palette = sns.color_palette('hls', n_color)
    palette = ['rgb({},{},{})'.format(*[x*256 for x in rgb]) for rgb in color_palette]
    if (i_color==-1):
        return _yield_color(n_color=12).__next__()
    else:
        return palette[i_color%n_color]

########################################################
# Others
########################################################
def Conv2Roman(num):

    num =int(num)

    # Storing roman values of digits from 0-9
    # when placed at different places
    m = ["", "M", "MM", "MMM"]
    c = ["", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM "]
    x = ["", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"]
    i = ["", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"]

    # Converting to roman
    thousands = m[num // 1000]
    hundreds = c[(num % 1000) // 100]
    tens = x[(num % 100) // 10]
    ones = i[num % 10]
    ans = (thousands + hundreds + tens + ones)

    return ans

########################################################
# Plotly
########################################################
def get_plotly_fig(rows=1, cols=1, shared_xaxes=False, shared_yaxes=False, horizontal_spacing=0.02, vertical_spacing=0.02, column_widths=None):

    import plotly.graph_objects as go

    if (rows == 1) and (cols==1):
        #fig = go.Figure()
        fig = make_subplots(rows=rows, cols=cols)
    else:
        fig = make_subplots(rows=rows, cols=cols, shared_xaxes=shared_xaxes, shared_yaxes=shared_yaxes, horizontal_spacing=horizontal_spacing, vertical_spacing=vertical_spacing)

    # Define layout   
    LayoutCommon = go.Layout(
        autosize=True,
        margin=dict(
            l=0,
            r=0,
            b=0,
            t=0,
            pad=4
            ),
        plot_bgcolor = '#FFFFFF',
        font=dict(size=18),
        title='',        
        xaxis = dict(
            mirror=True,
            showline=True,
            ticks="inside", 
            tickwidth=2, 
            ticklen=10, 
            showgrid=False, 
            zeroline=True
            ),
        yaxis = dict(
            mirror=False,
            showline=True,
            ticks="inside",
            tickwidth=2, 
            ticklen=10, 
            showgrid=False, 
            zeroline=True
            ),
        legend = dict(
            orientation="v",
            xanchor="left",
            yanchor="top",
            x=0.98,
            y=0.98,
            traceorder="normal",
            font=dict(
                family="sans-serif",
                size=16,
                color="black"
            ),
            bgcolor="#EEEEEE",
            bordercolor="Black",
            borderwidth=0,
            itemsizing='constant'
            )
        )
    fig.update_layout(LayoutCommon)

    # frame borders
    for i_xy in [0,1]:
        fig.add_shape(
            go.layout.Shape(
                type="line",
                    xref='paper', yref='paper',
                    x0=i_xy, x1=i_xy, y0=0, y1=1,
                    line=dict(color='black', width=2)
                    )
                )
        fig.add_shape(
            go.layout.Shape(
                type="line",
                    xref='paper', yref='paper',
                    y0=i_xy, y1=i_xy, x0=0, x1=1,
                    line=dict(color='black', width=2)
                    )
                )

    return fig

#------------------------------------------------------------
def add_line(fig, val, label, vert=True, log=False, row="all", col="all", color='grey', width=1, dash='dot', offset=0.5):

    import plotly.graph_objects as go
    
    if (vert is True):
        [xref, yref]=["x", "paper"]
        [x0, y0, x1, y1]=[val, 0.0, val, 1.0]
        if (log is True):
            [xc, yc] = [np.log10(val), offset]
        else:
            [xc, yc] = [val, offset]
        textangle=270.0
        # https://github.com/plotly/plotly.py/issues/3494
        #fig.add_vline(val, row=row, col=col,  annotation_text="", annotation_position="bottom right", exclude_empty_subplots=False, line=dict(color=color, width=width, dash=dash))
        fig.add_vline(val, row=row, col=col, exclude_empty_subplots=False, line=dict(color=color, width=width, dash=dash))
    else:
        [xref, yref]=["paper", "y"]
        [x0, y0, x1, y1]=[0.0, val, 1.0, val]
        if (log is True):
            [xc, yc] = [offset, np.log10(val)]
        else:
            [xc, yc] = [offset, val]
        textangle=0.0
        fig.add_hline(val, row=row, col=col, annotation_text="", annotation_position="bottom right", exclude_empty_subplots=False, line=dict(color=color, width=width, dash=dash))
    """
    fig.add_shape(
        go.layout.Shape(
            type="line",
            xref=xref, yref=yref,
            x0=x0, x1=x1, y0=y0, y1=y1,
            line=dict(color=color, width=width, dash=dash)
            ),
        col=col, row=row,
        )
"""
    fig.add_annotation(
        go.layout.Annotation(
            x=xc, y=yc,
            xref=xref, yref=yref,
            text=label,
            showarrow=False,
            textangle=textangle,
            font=dict(color=color),
            ),
        align="center"
        )

    return fig

#------------------------------------------------------------
def add_range(fig, val1, val2, label, vert=True, log=False, row="all", col="all", color='grey', offset=0.5):

    import plotly.graph_objects as go

    if (vert is True):
        [xref, yref]=["x", "paper"] 
        [x0, y0, x1, y1]=[val1, 0.0, val2, 1.0]
        if (log is True):
            [xc, yc] = [np.log10(np.sqrt(val1*val2)), offset]
        else:
            [xc, yc] = [(val2-val1)/2.0+val1, offset]
        textangle=270.0
        #fig.add_vrect(val1, val2, row=row, col=col, annotation_text="", exclude_empty_subplots=False, fillcolor=color, opacity=0.2, line_width=0)
        fig.add_vrect(val1, val2, row=row, col=col, exclude_empty_subplots=False, fillcolor=color, opacity=0.2, line_width=0)
    else:            
        [xref, yref]=["paper", "y"] 
        [x0, y0, x1, y1]=[0.0, val1, 1.0, val2]
        if (log is True):
            [xc, yc] = [offset, np.log10(np.sqrt(val1*val2))]
        else:
            [xc, yc] = [offset, (val2-val1)/2.0+val1]
        textangle=0.0
        fig.add_hrect(val1, val2, row=row, col=col, annotation_text="", exclude_empty_subplots=False, fillcolor=color, opacity=0.2, line_width=0)
    """
    fig.add_shape(
        go.layout.Shape(
            type="rect",
            xref=xref, yref=yref,
            x0=x0, x1=x1, y0=y0, y1=y1,
            fillcolor=color,
            opacity=0.1,
            layer="below",
            line_width=0,            
            ),
        col=col, row=row,
        )
"""
    fig.add_annotation(        
        go.layout.Annotation(
            x=xc, y=yc,
            xref=xref, yref=yref,
            text=label,
            showarrow=False,
            textangle=textangle,
            font=dict(color=color)            
            ),
        col=1, row=1,
        )

    return fig
