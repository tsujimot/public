#!/usr/bin/env python3

############################################################
# [Function]
# Plot accel vs freq during a given time.
############################################################

########################################################
# Imports
########################################################
import argparse, glob, os
import pandas as pd
import numpy as np
#
from resolve_utils import get_datetime_from_args, filter_by_datetime, get_freqs, save_files
from resolve_plot_ns import make_plot_ns
from resolve_plot_hk import str_now
from resolve_makedb_psd import chnum_default

########################################################
# User-defined parameters
########################################################

########################################################
# Functions
########################################################
#-------------------------------------------------------
def collate_psd(infiles, samplingrate, recordlength, chnum):
    
    # Add PSD    
    freqs = get_freqs(samplingrate, recordlength)
    psd2d_all = np.zeros((chnum,int(ns_len/2)))

    n_sample=0
    for infile in infiles:        
        psd2d_all += np.load(infile)['arr_0']
        n_sample+=1
    psd2d_all /= n_sample
                        
    return freqs, psd2d_all.T

    
########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--indir',
        help='Input dir.',
        dest='indir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-o', '--outdir',
        help='Output dir name.',
        dest='outdir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-r', '--sampling rate',
        help='Sampling date of the PSD data.',
        dest='samplingrate',
        type=float,        
        nargs=1
        )
    parser.add_argument(
        '-l', '--record length',
        help='Record length of the PSD data.',
        dest='recordlength',
        type=float,
        nargs=1
        )
    parser.add_argument(
        '-s', '--start',
        help='Start datetime (YYYY/MM/DD hh:mm:ss).',
        dest='start',
        type=str,
        nargs=2
        )
    parser.add_argument(
        '-e', '--end',
        help='End datetime (YYYY/MM/DD hh:mm:ss).',
        dest='end',
        type=str,
        nargs=2
        )
    parser.add_argument(
        '-R', '--reduction_factor',
        help='Reduction factor',
        dest='reduction_factor',
        type=int,
        default=1,
        nargs='?'
        )
    parser.add_argument(
        '-m', '--monitor',
        help='accel, mag, or busV',
        dest='monitor',
        default="accel",
        type=str,
        nargs='?'
        ),
    parser.add_argument(
        '-c', '--chnum',
        help='Number of channels.',
        dest='chnum',
        default=chnum_default,
        type=int,
        nargs='?'
        )
    args = parser.parse_args()

    datetime_start = get_datetime_from_args(args.start)
    datetime_end = get_datetime_from_args(args.end)
    
    # Get and filter infiles by time
    infiles = glob.glob('%s/psda*.npz' % (args.indir[0]))
    infiles = filter_by_datetime(infiles, 'epoch', datetime_start, datetime_end, 1)    
    if ( len(infiles) > 0 ):

        ns_len=int(args.samplingrate[0]*args.recordlength[0]/args.reduction_factor)
        
        # Outstem
        str_start = pd.to_datetime(datetime_start).strftime('%Y%m%d_%H%M%S')
        str_end = pd.to_datetime(datetime_end).strftime('%Y%m%d_%H%M%S')
        outstem=args.outdir[0] + '/%s_%s-%s' % (args.monitor, str_start, str_end)
        
        # Title
        str_start = pd.to_datetime(datetime_start).strftime('%Y/%m/%d %H:%M:%S')
        str_end = pd.to_datetime(datetime_end).strftime('%Y/%m/%d %H:%M:%S')
        if (args.monitor == "accel"):
            title='Accel PSD (%s-%s) updated at %s' % (str_start, str_end, str_now)
        elif (args.monitor == "busV"):
            title='BusV PSD (%s-%s) updated at %s' % (str_start, str_end, str_now)
        elif (args.monitor == "mag"):
            title='Mag EMI PSD (%s-%s) updated at %s' % (str_start, str_end, str_now)
        elif (args.monitor == "accel.LMS"):
            title='Accel PSD (%s-%s) updated at %s' % (str_start, str_end, str_now)
        # Collate, plot, and save.
        Xs, psd2d_all = collate_psd(infiles, args.samplingrate[0], args.recordlength[0], args.chnum)
        data_ns = make_plot_ns(Xs, psd2d_all, title, outstem, ns_len, args.monitor)
        save_files(data_ns, args.outdir[0], "%s.npz" % os.path.basename(outstem))
    else:
        print("No file found.")
        