#!/usr/local/anaconda3/bin/python

########################################################
# [Description]
# Plot required resolution for config, term, and level separations.
########################################################

########################################################
# Imports
########################################################
import numpy as np
import pandas as pd
import os
from fractions import Fraction

# Atomic database
import pyatomdb
os.environ['ATOMDB'] = '{}/.atomdb'.format(os.environ['HOME'])
from PyAstronomy import pyasl
an = pyasl.AtomicNo()
ap = pyasl.AbundancePatterns()
table="angr"
#data.index=ap.pattern(table, form="dict").keys()
abund=ap.pattern(table, form="array")

# physical constants
from scipy import constants
h = constants.h
c = constants.c
e = constants.e
keV_A = h*c/e*1e7

# plot
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns
palette = sns.color_palette("hls",n_colors=4)
sns.set_context("talk", 1.0)
font = {"family":"Times New Roman"}
mpl.rc('font', **font)


########################################################
# User-defined parameters
########################################################
datadir="./"
outfile="{}/f1-01.pdf".format(datadir)

Nmax = 5
Ls_lit = ['S', 'P', 'D', 'F']
Lmax = len(Ls_lit)-1
elements = ['H', 'He', 'C', 'N', 'O', 'Ne', 'Mg', 'Si', 'S', 'Ar', 'Ca', 'Cr', 'Mn', 'Fe', 'Ni', 'Zn']
#elements = ['Fe']

# Label, cofig_lo, level_lo, config_hi, level_hi
lines_H={
    'Ly-a2' : ["1s1", "1 2S1/2", "2p1", "2 2P1/2"],
    'Ly-a1' : ["1s1", "1 2S1/2", "2p1", "2 2P3/2"],
    'Ly-b2' : ["1s1", "1 2S1/2", "3p1", "3 2P1/2"],
    'Ly-b1' : ["1s1", "1 2S1/2", "3p1", "3 2P3/2"],
    'Ly-g2' : ["1s1", "1 2S1/2", "4p1", "4 2P1/2"],
    'Ly-g1' : ["1s1", "1 2S1/2", "4p1", "4 2P3/2"],
}
lines_He={
    'He-aw' : ["1s2", "1 1S0", "1s1 2p1", "2 1P1"],
    'He-ax' : ["1s2", "1 1S0", "1s1 2p1", "2 3P2"],
    'He-ay' : ["1s2", "1 1S0", "1s1 2p1", "2 3P1"],
    'He-az' : ["1s2", "1 1S0", "1s1 2s1", "2 3S1"],
}

########################################################
# Functions
########################################################
# -------------------------------------------------------
def get_fwhm_hetg(c,lmin,lmax):
    #https://space.mit.edu/HETG/technotes/ede_0102/ede_0102.html

    lambdas=np.linspace(lmin,lmax,101)
    FWHMs = [c[0] + c[1]*l + c[2]*l**2 + c[3]*l**3 for l in lambdas] 
    df_FWHMs = pd.DataFrame([lambdas, FWHMs]).T
    df_FWHMs.columns=["Lambda", "FWHM"]
    df_FWHMs["Energy"] = keV_A / df_FWHMs["Lambda"] 
    df_FWHMs["R"] = df_FWHMs["Lambda"] / df_FWHMs["FWHM"]
    return df_FWHMs


# -------------------------------------------------------
def conv_deg2J(deg):
    
    # ATOMDB seems to support fine structure splittting only partially and J values need to be guessed from degeneracy.
    deg = int(deg)
    if (deg < 15):
        J=[(deg-1)/2]
    #elif (deg == 9):
    #    J=[0,1,2]
    elif (deg == 15):
        J=[1,2,3]
    elif (deg == 18):
        J=[1.5,2.5,3.5]
    elif (deg == 21):
        J=[2,3,4]
    elif (deg == 24):
        J=[2.5,3.5,4.5]
    elif (deg == 27):
        J=[3,4,5]
    elif (deg == 30):
        J=[3.5,4,5,5.5]
    else:
        J=[np.nan]
    return J

# -------------------------------------------------------
def term_literal(N, L, S, J, tex=True):

    _N = int(N)
    _S = int(2*S+1)
    _L = Ls_lit[int(L)]
    _J=""
    
    for _j in J :
        _J = _J + "{},".format(Fraction(_j*2/2))
    _J = _J[:-1] # Remove trailing ,

    if (tex is True):
        term_literal = "${} ^{{{}}}{}_{{{}}}$".format(_N, _S, _L, _J)
    else:
        term_literal = "{} {}{}{}".format(_N, _S, _L, _J)
    
    return term_literal

# -------------------------------------------------------
def get_levels(Z, C):

    # Get LV data.
    levels = pyatomdb.atomdb.get_data(Z, C, 'LV')
    if (levels is False or len(levels) == 0):
        print("Data unavailable for Z={} and C={}.".format(Z, C))
        return None
        #sys.exit(1)

    data = None
    columns = ['ELEC_CONFIG', 'ENERGY', 'N_QUAN', 'L_QUAN', 'S_QUAN', 'LEV_DEG']
    for column in columns:
        datum = pd.DataFrame(levels[1].data[column].byteswap().newbyteorder(), columns=[column])
        if (data is None):
            data = datum
        else:
            data = pd.concat([data, datum], axis=1)

    # Give index
    data['ID_line'] = data.index+1

    # Select those of interest.
    data = data[(data.N_QUAN <= Nmax) & (data.L_QUAN <= Lmax)].sort_values('ENERGY', ascending=True)

    # Somehow, L and S are sometimes negative in ATOMDB
    data['L_QUAN'] = data['L_QUAN'].apply(np.abs)
    data['S_QUAN'] = data['S_QUAN'].apply(np.abs)

    # Give J.
    data['J_QUAN'] = data["LEV_DEG"].apply(conv_deg2J)

    # Give literal name of terms.
    data['LEV_LIT'] = data[['N_QUAN', 'L_QUAN', 'S_QUAN', 'J_QUAN']].apply(
        lambda x: term_literal(x[0], x[1], x[2], x[3], tex=True), axis=1)
    data['LEV_LIT2'] = data[['N_QUAN', 'L_QUAN', 'S_QUAN', 'J_QUAN']].apply(
        lambda x: term_literal(x[0], x[1], x[2], x[3], tex=False), axis=1)
    data["ELEC_CONFIG"] = data["ELEC_CONFIG"].apply(lambda x: x.rstrip())

    return data

# -------------------------------------------------------
def get_trans(Z, C):

    # Get LA data.
    trans = pyatomdb.atomdb.get_data(Z, C, 'LA')
    if (trans is False or len(trans) == 0):
        print("Data unavailable for Z={} and C={}.".format(Z, C))
        return None
        #sys.exit(1)

    data = None
    columns = ['UPPER_LEV', 'LOWER_LEV', 'WAVELEN', 'EINSTEIN_A']
    for column in columns:
        datum = pd.DataFrame(trans[1].data[column].byteswap().newbyteorder(), columns=[column])
        if (data is None):
            data = datum
        else:
            data = pd.concat([data, datum], axis=1)

    data['ENERGY'] = keV_A/data['WAVELEN']
    data = data.sort_values('EINSTEIN_A', ascending=False) 

    return data

#------------------------------------------------------------
def gen_lines(Z, C, element, lines):

    data_LV=get_levels(Z, C)
    data_LA=get_trans(Z, C)
    for key in lines.keys():
        config_lo, level_lo, config_hi, level_hi = lines[key]
        ID_line_lo=data_LV[(data_LV["ELEC_CONFIG"]==config_lo) & (data_LV["LEV_LIT2"]==level_lo)]['ID_line']
        ID_line_hi=data_LV[(data_LV["ELEC_CONFIG"]==config_hi) & (data_LV["LEV_LIT2"]==level_hi)]['ID_line']

        if (len(ID_line_lo)>0) and (len(ID_line_hi)>0):
            ID_line_lo=ID_line_lo.values[0]
            ID_line_hi=ID_line_hi.values[0]
            trans=data_LA[(data_LA["LOWER_LEV"]==ID_line_lo) & (data_LA["UPPER_LEV"]==ID_line_hi)]
            if (len(trans)>0):
                energy=trans["ENERGY"].values[0]
                A=trans["EINSTEIN_A"].values[0]
                yield [element, key, energy, A]

#------------------------------------------------------------
def main():
    
    # Instrument data
    fwhm_resolve = pd.read_csv("{}/f1-01.csv".format(datadir))
    fwhm_resolve_RoI = fwhm_resolve[(fwhm_resolve["Energy"]>1800) & (fwhm_resolve["Energy"]<12000)]
    fwhm_heg = get_fwhm_hetg([0.010019662, -0.00014101982, 1.4591413e-05, -1.9632994e-07], 1, 17)
    fwhm_heg_RoI=fwhm_heg[(fwhm_heg["Energy"]>0.8) & (fwhm_heg["Energy"]<10.0)]
    fwhm_meg = get_fwhm_hetg([0.018851868, -0.00033946575, 2.9781558e-05, -4.1097023e-07], 1, 26)
    fwhm_meg_RoI=fwhm_heg[(fwhm_meg["Energy"]>0.4) & (fwhm_meg["Energy"]<5.0)]
    fwhm_letg = np.array([
        [2.983425414364639, 43.26844259080463],
        [4.30939226519337, 66.33569857147869],
        [6.13259668508287, 105.73180977752293],
        [12.265193370165743, 245.38285176269008],
        [24.69613259668508, 548.0405530401467],
        [30.994475138121548, 683.2629436101997],
        [41.10497237569061, 874.4331914574329],
        [61.98895027624309, 1359.990357995593],
        [123.97790055248619, 2784.174296148909],
        [177.18232044198896, 3350.1936289590208],
    ])

    # Atomic data
    lines_ext=[]
    for element in elements:
        Z=an.getAtomicNo(element)

        # H-like
        lines=lines_H
        C=Z
        for line in gen_lines(Z, C, element, lines):
            lines_ext.append(line)
        df_lines_ext=pd.DataFrame(lines_ext, columns=["Element", "Label", "ENERGY", "EINSTEIN_A"])

        # He-like
        if (Z==1):
            continue
        lines=lines_He
        C=Z-1
        for line in gen_lines(Z, C, element, lines):
            lines_ext.append(line)
        df_lines_ext=pd.DataFrame(lines_ext, columns=["Element", "Label", "ENERGY", "EINSTEIN_A"])
    print(df_lines_ext)

    # Figure
    xmin, xmax=0, 12
    Xs=np.linspace(xmin, xmax, (xmax-xmin)*10+1)
    fig, ax = plt.subplots(1, 1, figsize=(6,6))

    data_level1=[]
    data_level2=[]
    data_term=[]
    data_config=[]

    for element in elements:
        Z=an.getAtomicNo(element)
        size=10*(np.log10(abund[Z-1])+8)

        # R for levels
        E1=df_lines_ext[(df_lines_ext["Element"]==element) & (df_lines_ext["Label"]=="Ly-a1")]["ENERGY"]
        E2=df_lines_ext[(df_lines_ext["Element"]==element) & (df_lines_ext["Label"]=="Ly-a2")]["ENERGY"]
        if (len(E1)>0 and len(E2)>0):
            R=np.abs(E1.values[0]/(E1.values[0]-E2.values[0]))
            data_level1.append([element, Z, E1.values[0], E2.values[0], E1.values[0]-E2.values[0], R, size])

        # R for levels
        E1=df_lines_ext[(df_lines_ext["Element"]==element) & (df_lines_ext["Label"]=="He-ax")]["ENERGY"]
        E2=df_lines_ext[(df_lines_ext["Element"]==element) & (df_lines_ext["Label"]=="He-ay")]["ENERGY"]
        if (len(E1)>0 and len(E2)>0):
            R=np.abs(E1.values[0]/(E1.values[0]-E2.values[0]))
            data_level2.append([element, Z, E1.values[0], E2.values[0], E1.values[0]-E2.values[0], R, size])

        # R for terms
        E1=df_lines_ext[(df_lines_ext["Element"]==element) & (df_lines_ext["Label"]=="He-aw")]["ENERGY"]
        E2=df_lines_ext[(df_lines_ext["Element"]==element) & (df_lines_ext["Label"]=="He-az")]["ENERGY"]
        if (len(E1)>0 and len(E2)>0):
            R=np.abs(E1.values[0]/(E1.values[0]-E2.values[0]))
            data_term.append([element, Z, E1.values[0], E2.values[0], E1.values[0]-E2.values[0], R, size])

        # R for config
        E1=df_lines_ext[(df_lines_ext["Element"]==element) & (df_lines_ext["Label"]=="Ly-a1")]["ENERGY"]
        E2=df_lines_ext[(df_lines_ext["Element"]==element) & (df_lines_ext["Label"]=="Ly-b1")]["ENERGY"]
        if (len(E1)>0 and len(E2)>0):
            R=np.abs(E1.values[0]/(E1.values[0]-E2.values[0]))
            data_config.append([element, Z, E1.values[0], E2.values[0], E1.values[0]-E2.values[0], R, size])

    labels=["Ly$\\alpha$ 1,2 (level)", "He$\\alpha$ x,y (level)", "He$\\alpha$ w,z (term)", "Ly$\\alpha$,$\\beta$ (config)"]
    for i, data in enumerate([data_level1, data_level2, data_term, data_config]):
        df_data = pd.DataFrame(data, columns=["Element", "Z", "E1", "E2", "dE", "R", "size"])
        ax.scatter(df_data["E1"], df_data["R"], ls='solid', lw=2, color=palette[i], label=labels[i], s=df_data["size"])
        for j, _row in df_data.iterrows():
            if (_row["E1"] < xmax):
                ax.text(_row["E1"], _row["R"], _row["Element"], fontsize=12, color=palette[i], va='bottom', ha='left')

    # Instrument resolution
    ax.plot(fwhm_resolve_RoI["Energy"]/1e3, fwhm_resolve_RoI["R"], ls='dashed', lw=1, color='grey')
    ax.text(3,2e3, "Resolve", fontsize=12, color="grey", va='bottom', ha='left')

    ax.plot(fwhm_heg_RoI["Energy"], fwhm_heg_RoI["R"], ls='dashed', lw=1, color='grey')
    ax.plot(fwhm_meg_RoI["Energy"], fwhm_meg_RoI["R"], ls='dashed', lw=1, color='grey')
    ax.text(1,500, "HETG", fontsize=12, color="grey", va='bottom', ha='right')
    ax.plot([keV_A/35.0, keV_A/15.0, keV_A/10.0], [c/700e3, c/1400e3, c/1900e3],  ls='dashed', lw=1, color='grey')
    ax.text(0.35,550, "RGS", fontsize=12, color="grey", va='top', ha='right')
    ax.plot([keV_A/x for x in fwhm_letg[:,0]], fwhm_letg[:,1], ls='dashed', lw=1, color='grey')
    ax.text(0.07,4000, "LETG", fontsize=12, color="grey", va='top', ha='right')
    ax.plot([4.01/1e3, 10.9/1e3], [c/2.5e3, c/2.5e3], ls='dashed', lw=1, color='grey')
    ax.text(9e-3,1e5, "STIS", fontsize=12, color="grey", va='top', ha='right')
    ax.plot([keV_A/905, keV_A/1185], [20e3, 20e3], ls='dashed', lw=1, color='grey')
    ax.text(1e-2,18e3, "FUSE", fontsize=12, color="grey", va='top', ha='left')

    #
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_xlabel('Energy (keV)')
    ax.set_ylabel('$R$')
    ax.set_xlim(8e-3,xmax)
    ax.set_ylim(1e0,1e6)
    
    # Create dummy Line2D objects for legend
    from matplotlib.lines import Line2D
    h1 = Line2D([0], [0], marker='o', markersize=np.sqrt(20), color=palette[0], linestyle='None')
    h2 = Line2D([0], [0], marker='o', markersize=np.sqrt(20), color=palette[1], linestyle='None')
    h3 = Line2D([0], [0], marker='o', markersize=np.sqrt(20), color=palette[2], linestyle='None')
    h4 = Line2D([0], [0], marker='o', markersize=np.sqrt(20), color=palette[3], linestyle='None')

    # Plot legend.
    ax.legend([h1, h2, h3, h4], labels, loc="upper right", fontsize=14, markerscale=2, scatterpoints=1)

    plt.tight_layout(pad=0.4, w_pad=0.0, h_pad=0.0)
    plt.savefig(outfile)
    print('The plot is generated in {}'.format(outfile))
    plt.close('all')

    return 0


########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    main()