#!/usr/local/anaconda3/bin/python

########################################################
# [Description]
# Plot power spectra of calibration data.
########################################################

########################################################
# Imports
########################################################
import pandas as pd
import numpy as np
from scipy import signal
#
import matplotlib as mpl
import matplotlib.mlab
import matplotlib.pyplot as plt
import seaborn as sns

#sns.set_context("talk", 1.0)
font = {"family":"Noto Sans CJK JP", "size": 14}
mpl.rc('font', **font)


########################################################
# User-defined parameters
########################################################
indir = "/home/astro/Work/LiteBIRD/20240507/dat"
outdir = "/home/astro/Work/LiteBIRD/20240507/fig"
sampling=128
freqs=[0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 1000, 10000]
axes=["x", "y", "z"]
palette = sns.color_palette("hls",n_colors=len(axes))

########################################################
# Functions
########################################################
def get_alias(f, sampling):

    f1 = f + sampling//2
    f2 = f1 % sampling
    f3 = f2 - sampling//2
    if (f3 > 0):
        f4 = f3
    else:
        f4 = -f3
    return f4

#------------------------------------------------------------
def mlab_psd_average(data, dt):
	num_per_interval = len(data)
	psd_sq,freq = matplotlib.mlab.psd(
		data,
		num_per_interval,
		1./dt,
		detrend=matplotlib.mlab.detrend_mean,
		window=None, #matplotlib.mlab.window_hanning,
		noverlap=0,
		sides='onesided',
		scale_by_freq=True
		)
	psd = np.sqrt(psd_sq)
	return (freq[:-1], psd[:-1])

#------------------------------------------------------------
def make_plot():
        
    results=[]
    for _j, freq in enumerate(freqs):

        # Figure
        _fs=16
        fig, ax = plt.subplots(1, 1, sharey=True, figsize=(8,3))
        
        # Plot
        for i, axis in enumerate(axes):
            data=np.load("{}/out_{}_{}.npy".format(indir, freq, axis))
            data2D=data.reshape([len(data)//sampling,sampling])
            Ys_all=[]
            for _i in range(len(data)//sampling):
                Xs, Ys = mlab_psd_average(data2D[_i,:], 1/sampling)
                sqrtdf=np.sqrt(np.diff(Xs)[0])
                Ys_scale = [Y*sqrtdf for Y in Ys]
                Ys_all.append(Ys_scale)
            Ys_all2D=np.array(Ys_all).reshape([len(data)//sampling,sampling//2])
            Ys_mean = [Ys_all2D[:,i].mean() for i in range(len(Ys_all2D[0,:]))] 
            Ys_stddev = [Ys_all2D[:,i].std() for i in range(len(Ys_all2D[0,:]))] 
            ax.errorbar(Xs, Ys_mean, yerr=Ys_stddev,fmt='o', markersize=3, lw=1, ecolor=palette[i], markeredgecolor=palette[i], color=palette[i], label=axis.upper())
            i_max=int(get_alias(freq, sampling)) #np.argmax(Ys_mean)
            results.append([freq, axis.upper(), Xs[i_max], Ys_mean[i_max], Ys_stddev[i_max]])
            #
            ax.set_xscale('linear')
            ax.set_yscale('log')
            ax.set_xlabel('Frequency (Hz)')
            ax.set_ylabel('Power (nT)')
            ax.set_xlim(0, 64)
            ax.set_ylim(1e-3, 1e+1) 

        ax.axvline(freq, color='grey', ls='dashed')
        ax.axvline(freq*2.0, color='grey', ls='dotted')
        ax.axvline(get_alias(freq, sampling), color='grey', ls='dashdot')
        ax.annotate("({}) {} Hz".format(chr(97+_j), freq), xy=(0.02, 0.85), xycoords='axes fraction', fontsize=_fs, ha='left', va='bottom')
        ax.legend(fontsize=_fs, ncol=3, loc='upper right', columnspacing=0)

        # Save
        outfile="{}/f05{}.png".format(outdir, chr(97+_j))
        plt.tight_layout(pad=0.4, w_pad=0.0, h_pad=0.0)
        plt.savefig(outfile)
        print('The plot is generated in {}'.format(outfile))
        plt.close('all')

    # Results
    results_df=pd.DataFrame(results, columns=["freq", "axis", "freq_peak", "B", "dB"])
    print(results_df)
    outfile="{}/f05.pkl".format(outdir)
    results_df.to_pickle(outfile)
    print('The results are saved in {}'.format(outfile))

    return 0
    
########################################################
# Main routine
########################################################
if __name__ == '__main__':

    make_plot()