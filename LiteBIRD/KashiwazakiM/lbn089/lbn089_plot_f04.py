#!/usr/local/anaconda3/bin/python

########################################################
# [Description]
# Plot time series data of calibration data.
########################################################

########################################################
# Imports
########################################################
import pandas as pd
import numpy as np
#
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns

#sns.set_context("talk", 1.0)
font = {"family":"Noto Sans CJK JP", "size": 12}
mpl.rc('font', **font)


########################################################
# User-defined parameters
########################################################
indir = "/home/astro/Work/LiteBIRD/20240507/dat"
outdir = "/home/astro/Work/LiteBIRD/20240507/fig"
sampling=128
freqs=[0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 1000, 10000]
axes=["x", "y", "z"]
palette = sns.color_palette("hls",n_colors=len(axes))


########################################################
# Functions
########################################################
#------------------------------------------------------------
def make_plot():
        
    for _j, freq in enumerate(freqs):
        
        # Figure
        _fs=10
        fig, ax = plt.subplots(1, 2, sharey=True, figsize=(8,3))
        
        # Plot
        for i, axis in enumerate(axes):
            data=np.load("{}/out_{}_{}.npy".format(indir, freq, axis))
            data_1min=data[0:60*sampling]
            print("{} Hz, {}-axis data: {} samples ({:.1f} s) for the one-min mean {:.1f}, stdev {:.3f}, p2p {:.3f}"
                .format(freq, axis, len(data), len(data)/sampling, data_1min.mean(), data_1min.std(), data_1min.max()-data_1min.min()))
            ts=[t/sampling for t in np.linspace(0, len(data)-1, len(data))]

            ax[0].scatter(ts, data-data.mean(), ls='solid', s=15, lw=0, color=palette[i], label=axis.upper())
            ax[1].scatter(ts, data-data.mean(), ls='solid', s=3, lw=0, color=palette[i], label=axis.upper())
            #
            ax[0].set_xlabel('Time (s)')
            ax[0].set_xlim(0.8, 1.2) 
            ax[0].set_ylabel("B (nT)") 
            ax[0].set_ylim(-2.5, 2.5) 
            ax[1].set_xlabel('Time (s)')
            ax[1].set_xlim(ts[0],ts[-1]) 
            
        ax[0].annotate("({}) {} Hz".format(chr(97+_j), freq), xy=(0.02, 0.9), xycoords='axes fraction', fontsize=14, ha='left', va='bottom')
        ax[0].legend(fontsize=_fs, ncol=3, loc='lower right', columnspacing=0)
        ax[0].axvline(1.0, ls='dotted', color='grey')

        # Save
        outfile="{}/f04{}.png".format(outdir, chr(97+_j))
        plt.tight_layout(pad=0.4, w_pad=0.0, h_pad=0.0)
        plt.savefig(outfile)
        print('The plot is generated in {}'.format(outfile))
        plt.close('all')

    return 0
    
########################################################
# Main routine
########################################################
if __name__ == '__main__':

    make_plot()