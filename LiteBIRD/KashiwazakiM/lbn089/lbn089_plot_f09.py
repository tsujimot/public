#!/usr/local/anaconda3/bin/python

########################################################
# [Description]
# Plot power spectra of a monochromatic wave.
########################################################

########################################################
# Imports
########################################################
import pandas as pd
import numpy as np
from scipy import signal
#
import matplotlib as mpl
import matplotlib.mlab
import matplotlib.pyplot as plt
import seaborn as sns

#sns.set_context("talk", 1.0)
font = {"family":"Noto Sans CJK JP", "size": 14}
mpl.rc('font', **font)


########################################################
# User-defined parameters
########################################################
outdir = "/home/astro/Work/LiteBIRD/20240507/fig"
from lbn089_plot_f05 import sampling, freqs, axes, mlab_psd_average, get_alias
palette = sns.color_palette("hls",n_colors=len(freqs))

########################################################
# Functions
########################################################
#------------------------------------------------------------
def make_plot():

    # Figure
    _fs=14
    _lw=0.5
    fig, ax = plt.subplots(1, 1, sharey=True, figsize=(8,4))

    N=1
    for _j, freq in enumerate(freqs[1:-2]):

        # Mock data
        data=[np.sin(2*np.pi*t/sampling*freq) for t in range(sampling*N)]
        Xs, Ys = mlab_psd_average(data, 1/sampling)
        sqrtdf=np.sqrt(np.diff(Xs)[0])
        Ys_scale = [Y*sqrtdf for Y in Ys]
        Ys_scale_max=np.max(Ys_scale)
        print(freq, Ys_scale_max)
        
        # Plot
        ax.scatter(Xs, Ys_scale, s=15, lw=0, color=palette[_j], label="{}".format(freq))

        # Annotation
        ax.axhline(Ys_scale_max, color="grey", ls='dashed', lw=_lw)
        ax.axvline(freq, color=palette[_j], ls='dashed', lw=_lw)
        ax.axvline(freq*2.0, color=palette[_j], ls='dotted', lw=_lw)
        ax.axvline(get_alias(freq, sampling), color=palette[_j], ls='dashdot', lw=_lw)

    ax.set_xscale('linear')
    ax.set_yscale('log')
    ax.set_xlabel('Frequency (Hz)')
    ax.set_ylabel('Power (nT)')
    ax.set_xlim(0, 64) 
    ax.set_ylim(1e-6, 1e+1) 
    ax.legend(fontsize=10, ncol=2, loc='lower right', columnspacing=0)

    # Save
    outfile="{}/f09.pdf".format(outdir)
    plt.tight_layout(pad=0.4, w_pad=0.0, h_pad=0.0)
    plt.savefig(outfile)
    print('The plot is generated in {}'.format(outfile))
    
    plt.close('all')

    return 0
    
########################################################
# Main routine
########################################################
if __name__ == '__main__':

    make_plot()