#!/usr/local/anaconda3/bin/python

########################################################
# [Description]
# Plot the AC response of the notch filter.
########################################################

########################################################
# Imports
########################################################
import pandas as pd
import numpy as np
from scipy import signal
from scipy.optimize import curve_fit  
#
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns

#sns.set_context("talk", 1.0)
font = {"family":"Noto Sans CJK JP", "size": 14}
mpl.rc('font', **font)


########################################################
# User-defined parameters
########################################################
indir = "/home/astro/Work/LiteBIRD/20240507/doc"
outdir = "/home/astro/Work/LiteBIRD/20240507/fig"
sampling=128
fs = np.linspace(1,128, 128*10)
axes=["X", "Y", "Z"]
palette = sns.color_palette("hls",n_colors=len(axes))

########################################################
# Functions
########################################################
#------------------------------------------------------------
def lowpass(f, fc, b):

    gain = 1.0/np.sqrt(1+(f/fc)**(2.0*b))
    gain_dB = np.log10(gain)*20.0
    return gain_dB

#------------------------------------------------------------
def notch(f, a, b):

    c=2.0
    f0 = 50.0 # Hz for notch
    df2 = np.abs(((f-f0)/a))**c
    gain = (1.0 - 1.0/(df2+b))
    gain_dB = np.log10(gain)*20.0
    return gain_dB

#------------------------------------------------------------
def make_plot():

    # Data
    data=pd.read_excel(open('{}/ノッチ.xlsx'.format(indir), 'rb'), index_col=None, header=2)
    data.columns=["Freq", "Att_X_woNF", "Att_Y_woNF", "Att_Z_woNF", "space", "Freq1", "Att_X_wNF", "Att_Y_wNF", "Att_Z_wNF"]
    del data["Freq1"]
    del data["space"]
    for axis in axes:
        data["Att_{}_NF".format(axis)] = data["Att_{}_wNF".format(axis)]-data["Att_{}_woNF".format(axis)]
        for i in ["Att_{}_woNF".format(axis), "Att_{}_wNF".format(axis), "Att_{}_NF".format(axis)]:
            data["{}_lin".format(i)] = data[i].apply(lambda x: 10**((x/10.0)))
    print(data)
    
    
    coeff_lowpass=[]
    coeff_notch=[]

    # Figure
    _fs=16
    fig, ax = plt.subplots(3, 1, sharex=True, figsize=(6,10))
    
    # Plot
    for i, axis in enumerate(axes):
        
        ax[0].scatter(data["Freq"], data["Att_{}_woNF".format(axis)], s=30, lw=0, color=palette[i], label=axis)
        ax[1].scatter(data["Freq"], data["Att_{}_wNF".format(axis)], s=30, lw=0, color=palette[i], label=axis)
        ax[2].scatter(data["Freq"], data["Att_{}_NF".format(axis)], s=30, lw=0, color=palette[i], label=axis)
        
        # Low-pass filter.
        popt, pcov = curve_fit(lowpass, data["Freq"], data["Att_{}_woNF".format(axis)])
        print("Best-fit cut-off frequency (Hz): {:.1f} +/- {:.1f} Hz".format(popt[0], np.sqrt(np.diag(pcov))[0]))
        print("Best-fit second parameter: {:.1f} +/- {:.1f}".format(popt[1], np.sqrt(np.diag(pcov))[1]))
        coeff_lowpass.append([axis, popt[0], popt[1], np.sqrt(np.diag(pcov))[0], np.sqrt(np.diag(pcov))[1]])
        model_lowpass = [lowpass(_f, popt[0], popt[1]) for _f in fs]
        ax[0].plot(fs, model_lowpass, lw=1, ls='dotted', color=palette[i], label=None)

        # Notch filter.
        #popt, pcov = curve_fit(notch, data[data["Freq"]!=50.0]["Freq"], data[data["Freq"]!=50.0]["Att_{}_NF".format(axis)], [4, 2])
        popt, pcov = curve_fit(notch, data["Freq"], data["Att_{}_NF".format(axis)], [4, 2])
        print("Best-fit first parameter: {:.1f} +/- {:.1f}".format(popt[0], np.sqrt(np.diag(pcov))[0]))
        print("Best-fit second parameter: {:.1f} +/- {:.1f}".format(popt[1], np.sqrt(np.diag(pcov))[1]))
        coeff_notch.append([axis, popt[0], popt[1], np.sqrt(np.diag(pcov))[0], np.sqrt(np.diag(pcov))[1]])
        model_notch = [notch(_f, popt[0], popt[1]) for _f in fs]
        ax[2].plot(fs, model_notch, lw=1, ls='dotted', color=palette[i], label=None)
    
    for j, label in enumerate(["w/o notch", "w/ notch", "Subtracted"]):
        ax[j].set_xscale('linear')
        ax[j].set_yscale('linear')
        ax[j].set_ylabel('Attenuation (dB)')
        ax[j].set_xlim(0, 130) 
        ax[j].set_ylim(-30, 5) 
        ax[j].axhline(0.0, color='grey', ls='dashed')
        ax[j].annotate("({}) {}".format(chr(97+j), label), xy=(0.02, 0.02), xycoords='axes fraction', fontsize=_fs, ha='left', va='bottom')
    ax[1].axvline(50.0, color='grey', ls='dashed')
    ax[2].axvline(50.0, color='grey', ls='dashed')
    ax[2].set_xlabel('Frequency (Hz)')
    ax[0].legend(fontsize=_fs, ncol=3, loc='lower right', columnspacing=0)
    ax[2].set_ylabel('Attenuation')
    #ax[2].set_ylim(0, 1.1) 
    #ax[2].axhline(1.0, color='grey', ls='dashed')

    # Save
    outfile="{}/f08.pdf".format(outdir)
    plt.tight_layout(pad=0.4, w_pad=0.0, h_pad=0.0)
    plt.savefig(outfile)
    print('The plot is generated in {}'.format(outfile))
    plt.close('all')

    # Results
    results_df1=pd.DataFrame(coeff_lowpass, columns=["axis", "a", "b", "da", "db"])
    outfile="{}/f08a.pkl".format(outdir)
    results_df1.to_pickle(outfile)
    print('The results are saved in {}'.format(outfile))
    results_df2=pd.DataFrame(coeff_notch, columns=["axis", "a", "b", "da", "db"])
    outfile="{}/f08b.pkl".format(outdir)
    results_df2.to_pickle(outfile)
    print('The results are saved in {}'.format(outfile))
    return 0
    
########################################################
# Main routine
########################################################
if __name__ == '__main__':

    make_plot()