#!/usr/local/anaconda3/bin/python

########################################################
# [Description]
# Plot power as a function of frequency.
########################################################

########################################################
# Imports
########################################################
import pandas as pd
import numpy as np
from scipy import signal
from scipy.optimize import curve_fit  
#
import matplotlib as mpl
import matplotlib.mlab
import matplotlib.pyplot as plt
import seaborn as sns

#sns.set_context("talk", 1.0)
font = {"family":"Noto Sans CJK JP", "size": 14}
mpl.rc('font', **font)


########################################################
# User-defined parameters
########################################################
indir = "/home/astro/Work/LiteBIRD/20240507/fig"
outdir = "/home/astro/Work/LiteBIRD/20240507/fig"
from lbn089_plot_f08 import lowpass, notch, axes, fs
fDSP = 0.575

palette = sns.color_palette("hls",n_colors=len(axes))

########################################################
# Functions
########################################################
#------------------------------------------------------------
def correct(B, freq, axis, coeff_lowpass, coeff_notch):
    
    a2 = coeff_lowpass[coeff_lowpass["axis"]==axis]["a"].values[0]
    b2 = coeff_lowpass[coeff_lowpass["axis"]==axis]["b"].values[0]
    a3 = coeff_notch[coeff_notch["axis"]==axis]["a"].values[0]
    b3 = coeff_notch[coeff_notch["axis"]==axis]["b"].values[0]
    factor1 = fDSP
    factor2 = 10**(lowpass(freq, a2, b2)/20.0)
    factor3 = 10**(notch(freq, a3, b3)/20.0)

    return B/factor1/factor2/factor3

def correct2(freq, coeff_lowpass2):
    factor4 = 10**(lowpass(freq, coeff_lowpass2[0], coeff_lowpass2[1])/20.0)
    return factor4

#------------------------------------------------------------
def make_plot():

    # Data
    data1 = pd.read_pickle("{}/f05.pkl".format(indir))
    data2 = pd.read_pickle("{}/f08a.pkl".format(indir))
    data3 = pd.read_pickle("{}/f08b.pkl".format(indir))
    data1["B_cor"] = data1[["B", "freq", "axis"]].apply(lambda x: correct(x[0], x[1], x[2], data2, data3), axis=1)
    data1["dB_cor"] = data1[["dB", "freq", "axis"]].apply(lambda x: correct(x[0], x[1], x[2], data2, data3), axis=1)

    # Figure
    _fs=16
    fig, ax = plt.subplots(1, 1, sharey=True, figsize=(8,5))
    
    # Plot
    for i, axis in enumerate(axes):
        data1_RoI=data1[(data1["axis"]==axis) & (data1["freq"]>0) & (data1["freq"]<150)]
        ax.errorbar(data1_RoI["freq"], data1_RoI["B"], yerr=data1_RoI["dB"], fmt='*', markersize=8, lw=1, ecolor=palette[i], markeredgecolor=palette[i], color=palette[i], label=axis.upper())
        ax.errorbar(data1_RoI["freq"], data1_RoI["B_cor"], yerr=data1_RoI["dB_cor"], fmt='o', markersize=8, lw=1, ecolor=palette[i], markeredgecolor=palette[i], color=palette[i], label=None)

        if (axis=="X"):
            # Low-pass filter.
            data1_RoI_no50=data1_RoI[data1_RoI["freq"]!=50]
            popt, pcov = curve_fit(lowpass, data1_RoI_no50["freq"], data1_RoI_no50["B_cor"].apply(lambda x: np.log10(x/2)*20), [40, -2])
            print("Best-fit cut-off frequency (Hz): {:.1f} +/- {:.1f} Hz".format(popt[0], np.sqrt(np.diag(pcov))[0]))
            print("Best-fit second parameter: {:.1f} +/- {:.1f}".format(popt[1], np.sqrt(np.diag(pcov))[1]))
            model_lowpass = [lowpass(_f, popt[0], popt[1]) for _f in fs]
            ax.plot(fs, [2*10**(x/20) for x in model_lowpass], lw=1, ls='dotted', color=palette[i], label=None)
    
    data1["factor4"] = data1[["freq"]].apply(lambda x: correct2(x, popt), axis=1)
    for i, axis in enumerate(axes[0]):
        data1_RoI=data1[(data1["axis"]==axis) & (data1["freq"]>0) & (data1["freq"]<150)]
        ax.errorbar(data1_RoI["freq"], data1_RoI["B_cor"]/data1_RoI["factor4"], yerr=data1_RoI["dB_cor"]/data1_RoI["factor4"], fmt='^', markersize=8, lw=1, ecolor=palette[i], markeredgecolor=palette[i], color=palette[i], label=None)

    ax.set_xscale('linear')
    ax.set_yscale('log')
    ax.set_xlabel('Frequency (Hz)')
    ax.set_ylabel('Power (nT)')
    ax.set_xlim(0, 105)
    ax.set_ylim(1e-3, 1e+1) 

    ax.axhline(2.0, color='grey', ls='dashed')
    ax.legend(fontsize=_fs, ncol=3, loc='upper right', columnspacing=0)

    # Save
    outfile="{}/f10.pdf".format(outdir)
    plt.tight_layout(pad=0.4, w_pad=0.0, h_pad=0.0)
    plt.savefig(outfile)
    print('The plot is generated in {}'.format(outfile))
    plt.close('all')

    return 0
    
########################################################
# Main routine
########################################################
if __name__ == '__main__':

    make_plot()