#!/usr/local/anaconda3/bin/python

########################################################
# [Description]
# Plot power spectra of calibration data.
########################################################

########################################################
# Imports
########################################################
import pandas as pd
import numpy as np
from scipy import signal
#
import matplotlib as mpl
import matplotlib.mlab
import matplotlib.pyplot as plt
import seaborn as sns

#sns.set_context("talk", 1.0)
font = {"family":"Noto Sans CJK JP", "size": 14}
mpl.rc('font', **font)


########################################################
# User-defined parameters
########################################################
from lbn088_plot_f04 import indir, outdir, sampling, axes, palette, datasets
from lbn089_plot_f05 import mlab_psd_average

########################################################
# Functions
########################################################
#------------------------------------------------------------
def make_plot():

    for _j, dataset in enumerate(datasets):
        label, infile, start, stop = dataset

        # Figure
        _fs=16
        fig, ax = plt.subplots(1, 1, sharey=True, figsize=(8,3))
        
        # Plot
        data_x=np.load("{}/{}".format(indir, infile))['arr_0'][start:start+180]
        data_y=np.load("{}/{}".format(indir, infile))['arr_1'][start:start+180]
        data_z=np.load("{}/{}".format(indir, infile))['arr_2'][start:start+180]

        for data, axis, i in zip([data_x, data_y, data_z], ["X", "Y", "Z"], range(3)): 
            Ys_all=[]
            for _i in range(len(data)):
                if (np.all(data[_i,:] == 0.0)):
                    print("Data loss.")
                    pass
                elif (np.all(data[_i,:] == np.nan)):
                    print("Data loss.")
                    pass
                else:
                    Xs, Ys = mlab_psd_average(data[_i,:], 1/sampling)
                    sqrtdf=np.sqrt(np.diff(Xs)[0])
                    Ys_scale = [Y*sqrtdf for Y in Ys]
                    Ys_all.append(Ys_scale)

            Ys_all2D=np.array(Ys_all) #.reshape([len(data)//sampling,sampling//2])
            Ys_mean = [Ys_all2D[:,i][Ys_all2D[:,i]>0].mean() for i in range(len(Ys_all2D[0,:]))] 
            Ys_stddev = [Ys_all2D[:,i][Ys_all2D[:,i]>0].std() for i in range(len(Ys_all2D[0,:]))] 
            ax.errorbar(Xs, Ys_mean, yerr=Ys_stddev,fmt='o', markersize=3, lw=1, ecolor=palette[i], markeredgecolor=palette[i], color=palette[i], label=axis.upper())
            #
            ax.axvline(25, ls='dashed', lw=1, color='grey')
            ax.set_xscale('linear')
            ax.set_yscale('log')
            ax.set_xlabel('Frequency (Hz)')
            ax.set_ylabel('Power (nT)')
            ax.set_xlim(0, 64)
            ax.set_ylim(1e-3, 1e+3) 

        #ax.axvline(freq, color='grey', ls='dashed')
        #ax.axvline(freq*2.0, color='grey', ls='dotted')
        #ax.axvline(get_alias(freq, sampling), color='grey', ls='dashdot')
        ax.annotate("({}) {}".format(chr(97+_j), label), xy=(0.02, 0.85), xycoords='axes fraction', fontsize=_fs, ha='left', va='bottom')
        ax.legend(fontsize=_fs, ncol=3, loc='upper right', columnspacing=0)

        # Save
        outfile="{}/f05{}.png".format(outdir, chr(97+_j))
        plt.tight_layout(pad=0.4, w_pad=0.0, h_pad=0.0)
        plt.savefig(outfile)
        print('The plot is generated in {}'.format(outfile))
        plt.close('all')

    return 0
    
########################################################
# Main routine
########################################################
if __name__ == '__main__':

    make_plot()