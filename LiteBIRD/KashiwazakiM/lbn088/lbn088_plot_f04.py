#!/usr/local/anaconda3/bin/python

########################################################
# [Description]
# Plot time series data of calibration data.
########################################################

########################################################
# Imports
########################################################
import pandas as pd
import numpy as np
#
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns

#sns.set_context("talk", 1.0)
font = {"family":"Noto Sans CJK JP", "size": 12}
mpl.rc('font', **font)


########################################################
# User-defined parameters
########################################################
indir = "/home/astro/Work/LiteBIRD/20240515/dat"
outdir = "/home/astro/Work/LiteBIRD/20240515/fig"
sampling=128
axes=["x", "y", "z"]
palette = sns.color_palette("hls",n_colors=len(axes))

datasets=[
    ["bkg0", "mtq_res_background.npz", 0, 1000],
    ["bkg1", "mtq_res_background.npz", 1001, 1547],
    ["+30%", "mtq_res.npz", 425, 689],
    ["+40%", "mtq_res.npz", 703, 994],
    ["+70%", "mtq_res.npz", 997, 1292],
    ["-30%", "mtq_res.npz", 1653, 1985],
    ["-40%", "mtq_res.npz", 1998, 2261],
    ["-70%", "mtq_res.npz", 2275, 2563],
]

########################################################
# Functions
########################################################
#------------------------------------------------------------
def make_plot():
        
    for _j, dataset in enumerate(datasets):
        label, infile, start, stop = dataset

        # Figure
        _fs=10
        fig, ax = plt.subplots(1, 2, sharey=True, figsize=(8,3))
        
        # Plot
        data_x=np.ravel(np.load("{}/{}".format(indir, infile))['arr_0'][start:start+180])
        data_y=np.ravel(np.load("{}/{}".format(indir, infile))['arr_1'][start:start+180])
        data_z=np.ravel(np.load("{}/{}".format(indir, infile))['arr_2'][start:start+180])

        for data, axis, i in zip([data_x, data_y, data_z], ["X", "Y", "Z"], range(3)): 
            #print("{} Hz, {}-axis data: {} samples ({:.1f} s) for the one-min mean {:.1f}, stdev {:.3f}, p2p {:.3f}"
            #    .format(freq, axis, len(data), len(data)/sampling, data_1min.mean(), data_1min.std(), data_1min.max()-data_1min.min()))
            ts=[t/sampling for t in np.linspace(0, len(data)-1, len(data))]
            data_df = pd.DataFrame([ts, data]).T
            data_df.columns=["t", "B"]
            data_df_RoI=data_df[data_df.B!=0.0]
            mean=data_df_RoI.describe().T["mean"]["B"]
            print(mean)            
            ax[0].scatter(data_df_RoI["t"], data_df_RoI["B"]-mean, ls='solid', s=15, lw=0, color=palette[i], label=axis.upper())
            ax[1].scatter(data_df_RoI["t"], data_df_RoI["B"]-mean, ls='solid', s=3, lw=0, color=palette[i], label=axis.upper())
            #
            ax[0].set_xlabel('Time (s)')
            ax[0].set_xlim(0.8, 1.2) 
            ax[0].set_ylabel("$\Delta$B (nT)")
            if (_j>1):
                ax[0].set_ylim(-100, 100) 
            else:
                ax[0].set_ylim(-2, 2) 
            ax[1].set_xlabel('Time (s)')
            ax[1].set_xlim(ts[0],ts[-1]) 
            
        ax[0].annotate("({}) {}".format(chr(97+_j), label), xy=(0.02, 0.9), xycoords='axes fraction', fontsize=14, ha='left', va='bottom')
        ax[0].legend(fontsize=_fs, ncol=3, loc='lower right', columnspacing=0)
        ax[0].axvline(1.0, ls='dotted', color='grey')

        # Save
        outfile="{}/f04{}.png".format(outdir, chr(97+_j))
        plt.tight_layout(pad=0.4, w_pad=0.0, h_pad=0.0)
        plt.savefig(outfile)
        print('The plot is generated in {}'.format(outfile))
        plt.close('all')

    return 0
    
########################################################
# Main routine
########################################################
if __name__ == '__main__':

    make_plot()