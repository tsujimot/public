#!/usr/bin/env python

############################################################################
# [Function]
# Continuous data aquisition from NI-USB 6USB
#
# [Revision]
# 201X/XX/XX    M. Sawada	    Coded from the scratch.
# 2020/12/14	Y. Uchida		Update viewgraphs.    
# 2020/03/16    M. Tsujimoto    Record length & sampling time given in command line.
# 2020/03/17	M. Tsujimoto	Gain fixed at 1 G/V.	
# 2021/05/07    R. Imamura      setup gain each ch by plot_set.json
# 2021/10/10	M. Tsujimoto	Relase for use in Resolve Inst-level test TC4.
# 2021/12/13	M. Tsujimoto	Bug fix.
#############################################################################

########################################################
# Imports
########################################################
import argparse, os, sys, time, datetime, glob
import numpy as np
from IPython import display
import matplotlib.pyplot as pl
from cycler import cycler
import codecs, json
#
from contdaqsavefile import chnum, topdir, prefix, stdout
import sxs_fft_mod
#
sys.path.append('.')
scrname = os.path.splitext(os.path.basename(sys.argv[0]))[0]

########################################################
# User-defined parameters
########################################################
reduction_factor = 10 # For using a part of the time-domain data for QL.

# Display setting.
# All costomization should be done with json_plot file.
json_plot = '/home/cmb/.workspace/litebird/KashiwazakiM/MagReadout/plot_set.json'

#chnames = ["ch%d" % ch for ch in range(chnum)]
coplotnum = 4
newstr = '@'
colors = [
	'mediumblue','orangered','darkgoldenrod','darkcyan','indigo'
]
params = {
   'mathtext.fontset':'stix',
   'mathtext.rm':'serif',
   'font.family':'serif',
   'font.serif':'Times New Roman',
   'font.size':8,
   'legend.fontsize':4,
   'axes.titlesize':4,
   'axes.formatter.useoffset':False,
   'axes.prop_cycle':cycler('color', colors),
   'xtick.direction':'in',
   'ytick.direction':'in',
   'figure.dpi':150,
   }
pl.rcParams.update(params)


#--------------------------------------------------------
def parse_accrawdata(data):

	shot_startTime = data[0]
	dt = data[1]
	voltages = data[2:]
	#
	shotlength=dt*len(voltages)/chnum
	print("Sampling rate = %.1f kHz, Record length = %.1f s.\n" % (1/dt/1e3, shotlength))
	#
	times = np.arange(0.,shotlength,dt)+shot_startTime
	times = times.T.reshape([len(times)/chnum,chnum]).T
	starttimes = times[:,0]
	endtimes = times[:,-1]
	#
	voltages = voltages.T.reshape([len(voltages)/chnum,chnum]).T

	return(dt,starttimes,endtimes,voltages)

#--------------------------------------------------------
def make_psd4plot(acces,dt,trange,reduction_factor):

	psd_b, freq_b = sxs_fft_mod.mlab_psd_average(acces,dt,int(len(acces)/reduction_factor),0)
	psd_n, freq_n = sxs_fft_mod.mlab_psd_average(acces,dt,len(acces),0)
	psd_n = psd_n[:int(len(psd_n)/reduction_factor)]
	freq_n = freq_n[:int(len(freq_n)/reduction_factor)]

	return(psd_b,freq_b,psd_n,freq_n)

#--------------------------------------------------------
class rtFigure():

	def __init__(self,ipython_flag=False):
		self.fig = pl.figure(figsize=(20,12))
		self.ipython_flag = ipython_flag
		if self.ipython_flag:
			#pl.ion()
			#pl.show()
			pass
		else:
			pl.ion()
			#pl.show(block=False)
			pl.pause(1)
			print ('Plot starting.')
		self.daqtimes = [''] * coplotnum
		self.t_axes = []
		self.v_axes = []
		self.b_axes = []
		self.n_axes = []
		self.tpls = {}
		self.bpls = {}
		self.npls = {}
		self.titletxt = self.fig.suptitle('')

	def settings(self, js=None) :

		# Top panel (time-domain)

		gains = js["gains"]["gain"]
		chnames = js["chnames"]["chname"]

		params = js["top"]

		for i in range(chnum):
			ax = self.fig.add_subplot(3,chnum,i+1)
			#vax = ax.twinx()
			self.tpls[ax] = []
			
			# Axis labels
			ax.set_xlabel('%s (%s)' %(params["xname"], params["xunit"]))
			if i == 0:
				ax.set_ylabel(r'%s (%s) $\times$ g (V/G)' % (params["yname"],params["yunit"]))
			
			# Title
			ax.set_title("%s (%s V/G)" % (chnames[i], gains[i]),fontsize=6)

			# Axis scale
			ax.set_xscale(params["xscale"])
			ax.set_yscale(params["yscale"])
			
			# Title
			if i == 7:
				ax.set_title("%s (%s V/N)" % (chnames[i], gains[i]),fontsize=6)
			else:
				ax.set_title("%s (%s V/G)" % (chnames[i], gains[i]),fontsize=6)
    
			self.t_axes.append(ax)
			
			# Grid
			ax.grid(b=True,which='major',color='dimgray',lw=0.2,ls=':')
			ax.grid(b=True,which='minor',color='gray',   lw=0.2,ls=':')

			# Annotations
			self.vline(ax, params)
			self.hline(ax, params)

			# Plots
			for j in range(coplotnum):
				tpl, = ax.plot([],[],alpha=0.5,label='')
				self.tpls[ax].append(tpl)

		# Middle panel (freq-domain, broad)
		params = js["middle"]

		for i in range(chnum):
			ax = self.fig.add_subplot(3,chnum,i+1+chnum)
			self.bpls[ax] = []
			
			# Axis label
			ax.set_xlabel('%s (%s)' %(params["xname"], params["xunit"]))
			if i == 0:
				ax.set_ylabel(r'%s (%s) $\times$ g (V/G)' % (params["yname"], params["yunit"]))

			# Axis scale
			ax.set_xscale(params["xscale"])
			ax.set_yscale(params["yscale"])
			
			self.b_axes.append(ax)
			
			# Grid
			ax.set_xticks(np.logspace(-2,3,6),minor=False)			
			ax.grid(b=True,which='major',color='dimgray',lw=0.2,ls=':')
			ax.grid(b=True,which='minor',color='gray',   lw=0.2,ls=':')

			# Annotations
			self.vline(ax, params)
			self.hline(ax, params)

			# Plots
			for j in range(coplotnum):
				bpl, = ax.plot([],[],alpha=0.5)
				self.bpls[ax].append(bpl)

		# Bottom panel (freq-domain, narrow)
		params = js["bottom"]

		for i in range(chnum):
			
			ax = self.fig.add_subplot(3,chnum,i+1+chnum*2)
			self.npls[ax] = []
			
			# Axis labels
			ax.set_xlabel('%s (%s)' %(params["xname"], params["xunit"]) )
			if i == 0:
				ax.set_ylabel(r'%s (%s) $\times$ g (V/G)' % (params["yname"], params["yunit"]) )

			# Axis scale
			ax.set_xscale(params["xscale"])
			ax.set_yscale(params["yscale"])
			
			self.n_axes.append(ax)
			
			# Grid
			ax.grid(b=True,which='major',color='dimgray',lw=0.2,ls=':')
			ax.grid(b=True,which='minor',color='gray',   lw=0.2,ls=':')

			# Annotation
			self.vline(ax, params)
			self.hline(ax, params)

			# Plots
			for i in range(coplotnum):
				npl, = ax.plot([],[],alpha=0.5)
				self.npls[ax].append(npl)

	def vline(self, ax, params) :
		if params["vline"] != "" :
			vline_keys = params["vline"].keys()			
			vline = params["vline"]
			for vline_key in vline_keys :
				ax.axvline(
					x=vline[vline_key]["x"], 
					ls=vline[vline_key]["ls"], 
					lw=vline[vline_key]["lw"],
					c=vline[vline_key]["color"], 
					alpha=vline[vline_key]["alpha"]
					)

	def hline(self, ax, params) :
		if params["hline"] != "" :
			hline_keys = params["hline"].keys()
			hline = params["hline"]
			for hline_key in hline_keys :
				ax.axhline(
					y=hline[hline_key]["y"],
					ls=hline[hline_key]["ls"], 
					lw=hline[hline_key]["lw"],
					c=hline[hline_key]["color"], 
					alpha=hline[hline_key]["alpha"]
					)

	def update_plot(self,plot_id,t0,times_allch,acces_allch,bfreqs_allch,bpsds_allch,nfreqs_allch,npsds_allch, js):

		acq = datetime.datetime.fromtimestamp(t0).strftime('%Y/%m/%d %H:%M:%S')
		now = datetime.datetime.fromtimestamp(time.time()).strftime('%Y/%m/%d %H:%M:%S')
		self.daqtimes[plot_id] = acq

		for j in range(coplotnum):
			if j != plot_id:
				for tax,bax,nax in zip(self.t_axes,self.b_axes,self.n_axes):
					self.tpls[tax][j].set_zorder(1)
					self.bpls[bax][j].set_zorder(1)
					self.npls[nax][j].set_zorder(1)
					label = self.tpls[tax][j].get_label()
					self.tpls[tax][j].set_label(label.replace(newstr,''))

		tax_params = js["top"]
		bax_params = js["middle"]
		nax_params = js["bottom"]
		
		for i,axdata in enumerate(zip(self.t_axes,self.b_axes,self.n_axes,times_allch,acces_allch,bfreqs_allch,bpsds_allch,nfreqs_allch,npsds_allch)):
			tax,bax,nax,times,acces,bfreqs,bpsds,nfreqs,npsds = axdata

			self.tpls[tax][plot_id].set_label('%s %s %s' % (newstr,acq,newstr))
			if i == chnum-1:
				tax.legend(frameon=False,prop={'size':8},bbox_to_anchor=(0.0, 1.4),ncol=coplotnum)

			# Top
			self.tpls[tax][plot_id].set_xdata(times)
			self.tpls[tax][plot_id].set_ydata(acces)
			self.tpls[tax][plot_id].set_zorder(101)			
			tax.set_xlim(min(times),max(times))
			tax.set_ylim(tax_params["ymin"], tax_params["ymax"])

			# Middle			
			self.bpls[bax][plot_id].set_xdata(bfreqs)
			self.bpls[bax][plot_id].set_ydata(bpsds)
			self.bpls[bax][plot_id].set_zorder(101)
			bax.set_xlim(bax_params["xmin"], bax_params["xmax"])
			bax.set_ylim(bax_params["ymin"], bax_params["ymax"])

			shotlength=1.0/(max(nfreqs)/(len(nfreqs)-1))
			sampling_rate=max(bfreqs)/(len(bfreqs)-1)*len(bfreqs)*2.0
						
			# Bottom			
			self.npls[nax][plot_id].set_xdata(nfreqs)
			self.npls[nax][plot_id].set_ydata(npsds)
			self.npls[nax][plot_id].set_zorder(101)
			nax.set_xlim(nax_params["xmin"],nax_params["xmax"])
			nax.set_ylim(nax_params["ymin"],nax_params["ymax"])
			
			if i > 0:
				tax.set_yticklabels([])
				bax.set_yticklabels([])
				nax.set_yticklabels([])

		self.titletxt.set_text('Last data aquisition %s & figure update %s (%.1f kHz, %.1f s)' % (acq,now, sampling_rate/1e3, shotlength))
		self.titletxt.set_size(10)

		if self.ipython_flag:
			display.clear_output(wait=True)
			display.display(pl.gcf())
		else:
			#pl.draw()
			pl.pause(0.01)
		return()

	def savefig(self, figurename) : 
		self.fig.savefig(figurename)
		print("saved as %s" %(figurename))

	def clear_plt(self) : 
		pl.cla()

	def close_plot(self):
		pl.close()


#--------------------------------------------------------
def main(ipython_flag=False):

	figure = None 
	prev_rawfile = None
	firstRun = True
	
	# Loop to display
	while True:
		plot_id = 0
		while True:
			# Setup for the first run.
			if firstRun : 
				with codecs.open(json_plot, encoding="utf-8_sig") as rawjsfile :
					jsfile = json.load(rawjsfile)
				figure = rtFigure(ipython_flag)
				figure.settings(jsfile)
				gains = jsfile["gains"]["gain"]
				firstRun = False
				
			# Read data
			date8 = datetime.datetime.now().strftime('%Y%m%d')
			datedir = '%s/%s' % (topdir,date8)
			rawfiles = sorted(glob.glob('%s/%s*.npz' % (datedir,prefix)))			
			if rawfiles == []:
				continue
			else:
				rawfile = rawfiles[-1]

			# Plot data
			if rawfile != prev_rawfile:
				now=datetime.datetime.now().strftime('%H%M%S')
				stdout('%s: %s' % (now,rawfile))
				try:
					data = np.load(rawfile, allow_pickle=False)['arr_0']
				except:
					print("Load failed and one-cleared in {}.".format(rawfile))
					data = np.ones_like(data)
				dt,stime_allch,etime_allch,volts_allch = parse_accrawdata(data)
				acces_allch = []
				bfreqs_allch = []
				bpsds_allch = []
				nfreqs_allch = []
				npsds_allch = []
				for ch,rows in enumerate(zip(stime_allch,etime_allch,volts_allch)):
					stime,etime,volts = rows
					trange = np.array([stime,etime])
					acces = volts * gains[ch]
					acces_allch.append(acces)
					bpsds,bfreqs,npsds,nfreqs = make_psd4plot(acces,dt,trange,reduction_factor)
					bpsds_allch.append(bpsds)
					bfreqs_allch.append(bfreqs)
					npsds_allch.append(npsds)
					nfreqs_allch.append(nfreqs)
				times_allch = np.array([np.arange(len(volts_allch[0]))*dt]*chnum)				
				figure.update_plot(plot_id,stime_allch[0],times_allch,acces_allch,bfreqs_allch,bpsds_allch,nfreqs_allch,npsds_allch, jsfile)
				plot_id += 1
			else:
				time.sleep(16.0) # Tweak needed for different shot length.
				pass
			
			prev_rawfile = rawfile

			if plot_id == coplotnum:
				outfile='%s/screenshot_%s-%s.png' % (datedir, date8, now)
				figure.savefig(outfile)
				stdout('%s saved.' % (outfile))
				break
			else:
				pass
	return()

#--------------------------------------------------------
if __name__ == '__main__':

	main()
