#!/usr/bin/env python

from PyDAQmx import *
from ctypes import *
import numpy as np
import os,time

totratemax = 400000.0

class ContDaq():

	def __init__(self, shotlength, rate, chnum, input_min, input_max, verbose=False):

		self.shotlength = shotlength
		self.rate = rate
		self.chnum = chnum
		self.verbose = verbose
		self.totdatalen = int(self.shotlength * self.rate * self.chnum)
		self.error = 0
		self.taskHandle = TaskHandle()
		self.read = int32()
		self.totrate = self.rate * self.chnum
		self.dt = 1./self.totrate
		self.timewait = 1
		self.input_min = input_min
		self.input_max = input_max

		if self.totrate > totratemax:
			print('Exceed maximum sampling rate: %d samples/s' % totratemax)
			exit()
		else:
			pass

		devid = 1
		self.physchan = 'Dev%d/ai%d:%d' % (devid, 0, chnum-1)
		self.pointsToRead = self.totdatalen/chnum
		self.data = np.zeros((self.totdatalen,), dtype=np.float64)

		self.timeout = self.shotlength + self.timewait

	def stdout(self,string):

		if self.verbose:
			print('[%s] %s' % (__name__,string))

		return()

	def create_task(self):

		self.stdout('create daqmx task')
		DAQmxCreateTask("",byref(self.taskHandle))
		self.stdout('create daqmx a/i voltage channels')
		DAQmxCreateAIVoltageChan(self.taskHandle,self.physchan,"",DAQmx_Val_Diff,self.input_min,self.input_max,DAQmx_Val_Volts,None)

		self.stdout('create daqmx clock timing')
		#DAQmxCfgSampClkTiming(self.taskHandle,"",self.rate,DAQmx_Val_Rising,DAQmx_Val_ContSamps,self.totdatalen)
		DAQmxCfgSampClkTiming(self.taskHandle,"",self.rate,DAQmx_Val_Rising,DAQmx_Val_FiniteSamps,self.totdatalen)

		self.stdout('create daqmx config input buffer')
		#DAQmxCfgInputBuffer(self.taskHandle,self.pointsToRead)
		DAQmxCfgInputBuffer(self.taskHandle,self.totdatalen)
		##DAQmxCfgInputBuffer(self.taskHandle,1000)

		return()

	def start_task(self):

		self.stdout('start daqmx task')
		DAQmxStartTask(self.taskHandle)

		return()

	def do_sampling(self):

		self.shot_startTime = time.time()
		self.stdout('reading daqmx data')
		DAQmxReadAnalogF64(self.taskHandle,self.pointsToRead,self.timeout,DAQmx_Val_GroupByScanNumber,self.data,self.totdatalen,byref(self.read),None)
		return(self.shot_startTime,self.data)

	def close_task(self):

		self.stdout('stop/clear daqmx task')

		DAQmxStopTask(self.taskHandle)
		DAQmxClearTask(self.taskHandle)
