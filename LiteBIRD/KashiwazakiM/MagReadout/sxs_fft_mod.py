#!/usr/bin/env python

import numpy as np
import scipy as sp
import matplotlib.mlab
import scipy as sp
import scipy.fftpack as sf
import scipy.stats

def mlab_psd(acces,dt):

	psd_sq,freq = matplotlib.mlab.psd(
		acces,
		len(acces),
		1./dt,
		window=matplotlib.mlab.window_hanning,
		sides='onesided',
		scale_by_freq=True
		)
	psd = np.sqrt(psd_sq)
	return(psd,freq)

def mlab_psd_average(acces,dt,num_per_interval=None,noverlap=0):

	if num_per_interval == None:
		num_per_interval = len(acces)
	else:
		pass

	psd_sq,freq = matplotlib.mlab.psd(
		acces,
		num_per_interval,
		1./dt,
		detrend=matplotlib.mlab.detrend_mean,
		window=matplotlib.mlab.window_hanning,
		noverlap=noverlap,
		sides='onesided',
		scale_by_freq=True
		)
	psd = np.sqrt(psd_sq)
	return(psd[:-1],freq[:-1])

def scipy_fft(acces,dt):

	bin = len(acces)
	hanning = sp.hanning(len(acces))

	freq = sf.fftfreq(bin,dt)[0:bin/2]
	df = freq[1]-freq[0]
	fft = sf.fft(acces*hanning,bin)[0:bin/2]

	real = fft.real
	imag = fft.imag
	psd = np.abs(fft)/np.sqrt(df/2.)/float(bin)

	return(freq,real,imag,psd)

def scipy_fft_psd(acces,dt):

	bin = len(acces)
	hanning = sp.hanning(len(acces))

	freq = sf.fftfreq(bin,dt)[0:bin/2]
	df = freq[1]-freq[0]
	fft = sf.fft(acces*hanning,bin)[0:bin/2]

	#real = fft.real
	#imag = fft.imag
	psd = np.abs(fft)/np.sqrt(df/2.)/float(bin)

	return(psd,freq)

def makepsd(times,acces,df):

	dt = times[1]-times[0]
	timelength_interval = 1./df
	bin_interval = timelength_interval / dt
	totbin = len(acces)
	num_interval = int(totbin / bin_interval)

	acces0 = acces - np.average(acces)
	midtimes = []
	reals = []
	imags = []
	absos = []
	psd2D = []
	for interval in range(num_interval):
		acces_interval = acces0[interval*bin_interval:(1+interval)*bin_interval]
		times_interval = times[interval*bin_interval:(1+interval)*bin_interval]
		midtime = np.average(times_interval)
		midtimes.append(midtime)
		freq,real,imag,psd = scipy_fft(acces_interval,dt)
		reals.append(real)
		imags.append(imag)
		psd2D.append(psd)
	midtimes = np.array(midtimes)
	freq2D,time2D = np.meshgrid(freq,midtimes)
	reals = np.array(reals)
	imags = np.array(imags)
	psd2D = np.array(psd2D)
	psddata = np.array([time2D,freq2D,reals,imags,psd2D])

	return(psddata)

def makepsd1d(times,acces,df):

	dt = times[1]-times[0]
	timelength_interval = 1./df
	bin_interval = timelength_interval / dt
	totbin = len(acces)
	num_interval = int(totbin / bin_interval)
	print('df = %1.1e Hz' % df)
	acces0 = acces - np.average(acces)
	psd2D = []
	for interval in range(num_interval):
		acces_interval = acces0[interval*bin_interval:(1+interval)*bin_interval]
		times_interval = times[interval*bin_interval:(1+interval)*bin_interval]
		freq,real,imag,psd = scipy_fft(acces_interval,dt)
		psd2D.append(psd)
	avepsd = np.average(psd2D,axis=0)

	return(freq,avepsd)

def calc_frf(reals,imags,force_reals,force_imags):

	ffts = reals + 1j * imags
	force_ffts = force_reals + 1j * force_imags

	frfs = ffts / force_ffts
	frf_real = np.average([frf.real for frf in frfs],axis=0)
	frf_imag = np.average([frf.imag for frf in frfs],axis=0)
	frf_abso = np.average([np.abs(frf) for frf in frfs],axis=0)
	frf_phas = np.arctan2(frf_imag,frf_real)
	frf_phas[frf_phas < 0] += 2*np.pi
	#frf_abso = np.abs(frf_real+frf_imag*1j)

	cross_fft = np.average(ffts.conjugate()*force_ffts,axis=0)
	fft_abso = np.average(ffts*ffts.conjugate(),axis=0)
	force_fft_abso = np.average(force_ffts*force_ffts.conjugate(),axis=0)
	coh = ((np.abs(cross_fft)**2)/(fft_abso*force_fft_abso)).real

	return(frf_real,frf_imag,frf_abso,frf_phas,coh)

def calc_peakfrfs(psds,reals,imags,force_reals,force_imags,freqs,peak_indices):

	# frf calculation for sine sweep data

	peakamps = [] # amplitude values at peaks
	peakpsds = [] # psd values at peaks
	peakfreqs = []
	frf_reals = []
	frf_imags = []
	frf_absos = []
	frf_phass = []
	cohs = []

	for psd,real,imag,freal,fimag,index in zip(psds,reals,imags,force_reals,force_imags,peak_indices):

		peakfreqs.append(freqs[index])
		peakamp = np.sum(psd[index-1:index+1+1])*np.sqrt(freqs[1]-freqs[0])
		peakamps.append(peakamp)
		peakpsds.append(psd[index])

		fft = (real + 1j * imag)[index]
		force_fft = (freal + 1j * fimag)[index]
		frf = fft / force_fft
		frf_real = frf.real
		frf_imag = frf.imag
		frf_abso = np.abs(frf)
		frf_phas = np.arctan2(frf_imag,frf_real)
		if frf_phas < 0:
			frf_phas += 2*np.pi
		cross_fft = fft.conjugate()*force_fft
		fft_abso = fft*fft.conjugate()
		force_fft_abso = force_fft*force_fft.conjugate()
		coh = ((np.abs(cross_fft)**2)/(fft_abso*force_fft_abso)).real
		frf_reals.append(frf_real)
		frf_imags.append(frf_imag)
		frf_absos.append(frf_abso)
		frf_phass.append(frf_phas)
		cohs.append(coh)

	peakfreqs = np.array(peakfreqs)
	peakamps = np.array(peakamps)
	peakpsds = np.array(peakpsds)

	frf_reals = np.array(frf_reals)
	frf_imags = np.array(frf_imags)
	frf_absos = np.array(frf_absos)
	frf_phass = np.array(frf_phass)
	cohs = np.array(cohs)

	return(peakfreqs,peakamps,peakpsds,frf_reals,frf_imags,frf_absos,frf_phass,cohs)

def segments2relerror(segments,significance):

	if significance <= 0 or significance >= 100:
		print('significance a should be 0<sig<100 (in unit of \%).')
		exit()
	else:
		pass

	if segments < 1:
		print('segments should be an integer being larger or equal to 1.')
		exit()
	else:
		pass

	alpha = 1.-significance/100.
	relerr_upper = segments*2/scipy.stats.chi2.ppf(alpha/2,segments*2)
	relerr_lower = segments*2/scipy.stats.chi2.ppf(1-alpha/2,segments*2)

	return(relerr_lower,relerr_upper)

def get_updown_regionlist(freqs):

	sign = np.sign(freqs[1:] - freqs[:-1])

	prev_s = 0
	edge_masklen = 30
	turnover_indices = []
	for i,s in enumerate(sign):
		if s * prev_s == -1:
			if i > edge_masklen and i < len(sign) - edge_masklen:
				turnover_indices.append(i)
			else:
				pass
			prev_s = s
		if s != 0:
			prev_s = s

	region_list = []
	if turnover_indices == []:
		region = np.bool8(np.zeros(len(sign)))
		region[:] = True
		region_list.append(region)
	else:
		prev_toind = 0
		for i,turnover_index in enumerate(turnover_indices):
			region = np.bool8(np.zeros(len(sign)))
			region[prev_toind:turnover_index] = True
			region_list.append(region)
			prev_toind = turnover_index
			if i == len(turnover_indices)-1:
				region = np.bool8(np.zeros(len(sign)))
				region[turnover_index:-1] = True
				region_list.append(region)

	return(region_list)
