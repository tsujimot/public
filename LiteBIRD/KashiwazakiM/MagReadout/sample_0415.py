import serial
import numpy as np
import time
import datetime
import sys

def convert_rawdata(lis,range_):
    
    value = int("".join(lis),16)

    if range_ == 150:
        if value < (int("FFFFFF",16))/2:
            
            return -value/(2**19)*range_*13.3/16
        else:
            return (int("FFFFFF",16)-value+1)/(2**19)*range_*13.3/16
    elif range_ == 500:
        if value < (int("FFFFFF",16))/2:
            return -value/(2**21)*range_
        else:
            return (int("FFFFFF",16)-value+1)/(2**21)*range_
    elif range_ > 500:
        if value < (int("FFFFFF",16))/2:
            return -value/(int("7FFFFF",16))*range_
        else:
            return ((int("FFFFFF",16))-value+1)/int("7FFFFF",16)*range_
        
def get_magvalue(i):

    # range definition
    range_map = {
        "1":150,
        "2":500,
        "3":2000,
        "4":64000
    }

    # separate header
    header = i[:10].decode()
    value = i[11:-2].decode()
    if header[0] != "$":
        print("error")

    # convert 16 to magvalue
    n=len(value)
    value_np = np.array(list(map(str,list(value)))).reshape(n//6,6)
    range_np = [range_map[header[2]],range_map[header[5]],range_map[header[8]]]*(n//6)

    # add dummy data
    a =  np.array(list(map(convert_rawdata,value_np,range_np))).reshape(len(value_np)//3,3)
    print(np.average(a,axis=0))
    print(a)
    b = np.zeros((len(value_np)//3,5))
    c = np.concatenate((a,b),axis=1)


    return c.reshape(1,len(c)*len(c[0]))[0]

def write_mag(value,file,command):
   with open(f"./{file}",f"{command}") as f:
       f.write(value)
   

if __name__ == "__main__":
    ser = serial.Serial('/dev/ttyUSB0',38400)
    timereso = 1/128
    directory = sys.argv[1]
    mintime = int(sys.argv[2])
    is_finish = False
    minute = 0

    while is_finish == False:

        #make filename
        dt = datetime.datetime.now()
        file_name = f"{directory}/{dt.strftime('%Y%m%d')}_{minute}"
        
        #write header
        #write_mag([f"{time.time()} "],filename)
        #write_mag([f"{timereso} "],filename)
        savelist = [f"{time.time()} ",f"{timereso} "]

        #write magval
        for count in range(60):
            
            # line 読み出し
            line = ""
            while(len(line)) != 2317:
                line = ser.readline()
        
            # Line から　磁場値
            result = get_magvalue(line)

            # 磁場 Write
            #value = " ".join(list(map(str,result)))
            savelist = savelist + list(result)
            np.savez(file_name,savelist)

            print(f"finish {minute}_{count}")
        
        minute += 1
        is_finish = ((minute >= mintime) and (input("finish?")=="yes"))
        
