#!/usr/bin/env python

############################################################################
# [Function]
# Convert npy to npz.
#
# [Revision]
# 2021/05/14    M. Tsujimoto	Coded from the scratch.
#############################################################################

########################################################
# Imports
########################################################
import argparse
import numpy as np
import numpy.matlib

########################################################
# User-defined parameters
########################################################
chnum=8

#########################################################
# Functions
#########################################################
def convert_npy2npz(infile, gains):

	data_in = np.load(infile)

	# Gain correction
	voltages = data_in[2:]
	gains_expand = np.matlib.repmat(gains,1,int(len(voltages)/len(gains)))
	accel = voltages/gains_expand[0]

	# save npz
	data_out = np.hstack([data_in[0:2],accel])
	outfile=infile.replace('npy','npz')
	#np.savez(outfile, data_out)
	np.savez_compressed(outfile, data_out)
	print("%s saved." % outfile)

	return 0

########################################################
# Main routine
########################################################
if __name__ == '__main__':

	# Command-line parser
	parser = argparse.ArgumentParser()
	parser.add_argument(
		'-i', '--infile',
		help='infile',
		dest='infile',
		type=str,
		nargs=1,
		)
	parser.add_argument(
		'-g', '--gains',
		help='Gains',
		dest='gains',
		type=float,
		nargs=8
		)
	args = parser.parse_args()

	convert_npy2npz(args.infile[0], args.gains)
