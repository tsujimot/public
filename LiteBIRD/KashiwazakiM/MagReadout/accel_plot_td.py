#!/usr/bin/env python

########################################################
# Imports
########################################################
import argparse
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

########################################################
# User-defined parameters
########################################################
#chnames=['DMS-Y_X','DMS-Y_Y','DMS-Y_Z','SC-A','SC-B','PC-A','PC-B','DMS TOP']
chnames=['DMS-Y_X','DMS-Y_Y','DMS-Y_Z','N/A','N/A','N/A','N/A','N/A']
n_chan=len(chnames)

########################################################
# Function
########################################################
#------------------------------------------------------------
def read_data(infile, start, end):

    data = np.load(infile,allow_pickle=True)['arr_0']
    n_sample = int(len(data[2:])/n_chan)
    sampling = data[1]
    shotlength =  sampling * n_sample
    print("Sampling rate {} Hz & Record length {} s.".format(1.0/sampling, shotlength))

    # Data
    times_all = np.linspace(0, shotlength, n_sample+1)[:-1]
    data_all = data[2:].T.reshape([n_sample,n_chan]).T

    # Extract from start to end.
    if (start < 0):
        i_start =0
    elif (start < shotlength):
        i_start = int(start / sampling)
        print("Start time to use {}s.".format(start))
    else:
        print("Start time {}s is designated wrong.".format(start))
        exit(1)
    
    if (end < 0):
        i_end =n_sample
    elif (end < shotlength):
        i_end = int(end / sampling)
        print("End time {}s.".format(end))
    else:
        print("End time to use {}s is designated wrong.".format(end))
        exit(1)

    # DataFrame
    col_names = ['time_s']
    col_names.extend(['accel{}_g'.format(i) for i in range(n_chan)])
    times = times_all[i_start:i_end]
    times_df = pd.DataFrame(times)
    accel_g = data_all[:,i_start:i_end]
    #
    for i in range(n_chan):
        data_all[i,i_start:i_end] -= np.mean( data_all[i,i_start:i_end])
    accel_g_df = pd.DataFrame(accel_g.T)
    data_df = pd.concat([times_df,accel_g_df],axis=1)
    data_df.columns = col_names

    return data_df

#------------------------------------------------------------
def make_plot(data, chnames, outfile_stem):

    # Figure
    fig, ax = plt.subplots(nrows=n_chan, sharex=True, figsize=(8,8))
    plt.rcParams['font.family'] = 'Times New Roman'
    plt.rcParams["font.size"] = 16
    _lw=0.5
    _fs=20

    Xs=data['time_s'].values-(data['time_s'][0])

    for ch in range(n_chan):
        Ys=data["accel{:d}_g".format(ch)].values
        ax[ch].plot(Xs, Ys, lw=_lw, color="k", ls="solid")
        ax[ch].annotate(chnames[ch], xy=(0.01, 0.9), xycoords="axes fraction", size=_fs, horizontalalignment='left', verticalalignment='top')

    # Axes
    ax[n_chan-1].set_xlabel('Time (s)', fontsize=_fs+4)
    ax[n_chan-1].set_xlim([0,np.max(Xs)])


    # Output
    
    plt.subplots_adjust(hspace=0.05)
    plt.subplots_adjust(left=0.12, right=0.98, bottom=0.08, top=0.98)
    plt.savefig("{}.pdf".format(outfile_stem))
    print("Saved in {}.pdf".format(outfile_stem))

    return 0


########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--input',
        help='Input file.',
        dest='infile',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-o', '--output',
        help='Output file.',
        dest='outfile',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-s', '--start',
        help='Start time (s). Use all if <0.',
        dest='start',
        type=float,
        default=[-1.0],
        nargs=1
        )
    parser.add_argument(
        '-e', '--end',
        help='End time (s). Use all if <0.',
        dest='end',
        type=float,
        default=[-1.0],
        nargs=1
        )
    args = parser.parse_args()

    data = read_data(args.infile[0], args.start[0], args.end[0])
    make_plot(data, chnames, args.outfile[0])
