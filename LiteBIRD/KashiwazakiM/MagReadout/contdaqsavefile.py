#!/usr/bin/env py -2

############################################################################
# [Function]
# Continuous data aquisition from NI-USB 6210
#
# [Revision]
# 201X/XX/XX    M. Sawada	    Coded from the scratch.
# 2020/03/16    M. Tsujimoto    Option for record length & sampling time.
# 2020/05/18    M. Tsujimoto    Option for gain, file save. Files saved in G/rtHz and compressed.
# 2021/09/25    M. Tsujimoto    Option for max voltage setting.
# 2021/10/10	M. Tsujimoto	Relase for use in Resolve Inst-level test TC4.
#############################################################################

########################################################
# Imports
########################################################
import argparse, datetime, os,time
from sys import exit
import numpy as np
import numpy.matlib

import sys
sys.path.append('.')

#import pydaqmxdriver8 as pydaqmxdriver
#import winsound as ws

########################################################
# User-defined parameters
########################################################
chnum = 8
vol_limit_default = 0.5 # V

#topdir = '/Volumes/SXSdisturbance/contdaq'
topdir = '/home/cmb/Work/20240417_MagReadout/'
prefix = 'raw'
taskrecyclenum = 5
scrname = os.path.splitext(os.path.basename(sys.argv[0]))[0]


#########################################################
# Functions
#########################################################
#--------------------------------------------------------
def stdout(string):

	print('[%s] %s' % (scrname,string))

	return()

#--------------------------------------------------------
def data_gen(session):

	while True:
		try:
			shot_startTime,voltages = session.do_sampling()
		except:
			yield None, None
		yield shot_startTime, voltages

#--------------------------------------------------------
def main(sessionlength, samplingrate, shotlength, gains, tvlevel, vol_limit):

	fmax = samplingrate/2.
	dt = 1./samplingrate
	df = 1./shotlength
	input_min = -vol_limit
	input_max = vol_limit

	session = pydaqmxdriver.ContDaq(shotlength, samplingrate, chnum, input_min, input_max, True)
	session_startTime = time.time()

	while True:
		session.create_task()
		session.start_task()
		task_startTime = time.time()

		# Check and make save directory.
		if not os.path.exists(topdir):
			print('USB SSD seems not connected! Abort.')
			session.close_task()
			exit()
		date8 = datetime.datetime.now().strftime('%Y%m%d')
		datedir = '%s/%s' % (topdir,date8)
		if not os.path.exists(datedir):
			os.mkdir(datedir)

		# Data aquisition
		for i in range(taskrecyclenum):
			shot_startTime,voltages = data_gen(session).next()
			if shot_startTime == None:
				success = False
			else:
				success = True
			if success:
				stdout('%s: new data acquired.' % datetime.datetime.now().strftime('%H%M%S'))
			else:
				print('failed to acquire data. retrying...')
				continue

			# Format data
			shot_startDatetime = datetime.datetime.fromtimestamp(shot_startTime)
			npyname = shot_startDatetime.strftime('raw%Y%m%d-%H%M%S')
			t0dt = np.array([shot_startTime,dt])
			gains_expand = np.matlib.repmat(gains,1,int(len(voltages)/len(gains)))
			accel = voltages/gains_expand[0]
							
			# Save data.
			savedata = np.concatenate([t0dt,accel], axis=0)
			# Uncompressed save.
			#npyfile = '%s/%s.npy' % (datedir, npyname)
			#np.save(npyfile,savedata)
			# Compressed save.
			npzfile = '%s/%s.npz' % (datedir, npyname)
			np.savez_compressed(npzfile, savedata)
			stdout('%s: data saved at %s.' % (datetime.datetime.now().strftime('%H%M%S'), shot_startTime))
				
			# Beep if accel exceeds the tvlevel for each channel.
			accel_reshape = accel.T.reshape([len(accel)/chnum,chnum]).T
			for ch,data_ch in enumerate(accel_reshape):
				if np.max(np.abs(data_ch)) > tvlevel:
					ws.Beep(2000, 250)
					print('')
					print('*** Warning: voltage amplitude %f of Ch%d is too large!' % (np.max(np.abs(data_ch)) ,ch+1,))
					print('')

		session.close_task()


########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
	# Command-line parser
	parser = argparse.ArgumentParser()
	parser.add_argument(
		'-r', '--sampling_rate',
     	help='Sampling rate (Hz)',
      	dest='samplingrate',
       	type=float,
       	default=[10.e3],
        nargs=1
        )
	parser.add_argument(
        '-l', '--record_length',
        help='Record length (s)',
        dest='shotlength', 
        type=float,
        default=[32.0],
        nargs=1
        )
	parser.add_argument(
		'-g', '--gains',
     	help='Gains (V/G)',
      	dest='gains',
       	type=float,
       	default=[0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1],
        nargs=chnum,
        )
	parser.add_argument(
		'-t', '--tvlevel',
     	help='Beep threshold in G',
      	dest='tvlevel',
       	type=float,
       	default=[1e+1],
        nargs=1
        )
	parser.add_argument(
		'-v', '--vol',
     	help='Voltage limit of ADC in V',
      	dest='vol_limit',
       	type=float,
       	default=[vol_limit_default],
        nargs=1
        )
	args = parser.parse_args()

	# Print setting.
	print("Data acquisition with the following settings:")
	print(" Sampling rate = %.1f [Hz]" % args.samplingrate[0])
	print(" Record length = %.1f [s]" % args.shotlength[0])
	print(" Freq coverage = %.3f - %.1f [Hz]" % (1.0/args.shotlength[0], args.samplingrate[0]/2.0))
	print(" Gains & linearity range:")
	for ch, gain in enumerate(args.gains):
    		print("ch{} with {:.1f} [V/G] to +/-{:.1f} [V] = +/-{:.1f} [G]").format(ch, gain, args.vol_limit[0], args.vol_limit[0]/gain)

	# Run main session
	sessionlength = None
	main(sessionlength, args.samplingrate[0], args.shotlength[0], args.gains, args.tvlevel[0], args.vol_limit[0])