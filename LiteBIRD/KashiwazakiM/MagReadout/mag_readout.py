#!/usr/bin/env python

########################################################
# Imports
########################################################
import serial
import argparse
import numpy as np
import time, schedule
import datetime
import sys, os

########################################################
# User-defined parameters
########################################################
outdir_top="/home/cmb/Work/20240417_MagReadout/"
baudrate=38400 # bps
sampling=128 # Hz
device="/dev/ttyUSB0"

gain_map = {
    "1":150,
    "2":500,
    "3":2000,
    "4":64000
}

########################################################
# Function
########################################################
#------------------------------------------------------------
def convert_rawdata(lis,range_):
    
    value = int("".join(lis),16)
    if range_ == 150:
        if value < (int("FFFFFF",16))/2:
            return -value/(2**19)*range_*13.3/16
        else:
            return (int("FFFFFF",16)-value+1)/(2**19)*range_*13.3/16
    elif range_ == 500:
        if value < (int("FFFFFF",16))/2:
            return -value/(2**21)*range_
        else:
            return (int("FFFFFF",16)-value+1)/(2**21)*range_
    elif range_ > 500:
        if value < (int("FFFFFF",16))/2:
            return -value/(int("7FFFFF",16))*range_
        else:
            return ((int("FFFFFF",16))-value+1)/int("7FFFFF",16)*range_

#------------------------------------------------------------
def get_magvalue(i):

    # separate header
    header = i[:10].decode()
    value = i[11:-2].decode()
    if header[0] != "$":
        print("error")

    # convert 16 to magvalue
    n=len(value)
    value_np = np.array(list(map(str,list(value)))).reshape(n//6,6)
    range_np = [gain_map[header[2]],gain_map[header[5]],gain_map[header[8]]]*(n//6)

    # add dummy data
    a =  np.array(list(map(convert_rawdata,value_np,range_np))).reshape(len(value_np)//3,3)
    print(np.average(a,axis=0))
    print(a)
    b = np.zeros((len(value_np)//3,5))
    c = np.concatenate((a,b),axis=1)

    return c.reshape(1,len(c)*len(c[0]))[0]


#------------------------------------------------------------
def main(args):

    # Output directory
    dt = datetime.datetime.now()
    outdir = "{}/{}".format(outdir_top,dt.strftime('%Y%m%d'))
    os.makedirs(outdir, exist_ok=True)
    print("Saving date in {}",format(outdir))

    # Connect device.
    try:
        ser = serial.Serial(device,baudrate)
    except:
        print("Device connection failed.")
        sys.exit(1)

    # Define job in each loop
    line = []
    cnt = 0
    def job():

        # At every 1 min.
        if (cnt%60==0):
            savelist = [time.time(), 1.0/sampling]

        # Read data.
        while(len(line)) != 2317:
            line = ser.readline()

        # Convert data.
        result = get_magvalue(line)
        savelist = savelist + list(result)

        # At every 1 min.
        if (cnt%60==0):
            # Write data.
            dt = datetime.datetime.now()
            outfile = "{}/raw{}.npz".format(outdir,dt.strftime('%Y%m%d-%H%M%S'))
            print("Saving data to {}.".format(outfile))
            np.savez(outfile,savelist)

        # Count up
        cnt+=1

    # Loop
    schedule.every(1).seconds.do(job)
    while True:
        schedule.run_pending()
        time.sleep(0.01) 


########################################################
# Main routine
########################################################
if __name__ == "__main__":
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    args = parser.parse_args()
    main(args)




        
