#!/usr/local/conda/bin/python

"""
データ圧縮用に TOD データを整形する。
出力フォーマット :
- 単位は W。浮動小数点。
- 時刻情報は不要。20M/2**17 = 153 Hz で固定。
- 検出器情報も不要。
-- 検出器毎 (22個) に別ファイルで作成。
-- ファイル名で区別する。-o TOD_L2-050 などと指定。
- データ種類ごとに、所定の位置にデータを格納する。numpy.array([ch, datalen])
-- 0 : total ... この script で計算
-- 1 : white noise ... この script で計算
-- 2 : 1/f noise ... この script で計算
-- 3 : CR ... by Stever+
-- 4 : foreground (synchrotron) by PySM3
-- 5 : foreground (dust) by PySM3
-- 6 : foreground (free-free) by PySM3
-- 7 : foreground (ams) by PySM3
-- 8 : CMB (unisotropy) by PySM3
-- 9 : CMB (diople) by TOAST
"""

############################################################
# Imports
############################################################
import numpy as np
import argparse
import sys

########################################################                                           
# User-defined pars                                                                                        
########################################################    
# Parameters
sampling_rate_default = 20e6/2**17
knee_default = 0.02 # Hz
slope_default = 2.0 

# Files
file_cr='full_TOD.npy'
file_fg='test_fg_tod.npz'
file_dp=''

# Channel names
chnames=[
    'total', 'white noise', '1/f noise', 'CR', 'fg(sync)', 'fg(dust)', 'fg(f-f)', 'fg(ams)', 'cmb(unisotropy)', 'cmb(dipole)'
]
########################################################                                           
# Functions                                                                                        
########################################################    
#----------------------------------------------------------------------                            
def load_data(infile):
    data = np.load(infile)['arr_0']
    print("Data loaded from {}".format(infile))
    return data

#----------------------------------------------------------------------                            
def save_data(outfile, data):
    np.savez(outfile, data)
    print("Data saved in {}.".format(outfile))

#----------------------------------------------------------------------                            
# Generate white noise
def gen_white_noise(length, sampling_rate, sigma):
    amp = sigma * np.sqrt(sampling_rate) * 1e-18 # aW -> W
    return np.random.normal(loc=0.0, scale=amp, size=length)

#----------------------------------------------------------------------                            
# Generate 1/f noise
def gen_red_noise(length, sampling_rate, sigma, knee, slope):
    return np.zeros(length)

#----------------------------------------------------------------------                            
# Extract foreground
def get_fg(file_fg, num_required):
    conv = 0.10*1e-18 # uK_CMB to W
    data_fg_s = np.load(file_fg)["arr_1"][:num_required] * conv
    data_fg_d = np.load(file_fg)["arr_5"][:num_required] * conv
    data_fg_f = np.load(file_fg)["arr_2"][:num_required] * conv
    data_fg_a = np.load(file_fg)["arr_3"][:num_required] * conv
    data_fg_c = np.load(file_fg)["arr_4"][:num_required] * conv
    return data_fg_s, data_fg_d, data_fg_f, data_fg_a, data_fg_c

#----------------------------------------------------------------------                            
# Extract dipole
def get_dp(file_dp, num_required):
    return np.zeros(num_required)

#----------------------------------------------------------------------                            
# Extract CR
def get_cr(file, num_required):

    # Read seed data.
    data_cr_orig = np.load(file_cr)
    num_ch = len(data_cr_orig[0])
    num_sample = len(data_cr_orig[0][0])

    # Check total length.
    if (num_required > num_sample * num_ch):
        print("{} does not contain sufficient data.".format(file_cr))
        sys.exit(1)

    # Concat to the length required.
    ch_cur = 0
    index_cur = num_required
    data_cr = [] 
    while (index_cur > num_sample):
        data_cr.extend(data_cr_orig[1][ch_cur])
        ch_cur += 1
        index_cur -= num_sample
    data_cr.extend(data_cr_orig[1][ch_cur][:index_cur])

    return data_cr

########################################################
# Main
########################################################
if __name__ == '__main__':

    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-l', '--length',
        help='Total number of samples',
        dest='length',
        type=int,
        nargs=1,
    )
    parser.add_argument(
        '-r', '--rate',
        help='Sampling rate (Hz) ; default %.2f.' % sampling_rate_default,
        dest='sampling_rate',
        default=[sampling_rate_default],
        type=float,
        nargs='?',
    )
    parser.add_argument(
        '-w', '--white_noise',
        help='White noise level (aW/rtHz)',
        dest='wn',
        type=float,
        nargs=1,
    )
    parser.add_argument(
        '-k', '--knee',
        help='1/f noise fnee (Hz) ; default %.3f.' % knee_default,
        dest='knee',
        default = [knee_default], 
        type=float,
        nargs='?',
    )
    parser.add_argument(
        '-s', '--slope',
        help='1/f noise slope ; default %.2f.' % slope_default,
        dest='slope',
        default=[slope_default],
        type=float,
        nargs='?',
    )
    parser.add_argument(
        '-o', '--outfile',
        help='Output file',
        dest='outfile',
        type=str,
        nargs=1,
    )
    args = parser.parse_args()
    
    ack_par="""
    Label : {}
    Length : {} samples
    Data rate : {} Hz
    White noise : {} aW/rtHz
    1/f noise knee : {} Hz
    1/f noise slope : {}
    """.format(args.label[0], args.length[0], args.sampling_rate[0], args.wn[0], args.knee[0], args.slope[0])
    print(ack_par)

    # Assemble data.
    data_wn = gen_white_noise(args.length[0], args.sampling_rate[0], args.wn[0])
    data_rn = gen_red_noise(args.length[0], args.sampling_rate[0], args.wn[0], args.knee[0], args.slope[0])
    data_cr = get_cr(file_cr, args.length[0])
    data_fg_s, data_fg_d, data_fg_f, data_fg_a, data_fg_c = get_fg(file_fg, args.length[0])
    data_dp = get_dp(file_dp, args.length[0])

    data_tot = data_wn + data_rn + data_cr + data_fg_s + data_fg_d + data_fg_f + data_fg_a + data_fg_c + data_dp
    data_all = np.array([data_tot, data_wn, data_rn, data_cr, data_fg_s, data_fg_d, data_fg_f, data_fg_a, data_fg_c, data_dp])
    
    # Save file.
    save_file(args.outfile[0], data_all)