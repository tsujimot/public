#!/usr/local/conda/bin/python

"""
Encoding
"""

############################################################
# Imports
############################################################
import numpy as np
import argparse

from CR_calc_comp1 import load_data, save_data
from CR_calc_comp2 import vdigitize

############################################################
# User-defined pars  
############################################################
encodings=[
    'huffman', 'gr', 'weyl'
]

############################################################
# Functions                                                                                        
############################################################
#----------------------------------------------------------------------
def encoding_gr(data, encpars):


    aho
    data_new = np.diff(data)
    return data_new

#----------------------------------------------------------------------
def golomb_enc(x,m):
    c = int(math.ceil(math.log(m,2)))
    remin = x % m
    quo =int(math.floor(x / m))
    #print "quo is",quo
    #print "reminder",remin
    #print "c",c
    div = int(math.pow(2,c) - m)
    #print "div",div
    first = ""
    for i in range(quo):
	first = first + "1"
    #print first

    if (remin < div):
	b = c - 1
	a = "{0:0" + str(b) + "b}"
	#print "1",a.format(remin)
	bi = a.format(remin)
    else:
	b = c
	a = "{0:0" + str(b) + "b}"
	#print "2",a.format(remin+div)
	bi = a.format(remin+div)

    final = first + "0" +str(bi)
    #print "final",final
    return final


########################################################
# Main
########################################################
if __name__ == '__main__':

    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--input',
        help='Input file.',
        dest='infile',
        type=str,
        nargs=1,
    )
    parser.add_argument(
        '-o', '--output',
        help='Output file.',
        dest='outfile',
        type=str,
        nargs=1,
    )
    parser.add_argument(
        '-e', '--encoding',
        help='Encoding algorithm.',
        dest='encoding',
        choices=encodings,
        type=str,
        nargs=1,
    )
    parser.add_argument(
        '-p', '--par',
        help='Encoding parameter.',
        dest='encpars',
        default=[0],
        type=int,
        nargs='?',
    )
    args = parser.parse_args()

    # Load data
    data = load_data(args.infile[0])

    # Smooth data
    encoding = eval('encoding_'+args.encoding[0])
    data = encoding(data, args.encpars)

    # Save data
    save_data(args.outfile[0], data)
