#!/usr/local/conda/bin/python

"""
Component 毎に
1. TOD 表示
2. 頻度分布表示
3. 情報エントロピー計算
4. Golomb-Rice encoding
"""

############################################################
# Imports
############################################################
import numpy as np
import pandas as pd
import argparse

# Plotly offline       
import plotly                                                                            
import plotly.offline as offline
import plotly.graph_objects as go
import plotly.io as pio

from CR_calc_comp1 import load_data, save_data, chnames

############################################################
# User-defined pars
############################################################
m_max=12
ms=[2**int(i) for i in np.linspace(1,m_max,m_max)] # Rice par

############################################################
# Functions
############################################################
#--------------------------------------------------------------------------------
def calc_entropy(hist):
    "Calculate info entropy."

    hist_nz = hist[hist>0]
    norm_hist_nz = hist_nz.sum()
    prob = hist_nz / float(norm_hist_nz)
    plogp = -prob*np.log2(prob)
    return plogp.sum()

#--------------------------------------------------------------------------------
def positive(x):
    "Reflect at x."

    if (x>=0):
        return 2*x
    else:
        return (-x-1)*2+1

#--------------------------------------------------------------------------------
def calc_gr(hist, m):
    "Calc Rice coding average bits."

    Xs=hist[-1]
    Ys=hist[0]

    df = pd.DataFrame([Xs[:-1], Ys]).T
    df.columns=['X','Y']
    df['X_new'] = df['X'].apply(lambda x : positive(x))
    df.sort_values('X_new', inplace=True)
    df['q'] = df['X_new']//m
    df['r'] = df['X_new']%m
    df['a'] = df['q'] + int(np.log2(m))
    df['tot_a'] = df['a'] * df['Y']
    tot = df['Y'].values.sum()
    tot_a=df['tot_a'].values.sum()
    return tot_a, tot

#--------------------------------------------------------------------------------
def calc_hist(data):

    assert len(data) == len(chnames)

    max, min = np.max(data), np.min(data)
    bins = np.linspace(min, max, max-min+1)
    hists=[]

    # Iterate over channels.
    for i, chname in enumerate(chnames):
        datum = data[i]
        max, min = np.max(datum), np.min(datum)
        hist=np.histogram(data[i], bins=bins)
        entropy=calc_entropy(hist[0])
        hists.append(hist[0])
        print("{:20} : min={:8}, max={:8}, entropy={:4.1f} bits".format(chname, min, max, entropy))

    # Add bins
    hists.append(hist[1])

    return hists

#--------------------------------------------------------------------------------
# Define layout
def get_layout(title, xtitle, ytitle, xtype, ytype):

    layout = go.Layout(
        title=title,
        xaxis=dict(
            title = xtitle,
            type = xtype,
            autorange = True,
            tickmode = 'auto'
            ),
        yaxis=dict(
            title = ytitle,
            type = ytype,
            autorange = True,
            tickmode = 'auto',
            ),
        font=dict(
            size=16
            ),
        legend={
            'yanchor' : "top",
            'y': 0.99,
            'xanchor' : "left",
            'x' : 0.01,
            'orientation' : "h",
        },
        #paper_bgcolor='rgba(0,0,0,0)',
        #plot_bgcolor='rgba(0,0,0,0)'
        )
    return layout

#--------------------------------------------------------------------------------
def plot_tod(data, infile, outstem, limit):

    title="TOD ({})".format(infile)
    xtitle="Sample (6.55 ms/sample)"
    ytitle="ADC"
    xtype="linear"
    ytype="linear"
    outstem = outstem + "_tod"

    # Define layout
    layout = get_layout(title, xtitle, ytitle, xtype, ytype)

    # Define data
    trace = []
    num_samples = len(data[0])
    Xs = np.linspace(0,num_samples-1,num_samples)
    for i, chname in enumerate(chnames):
        trace.append(go.Scatter(x=Xs[:limit], y=data[i][:limit], name=chname))

    # Define figure
    fig = go.Figure(layout=layout, data=trace)

    # Save file.
    offline.plot(fig, filename='%s.html' % outstem, auto_open=False, include_mathjax='cdn')
    print("File saved in %s.html." % (outstem))

    return 0

#--------------------------------------------------------------------------------
def plot_hist(hists, infile, outstem, norm=True):

    title="Histogram ({})".format(infile)
    xtitle="ADC"
    if (norm is True):
        ytitle="Number norm'ed at peak."
    else:
        ytitle="Number"
    xtype="linear"
    ytype="linear"
    outstem = outstem + "_hist"

    # Define layout
    layout = get_layout(title, xtitle, ytitle, xtype, ytype)

    # Define data
    trace = []
    Xs = hists[-1]
    for i, chname in enumerate(chnames):
        if (chname != '1/f noise') and (chname != 'cmb(dipole)'): # Not implemented yet.
            if (norm is True):
                Ys = hists[i] / np.max(hists[i])
            else:
                Ys = hists[i]
            trace.append(go.Scatter(x=Xs, y=Ys, name=chname))

    # Define figure
    fig = go.Figure(layout=layout, data=trace)

    # Save file.
    offline.plot(fig, filename='%s.html' % outstem, auto_open=False, include_mathjax='cdn')
    print("File saved in %s.html." % (outstem))

    return 0


########################################################
# Main
########################################################
if __name__ == '__main__':

    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--input',
        help='Input file.',
        dest='infile',
        type=str,
        nargs=1,
    )
    parser.add_argument(
        '-o', '--outstem',
        help='Output file stem.',
        dest='outstem',
        type=str,
        nargs=1,
    )
    parser.add_argument(
        '-limit', '--limit',
        help='Limit the number of samples to display.',
        dest='limit',
        type=int,
        default=50000,
        nargs='?',
    )
    args = parser.parse_args()

    # Load data
    data = load_data(args.infile[0])

    # Plot TOD
    plot_tod(data, args.infile[0], args.outstem[0], args.limit)

    # Calc histogram and info entropy
    hist = calc_hist(data)

    # Calc histogram and info entropy
    print("Rice encoding: m and average bits.")
    for m in ms:
        tot_a, tot = calc_gr(hist, m)
        print("{:5d} : {:7.1f} bits".format(m, tot_a/tot))

    # Plot histogram (absolute)
    plot_hist(hist, args.infile[0], args.outstem[0], norm=False)

    # Plot histogram (relative)
    plot_hist(hist, args.infile[0], args.outstem[0]+'_norm', norm=True)

