#!/usr/local/conda/bin/python

"""
様々な digital processing
"""

############################################################
# Imports
############################################################
import numpy as np
import argparse
from scipy.optimize import curve_fit

from CR_calc_comp1 import load_data, save_data
from CR_calc_comp2 import vdigitize

############################################################
# User-defined pars
############################################################
smoothings=[
    'diff', # take diff from the previous sample.
    'cic', # CIC filter with a width given by "par".
    'fir', # FIR filter with a pre-defined coeff.
    'resample', # Resample by a factor given by "par".
    'fit0', # Fit the "par" number of samples with a constant (c) value and subtract it.
    'fit1', # Fit the "par" number of samples with a linear function (y=ax+b) and subtract it.
    'fit2', # Fit the "par" number of samples with a square function (y=ax^2+bx+c) and subtract it.
    'fit3', # Fit the "par" number of samples with a cubic function (y=ax^3+bx^2+cx+d) and subtract it.
    'pcd',  # Subtract predictable components.
    'tpleak' # Fit T/P leakage component. "par" is the rpm of HWP.
]

############################################################
# Functions
############################################################
#----------------------------------------------------------------------
def smoothing_diff(data, smo_par, subtract=True):
    data_new = np.diff(data)
    return data_new

#----------------------------------------------------------------------
def smoothing_cic(data, smo_par, subtract=True):

    window = smo_par
    w = np.ones(window)/window
    data_new=[]
    for i in range(len(data)):
        if subtract is True:
            datum_new = np.convolve(data[i], w, mode='same') - data[i]
        else:
            datum_new = np.convolve(data[i], w, mode='same')
        data_new.append(datum_new)
    return vdigitize(np.array(data_new))

#----------------------------------------------------------------------
def smoothing_fir(data, smo_par, subtract=True):
    data_new = data

    return vdigitize(data_new)

#----------------------------------------------------------------------
def smoothing_resample(data, smo_par, subtract=True):
    data_new=[]
    for i in range(len(data)):
        data_new.append(data[i][::smo_par])

    return np.array(data_new)

#----------------------------------------------------------------------
def fit0(x, par):
    a=par
    return a

def fit1(x, par):
    a, b = par
    return a*x + b

def fit2(x, par):
    a, b, c = par
    return a*x*x + b*x + c

def fit3(x, par):
    a, b, c, d = par
    return a*x*x*x ; b*x*x + c*x + d


#----------------------------------------------------------------------
def smoothing_fit(data, smo_par, fit, subtract=True):

    data_new = data

    # Loop
    num_chunk=int(len(data[0])/smo_par)
    Xs=np.linspace(0,smo_par-1,smo_par)
    for i in range(num_chunk):
        Ys=data[0][i*smo_par:(i+1)*smo_par]
        param, cov = curve_fit(fit, Xs, Ys)
        param = [int(x) for x in param]
        Ys_fit = [fit(x, param)[0] for x in Xs]
        if (subtract is True):
            data_new[0][i*smo_par:(i+1)*smo_par] = Ys - Ys_fit
        else:
            data_new[0][i*smo_par:(i+1)*smo_par] = Ys_fit

    # Process remainders if sufficient number left.
    Ys=data[0][(i+1)*smo_par:]
    len_remainder = len(Ys)
    if (len_remainder>0):
        Xs=np.linspace(0, len_remainder-1,len_remainder)
        param, cov = curve_fit(fit, Xs, Ys)
        param = [int(x) for x in param]
        Ys_fit = [fit(x, param)[0] for x in Xs]
        if (subtract is True):
            data_new[0][(i+1)*smo_par:len(data_new[0])] = Ys - Ys_fit
        else:
            data_new[0][(i+1)*smo_par:len(data_new[0])] = Ys_fit
    # Else, return 0.
    else:
        data_new[0][(i+1)*smo_par:] = 0

    return vdigitize(data_new)
#----------------------------------------------------------------------
def tpleak(x, A, phi, c):
    #A, phi, c = par
    
    return A*np.sin(x+phi) + c 

def smoothing_tpleak(data, smo_par, f_hwp, subtract=True):
    
    fit = tpleak
    w_hwp = 2.0 * np.pi * f_hwp / 60.0
    data_new = data

    # Loop
    num_chunk=int(len(data[0])/smo_par)
    Xs=np.linspace(0,smo_par-1,smo_par)
    for i in range(num_chunk):
        Ys=data[0][i*smo_par:(i+1)*smo_par]
        param, cov = curve_fit(fit, Xs*2*w_hwp, Ys)
        #param = [int(x) for x in param]
        Ys_fit = [fit(x, param[0], param[1], param[2]) for x in Xs]
        if (subtract is True):
            data_new[0][i*smo_par:(i+1)*smo_par] = Ys - Ys_fit
        else:
            data_new[0][i*smo_par:(i+1)*smo_par] = Ys_fit

    # Process remainders if sufficient number left.
    Ys=data[0][(i+1)*smo_par:]
    len_remainder = len(Ys)
    if (len_remainder>0):
        Xs=np.linspace(0, len_remainder-1,len_remainder)
        param, cov = curve_fit(fit, Xs*2*w_hwp, Ys)
        #param = [int(x) for x in param]
        Ys_fit = [fit(x, param[0], param[1], param[2]) for x in Xs]
        if (subtract is True):
            data_new[0][(i+1)*smo_par:len(data_new[0])] = Ys - Ys_fit
        else:
            data_new[0][(i+1)*smo_par:len(data_new[0])] = Ys_fit
    # Else, return 0.
    else:
        data_new[0][(i+1)*smo_par:] = 0

    return vdigitize(data_new)

#----------------------------------------------------------------------
def smoothing_pcd(data, smo_par, subtract=True):

    data_new = data
    if (subtract is True):
        data_new[0][:] = data[1][:] + data[2][:] + data[3][:]
    else:
        data_new[0][:] = data[0][:] - data[1][:] - data[2][:] - data[3][:]
    return data_new


########################################################
# Main
########################################################
if __name__ == '__main__':

    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--input',
        help='Input file.',
        dest='infile',
        type=str,
        nargs=1,
    )
    parser.add_argument(
        '-o', '--output',
        help='Output file.',
        dest='outfile',
        type=str,
        nargs=1,
    )
    parser.add_argument(
        '-s', '--smoothing',
        help='Smoothing algorithm.',
        dest='smoothing',
        choices=smoothings,
        type=str,
        nargs=1,
    )
    parser.add_argument(
        '-n', '--smo_par',
        help='Smoothing parameter.',
        dest='smo_par',
        default=0,
        type=int,
        nargs='?',
    )
    parser.add_argument(
        '-r', '--rpm_hwp',
        help='Number of HWP rotation per sample.',
        dest='f_hwp',
        default=46.0/60/19.1,
        type=float,
        nargs='?',
    )
    parser.add_argument(
        '-S', '--subtract',
        help='Subtract predicted TOD or not.',
        dest='flg_subtract',
        default=True,
        action='store_true'
        )
    args = parser.parse_args()

    # Load data
    data = load_data(args.infile[0])

    # Smooth data
    if ("fit" in args.smoothing[0]):
        fit = eval(args.smoothing[0])
        data = smoothing_fit(data, args.smo_par, fit, subtract=args.flg_subtract)
    elif ("tpleak" in args.smoothing[0]):
        smoothing = eval('smoothing_'+args.smoothing[0])
        data = smoothing(data, args.smo_par, args.f_hwp, subtract=args.flg_subtract)
    else:
        smoothing = eval('smoothing_'+args.smoothing[0])
        data = smoothing(data, args.smo_par, subtract=args.flg_subtract)

    # Save data
    save_data(args.outfile[0], data)
