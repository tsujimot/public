#!/usr/local/conda/bin/python

"""
1. デジタル化
2. クリッピング
"""

############################################################
# Imports
############################################################
import numpy as np
import argparse

from CR_calc_comp1 import load_data, save_data

############################################################
# User-defined pars  
############################################################


############################################################
# Functions                                                                                        
############################################################
#----------------------------------------------------------------------
def digitize(x):
    if (x < 0.0):
        y = int(x-0.5)
    else:
        y = int(x+0.5)
    return int(y)
vdigitize = np.vectorize(digitize)

#----------------------------------------------------------------------
def clip(x, msb_int):
    if (x > msb_int-1):
        y = msb_int-1
        print("Clipped at upper bound for {}.".format(x))
    elif (x < -msb_int):
        y = -msb_int
        print("Clipped at lower bound for {}.".format(x))
    else:
        y = x
    return y
vclip = np.vectorize(clip)


#----------------------------------------------------------------------                            
def adc(data, msb, bits):

    msb_int = int(2**(bits-1))
    data /= msb
    data *= msb_int
    data = vdigitize(data)
    data = vclip(data, msb_int)

    return data


########################################################
# Main
########################################################
if __name__ == '__main__':

    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--input',
        help='Input file.',
        dest='infile',
        type=str,
        nargs=1,
    )
    parser.add_argument(
        '-o', '--output',
        help='Output file.',
        dest='outfile',
        type=str,
        nargs=1,
    )
    parser.add_argument(
        '-m', '--msb',
        help='MSB (pW).',
        dest='msb',
        type=float,
        nargs=1,
    )
    parser.add_argument(
        '-b', '--bit',
        help='Dynamic range (including oversampling, signs) in bits.',
        dest='bits',
        type=int,
        nargs=1,
    )
    args = parser.parse_args()

    # Load data
    data = load_data(args.infile[0])

    # Digitize data
    data = adc(data, args.msb[0]*1e-12, args.bits[0])

    # Save data
    save_data(args.outfile[0], data)
