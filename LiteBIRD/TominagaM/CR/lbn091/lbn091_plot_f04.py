#!/usr/bin/env python

########################################################
# [Description]
# Convert the root file to projected distribution on x,y,z planes.
########################################################


############################################################
# Imports
############################################################
import pandas as pd
import numpy as np
import argparse
import plotly.graph_objects as go


########################################################                                           
# User-defined pars                                                                                        
########################################################    


########################################################                                           
# Functions                                                                                        
########################################################    
#----------------------------------------------------------------------   
def main(infile, num):

    outfile_stem=infile.replace(".pkl","")

    pd_dtree_valid=pd.read_pickle(infile)

    fig = go.Figure(data=go.Scattergl(
        x = pd_dtree_valid["x2"],
        y = pd_dtree_valid["y2"],
        mode='markers',
        #marker=dict(
        #    color=np.random.randn(N),
        #    colorscale='Viridis',
        #    line_width=1
        #)
    ))
    fig.update_xaxes(range=[-5,5], title="x2")
    fig.update_yaxes(range=[-5,5], title="y2")
    fig.update_layout(
        autosize=False,
        width=800,
        height=800,
        margin=dict(
            l=50,
            r=50,
            b=100,
            t=100,
            pad=4
        ),
    )
    fig.write_html("{}_xy.html".format(outfile_stem))
    print("Saved in {}_xy.html".format(outfile_stem))
    fig.write_image("{}_xy.png".format(outfile_stem))
    print("Saved in {}_xy.png".format(outfile_stem))

    fig = go.Figure(data=go.Scattergl(
        x = pd_dtree_valid["x2"],
        y = pd_dtree_valid["z2"],
        mode='markers',
        #marker=dict(
        #    color=np.random.randn(N),
        #    colorscale='Viridis',
        #    line_width=1
        #)
    ))
    fig.update_xaxes(range=[-5,5], title="x2")
    fig.update_yaxes(range=[-5,5], title="z2")
    fig.update_layout(
        autosize=False,
        width=800,
        height=800,
        margin=dict(
            l=50,
            r=50,
            b=100,
            t=100,
            pad=4
        ),
    )
    fig.write_image("{}_xz.png".format(outfile_stem))
    print("Saved in {}_xz.png".format(outfile_stem))
    fig.write_html("{}_xz.html".format(outfile_stem))
    print("Saved in {}_xz.html".format(outfile_stem))

    fig = go.Figure(data=go.Scattergl(
        x = pd_dtree_valid["y2"],
        y = pd_dtree_valid["z2"],
        mode='markers',
        #marker=dict(
        #    color=np.random.randn(N),
        #    colorscale='Viridis',
        #    line_width=1
        #)
    ))
    fig.update_xaxes(range=[-5,5], title="y2")
    fig.update_yaxes(range=[-5,5], title="z2")
    fig.update_layout(
        autosize=False,
        width=800,
        height=800,
        margin=dict(
            l=50,
            r=50,
            b=100,
            t=100,
            pad=4
        ),
    )
    fig.write_image("{}_yz.png".format(outfile_stem))
    print("Saved in {}_yz.png".format(outfile_stem))
    fig.write_html("{}_yz.html".format(outfile_stem))
    print("Saved in {}_yz.html".format(outfile_stem))

    fig = go.Figure(data=go.Scatter3d(
        x = pd_dtree_valid["x2"],
        y = pd_dtree_valid["y2"],
        z = pd_dtree_valid["z2"],
        mode='markers',
        marker=dict(
            size=1,
            color=pd_dtree_valid["no2"],
            opacity=0.8
        )
        #marker=dict(
        #    color=np.random.randn(N),
        #    colorscale='Viridis',
        #    line_width=1
        #)
    ))
    """
    fig.update_xaxes(range=[-5,5], title="x")
    fig.update_yaxes(range=[-5,5], title="y")
    fig.update_zaxes(range=[-5,5], title="z")

    fig.update_layout(
        autosize=False,
        width=800,
        height=800,
        margin=dict(
            l=50,
            r=50,
            b=100,
            t=100,
            pad=4
        ),
    )
"""
    fig.write_image("{}_xyz.png".format(outfile_stem))
    print("Saved in {}_xyz.png".format(outfile_stem))
    fig.write_html("{}_xyz.html".format(outfile_stem))
    print("Saved in {}_xyz.html".format(outfile_stem))

    return 0


########################################################
# Main
########################################################
if __name__ == '__main__':

    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--infile',
        help='The input pkl file',
        dest='infile',
        type=str,
    )
    parser.add_argument(
        '-n', '--num',
        help='Maximum number of events to plot. 0 for all.',
        dest='num',
        default=0,
        type=int,
    )
    args = parser.parse_args()

    main(args.infile, args.num)

