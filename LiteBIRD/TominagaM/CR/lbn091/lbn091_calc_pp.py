#!/usr/bin/env python

########################################################
# Decription
########################################################
# Post process the Geant4 root products
# (1) Add weight based on the input spectrum by -w.
# (2) Calculate deposit energy for each component.
# (3) Actual time of simulation.


########################################################
# Usage
########################################################
# python lbn091_calc_pp.py -i 1710857640.root -w weight_gcr.npz


########################################################
# Imports
########################################################
import numpy as np
import pandas as pd
import argparse
import uproot
from scipy import interpolate


########################################################
# User-defined parameters
########################################################
Emin=0.1 # MeV
Emax=1000.0 # MeV
Ebin=0.001 # keV
Enum=int((Emax-Emin)/Ebin)
Es = np.linspace(Emin, Emax, Enum+1)

comps=["world", "lenslet", "seatingwafer", "invar", "foil", "wafer", "backshort"] 
keys=['evt', 'x', 'y', 'z', 'time', 'ekin', 'edep', 'no1', 'run', 'px', 'py', 'pz', 'proc', 'particle', 'x2', 'y2', 'z2', 'px2', 'py2', 'pz2', 'proc2', 'time2', 'ekin2', 'no2', 'eini']

########################################################
# Functions
########################################################
#------------------------------------------------------------
def calc_stat(pd_dtree, sum_spec_weight):
    
    total_num = pd_dtree["weight"].sum()
    dt = total_num / (sum_spec_weight * 0.1 * 0.1 * 2 * 3.14) # n/ (n/str/m2/s * m2 * str) = s

    print("Simulation time: {:.2e} s".format(dt))
    print("")
    print("Total hit rate: {:.2e} /s/cm2".format(total_num/dt/100.))
    print("Total deposit energy: {:.2e} MeV/s".format(pd_dtree["w_edep"].sum()/dt))
    for i_comp, comp in enumerate(comps):
        print("")
        print("Hit rate in {}: {:.2e} /s/cm2".format(comp, pd_dtree[pd_dtree["no2"]==i_comp]["weight"].sum()/dt/100.))
        print("Deposit energy in {}: {:.2e} MeV/s".format(comp, pd_dtree[pd_dtree["no2"]==i_comp]["w_edep"].sum()/dt))

    return 0

#------------------------------------------------------------
def main(args):

    # Read events.
    infile = args.infile[0]
    outfile = infile.replace(".root", ".pkl")
    
    with uproot.open(infile) as file:
        dtree = file['data;1']
        
        pd_dtree = None
        #keys=["evt", "ekin2"]
        keys=dtree.keys()
        for key in keys:
            print("Adding {}".format(key))
            _d = pd.DataFrame(dtree[key].array())
            if (pd_dtree is None):
                pd_dtree = _d
            else:
                pd_dtree = pd.concat([pd_dtree, _d], axis=1)
        pd_dtree.columns=keys
        print("Loaded {} events in {}.".format(len(pd_dtree), infile))
        print("Loaded {} primaries in {}.".format(pd_dtree.tail(1)["evt"].values[0]+1, infile))

        Ekins = pd_dtree[["evt","ekin2"]].groupby("evt").max()
        Ekins.reset_index(inplace=True)
        pd_dtree["einit"]=pd_dtree["evt"].apply(lambda x: Ekins[Ekins.evt==x]["ekin2"].values[0])

        # Calculate weight.
        if (args.weight[0] is not None):
            
            # Read weight file.
            spec_weight_np = np.load(args.weight[0])["arr_0"]
            spec_weight = pd.DataFrame(spec_weight_np, columns=["E", "I"])
            dE=[0]
            dE.extend(np.diff(spec_weight["E"]))
            spec_weight["dE"] = dE

            # Interpolate weight.
            spec_interp = interpolate.interp1d(spec_weight["E"].values, spec_weight["I"].values, fill_value='extrapolate')
            new_weight = pd.DataFrame(Es, columns=["E"])
            new_weight["I"] = spec_interp(new_weight["E"].values)
            new_weight["I/max"] = new_weight["I"] / np.max(new_weight["I"])
            sum_spec_weight = (np.diff(Es) * new_weight["I"].values[1:]).sum()

            # Ekins should be the max Ekin among evt.
            pd_dtree["weight"]=pd_dtree["einit"].apply(lambda x: spec_interp(x))
        else:
            pd_dtree['weight'] = pd_dtree['ekin'].apply(lambda x: 1.0)
            sum_spec_weight = 1.0
        
        pd_dtree["w_edep"] = pd_dtree["edep"] * pd_dtree["weight"]

        # Calculate statistics.
        calc_stat(pd_dtree, sum_spec_weight)

        # Save file.        
        pd_dtree.to_pickle("{}".format(outfile))
        print("{:10d} events saved in {}.".format(len(pd_dtree), outfile))

    return 0

########################################################
# Main routine
########################################################
if __name__ == '__main__':

    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--infile',
        help='Input file.',
        dest='infile',
        type=str,
        nargs=1,
        )
    parser.add_argument(
        '-w', '--weight',
        help='Weight file (photon/s/keV).',
        dest='weight',
        type=str,
        nargs=1,
        default=[None],
        )
    args = parser.parse_args()

    main(args)
