#!/usr/bin/env python

########################################################
# [Description]
# Convert the root file to vtu file for display with paraview.
########################################################


############################################################
# Imports
############################################################
import uproot
import pandas as pd
import numpy as np
import argparse
from pyevtk.hl import pointsToVTK


########################################################                                           
# User-defined pars                                                                                        
########################################################    
cnt_min_frac=0.001 # minimum fraction of counts to show for enumeration (only for displaying purpose).


########################################################                                           
# Functions                                                                                        
########################################################    
#----------------------------------------------------------------------   
def main(args):

    infile=args.infile
    components=args.components
    maxnum=args.maxnum
    xmin, xmax=args.rx
    ymin, ymax=args.ry
    zmin, zmax=args.rz

    outfile=infile.replace(".root", "")

    with uproot.open(infile) as file:
        dtree = file['data;1']
        pd_dtree = None
        for key in dtree.keys():
            print("Adding {}".format(key))
            _d = pd.DataFrame(dtree[key].array())
            if (pd_dtree is None):
                pd_dtree = _d
            else:
                pd_dtree = pd.concat([pd_dtree, _d], axis=1)
        pd_dtree.columns=dtree.keys()

        # Clean events
        #pd_dtree_valid=pd_dtree[(pd_dtree.time>0) & (pd_dtree.edep>0)]
        pd_dtree_valid=pd_dtree[(pd_dtree.no1>-1) & (pd_dtree.time2>0)]
    
        #pd_dtree_valid=pd_dtree

        
        # Select events
        pd_dtree_valid=pd_dtree_valid[(pd_dtree_valid.x>xmin) & (pd_dtree_valid.x<xmax) & (pd_dtree_valid.y>ymin) & (pd_dtree_valid.y<ymax) & (pd_dtree_valid.z>zmin) & (pd_dtree_valid.z<zmax)]
        print("Removed events outside of x:{} to {}".format(xmin,xmax))
        print("Removed events outside of y:{} to {}".format(ymin,ymax))
        print("Removed events outside of z:{} to {}".format(zmin,zmax))
        if (components is not None):
            pd_dtree_valid["no2_str"]=pd_dtree_valid["no2"].apply(lambda x: str(int(x)))
            pd_dtree_valid=pd_dtree_valid[pd_dtree_valid["no2_str"].isin(components)]
            print("Components {} is selected".format(components))
            outfile="{}_{}".format(outfile,"_".join(components))
        if (maxnum>0):
            pd_dtree_valid = pd_dtree_valid.head(maxnum)
            print("Limited to {} events.".format(maxnum))  
        else:
            print("Number of events {}.".format(len(pd_dtree_valid)))

        # Add info
        pd_dtree_valid["cnt"]=1
        pd_dtree_valid["logedep"]=pd_dtree_valid["edep"].apply(lambda x: (np.log(x)/np.log(10)) if x>0 else np.nan)
        #pd_dtree_valid["logedep"]=pd_dtree_valid["edep"].apply(lambda x: 5*(np.log(x)/np.log(10)+1.0+0.1) if x > 0.1 else 0.5)

        # Enumerate
        def enum_property(property):
            sum=(pd_dtree_valid[["cnt", property]]).groupby(property).sum().sort_values(by=["cnt"], ascending=False)
            sum.reset_index(inplace=True)
            print("Enumeration of {}.".format(property))
            print(sum[sum.cnt>cnt_min_frac*len(pd_dtree_valid)])
            sum.to_pickle("{}_{}.pkl".format(outfile,property))
            print("Saved in {}_{}.pkl".format(outfile,property))

            enum=sum[property].values
            pd_dtree_valid["enum_{}".format(property)]=pd_dtree_valid[property].apply(lambda x: list(enum).index(x))

        enum_property("particle")
        enum_property("proc")
        enum_property("proc2")
        enum_property("no1")
        enum_property("no2")

        # Save pkl file.
        pd_dtree_valid.to_pickle("{}.pkl".format(outfile))
        print("Saved in {}.pkl".format(outfile))

        # Save vtu file.
        x=pd_dtree_valid["x"].values
        y=pd_dtree_valid["y"].values
        z=pd_dtree_valid["z"].values
        x2=pd_dtree_valid["x2"].values
        y2=pd_dtree_valid["y2"].values
        z2=pd_dtree_valid["z2"].values
        pos=np.ones(len(pd_dtree_valid))
        comp=pd_dtree_valid["no1"].values
        comp2=pd_dtree_valid["no2"].values
        edep=pd_dtree_valid["edep"].values
        logedep=pd_dtree_valid["logedep"].values
        ekin=pd_dtree_valid["ekin"].values
        ekin2=pd_dtree_valid["ekin2"].values
        enum_particle=pd_dtree_valid["enum_particle"].values
        enum_proc=pd_dtree_valid["enum_proc"].values
        enum_proc2=pd_dtree_valid["enum_proc2"].values
        point_data = {"pos": pos, "comp": comp, "comp2": comp2, "Edep": edep, "logEdep": logedep, "Ekin": ekin, "Ekin2": ekin2, "particle": enum_particle, "proc": enum_proc, "proc2": enum_proc2}
        pointsToVTK("{}".format(outfile), x2, y2, z2, data=point_data)
        print("Saved in {}.vtu".format(outfile))

    return 0


########################################################
# Main
########################################################
if __name__ == '__main__':

    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--infile',
        help='The input root file',
        dest='infile',
        type=str,
    )
    parser.add_argument(
        '-c', '--components',
        help='The component number to exract.',
        dest='components',
        #type=int,
        nargs='+',
        default=None,
    )
    parser.add_argument(
        '-N', '--maxnum',
        help='Maximum number of events to extract.',
        dest='maxnum',
        type=int,
        default=0,
    )
    parser.add_argument(
        '--rx',
        help='Range of x',
        dest='rx',
        nargs=2,
        type=float,
        default=[-10,10],
    )
    parser.add_argument(
        '--ry',
        help='Range of y',
        dest='ry',
        nargs=2,
        type=float,
        default=[-10,10],
    )
    parser.add_argument(
        '--rz',
        help='Range of z',
        dest='rz',
        nargs=2,
        type=float,
        default=[-10,10],
    )
    args = parser.parse_args()
    main(args)

