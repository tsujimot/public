#!/usr/bin/env python

########################################################
# Decription
########################################################
# Make energy spectrum from ROOT outputs of Geant4 with weighting.
# Usage:
# python post_proc.py -i 1710857640.root -o 1710857640 -w weight_gcr.npz

# output: pkl file including weighted deposit energy.
# txt output: simulatiob corresponding time (sec).

########################################################
# Imports
########################################################
import os
import numpy as np
import pandas as pd
import argparse
import pickle
import uproot
from scipy import interpolate

########################################################
# User-defined parameters
########################################################
Emin=0.1 # MeV
Emax=1000.0 # MeV
Ebin=0.001 # keV
Enum=int((Emax-Emin)/Ebin)
Es = np.linspace(Emin, Emax, Enum+1)

comps=["world", "lenslet", "seatingwafer", "invar", "foil", "wafer", "backshort"] 
keys=['evt', 'time', 'ekin', 'edep', 'no1', 'run', 'proc', 'particle', 'proc2', 'time2', 'ekin2', 'no2', 'eini']

########################################################
# Functions
########################################################
#------------------------------------------------------------
def collate_evt(infile, max=-1):
    
    with uproot.open(infile) as file:
        dtree = file['data;1']
        pd_dtree = None
        #keys=dtree.keys()
        for key in keys:
            print("Adding {}".format(key))
            _d = pd.DataFrame(dtree[key].array())
            if (pd_dtree is None):
                pd_dtree = _d
            else:
                pd_dtree = pd.concat([pd_dtree, _d], axis=1)
        pd_dtree.columns=keys
        
        if (max!=-1):
            pd_dtree = pd_dtree.head(max)  
        print("Loaded {} events in {}.".format(len(pd_dtree), infile))
        print("Loaded {} primaries in {}.".format(pd_dtree.tail(1)["evt"].values[0]+1, infile))
        return pd_dtree
    return None

"""
    with uproot.open(infile) as _infile:
        dtree = _infile['data;1']
        df_dtree=dtree.arrays(dtree.keys(), library="pd")
        print("Number of events.")
        print(len(dtree['evt'].array()))
        print("Event properties.")
        print(dtree.keys())

        # d_tree
        d_evt=pd.DataFrame(dtree['evt'].array())
        d_x=pd.DataFrame(dtree['x'].array())
        d_y=pd.DataFrame(dtree['y'].array())
        d_z=pd.DataFrame(dtree['z'].array())
        d_time=pd.DataFrame(dtree['time'].array())
        d_ekin=pd.DataFrame(dtree['ekin'].array())
        d_edep=pd.DataFrame(dtree['edep'].array())
        d_no1=pd.DataFrame(dtree['no1'].array())
        d_run=pd.DataFrame(dtree['run'].array())
        d_px=pd.DataFrame(dtree['px'].array())
        d_py=pd.DataFrame(dtree['py'].array())
        d_pz=pd.DataFrame(dtree['pz'].array())
        d_proc=pd.DataFrame(dtree['proc'].array())
        d_particle=pd.DataFrame(dtree['particle'].array())
        pd_dtree=pd.concat([d_evt, d_x, d_y, d_z, d_time, d_ekin, d_edep, d_no1, d_run, d_px, d_py, d_pz, d_proc, d_particle], axis=1)
        pd_dtree.columns=['evt', 'x', 'y', 'z', 'time', 'ekin', 'edep', 'no1', 'run', 'px', 'py', 'pz', 'proc', 'particle']
"""
    

#------------------------------------------------------------
def make_spec(events, column, i_comp, spec_all, Es):

    if (len(events)>0):
        print("{:8d} events in {}".format(len(events), comps[i_comp]))

    #spec = pd.DataFrame(np.histogram(events[column].values, Es)[0])
    #spec.columns=[name]
    spec_weight = pd.DataFrame(np.histogram(events[column].values, Es, weights=events['weight'].values, density=False)[0])
    spec_weight.columns=[i_comp]

    spec_all = pd.concat([spec_all, spec_weight], axis=1)

    return spec_all

#------------------------------------------------------------
def decompose_tree(pd_tree, spec_weight, sum_spec_weight, spec_interp):
    
    if (spec_weight is not None):
        # Ekins should be the max Ekin among evt.
        Ekins = pd_tree[["evt","ekin2"]].groupby("evt").max()
        Ekins.reset_index(inplace=True)
        pd_tree["einit"]=pd_tree["evt"].apply(lambda x: Ekins[Ekins.evt==x]["ekin2"].values[0])
        pd_tree["weight"]=pd_tree["einit"].apply(lambda x: spec_interp(x))
        total_num = pd_tree["weight"].sum()
    else:
        pd_tree['weight'] = pd_tree['ekin'].apply(lambda x: 1.0)
        total_num = pd_tree["weight"].sum()

    print("Calculating weighted Edep...")
    pd_tree["w_edep"] = pd_tree["edep"] * pd_tree["weight"]

    dt = total_num / (sum_spec_weight * 0.1 * 0.1 * 2 * 3.14) # n/ (n/str/m2/s * m2 * str) = s

    print("simulation time ; ", dt, "sec")
    print("total hit rate ; ", total_num/dt/100., "counts/sec/cm2")
    print("total deposit energy ; ", pd_tree["w_edep"].sum()/dt, "MeV/sec")
    for i_comp, comp in enumerate(comps):
        print("deposit energy in", comp, ": ", pd_tree[pd_tree["no2"]==i_comp]["w_edep"].sum()/dt, "MeV/sec")
        print("hit rate in", comp, "; ", pd_tree[pd_tree["no2"]==i_comp]["weight"].sum()/dt/100., "counts/sec/cm2")
    # print(pd_tree)
        
    # for i_comp, comp in enumerate(comps):
    #     events=pd_tree[pd_tree.no1==float(i_comp)]
    #     spec_all_tree = make_spec(events, "edep", i_comp, spec_all_tree, Es)
    
    return pd_tree
    
#------------------------------------------------------------
def extract_spec(infile, outfile_stem, spec_weight, sum_spec_weight, spec_interp, max=-1):

    pd_dtree = collate_evt(infile,  max)

    # (1) dtree photon spectrum
    if (pd_dtree is not None):
        print("Decomposing dtree...")
        spec_all_dtree = decompose_tree(pd_dtree, spec_weight, sum_spec_weight, spec_interp)
        #spec_all_dtree.to_pickle("{}_dtree.pkl".format(outfile_stem), protocol=4)
        #print("{:10d} events saved in {}_dtree.pkl.".format(len(pd_dtree), outfile_stem))


    return 0

########################################################
# Main routine
########################################################
if __name__ == '__main__':

    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--infile',
        help='Input file.',
        dest='infile',
        type=str,
        nargs=1,
        )
    parser.add_argument(
        '-o', '--outfile_stem',
        help='Output file stem.',
        dest='outfile_stem',
        type=str,
        nargs=1,
        )
    parser.add_argument(
        '-w', '--weight',
        help='Weight file (photon/s/keV).',
        dest='weight',
        type=str,
        nargs=1,
        default=[None],
        )
    parser.add_argument(
        '-m', '--max',
        help='Maximum number of events (-1: all events).',
        dest='max',
        type=int,
        nargs=1,
        default=[-1],
        )
    args = parser.parse_args()
    
    if (args.weight[0] is not None):

        # Read weight spectrum
        spec_weight_np = np.load(args.weight[0])["arr_0"]
        spec_weight = pd.DataFrame(spec_weight_np, columns=["E", "I"])
        dE=[0]
        dE.extend(np.diff(spec_weight["E"]))
        spec_weight["dE"] = dE

        # Interpolate to the given grid.
        spec_interp = interpolate.interp1d(spec_weight["E"].values, spec_weight["I"].values, fill_value='extrapolate')
        new_weight = pd.DataFrame(Es, columns=["E"])
        new_weight["I"] = spec_interp(new_weight["E"].values)
        new_weight["I/max"] = new_weight["I"] / np.max(new_weight["I"])
        sum_spec_weight = (np.diff(Es) * new_weight["I"].values[1:]).sum()
    else:
        new_weight = None
        spec_interp = None

    extract_spec(args.infile[0], args.outfile_stem[0], new_weight, sum_spec_weight, spec_interp, args.max[0])
