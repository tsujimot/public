#!/usr/local/conda/bin/python

"""
Thermal diffusion equation in a box.

  u'= Laplace(u)         # Governing equation
  du/dx = - gamma * u    # Robin boundary condition
  du/dx = 0              # Neumann boundary condition
  u = 1 (|x|,|y|,|z| < beta/2) 
    = 0 (otherwise)      # Initial condition
"""

############################################################
# Imports
############################################################
from __future__ import print_function
from fenics import *
from dolfin import *
import numpy as np
import argparse
import sys


########################################################                                           
# User-defined pars                                                                                        
########################################################    
a = 100e-3
b = 100e-3
c = 2.5e-3
dqdt = 8.7e-11 / (a*b*c) # W/m3
Q = 2.1e-13 # J
beta= 0.1
delta = 40 # int
Cv = 5.98e-4 # J/K/m^3
K = 6.9e-3 # W/K/m
G = 1.5e-4 # W/K
g = G/2/c/(a+b)
alpha = dqdt * (c*beta)**3.0/Q
gamma = c * g / K


########################################################                                           
# Functions                                                                                        
########################################################    
#----------------------------------------------------------------------                            
def solve_equation(alpha, beta, gamma, delta, setup, outdir):
  
  # Dynamic range and resolution
  mins=np.array([0, 0, 0, 0]) # Min values for x, y, z, t
  maxs=np.array([int(delta), int(delta), 1, 10]) # Max values for x, y, z, t
  partitions=np.array([2, 2, 10, 100]) # Parition in a unit for x, y, z, t
  n_cells=(maxs-mins)*partitions
  resolutions=1/partitions

  # Create mesh and define function space
  mesh = BoxMesh(Point(mins), Point(maxs), n_cells[0], n_cells[1], n_cells[2])
  V = FunctionSpace(mesh, 'P', 1)
  
  # Define variational problem
  u = TrialFunction(V)
  v = TestFunction(V)

  # Define boundaries.
  boundary_markers = MeshFunction("size_t", mesh, mesh.topology().dim()-1, 0)
  tol = 1E-14
  class Boundary0(SubDomain):
    def inside(self, x, on_boundary): 
      return on_boundary and near(x[0], mins[0], tol)
  b0 = Boundary0()
  b0.mark(boundary_markers, 0)
  class Boundary1(SubDomain):
    def inside(self, x, on_boundary): 
      return on_boundary and near(x[0], maxs[0], tol)
  b1 = Boundary1()
  b1.mark(boundary_markers, 1)
  class Boundary2(SubDomain):
    def inside(self, x, on_boundary): 
      return on_boundary and near(x[1], mins[1], tol)
  b2 = Boundary2()
  b2.mark(boundary_markers, 2)
  class Boundary3(SubDomain):
    def inside(self, x, on_boundary): 
      return on_boundary and near(x[1], maxs[1], tol)
  b3 = Boundary3()
  b3.mark(boundary_markers, 3)
  class Boundary4(SubDomain):
    def inside(self, x, on_boundary): 
      return on_boundary and near(x[2], mins[2], tol)
  b4 = Boundary4()
  b4.mark(boundary_markers, 4)
  class Boundary5(SubDomain):
    def inside(self, x, on_boundary): 
      return on_boundary and near(x[2], maxs[2], tol)
  b5 = Boundary5()
  b5.mark(boundary_markers, 5)

  # Check boundaries.
  debug=False
  if debug:
    for x in mesh.coordinates(): 
      if b0.inside(x, True): 
        print('%s is on x = 0' % x) 
      if b1.inside(x, True): 
        print('%s is on x = 1' % x)
      if b2.inside(x, True):
        print('%s is on y = 0' % x)
      if b3.inside(x, True):
        print('%s is on y = 1' % x)
      if b4.inside(x, True):
        print('%s is on z = 0' % x)
      if b5.inside(x, True):
        print('%s is on z = 1' % x)

  # Define boundary conditions.
  ds = Measure('ds', domain=mesh, subdomain_data=boundary_markers)
  if (setup == 0):
    boundary_conditions = {
      0: {'Robin': (gamma, 0)}, # x=x_min 
      1: {'Robin': (gamma, 0)}, # x=x_max
      2: {'Robin': (gamma, 0)}, # y=y_min
      3: {'Robin': (gamma, 0)}, # y=y_max
      4: {'Neumann': 0}, # z=z_min
      5: {'Neumann': 0}  # z=z_max
      }
  elif (setup==1):
    boundary_conditions = {
      0: {'Robin': (gamma, 0)}, # x=x_min 
      1: {'Robin': (gamma, 0)}, # x=x_max
      2: {'Robin': (gamma, 0)}, # y=y_min
      3: {'Robin': (gamma, 0)}, # y=y_max
      4: {'Robin': (gamma, 0)}, # z=z_min
      5: {'Neumann': 0}  # z=z_max
      }   

  # Assemble boundary conditions.
  bcs = [] # Dirichlet
  integrals_N = [] # Neumann
  integrals_R = [] # Robin
  for i in boundary_conditions: 
    if 'Dirichlet' in boundary_conditions[i]: 
      bc = DirichletBC(V, boundary_conditions[i]['Dirichlet'], boundary_markers, i)
      bcs.append(bc)
    if 'Neumann' in boundary_conditions[i]:
      g = boundary_conditions[i]['Neumann']
      if (g != 0):
        integrals_N.append(g*v*ds(i))
    if 'Robin' in boundary_conditions[i]:
      r, s = boundary_conditions[i]['Robin']
      if (r != 0) or (s != 0):
        integrals_R.append(r*(u - s)*v*ds(i))

  # Define initial value
  u_D = Expression('pow(x[0]-delta/2,2)+pow(x[1]-delta/2,2)+pow(x[2]-0.5,2)<beta*beta ? norm : 0', beta=beta, delta=delta, degree=2, norm=1)
  u_n = interpolate(u_D, V)

  # Governing equation
  dt = resolutions[3]
  f = Expression('0', degree=0)
  F = u*v*dx + dt*dot(grad(u),grad(v))*dx - (u_n + dt*f)*v*dx + sum(integrals_R) + sum(integrals_N)
  a, L = lhs(F), rhs(F)

  # Create VTK file for saving solution 
  vtkfile = File('%s/solution.pvd' % outdir)

  # Time-stepping
  u = Function(V)
  t = 0
  
  for n in range(n_cells[3]):
    
    # Update current time
    t += dt
    u_D.norm = 0
    print("t={}".format(t))

    # Compute solution
    solve(a == L, u, bcs)

    # Save to file and plot solution 
    vtkfile << (u, t) 

    # Compute error at vertices
    #u_e = interpolate(u_D, V)
    #error = np.abs(u_e.vector().array() - u.vector().array()).max()
    #print('t = %.2f: error = %.3g' % (t, error))

    # Update previous solution
    u_n.assign(u)

  return 0


########################################################
# Main
########################################################
if __name__ == '__main__':

    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-a', '--alpha',
        help='The relative value of alpha (default 1.0)',
        dest='relative_alpha',
        default=[1.0],
        type=float,
        nargs='+',
    )
    parser.add_argument(
        '-g', '--gamma',
        help='The relative value of gamma (default 1.0)',
        dest='relative_gamma',
        default=[1.0],
        type=float,
        nargs='+',
    )
    parser.add_argument(
        '-s', '--setup',
        help='Setup (0: Neuman on bottom, 1: Robin on bottom)',
        dest='setup',
        choices=[0,1],
        type=int,
        nargs=1,
    )
    parser.add_argument(
        '-o', '--outdir',
        help='Output directory',
        dest='outdir',
        type=str,
        nargs=1,
    )
    args = parser.parse_args()
    
    alpha = alpha * args.relative_alpha[0]
    beta  = beta * 1.0
    gamma = gamma * args.relative_gamma[0]
    delta = delta * 1.0
    
    print("""Parameters:
alpha = %e
beta  = %e
gamma = %e
delta = %e
""" % (alpha, beta, gamma, delta))
    
    solve_equation(alpha, beta, gamma, delta, args.setup[0], args.outdir[0])

