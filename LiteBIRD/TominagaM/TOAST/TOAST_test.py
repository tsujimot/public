#!/usr/local/conda/bin/python

########################################################
# Imports
########################################################                                           
import sys
#sys.path.insert(0, "..")
import numpy as np
import matplotlib.pyplot as plt
import healpy as hp
import toast
env = toast.Environment.get()
env.set_log_level("DEBUG")
print(env)
from toast.todmap import (
    TODSatellite,
    slew_precession_axis,
    OpPointingHpix,
    OpAccumDiag
)
from toast.map import (
    DistPixels
)
from lesson_tools import (
    fake_focalplane    
)

########################################################
# User-defined parameters
########################################################
# output dir
outdir='./maps/'

# Detector
npix = 1 # x2 detectors
samplerate = 20.0 # Hz
fknee=0.05 # Knee frequency
net=1.0

# Scan
alpha = 50.0      # precession opening angle, degrees
beta = 45.0       # spin opening angle, degrees
p_alpha = 25.0    # precession period, minutes
p_beta = 1.25     # spin period, minutes
hwprpm = 6.0      # HWP rotation in RPM

# Observation (in the unit of days).
# 1 obs = 1 day
obs_samples = int(24 * 3600.0 * samplerate) - 1
nobs = 366 # days.

# Map
nside = 64        # Healpix NSIDE


########################################################
# Classes
########################################################  

########################################################
# Functions
########################################################  

########################################################
# Main routine
########################################################                                           
if __name__ == '__main__':

    # Default Comm (one group for this example)
    comm = toast.Comm()    
    
    # Define focal plane.
    fp = fake_focalplane(npix=npix, samplerate=samplerate, epsilon=0, net=net, fmin=0, alpha=1, fknee=fknee, fwhm=30, fov=3.0)    
    detnames = list(sorted(fp.keys()))
    detquat = {x: fp[x]["quat"] for x in detnames}

    # Slew the precession axis so that it completes one circle
    deg_per_day = 360.0 / nobs

    # Instantiate and append observations    
    data = toast.Data(comm)
    
    for ob in range(nobs):
        obsname = "{:03d}".format(ob)
        obsfirst = ob * (obs_samples + 1)
        obsstart = 24 * 3600.0
        tod = TODSatellite(
            comm.comm_group, 
            detquat, 
            obs_samples, 
            firstsamp=obsfirst,
            firsttime=obsstart,
            rate=samplerate,
            spinperiod=p_beta,
            spinangle=beta,
            precperiod=p_alpha,
            precangle=beta,
            coord="E",
            hwprpm=hwprpm
            )
        qprec = np.empty(4 * tod.local_samples[1], dtype=np.float64).reshape((-1, 4))
        slew_precession_axis(
            qprec,
            firstsamp=obsfirst,
            samplerate=samplerate,
            degday=deg_per_day,
        )
        tod.set_prec_axis(qprec=qprec)
        obs = dict()
        obs["tod"] = tod
        #obs["noise"] = noise
        data.obs.append(obs)
    

    name = "signal"
    toast.tod.OpCacheClear(name).exec(data)

    # Add glitches
    prop=dict()    
    simCR = toast.tod.OpSimGlitch(prop)
    toast.tod.OpCacheClear("signal").exec(data)
    simCR.exec(data)

    # Hpix    
    toast.todmap.OpPointingHpix(nside=nside, nest=True, mode="IQU").exec(data)

    # Make maps
    mapmaker = toast.todmap.OpMapMaker(
        nside=nside,
        nnz=3,
        name=name,
        outdir=outdir,
        outprefix="toast_test_",
        baseline_length=10,
        # maskfile=self.maskfile_binary,
        # weightmapfile=self.maskfile_smooth,
        # subharmonic_order=None,
        iter_max=100,
        use_noise_prior=False,
        # precond_width=30,
    )
    mapmaker.exec(data)

    tod = data.obs[0]["tod"]
    times = tod.local_times()
    
    fig = plt.figure(figsize=[12, 8])
    for idet, det in enumerate(tod.local_dets):
        sky = tod.local_signal(det, "signal")
        ind = slice(0, 1000)
        ax = fig.add_subplot(4, 4, 1 + idet)
        ax.set_title(det)
        ax.plot(times[ind], sky[ind], '.', label="signal", zorder=100)
    ax.legend(bbox_to_anchor=(1.1, 1.00))
    fig.subplots_adjust(hspace=0.6)
    
    
    plt.figure(figsize=[12, 8])
    
    hitmap = hp.read_map("maps/toast_test_hits.fits")
    hitmap[hitmap == 0] = hp.UNSEEN
    hp.mollview(hitmap, sub=[2, 2, 1], title="hits")
    
    binmap = hp.read_map("maps/toast_test_binned.fits")
    binmap[binmap == 0] = hp.UNSEEN
    hp.mollview(binmap, sub=[2, 2, 2], title="binned map", cmap="coolwarm")
    
    destriped = hp.read_map("maps/toast_test_destriped.fits")
    destriped[destriped == 0] = hp.UNSEEN
    hp.mollview(destriped, sub=[2, 2, 3], title="destriped map", cmap="coolwarm")
    

    print(np.sum(hitmap[hitmap != hp.UNSEEN]) / 1400000.0)
    
    # Plot the white noise covariance
    
    plt.figure(figsize=[12, 8])
    wcov = hp.read_map("maps/toast_test_npp.fits", None)
    wcov[:, wcov[0] == 0] = hp.UNSEEN
    hp.mollview(wcov[0], sub=[3, 3, 1], title="II", cmap="coolwarm")
    hp.mollview(wcov[1], sub=[3, 3, 2], title="IQ", cmap="coolwarm")
    hp.mollview(wcov[2], sub=[3, 3, 3], title="IU", cmap="coolwarm")
    hp.mollview(wcov[3], sub=[3, 3, 5], title="QQ", cmap="coolwarm")
    hp.mollview(wcov[4], sub=[3, 3, 6], title="QU", cmap="coolwarm")
    hp.mollview(wcov[5], sub=[3, 3, 9], title="UU", cmap="coolwarm")
    
    plt.show()