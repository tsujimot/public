#!/usr/local/conda/bin/python


# Load common tools for all lessons
import sys
sys.path.insert(0, "..")
from lesson_tools import (
    fake_focalplane
)


fp = fake_focalplane()

detnames = list(sorted(fp.keys()))
detquat = {x: fp[x]["quat"] for x in detnames}
detfwhm = {x: fp[x]["fwhm_arcmin"] for x in detnames}
#detlabels = {x: x for x in detnames}
#detpolcol = {x: "red" if i % 2 == 0 else "blue" for i, x in enumerate(detnames)}

print(detquat)

breakpoint()