#/usr/bin/env python3

############################################################
# Description
############################################################
# Generates cosmic-ray glitch TOD at a high sampling rate,
# decimate the data to a low sampling rate, 
# and save it to a file.

# If input file is given, CR signals are added upon it, which allows to add different populations of CRs.

# MPI option available. An example follows.
# mpiexec -np 4 python ~/bin/SimTOD.py -o /tmp/test -d 100 --mpi

############################################################
# Revisions
############################################################
# 2020/04/09 M. Tsujimoto    Written from scracth.
# 2020/04/09 M. Tominaga     Debug
# 2020/05/12 M. Tsujimoto    MPI option. Run on HPC.


########################################################                                           
# Imports                                                                                          
########################################################                                           
import argparse
from sys import exit
import numpy as np
from scipy import signal
from scipy.ndimage import convolve1d
import math


# Plotly offline       
import plotly                                                                            
import plotly.offline as offline
import plotly.graph_objects as go
import plotly.io as pio


########################################################                                           
# User parameters
########################################################                                           
sampling_rate_default = 10e3 # Hz
duration_default = 3600*24.0 # s
props_cr_default = [
    100.0, # Frequency of CR hit in Hz.
    1.0,  # Mean of PHA in uK_CMB.
    0.1,  # Standard deviation of PHA in uK_CMB.
    3e-3, # Mean of decay in s.      
    3e-4  # Standard deviation of decay in s.     
    ]
datadir='/home/astro/Work/LiteBIRD/20170607/Figures/04/'


########################################################                                           
# Class                                                                                            
########################################################  
class myToD:

    # Constructor                                                                                  
    def __init__(self, sampling_rate, duration, mytod_init=None):

        # Size of the data.                                                                        
        self.sampling_rate = sampling_rate #Hz
        self.duration = duration # s
        self.n_sample = int(self.sampling_rate * self.duration) #data duration

        # Blank container for mytod.                                             
        if (mytod_init is None):
            self.mytod = np.zeros(self.n_sample)
        else:
            self.mytod = mytod_init
                
        # Time bins                                                                  
        self.Ts = np.linspace(0, self.duration, int(self.n_sample+1))[:-1]
        

    # Calc/Update PSD                                                                              
    def calc_psd(self): #scipy, power spectral density
    
        self.Fs, self.mytod_psd = signal.welch(self.mytod, self.sampling_rate)        
        #self.Fs, self.mytod_psd = signal.periodogram(self.mytod, self.sampling_rate*0.5)
        print("PSD calculated for the current ToD.")

    # Get data.                                                                                    
    def get_data(self, flg_time):
        if (flg_time is True):
            return self.Ts, self.mytod
        else:
            return self.Fs, self.mytod_psd
    
    # Plot data                                                                                    
    def plot_data(self, Xs, Ys, outstem, flg_time):
        plotly_data(Xs, Ys, outstem, flg_time)

    # Save data.                                                                                   
    def save_data(self, outstem, flg_time): 
        if (flg_time is True):
            np.savez(outstem+'_TD.npz', self.Ts, self.mytod)
            print("ToD saved in %s_TD.npz." % outstem)
        else:
            np.savez(outstem+'_FD.npz', self.Fs, self.mytod_psd)
            print("ToD saved in %s_FD.npz." % outstem)
                            
    # Add cosmic-rays of a single population.                                                                             
    def add_cosmic_rays(self, props_cr):
        
        # Number of CRs to inject.
        self._cr_freq = props_cr['freq']
        self._num_cr = int(self.duration * self._cr_freq)
        
        # Select the time of CR injection at random and update the total num. 
        self._dice = np.random.rand(len(self.mytod))
        self._hits = [i for i, x in enumerate(self._dice) if x<=self._num_cr/len(self.mytod)]
        self._num_cr = len(self._hits)
        
        # Generate PHA and decay at random.
        self._cr_pha_mean, self._cr_pha_sigma = props_cr['pha']
        self._cr_decay_mean, self._cr_decay_sigma = props_cr['decay']
        self._cr_phas =  np.random.normal(self._cr_pha_mean, self._cr_pha_sigma, self._num_cr)
        self._cr_decays =  np.random.normal(self._cr_decay_mean, self._cr_decay_sigma, self._num_cr)
        
        self._len_cr = 10.0 * self._cr_decay_mean # CR profile duration in s.
        self._sample_cr = int(self._len_cr * self.sampling_rate)
        
        print("A total of %s CRs added." % self._num_cr)
        
        # Add CR profile one by one.
        for i in range(self._num_cr):
            self._cr_pha = self._cr_phas[i]
            self._cr_decay = self._cr_decays[i]
            self._hit = self._hits[i]
            self._cr_profile = [self._cr_pha*np.exp(-t/self.sampling_rate/self._cr_decay) for t in range(self._sample_cr)]            
            
            # Assert positive PHA and decay.
            assert self._cr_pha > 0
            assert self._cr_decay > 0
            
            # If in excess, add to the beginning.
            if (self._hit+self._sample_cr < self.n_sample):
                self.mytod[self._hit:self._hit+self._sample_cr] += self._cr_profile
            else:
                self.mytod[self._hit:] += self._cr_profile[:self.n_sample-self._hit]
                self.mytod[0:self._hit+self._sample_cr-self.n_sample] += self._cr_profile[self.n_sample-self._hit:]

            #print("CR (PHA %.3f uK, decay %.3f ms) added at %.3f s." % 
            #      (self._cr_pha, self._cr_decay*1e3, self._hit/self.sampling_rate))

    
    # FIR filter including CIC.                                                                    
    def filter_FIR(self, file_coeff):                
     hi self._win = np.load(file_coeff)['ba'][0]        
        self.mytod = convolve1d(self.mytod, self._win, mode='wrap')
        
        # Decimate by a factor of len_win and update attributes of mytod.
        self._len_win = len(self._win)        
        self.mytod = self.mytod[self._len_win//2-1::self._len_win]
        self.Ts = self.Ts[self._len_win//2-1::self._len_win]
        self.sampling_rate /= self._len_win
        self.n_sample = len(self.Ts)
        self.duration = self.Ts[-1] - self.Ts[0]
        
        
########################################################                                           
# Functions                                                                                        
########################################################    
#----------------------------------------------------------------------                            
def plotly_data(Xs, Ys, outstem, flg_time):
    "Plot data."

    # Time-domain plot                                                                             
    if (flg_time is True):
        title="Time-domain data (%s)" % (outstem)
        xtitle="Time (s)"
        ytitle="ADU"
        xtype="linear"
        ytype="linear"
        outstem = outstem + "_TD"        
    # Frequency-domain plot                                                                        
    else:
        title="Frequency-domain data (%s)" % (outstem)
        xtitle="Time (Hz)"
        ytitle="uK/sqHz"
        xtype="log"
        ytype="log"
        outstem = outstem + "_FD"        
    # Define layout                                                                                
    Layout = go.Layout(
        title=title,
        xaxis=dict(
            title = xtitle,
            type = xtype,
            autorange = True,
            tickmode = 'auto'
            ),
        yaxis=dict(
            title = ytitle,
            type = ytype,
            autorange = True,
            tickmode = 'auto',    
            ),
        font=dict(
            size=30
            ),
        )
    # Define data                                                                                  
    Data = go.Scattergl(
            x=Xs,
            y=Ys,
            )
    # Define figure                                                                                
    fig = go.Figure(layout=Layout, data=Data)

    # Save file.
    offline.plot(fig, filename='%s.html' % outstem, auto_open=False, include_mathjax='cdn')
    print("File saved in %s.html." % (outstem))


    return 0

#----------------------------------------------------------------------                            
def confirm_data(mytod, outstem):

    # (1) Confirm time-domain data.  
    Xs, Ys = mytod.get_data(flg_time=True)
    mytod.save_data(outstem, flg_time=True)        
    #mytod.plot_data(Xs, Ys, outstem, flg_time=True)

    # (2) Confirm freq-domain data.     
    Xs, Ys = mytod.get_data(flg_time=False)
    mytod.save_data(outstem, flg_time=False)
    #mytod.plot_data(Xs, Ys, outstem, flg_time=False)

    return 0


########################################################                                           
# Main routine                                                                                     
########################################################                                           
if __name__ == '__main__':

    #                                                                                              
    # Command-line parser                                                                          
    #                                                                                              
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--input',
        help='Input file',
        dest='infile',
        default=None,
        type=str,
        nargs='?'
        )
    parser.add_argument(
        '-o', '--output',
        help='Output file stem',
        dest='outstem',        
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-s', '--sampling rate',
        help='Sampling rate in Hz',
        dest='sampling_rate',
        type=float,
        default=sampling_rate_default,
        nargs='?'
        )
    parser.add_argument(
        '-d', '--duration',
        help='Duration of data in s',
        dest='duration',
        type=float,
        default=duration_default,
        nargs='?'
        )
    parser.add_argument(
        '-p', '--prop',
        help='Properties of CR (freq, pha_mean, pha_sig, decay_mean, decay_sig)',
        dest='props_cr',
        type=float,
        default=props_cr_default,
        nargs=5
        )
    parser.add_argument(
        '--mpi',
        help='MPI on',
        dest='flag_MPI',
        action='store_true',
        default=False,                
        )
    
    # Parse argments.
    args = parser.parse_args()
    infile = args.infile
    outstem = args.outstem[0]
        
    sampling_rate = args.sampling_rate
    duration = args.duration
    props_cr={
    'freq' : args.props_cr[0], # Frequency of CR hit in Hz.
    'pha': args.props_cr[1:3], # Mean and standard deviation of PHA in uK_CMB.
    'decay' : args.props_cr[3:5], # Mean and standard deviation of decay in s. 
    }

    #                                                                                              
    # Main process                                                                                 
    #                                                                                              
    # (1) Split into multi-processing.
    if (args.flag_MPI is True):
        from mpi4py import MPI
        comm = MPI.COMM_WORLD
        rank  = comm.Get_rank()
        size = comm.Get_size()
        print("MPI on with %d processors." % size)
        outstem = outstem + '_%d' % rank
        if rank == 0:
            recvbuf_tod = np.empty(int(sampling_rate*duration), dtype=np.float)
            recvbuf_Ts = np.empty(int(sampling_rate*duration), dtype=np.float)
        else:
            recvbuf_tod = None
            recvbuf_Ts = None
        duration /= size 
                        
    # (2) Make myToD of a sampling rate of 10000 Hz and a duration of 10 s (data are 0).                   
    if (infile is None):
        mytod = myToD(sampling_rate, duration)
    else:
        # Read file.
        Ts = np.load(infile)['arr_0']
        mytod_init = np.load(infile)['arr_1']
        # Instantiate mytod with the initla data.
        sampling_rate = 1/np.diff(Ts)[0]
        duration = len(Ts)*np.diff(Ts)[0]
        print("Initial data loaded from %s with sampling rate %.1f kHz and duration %.1f s." % 
              (infile, sampling_rate/1e3, duration))
        mytod = myToD(sampling_rate, duration, mytod_init)

    mytod.calc_psd()
    #confirm_data(mytod, outstem+'_init')


    # (3) Add CRs of one population.       
    mytod.add_cosmic_rays(props_cr)                                                                         
    mytod.calc_psd()
    confirm_data(mytod, outstem+'_cr')
    """

    # (4) CIC decimation #1                                                                        
    mytod.filter_FIR(datadir + "CIC1.npz")                                                                  
    mytod.calc_psd()
    confirm_data(mytod, outstem+'_CIC1')
    
    # (5) CIC decimation #2                                                                        
    mytod.filter_FIR("CIC2.npz")                                                                  
    mytod.calc_psd()
    confirm_data(mytod, outstem+'_CIC2')

    # (6) FIR decimation                                                                           
    mytod.filter_FIR("FIR.npz")                                                                  
    mytod.calc_psd()
    confirm_data(mytod, outstem+'_FIR')
"""

    # (7) Merge from multi-processing.
    if (args.flag_MPI is True):
        Xs, Ys = mytod.get_data(flg_time=True)
        comm.Gather(Xs, recvbuf_Ts, root=0)
        comm.Gather(Ys, recvbuf_tod, root=0)
        print("Data from rank %d gathered." % rank)
        comm.Barrier()
        mytod.Ts = recvbuf_Ts
        mytod.mytod = recvbuf_tod
        
    # (8) Save to file.
    #mytod.calc_psd()    
    confirm_data(mytod, outstem+'_all')
    
    exit(0)
