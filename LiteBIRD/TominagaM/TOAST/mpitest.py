#/usr/bin/env python3

import numpy as np
from mpi4py import MPI


def addCRs(data, props, rank):
    np.random.seed(seed=rank)
    data = np.random.rand(len(data))*rank
    return data

#def decimate():
#    return 0


comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

total_duration = 7.0
n_obs = 7.0
sample_obs = int(10e2 * 24 * 3600 / size)

props={
    'dist' : 'Poisson',
    'freq' : 75.0, # Hz
    'amp' : 1.0, # Amplitude in uK.
    }



data = np.empty(sample_obs, dtype=np.float64)
#comm.Bcast(data, root=0) 

sendbuf = addCRs(data, props, rank)
recvbuf = None
if rank == 0:
    recvbuf = np.empty(sample_obs*size, dtype=np.float64)

comm.Gather(sendbuf, recvbuf, root=0)

print("%d, %d" % (rank, len(sendbuf)))
comm.Barrier()

if rank == 0:
    print("%d, %d" % (rank, len(recvbuf)))
#    breakpoint()
        
#MPI.File

#検出器ごとに、
# obs (1 day など)ごとに処理する
# zero を broadcast する
# gather する
#Pythonのオブジェクトをpickle (シリアライズ)して送る小文字のメソッド(send(), recv
#(), bcast() scatter(), gather(), etc.)と
#、バッファを直接送る頭文字が大文字のメソッド(Send(), Recv(), Bcast() Scatter(), 
#Gather(), etc.)の2系統に分かれています。




#検出器ごとに、
# obs (1 day など)ごとに処理する
# zero を broadcast する
# gather する
#Pythonのオブジェクトをpickle (シリアライズ)して送る小文字のメソッド(send(), recv(), bcast() scatter(), gather(), etc.)と
#、バッファを直接送る頭文字が大文字のメソッド(Send(), Recv(), Bcast() Scatter(), Gather(), etc.)の2系統に分かれています。


# broadcast numData and allocate array on other ranks:



#print('Rank: ',rank, ', data received: ',data)

