# Copyright (c) 2020 and thereafter by 
# * Mayu Tominaga (U. of Tokyo) <tominaga@ac.jaxa.jp> 
# * Masahiro Tsujimoto (JAXA ISAS) <tsujimot@astro.isas.jaxa.jp> 
#
# All rights reserved.  Use of this source code is governed by
# a BSD-style license that can be found in the LICENSE file.

import numpy as np
from astropy.io import fits
from ..op import Operator
from ..timing import function_timer

def addGlitch(toi, infile, inplace=False):
    """Add cosmic-ray glitch signals from a file.
    
    Args:
        toi (float): TOI samples to add glitches.
        infile (str): Input npz file that contain cosmic-rays.
        inplace (bool): Overwrite input TOI.

    Returns:
        Glitch-added timestream.

    """

    if inplace:
        toi_out = toi
    else:
        toi_out = np.zeros_like(toi)

    toi_out[:] = np.random.rand(len(toi))

    return toi_outss


class OpSimGlitch(Operator):
    """Operator which adds cosmic-ray glitches.

    Args:
        prop (dict):  Dictionary to contain cosmic-ray glitch properties.            
        name (str):  Name of the output signal cache object will be
            <name_in>_<detector>.  If the object exists, it is used as
            input.  Otherwise signal is read using the tod read method.

    """

    def __init__(self, prop, name=None):
        self._prop = prop
        self._name = name
        # Call the parent class constructor
        super().__init__()

    @function_timer
    def exec(self, data):
        """Add cosmic-ray glitches.

        Args:
            data (toast.Data): The distributed data.

        """
        for obs in data.obs:

            tod = obs["tod"]

            for det in tod.local_dets:

                # Cache the output signal
                ref = tod.local_signal(det, self._name)
                obs_times = tod.read_times()

                addGlitch(
                    obs_times,
                    ref,
                    inplace=True,
                )

                assert np.isnan(ref).sum() == 0, "The signal timestream includes NaN"

                del ref

        return