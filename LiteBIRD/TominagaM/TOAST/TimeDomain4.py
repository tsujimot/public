########################################################                                           
# Imports                                                                                          
########################################################                                           
import argparse
from sys import exit
import numpy as np
from scipy import signal
import math
import healpy as hp

import toast
import toast.pipeline_tools
import toast.qarray as qa
from toast.todmap import (
    slew_precession_axis,
    TODSatellite
)

comm = toast.Comm()
from toast.todmap import (
    OpPointingHpix,
    OpAccumDiag
)
from toast.map import (
    DistPixels
)

import matplotlib.pyplot as plt

# Plotly offline       
import plotly                                                                            
import plotly.offline as offline
import plotly.graph_objects as go
########################################################                                           
# Class                                                                                            
########################################################     
class myToD:
    def __init__(self, sampling_rate, duration, outstem):

        # Size of the data.                                                                        
        self.sampling_rate = sampling_rate #Hz
        self.duration = duration #s
        self.n_sample = int(self.sampling_rate * self.duration) #data duration
        self.Tmax = self.duration
        self.Tmin = 0.0
        self.dT = 1.0/self.sampling_rate
        self.Fmax = self.sampling_rate/2.0 # Nyquist freq, finest        
        self.Fmin = 1.0/self.Tmax 
        self.dF = self.Fmin

        self.outstem=outstem
        
    def load_data(self):
        data=np.load("%s.npz" % self.outstem)
        Xs=np.load("%s.npz" % self.outstem)[data.files[0]]
        Ys=np.load("%s.npz" % self.outstem)[data.files[1]]
        return Xs, Ys
                       
class Args_Sate:
    def __init__(self, sampling_rate):
        self.sampling_rate = sampling_rate
        
    hwp_rpm = None#6.0                                                                                                             
    hwp_step_deg = None
    hwp_step_time_s = None
    spin_period_min = 1 # 10                                                                                                       
    spin_angle_deg = 20 # 30                                                                                                       
    prec_period_min = 100 # 50                                                                                                     
    prec_angle_deg = 30 # 65                                                                                                       
    coord = "E"
    nside = 64
    nnz = 3
    outdir = "maps"
    #sampling_rate
#######################################################                                           
# Functions                                                                                        
########################################################    
#----------------------------------------------------------------------

#-------------------------------------------------------
def fill_tod(tod, fp, nsample):
    t_delta = 1.0/args_sate.sampling_rate
    #tod.write_times(stamps=np.arange(0.0, mytod.n_sample * mytod.dT, mytod.dT))
    #Tsと同じ
    #ノイズ時刻を書き込む                                                                                                          
    tod.write_boresight(
        data=qa.from_angles(
            (np.pi / 2) * np.ones(nsample),
            (2 * np.pi / nsample) * np.arange(nsample),
            np.zeros(nsample)
        )
    )
    tod.write_position(pos=np.zeros((nsample, 3), dtype=np.float64))
    tod.write_velocity(vel=np.zeros((nsample, 3), dtype=np.float64))
    tod.write_common_flags(flags=np.zeros(nsample, dtype=np.uint8))
    
    for d in detnames:
    #tod.write(                                                                                                                
        #    detector=d, data=np.random.normal(                                                                                    
        #        scale=fp[d]["NET"],                                                                                               
        #        size=nsample                                                                                                      
        #    )                                                                                                                     
        #) #random                                                                                                                 
        tod.write_flags(
            detector=d, flags=np.zeros(nsample, dtype=np.uint8)
        )

#----------------------------------------------------------------------
def toast_map(data, name):
    npix = 12 * args_sate.nside ** 2
    hitmap = np.zeros(npix)
    tod = data.obs[0]["tod"]
    for det in tod.local_dets:
        pixels = tod.cache.reference("pixels_{}".format(det))
        hitmap[pixels] = 1
    hitmap[hitmap == 0] = hp.UNSEEN
    hp.mollview(hitmap, nest=True, title="all hit pixels", cbar=False)
    hp.graticule(22.5, verbose=False)

    # Scan the signal from a map                                                                                                       
    distmap = toast.map.DistPixels(
        data,
        nnz=3,
        dtype=np.float32,
        )
    distmap.read_healpix_fits("sim_map.fits")
    toast.todmap.OpSimScan(distmap=distmap, out=name).exec(data)
    # Copy the sky signal                                                                                                              
    toast.tod.OpCacheCopy(input=name, output="sky_signal", force=True).exec(data)


    #Destripe the signal and make a map                                                                                                
    mapmaker = toast.todmap.OpMapMaker(
        nside=args_sate.nside,
        nnz=3,
        name=name,
        outdir=args_sate.outdir,
        outprefix="toast_test_",
        baseline_length=10,
        #maskfile=self.maskfile_binary,                                                                                                
        #weightmapfile=self.maskfile_smooth,                                                                                           
        #subharmonic_order=None,                                                                                                       
        iter_max=100,
        use_noise_prior=False,
        #precond_width=30,                                                                                                             
    )
    mapmaker.exec(data)

    #Plot a segment of the timelines                                                                                                   
    tod = data.obs[0]["tod"]#TODSatellite                                                                                              
    times = tod.local_times()

    fig = plt.figure(figsize=[12, 8])
    for idet, det in enumerate(tod.local_dets):
        sky = tod.local_signal(det, "sky_signal")#siganl行列を取得                                                                     
        #full = tod.local_signal(det)#今はdataの種類が１つしかない                                                                     
        #cleaned = tod.local_signal(det, name)                                                                                         

        ind = slice(0, 1000)
        ax = fig.add_subplot(4, 4, 1 + idet)
        ax.set_title(det)
        ax.plot(times[ind], sky[ind], '.', label="signal", zorder=100)
        #ax.plot(times[ind], full[ind], '.', label="full")                                                                             
        #ax.plot(times[ind], full[ind] - cleaned[ind], '.', label="baselines")                                                         
    ax.legend(bbox_to_anchor=(1.1, 1.00))
    fig.subplots_adjust(hspace=.6)
    plt.figure(figsize=[12, 8])

    hitmap = hp.read_map("maps/toast_test_hits.fits")
    hitmap[hitmap == 0] = hp.UNSEEN
    hp.mollview(hitmap, sub=[2, 2, 1], title="hits")

    binmap = hp.read_map("maps/toast_test_binned.fits")
    binmap[binmap == 0] = hp.UNSEEN
    hp.mollview(binmap, sub=[2, 2, 2], title="binned map", cmap="coolwarm")

    destriped = hp.read_map("maps/toast_test_destriped.fits")
    destriped[destriped == 0] = hp.UNSEEN
    hp.mollview(destriped, sub=[2, 2, 3], title="destriped map", cmap="coolwarm")

    inmap = hp.read_map("sim_map.fits")
    inmap[hitmap == hp.UNSEEN] = hp.UNSEEN
    hp.mollview(inmap, sub=[2, 2, 4], title="input map", cmap="coolwarm")
    print(np.sum(hitmap[hitmap != hp.UNSEEN]) / 1400000.0)
    plt.show()

########################################################                                           
# Main routine                                                                                     
########################################################                                           
if __name__ == '__main__':

    #                                                                                              
    # Command-line parser                                                                          
    #                                                                                              
    #parser = argparse.ArgumentParser()
    #parser.add_argument(
    #    '--white_noise',
    #    help='Input white noise properties (amplitude in uK/rtHz).',
    #    dest='par_wn',
    #    default=0.5,
    #    type=float,
    #    nargs='?'
    #    )

        #                                                                                              
    # Main process                                                                                 
    #                                                                                              
    # (1) load data from .npz made in TimeDomain3.py 
    mytod=myToD(10e3,50,"mytod_cr_TD")
    #print(mytod.load_data()[0])
    #print(mytod.load_data()[1])
    # (2) map making
    args_sate=Args_Sate(mytod.sampling_rate)
    from lesson_tools import (
        fake_focalplane
    )
    
                                                      
    fp=fake_focalplane(samplerate=mytod.sampling_rate, npix=1, fknee=0.1, alpha=2)
    detnames = list(sorted(fp.keys()))
    detquats = {x: fp[x]["quat"] for x in detnames}
    obs = dict()
    obs["name"] = "simulation"
    start_sample = 0
    start_time = mytod.Tmin 
    
    tod = toast.tod.TODCache(None, detnames, mytod.n_sample, detquats=detquats)
    tod_sate = toast.todmap.TODSatellite(
        comm.comm_group,
        detquats,
        mytod.n_sample,
        coord=args_sate.coord,
        firstsamp=start_sample,
        firsttime=start_time,
        rate=args_sate.sampling_rate,
        spinperiod=args_sate.spin_period_min,
        spinangle=args_sate.spin_angle_deg,
        precperiod=args_sate.prec_period_min,
        precangle=args_sate.prec_angle_deg,
        detranks=comm.group_size,
        hwprpm=args_sate.hwp_rpm,
        hwpstep=args_sate.hwp_step_deg,
        hwpsteptime=args_sate.hwp_step_time_s,
    )
    tod.write_times(stamps=mytod.load_data()[0])
    for d in detnames:                                                                                               
        tod.write(                                                                                                                    
            detector=d,                                                                                                                                                                                                                             
            data=mytod.load_data()[1]
            #np.random.normal(                                                                                                    
             ##  size=nsample                                                                                                          
               # ) #random                                                                                                             
        )     
            
    fill_tod(tod, fp, mytod.n_sample)
    print(tod_sate.local_samples)
    qprec = np.empty(4 * tod_sate.local_samples[1], dtype=np.float64).reshape((-1, 4)) #data array from Cache                          
    deg_per_day = 1.0
    slew_precession_axis(
        qprec,
        firstsamp=start_sample + tod.local_samples[1],
        samplerate=mytod.sampling_rate,
        degday=360.0/365.25,
    )
    
    tod_sate.set_prec_axis(qprec=qprec)
    
    data = toast.Data(comm)
    for iobs in range(int(len(detnames))):
        obs = {}
        obs["name"] = "science_{:05d}".format(iobs)
        obs["tod"] = tod_sate
        obs["intervals"] = None
        obs["baselines"] = None
        obs["CR"] = tod.local_signal(detnames[iobs])# loop for iobs ?                                                                  
        obs["id"] = iobs
        data.obs.append(obs)
        
    name = "signal"
    toast.tod.OpCacheClear(name).exec(data)
    toast.todmap.OpPointingHpix(nside=args_sate.nside, nest=True, mode="IQU").exec(data)

    toast_map(data, name)

    exit(0)