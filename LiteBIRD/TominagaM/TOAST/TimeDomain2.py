#/usr/bin/env python3
############################
# detector : lesson_tool(toast_litebirdではない)
# white noise: random, fftに変更する
# CR: randomに生成できる、CR.txtを読み込むこともできる
# CIC:
# FIR:
# map
############################

############################
# python settings
############################
import matplotlib.pyplot as plt
import numpy as np
import math

############################
# toast
###########################
import toast
import toast.pipeline_tools
import toast.qarray as qa
from toast.todmap import (
    slew_precession_axis,
    TODSatellite
)
comm = toast.Comm()
from toast.todmap import (
    OpPointingHpix,
    OpAccumDiag
)
from toast.map import (
    DistPixels
)

#####################
# sys
#####################
import sys
sys.path.insert(0, "..")

############################                                    
# define prameters for TODSatellite as "args"                   
############################                                     
class args:
    sample_rate = 1000  # Hz 
    cic_rate = 20
    fir_rate =10
    down_sample_rate = 20
    hwp_rpm = None#6.0
    hwp_step_deg = None
    hwp_step_time_s = None
    spin_period_min = 1 # 10                                    
    spin_angle_deg = 20 # 30                                   
    prec_period_min = 100 # 50                                   
    prec_angle_deg = 30 # 65                                   
    coord = "E"
    nside = 64
    nnz = 3
    outdir = "maps"

#############################
# parameters for pyfdax
############################
class pyfda:
    m = 64
    stage = 1

############################
# fake_focalplane
############################
from lesson_tools import (
    fake_focalplane
)
#fp = fake_focalplane()
fp=fake_focalplane(samplerate=args.sample_rate, fknee=0.1, alpha=2)
detnames = list(sorted(fp.keys()))
detquats = {x: fp[x]["quat"] for x in detnames}
nsample = 30
start_sample = 0
start_time = 0
obs = dict()
obs["name"] = "simulation"

tod = toast.tod.TODCache(None, detnames, nsample, detquats=detquats)
#print(detquat)                                                         


##########################                                       
# pdf                                                          
###########################                                   

#from matplotlib.backends.backend_pdf import PdfPages
#fig=plt.figure(figsize=(10,10))
#ax = fig.add_subplot(1,1,1)


##########################
# TOD
#########################
'''
samplerate = 10.0
precperiod = 90.0
precangle = 45.0
spinperiod = 1.0
spinangle = 45.0
hwprpm = 6.0
'''

tod_sate = toast.todmap.TODSatellite(
    comm.comm_group,
    detquats,
    nsample,
    coord=args.coord,
    firstsamp=start_sample,
    firsttime=start_time,
    rate=args.sample_rate,
    spinperiod=args.spin_period_min,
    spinangle=args.spin_angle_deg,
    precperiod=args.prec_period_min,
    precangle=args.prec_angle_deg,
    detranks=comm.group_size,
    hwprpm=args.hwp_rpm,
    hwpstep=args.hwp_step_deg,
    hwpsteptime=args.hwp_step_time_s,
)

'''
#######################
# noise class test
#######################
noise = toast.pipeline_tools.get_analytic_noise(args, comm, fp)
'''


#print(type(noise))

########################                                       
# precession axis                                              
########################                                     
qprec = np.empty(4 * tod_sate.local_samples[1], dtype=np.float64).reshape((-1, 4)) #data array from Cache
deg_per_day = 1.0
slew_precession_axis(
    qprec,
    firstsamp=start_sample + tod.local_samples[0],
    samplerate=args.sample_rate,
    degday=360.0/365.25,
)
tod_sate.set_prec_axis(qprec=qprec)#add to Satellite data

#########################
# shape of CR
########################
def add_CR(time,timedist,height, det):
    y=np.zeros(nsample)
    #t=np.array(range(start_time,start_time+nsample/args.sample_rate,nsample))
    t=np.array(tod.local_times())
    for i in range(nsample):
        if t[i]<time-0.0005:
            y[i]+=0
        elif t[i]>=time-0.0005 and t[i]<=time+0.0005:
            y[i]+=height
        else:
            y_pre=height*math.exp(-(timedist-time)*(t[i]-time))
            if y_pre<1e-10:
                y_pre=0
            y[i]+=y_pre
    return y


##########################
# fill_tod to Cache data
##########################
def fill_tod(tod, fp):
    t_delta = 1.0/args.sample_rate 
    tod.write_times(stamps=np.arange(0.0, nsample * t_delta, t_delta))
    #ノイズ時刻を書き込む
    tod.write_boresight(
        data=qa.from_angles(
            (np.pi / 2) * np.ones(nsample),
            (2 * np.pi / nsample) * np.arange(nsample),
            np.zeros(nsample)
        )
    )
    tod.write_position(pos=np.zeros((nsample, 3), dtype=np.float64))
    tod.write_velocity(vel=np.zeros((nsample, 3), dtype=np.float64))
    tod.write_common_flags(flags=np.zeros(nsample, dtype=np.uint8))
    for d in detnames:
        #tod.write(
        #    detector=d, data=np.random.normal(
        #        scale=fp[d]["NET"], 
        #        size=nsample
        #    )
        #) #random
        tod.write_flags(
            detector=d, flags=np.zeros(nsample, dtype=np.uint8)
        )

fill_tod(tod, fp)


##########################                                                
# read CRs         readlines->read                                                     
##########################                                     
xdata=tod.local_times()
CRs=[]
k=0
print("xdata")
print(xdata)
##################### t,decay,heightをrandamに生成する                                                                                                                
detlist=list(detnames)
y="y"
if input('read CR files? (y/n) : ')==y:
    for i in open("CR.txt").readlines(): ###pd.read_csv : ファイルを一回で読む、loopをやめる
        data=i[:-1].split(" ")
        CRs.append([])
        for j in range(int(len(data))):
            if j==3:
                CRs[k].append(data[j])
            elif j==4:
                CRs[k].append(int(data[j]))
            else:
                CRs[k].append(float(data[j]))
        k+=1
    print(CRs)

else:
    val1 = input('how many CRs? : ')
    num = int(val1)
    val2 = input('how often CRs come? (Hz) : ')
    deltat = 1.0/float(val2)
    val3= input('average of CR height (microK) : ')
    T=float(val3)
    val4=input('sigma of CR height (microK) :')
    sigT=float(val4)
    t0=np.random.poisson(lam=deltat,size=num)
    t=[]
    h=np.random.normal(loc=T,scale=sigT,size=num)
    decay0=np.random.poisson(lam=deltat*0.5,size=num)
    decay=[]
    random_det_num=np.random.randint(0,int(len(detnames)),num)
    for i in range(num):
#        if deltat*i #CRの時間の大小関係を制限するか？
        t.append(t0[i]+deltat*(i+0.5))
        decay.append(decay0[i]+t[i])
    for i in range(num):
        CRs.append([t[i],decay[i],h[i],detlist[random_det_num[i]],random_det_num[i]])
    print(CRs)

############################
# add CRs to white noise
############################
ydata=[]
data0=[]
pre_data1=[]
for k in range(int(len(detnames))):
    #ydata.append(tod.local_signal(detnames[k]))
    ydata.append([])

    for l in range(nsample):
        ydata[k].append(0.)

for i in range(int(len(CRs))):
    det_num=CRs[i][4]
    ydata[det_num]+= add_CR(CRs[i][0],CRs[i][1],CRs[i][2],CRs[i][3]) 

k=0


for d in detnames: ################ fftに変更する
    data0.append([])
    data0[k]=np.random.normal(
            scale=fp[d]["NET"],
            size=nsample
            )+ydata[k]
    k+=1
#print("data0")
#print(data0)
###################
# CIC filter はじめと最後が抜けている、上書きをどう解決するか
###################
data1=[]
t_delta1=1/args.cic_rate
nsample1=nsample//(args.sample_rate/args.cic_rate)#少数になる可能性がある、これをどうするか？

tod1 = toast.tod.TODCache(None, detnames, nsample1, detquats=detquats)
tod1.write_times(stamps=np.arange(t_delta1*0.5,nsample * 1.0/args.sample_rate , t_delta1))
xdata1=tod1.local_times()

i=0
count0=0

data0= np.array(data0)
pre_data1=np.zeros(int(len(detnames))) #200Hzの時間幅のデータをdetごとにたす。次の時間幅では0に戻す
#print(data0[:,1]) ##これでdet毎の時間データが出る

for j in range(nsample):#100kHzの時系列データ
    if t_delta1*i<=xdata[j] and xdata[j]<t_delta1*(i+1):
        for k in range(int(len(detnames))):
            pre_data1[k]+=data0[:,k]
            count0+=1
            if xdata[j+1]>=t_delta1*(i+1):
                data1.append(pre_data1/count0)
                i+=1
                print(i,xdata[j])
                count0=0
                pre_data1=np.zeros(int(len(detnames))) 

print("data1")
print(data1)

#for k in range(int(len(detnames))):
#    tod.write(
#        detector=d,
#        data=data1
        #data=np.random.normal(                                 
         #   scale=fp[d]["NET"],                                       
          #  size=nsample 
           # ) #random 
#        )


'''
############# TOD plot
for k in range(int(len(detnames))):    
    plt.subplot(int(len(detnames)),1,k+1)
    plt.plot(xdata,tod.local_signal(detnames[k]))
    #plt.xlabel("time")
    #plt.ylabel("amplitude+CR")
#plt.show()#original vs add_CR tod plot
plt.show()

##########################
# map making
#########################
import healpy as hp
lmax = args.nside * 2 #nsideは大きさ？
'''
'''
distmap.read_healpix_fits("sim_map.fits")#読み込むmap <- 球面調和関数で適当に決めたもの
toast.todmap.OpSimScan(distmap=distmap, out=name).exec(data)

#noise入りのdataに対し、mapをscanしてsky signalを作り、execでtimestreamsをscanにより作る

# Copy the sky signal
toast.tod.OpCacheCopy(input=name, output="sky_signal", force=True).exec(data)

# Simulate noise:Operator which generates noise timestreams.
toast.tod.OpSimNoise(out=name, realization=0).exec(data)
# realization(int): if simulating multiple realizations, the realization index.

toast.tod.OpCacheCopy(input=name, output="full_signal", force=True).exec(data)
'''
'''
#########################                                        
# obs dictionary                                                 
########################                                       
data = toast.Data(comm)
for iobs in range(int(len(detnames))):
    obs = {}
    obs["name"] = "science_{:05d}".format(iobs)
    obs["tod"] = tod_sate
    obs["intervals"] = None
    obs["baselines"] = None
    obs["CR"] = tod.local_signal(detnames[iobs])# loop for iobs ?
    obs["id"] = iobs
    data.obs.append(obs)

#########################
# map making
#########################
#simulate sky signal and noise
name = "signal"
toast.tod.OpCacheClear(name).exec(data)
toast.todmap.OpPointingHpix(nside=args.nside, nest=True, mode="IQU").exec(data)

npix = 12 * args.nside ** 2
hitmap = np.zeros(npix)
tod = data.obs[0]["tod"]
for det in tod.local_dets:
    pixels = tod.cache.reference("pixels_{}".format(det))
    hitmap[pixels] = 1
hitmap[hitmap == 0] = hp.UNSEEN
hp.mollview(hitmap, nest=True, title="all hit pixels", cbar=False)
hp.graticule(22.5, verbose=False)

# Scan the signal from a map
distmap = toast.map.DistPixels(
    data,
    nnz=3,
    dtype=np.float32,
)
distmap.read_healpix_fits("sim_map.fits")
toast.todmap.OpSimScan(distmap=distmap, out=name).exec(data)

# Copy the sky signal
toast.tod.OpCacheCopy(input=name, output="sky_signal", force=True).exec(data)


#Destripe the signal and make a map
mapmaker = toast.todmap.OpMapMaker(
    nside=args.nside,
    nnz=3,
    name=name,
    outdir=args.outdir,
    outprefix="toast_test_",
    baseline_length=10,
    #maskfile=self.maskfile_binary,
    #weightmapfile=self.maskfile_smooth,
    #subharmonic_order=None,
    iter_max=100,
    use_noise_prior=False,
    #precond_width=30,
)
mapmaker.exec(data)

#Plot a segment of the timelines
tod = data.obs[0]["tod"]#TODSatellite
times = tod.local_times()

fig = plt.figure(figsize=[12, 8])
for idet, det in enumerate(tod.local_dets):
    sky = tod.local_signal(det, "sky_signal")#siganl行列を取得
    #full = tod.local_signal(det)#今はdataの種類が１つしかない
    #cleaned = tod.local_signal(det, name)

    ind = slice(0, 1000)
    ax = fig.add_subplot(4, 4, 1 + idet)
    ax.set_title(det)
    ax.plot(times[ind], sky[ind], '.', label="add CR", zorder=100)
    #ax.plot(times[ind], full[ind], '.', label="full")
    #ax.plot(times[ind], full[ind] - cleaned[ind], '.', label="baselines")
ax.legend(bbox_to_anchor=(1.1, 1.00))
fig.subplots_adjust(hspace=0.6)
fig = plt.figure(figsize=[12, 8])

#tod written with lines
for idet, det in enumerate(tod.local_dets):
    sky = tod.local_signal(det, "sky_signal")
    #full = tod.local_signal(det, "full_signal")
    #cleaned = tod.local_signal(det, name)
    ax = fig.add_subplot(4, 4, 1 + idet)
    ax.set_title(det)
    plt.plot(times[ind], sky[ind], '-', label="sky_signal", zorder=100)
    #plt.plot(times, full - sky, '.', label="noise")
    #plt.plot(times, full - cleaned, '-', label="baselines")
ax.legend(bbox_to_anchor=(1.1, 1.00))
fig.subplots_adjust(hspace=.6)
plt.figure(figsize=[12, 8])

hitmap = hp.read_map("maps/toast_test_hits.fits")
hitmap[hitmap == 0] = hp.UNSEEN
hp.mollview(hitmap, sub=[2, 2, 1], title="hits")

binmap = hp.read_map("maps/toast_test_binned.fits")
binmap[binmap == 0] = hp.UNSEEN
hp.mollview(binmap, sub=[2, 2, 2], title="binned map", cmap="coolwarm")

destriped = hp.read_map("maps/toast_test_destriped.fits")
destriped[destriped == 0] = hp.UNSEEN
hp.mollview(destriped, sub=[2, 2, 3], title="destriped map", cmap="coolwarm")

inmap = hp.read_map("sim_map.fits")
inmap[hitmap == hp.UNSEEN] = hp.UNSEEN
hp.mollview(inmap, sub=[2, 2, 4], title="input map", cmap="coolwarm")
print(np.sum(hitmap[hitmap != hp.UNSEEN]) / 1400000.0)
plt.show()

'''
